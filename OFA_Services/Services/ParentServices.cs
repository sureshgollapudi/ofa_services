﻿using OFA_Services.Entities;
using OFA_Services.Models;
using OFA_Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;
using AutoMapper;
using OFA_Services.Helpers;
using OFA_Services.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace OFA_Services.Services
{
    public class ParentServices : IParentServices
    {
        private readonly UnitOfWork.UnitOfWork _context;
        public ParentServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public IEnumerable<OfaParentRegistration> GetAll()
        {
            return _context.userparentguardianRepository.GetAll();
        }
        public OfaParentRegistration Create(OfaParentRegistration user)
        {
            OfaParentRegistration SavedUser = new OfaParentRegistration();
            try
            {
                var parentuser = _context.userRepository.Get(u => u.ParentId == user.ParentId);
                //var ofaParentGuardianProfile = new OfaParentRegistration()
                //{
                //    RelationshipToChild = user.RelationshipToChild,
                //    ChildRelationshipToAdultId = user.ChildRelationshipToAdultID,
                //    Custody = user.Custody
                //    ,
                //    LivesWithChild = user.LivesWithChild,
                //    Address = user.Address,
                //    City = user.City,
                //    PrimaryLanguageSpoken = 1,
                //    PrimaryLanguageRead = 1,
                //    CommunicationLanguageWithChild = 1,
                //    EducationLevel = 1,
                //    EmployementStatus = 1,
                //    CreatedBy = null,
                //    CreatedOn = DateTime.Now
                //};
                //parentuser.OfaParentGuardianProfile.Add(ofaParentGuardianProfile);
                _context.Save();
            }
            catch (Exception ee)
            {

            }
            return SavedUser;
        }
        public OfaParentRegistration Update(OfaParentRegistration user)
        {
            OfaParentRegistration SavedUser = new OfaParentRegistration();
            try
            {
                //var parentuser = _context.userRepository.Get(u => u.ParentId == user.ParentId);
                //parentuser.FirstName = user.FirstName;
                //parentuser.LastName = user.LastName;
                //parentuser.DateofBirth = user.DateofBirth;
                //parentuser.Email = user.Email;
                //parentuser.ZipCode = user.ZipCode;
                //parentuser.LastModifiedBy = user.ParentId;
                //parentuser.LastModifiedOn = DateTime.Now;
                //_context.userRepository.Update(parentuser);
                //var parentguardianprofile = _context.userparentguardianRepository.Get(u => u.ParentId == user.ParentId);
                //parentguardianprofile.RelationshipToChild = user.RelationshipToChild;
                //parentguardianprofile.ChildRelationshipToAdultId = user.ChildRelationshipToAdultID;
                //parentguardianprofile.Address = user.Address;
                //parentguardianprofile.City = user.City;
                //parentguardianprofile.Custody = user.Custody;
                //parentguardianprofile.LivesWithChild = user.LivesWithChild;
                //parentguardianprofile.TeenParent = user.TeenPareent;
                //parentguardianprofile.StateIdFk = user.StateIdFk;
                //var parentregistration = _context.phonenumbertypeRepository.Get(u => u.ParentId == user.ParentId);
                //parentregistration.PhoneNumber = user.PhoneNumber;
                //_context.userparentguardianRepository.Update(parentguardianprofile);
                _context.Save();
            }
            catch (Exception ee)
            {

            }
            return SavedUser;
        }
        public OfaParentRegistration GetById(int id)
        {
            var OfaParentGuardianProfile = _context.userparentguardianRepository.GetByID(id);
            return OfaParentGuardianProfile;
        }
        public IEnumerable<State> GetAllStates()
        {
            return _context.stateRepository.GetAll();
        }
    }
}
