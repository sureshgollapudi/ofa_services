﻿using OFA_Services.Entities;
using OFA_Services.Models;
using OFA_Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;
using AutoMapper;
using OFA_Services.Helpers;
using OFA_Services.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;


namespace OFA_Services.Services
{
    public class GranteeServices : IGranteeServices
    {
        private readonly UnitOfWork.UnitOfWork _context;
        public GranteeServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public OfaGranteeTableData GetGranteeData(Int32 GranteeId)
        {
            var GranteeData = _context.granteeRepository.Get(u => u.GranteeId == GranteeId);
            return GranteeData;
            //var GranteeData = _context.granteeRepository.Get(u => u.GranteeId == GranteeId);
            //var GranteeData = _context.granteeRepository.Get(u => u.GranteeId ==GranteeId);

        }
    }
}
