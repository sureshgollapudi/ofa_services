﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OFA_Services.Models;
using OFA_Services.Entities;


namespace OFA_Services.Services
{
    public interface IParticipantServices
    {
        OfaParticipant Create(OfaParticipant participant);
        IEnumerable<OfaParticipant> GetAll();
        List<OfaParticipant> GetByParentId(int id);
        OfaParticipantFileAttachment CreateParticipantFileAttachment(OfaParticipantFileAttachment ParticipantFileAttachment);
        OfaParticipantFileAttachment GetParticipantFileAttachmentbyParticipantId(int id);
    }
}
