﻿using OFA_Services.Entities;
using OFA_Services.Models;
using OFA_Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;
using AutoMapper;
using OFA_Services.Helpers;
using OFA_Services.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace OFA_Services.Services
{
    public class UserServices : IUserServices
    {
        // private readonly UnitOfWork.UnitOfWork _UnitOfWork;
        private readonly UnitOfWork.UnitOfWork _context;
        public UserServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public OfaParentRegistration Authenticate(int granteeid, string username,int parentId, string password, string secretword)
        {
            OfaParentRegistration LoggedInUser = new OfaParentRegistration();
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;
            var user = _context.userRepository.Get(u =>( u.UserName.ToLower() == username.ToLower()|| u.ParentId == parentId) && u.GranteeId == granteeid);
            // check if username exists
            if (user == null)
                return null;
            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;
            // authentication successful
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secretword);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                            new Claim(ClaimTypes.Email, user.ParentId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            LoggedInUser.FirstName = user.FirstName;
            LoggedInUser.LastName = user.LastName;
            LoggedInUser.Token = tokenString;
            LoggedInUser.ParentId = user.ParentId;
            return LoggedInUser;
        }

        public IEnumerable<OfaParentRegistration> GetAll()
        {
            return _context.userRepository.GetAll();
        }

        public OfaParentRegistration GetById(int id)
        {
            OfaParentRegistration objuser = new OfaParentRegistration();
            objuser = _context.userRepository.Get(PId => PId.ParentId == id) ?? null;
            //OfaParentGuardianProfile userprof = _context.userparentguardianRepository.Get(PId => PId.ParentRegParentIdFk == id) ?? null;
            //OfaParentRegistration user = _context.userRepository.GetByID(id);
            //OfaParentPhoneNumber userphone = _context.phonenumbertypeRepository.Get(P => P.ParentId == id);
            //objuser.PhoneNumber = userphone.PhoneNumber;
            //objuser.FirstName = user.FirstName;
            //objuser.LastName = user.LastName;
            //objuser.ZipCode = user.ZipCode;
            //if (userprof != null)
            //{
            //    objuser.ChildRelationshipToAdultID = userprof.ChildRelationshipToAdultId ?? 0;
            //    objuser.RelationshipToChild = userprof.RelationshipToChild;
            //    objuser.Custody = userprof.Custody ?? true;
            //    objuser.LivesWithChild = userprof.LivesWithChild ?? true;
            //    objuser.Address = userprof.Address;
            //    objuser.City = userprof.City;
            //    objuser.TeenPareent = userprof.TeenParent ?? true;
            //    objuser.StateIdFk = userprof.StateIdFk ?? 1;
            //}
            //var phonenumber = userphone.PhoneNumber;
            return objuser;
        }
        public List<OfaParentPhoneNumber> GetByIdForPhone(int id)
        {
            return _context.phonenumbertypeRepository.GetMany(spt => spt.ParentId == id).ToList();
        }
        public IEnumerable<OfaParentPhoneNumber> GetAllPhones()
        {
            return _context.phonenumbertypeRepository.GetAll();
        }
        public OfaParentRegistration Create(OfaParentRegistration user)
        {
            OfaParentRegistration SavedUser = new OfaParentRegistration();
            try
            {
                // validation
                if (string.IsNullOrWhiteSpace(user.Password))
                    throw new AppException("Password is required");
                //if (_context.Users.Any(x => x.Username == user.Username))
                //    throw new AppException("Username \"" + user.Username + "\" is already taken");
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);
                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.CreatedOn = DateTime.Now;
                user.UserName = user.UserName.TrimStart().TrimEnd();
                user.FirstName = user.FirstName.TrimStart().TrimEnd();
                user.LastName = user.LastName.TrimStart().TrimEnd(); 
                _context.userRepository.Insert(user);
                _context.Save();
                var parentuser = _context.userRepository.Get(u => u.UserName == user.UserName && u.GranteeId == user.GranteeId);
                SavedUser.FirstName = parentuser.FirstName;
                SavedUser.LastName = parentuser.LastName;
                SavedUser.Email = parentuser.Email;
                SavedUser.ParentId = parentuser.ParentId;
            }
            catch (Exception ee)
            {

            }
            return SavedUser;
        }

        public OfaParentRegistration Update(OfaParentRegistration user)
        {
            OfaParentRegistration SavedUser = new OfaParentRegistration();
            try
            {
                var parentuser = _context.userRepository.Get(u => u.ParentId == user.ParentId);
                parentuser.FirstName = user.FirstName;
                parentuser.LastName = user.LastName;
                parentuser.DateofBirth = user.DateofBirth;
                parentuser.Email = user.Email;
                parentuser.ZipCode = user.ZipCode;
                parentuser.LastModifiedBy = user.ParentId;
                parentuser.LastModifiedOn = DateTime.Now;
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);
                parentuser.PasswordHash = passwordHash;
                parentuser.PasswordSalt = passwordSalt;
                parentuser.OfaParentPhoneNumber = user.OfaParentPhoneNumber;
                _context.userRepository.Update(user);
                _context.Save();
                SavedUser = _context.userRepository.Get(u => u.ParentId == user.ParentId);
            }
            catch (Exception ee)
            {

            }
            return SavedUser;
        }

        public void Delete(int id)
        {
            //var user = _context.Users.Find(id);
            //if (user != null)
            //{
            //    _context.Users.Remove(user);
            //    _context.SaveChanges();
            //}
        }

        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
