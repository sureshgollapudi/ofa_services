﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OFA_Services.Models;
namespace OFA_Services.Services
{
    public interface ITableColumnValueServices
    {
        List<TableColumnValue> GetByTablenameColumnname(string tableName, string columnName);
    }
}
