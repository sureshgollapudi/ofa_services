﻿using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;
using AutoMapper;
using OFA_Services.Helpers;
using OFA_Services.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using OFA_Services.Models;
using System.Linq;
using System.Collections.Generic;

namespace OFA_Services.Services
{
    public class TableColumnValueServices : ITableColumnValueServices
    {
        private readonly UnitOfWork.UnitOfWork _context;
        public TableColumnValueServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public List<TableColumnValue> GetByTablenameColumnname(string TableName, string ColumnName)
        {  
            return _context.tablecolumnRepository.GetMany(tcv => tcv.TableName == TableName && tcv.ColumnName== ColumnName).ToList();
        }





    }
   
}
