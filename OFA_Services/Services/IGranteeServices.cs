﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Models;
using OFA_Services.Entities;

namespace OFA_Services.Services
{
    public interface IGranteeServices
    {
        OfaGranteeTableData GetGranteeData(Int32 GranteeName);
    }
}
