﻿using OFA_Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Services
{
    public interface IFileUploadServices
    {
        int Create(FileUpload file);
        int Update(FileUpload file);
    }
}
