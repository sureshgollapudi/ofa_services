﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Models;
using OFA_Services.Entities;

namespace OFA_Services.Services
{
    public interface IParentServices
    {
        OfaParentRegistration GetById(int id);
        OfaParentRegistration Create(OfaParentRegistration user);
        OfaParentRegistration Update(OfaParentRegistration user);
        IEnumerable<OfaParentRegistration> GetAll();
        IEnumerable<State> GetAllStates();
    }
}
