﻿using OFA_Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OFA_Services.Services
{
    public class FileUploadServices : IFileUploadServices
    {
       public int Create(FileUpload file)
        {
            if (file != null)
            {
                if (!Directory.Exists(file.FilePath)) Directory.CreateDirectory(file.FilePath);
                string filepath = file.FilePath + file.FileName;
                var bytess = Convert.FromBase64String(file.Base64String);
                using (var imageFile = new FileStream(filepath, FileMode.Create))
                {
                    imageFile.Write(bytess, 0, bytess.Length);
                    imageFile.Flush();
                }
                return 1;
            }
            else { return 0; }
        }
        public int Update(FileUpload file)
        {
            return 1;
        }
    }
}
