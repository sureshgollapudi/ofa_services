﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Models;
using OFA_Services.Entities;

namespace OFA_Services.Services
{
    public interface IUserServices
    {
        OfaParentRegistration Authenticate(int granteeid,string username,int parentId, string password,string secretword);
        IEnumerable<OfaParentRegistration> GetAll();
        OfaParentRegistration GetById(int id);
        List<OfaParentPhoneNumber> GetByIdForPhone(int id);
        IEnumerable<OfaParentPhoneNumber> GetAllPhones();
        OfaParentRegistration Create(OfaParentRegistration user);
        OfaParentRegistration Update(OfaParentRegistration user);
        void Delete(int id);
    }
}
