﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OFA_Services.Models;

namespace OFA_Services.Services
{
    public class ParticipantServices : IParticipantServices
    {
        private readonly UnitOfWork.UnitOfWork _context;

        public ParticipantServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public OfaParticipant Create(OfaParticipant participant)
        {
            OfaParticipant SavedParticipant = new OfaParticipant();
            try
            {
                var ofaParticipant = new OfaParticipant()
                {
                    FirstName = participant.FirstName.Trim(),
                    LastName = participant.LastName.Trim(),
                    DateofBirth = participant.DateofBirth,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    ParentId=participant.ParentId,
                    StateId=4,
                    GranteeId=participant.GranteeId
                    
                };
               _context.participantRepository.Insert(ofaParticipant);
                _context.Save();
                var parentuser = _context.participantRepository.Get(p => p.ParentId == participant.ParentId && p.FirstName == participant.FirstName && p.LastName == participant.LastName && p.GranteeId == participant.GranteeId && p.DateofBirth == participant.DateofBirth);
                SavedParticipant.FirstName = parentuser.FirstName;
                SavedParticipant.LastName = parentuser.LastName;                
                SavedParticipant.ParentId = parentuser.ParentId;
                SavedParticipant.OfaParticipantId = parentuser.OfaParticipantId;
            }
            catch (Exception ee)
            {

            }
            return SavedParticipant;
        }
        public IEnumerable<OfaParticipant> GetAll()
        {
            return _context.participantRepository.GetAll();
        }

        public List<OfaParticipant> GetByParentId(int id)
        {
            return _context.participantRepository.GetMany(pId => pId.ParentId == id).ToList();
        }
        public OfaParticipantFileAttachment GetParticipantFileAttachmentbyParticipantId(int id)
        {
            return _context.ParticipantFileAttachmentRepository.GetMany(pId => pId.OfaParticipantId == id).FirstOrDefault();
        }
        public OfaParticipantFileAttachment CreateParticipantFileAttachment(OfaParticipantFileAttachment ParticipantFileAttachment)
        {
            OfaParticipantFileAttachment SavedParticipantFileAttachment = new OfaParticipantFileAttachment();
            try
            {
                var OfaParticipantFileAttachment = new OfaParticipantFileAttachment()
                {
                    OfaParticipantId = ParticipantFileAttachment.OfaParticipantId,
                    DocumentTypeId = ParticipantFileAttachment.DocumentTypeId,
                    FileName = ParticipantFileAttachment.FileName,
                    FilePath = ParticipantFileAttachment.FilePath,
                    DisplayName = ParticipantFileAttachment.DisplayName,
                    CreatedBy = ParticipantFileAttachment.CreatedBy,
                    LastModifiedBy = ParticipantFileAttachment.LastModifiedBy,
                    CreatedOn = ParticipantFileAttachment.CreatedOn,
                    LastModifiedOn = ParticipantFileAttachment.LastModifiedOn
                };
                _context.ParticipantFileAttachmentRepository.Insert(OfaParticipantFileAttachment);
                _context.Save();
                var ParticipantAttach = _context.ParticipantFileAttachmentRepository
                    .Get(p => p.OfaParticipantId == ParticipantFileAttachment.OfaParticipantId && p.DocumentTypeId == ParticipantFileAttachment.DocumentTypeId);
                SavedParticipantFileAttachment.FileAttachmentId = ParticipantAttach.FileAttachmentId;
            }
            catch (Exception ee)
            {

            }
            return SavedParticipantFileAttachment;
        }

    }
}