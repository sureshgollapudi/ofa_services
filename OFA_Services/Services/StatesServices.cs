﻿using OFA_Services.Entities;
using OFA_Services.Models;
using OFA_Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;
using AutoMapper;
using OFA_Services.Helpers;
using OFA_Services.Repositories;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace OFA_Services.Services
{
    public class StatesServices : IStatesServices
    {
        private readonly UnitOfWork.UnitOfWork _context;
        public StatesServices()
        {
            _context = new UnitOfWork.UnitOfWork();
        }
        public IEnumerable<State> GetAll()
        {
            return _context.stateRepository.GetAll();
        }
    }
}
