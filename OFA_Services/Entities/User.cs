﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Entities
{
    public partial class  User
    {
        public int ParentId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }
        public string PhoneNumber { get; set; }
        public string ZipCode { get; set; }
        public string Password;
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string Token { get; set; }

        public string RelationshipToChild { get; set; }
        public int ChildRelationshipToAdultID { get; set; }
        public bool Custody { get; set; }
        public bool LivesWithChild { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int PrimaryLanguageSpoken { get; set; }
        public int PrimaryLanguageRead { get; set; }
        public int CommunicationLanguageWithChild { get; set; }
        public int EducationLevel { get; set; }
        public int EmployementStatus { get; set; }
        public bool TeenPareent { get; set; }
        public int StateIdFk { get; set; }

        public string pathlocation { get; set;}




    }
}
