﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Entities
{
    public class resultEntity<TEntity> where TEntity : class
    {
        public List<TEntity> results { get; set; }
        public responseInfoEntity responseInfo { get; set; }
    }
}
