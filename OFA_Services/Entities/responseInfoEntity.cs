﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Entities
{
    public class responseInfoEntity
    {
        public string title { get; set; }
        public string message { get; set; }
        public int responseCode { get; set; }
    }
}
