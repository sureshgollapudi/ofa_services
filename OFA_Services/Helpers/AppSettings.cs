﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string UploadDrive { get; set; }
    }
}
