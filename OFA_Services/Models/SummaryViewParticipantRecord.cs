﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class SummaryViewParticipantRecord
    {
        public int? GranteeId { get; set; }
        public int? ParticipantId { get; set; }
        public int? ParticipantRecordStatus { get; set; }
        public int ParticipantRecordId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CenterProgramYearId { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public int? ParticipantParentGuardianId { get; set; }
        public int? FamilyAdvocateUserId { get; set; }
        public DateTime? StatusLastUpdatedDate { get; set; }
        public string PhotoUrl { get; set; }
        public string ProgramName { get; set; }
        public int? ProgramTypeId { get; set; }
        public int? ProgramId { get; set; }
        public string CenterName { get; set; }
        public int? ClassroomProgramYearId { get; set; }
        public int? SchoolYearId { get; set; }
        public string SchoolYear { get; set; }
        public string ClassroomName { get; set; }
        public string ApplicationDate { get; set; }
        public string ProgramYear { get; set; }
        public string ProgramYearId { get; set; }
        public string CenterId { get; set; }
        public string ClassroomId { get; set; }
        public int? N1 { get; set; }
        public int? D1 { get; set; }
        public int? C1 { get; set; }
        public int? N2 { get; set; }
        public int? D2 { get; set; }
        public int? C2 { get; set; }
        public int? N3 { get; set; }
        public int? D3 { get; set; }
        public int? C3 { get; set; }
        public int? N5 { get; set; }
        public int? D5 { get; set; }
        public int? C5 { get; set; }
        public int? N12 { get; set; }
        public int? D12 { get; set; }
        public int? C12 { get; set; }
        public int? N13 { get; set; }
        public int? D13 { get; set; }
        public int? C13 { get; set; }
        public int? N14 { get; set; }
        public int? D14 { get; set; }
        public int? C14 { get; set; }
        public int? N15 { get; set; }
        public int? D15 { get; set; }
        public int? C15 { get; set; }
        public int? N16 { get; set; }
        public int? D16 { get; set; }
        public int? C16 { get; set; }
        public int? N17 { get; set; }
        public int? D17 { get; set; }
        public int? C17 { get; set; }
        public int? N27 { get; set; }
        public int? D27 { get; set; }
        public int? C27 { get; set; }
        public int? N36 { get; set; }
        public int? D36 { get; set; }
        public int? C36 { get; set; }
        public int? N38 { get; set; }
        public int? D38 { get; set; }
        public int? C38 { get; set; }
        public int? AlertGroupFamilyServices { get; set; }
        public int? AlertGroupHealth { get; set; }
        public int? AlertGroupMentalHealth { get; set; }
        public int? AlertGroupDisabilities { get; set; }
        public int? AlertGroupEducation { get; set; }
        public int? AlertGroupHomeBased { get; set; }
        public int? AlertGroupExtendedDay { get; set; }
        public int? AlertGroupClerkAttendance { get; set; }
        public int? AlertGroupErsea { get; set; }
        public int? AlertGroupExecutiveDirector { get; set; }
        public int? AlertGroupAdmin { get; set; }
        public int? AlertGroupInKind { get; set; }
        public int? AlertGroupEnrollment { get; set; }
        public int? EnrollmentTerminationReason { get; set; }
        public DateTime? EnrollmentTerminationDate { get; set; }
        public string ProgramType { get; set; }
        public int? ParticipantTypeId { get; set; }
        public DateTime? ParticipantDateofBirth { get; set; }
        public DateTime? SchoolDistrictCutoffDate { get; set; }
        public int? ParticipationYear { get; set; }
        public int? AlertsCount { get; set; }
    }
}
