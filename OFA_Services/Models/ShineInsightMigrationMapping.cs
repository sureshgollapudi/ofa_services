﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ShineInsightMigrationMapping
    {
        public int Id { get; set; }
        public string SourceName { get; set; }
        public string DestinationName { get; set; }
        public string FieldName { get; set; }
        public string GranteeName { get; set; }
    }
}
