﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordGoalFollowUp
    {
        public int ParticipantRecordGoalFollowUpId { get; set; }
        public int? ParticipantRecordGoalId { get; set; }
        public DateTime? Date { get; set; }
        public string Note { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile CreatorUser { get; set; }
        public ParticipantRecordGoal ParticipantRecordGoal { get; set; }
    }
}
