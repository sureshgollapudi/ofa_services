﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhbehaviorPlanStrategyForHome
    {
        public int ParticipantDmhbehaviorPlanStrategyForHomeId { get; set; }
        public int ParticipantDmhbehaviorPlanId { get; set; }
        public int StrategyForHome { get; set; }

        public ParticipantDmhbehaviorPlan ParticipantDmhbehaviorPlan { get; set; }
    }
}
