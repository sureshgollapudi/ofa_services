﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserCoaching
    {
        public int UserCoachingId { get; set; }
        public int UserId { get; set; }
        public DateTime? CpaagreementDate { get; set; }
    }
}
