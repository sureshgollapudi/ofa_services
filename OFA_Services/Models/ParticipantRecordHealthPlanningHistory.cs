﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordHealthPlanningHistory
    {
        public int ParticipantRecordHealthPlanningHistoryId { get; set; }
        public int HealthInformationId { get; set; }
        public int ParticipantRecordHealthPlanningId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecordHealthPlanning ParticipantRecordHealthPlanning { get; set; }
    }
}
