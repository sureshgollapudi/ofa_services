﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class LevelMaster
    {
        public int LevelId { get; set; }
        public int AssessmentTypeId { get; set; }
        public string Name { get; set; }

        public AssessmentType AssessmentType { get; set; }
    }
}
