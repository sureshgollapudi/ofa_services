﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationHomeVisitDocumentation
    {
        public int ParticipantRecordEducationHomeVisitDocumentationId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int HomeVisit { get; set; }
        public int? EducationHvptcdocumentation { get; set; }
        public DateTime EducationHvptcdocumentationDate { get; set; }
        public int EducationHvptcdocumentationLocation { get; set; }
        public string EducationHvptcdocumentationOtherDescription { get; set; }
        public string EducationDocumentationHvptcnotes { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? PeeractivityModeling { get; set; }
        public int FatherFigureInvolved { get; set; }
        public int? LastModifiedBy { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
