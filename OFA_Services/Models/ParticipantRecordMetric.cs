﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordMetric
    {
        public int ParticipantRecordMetricId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? MetricId { get; set; }
        public int? Numerator { get; set; }
        public int? Denominator { get; set; }
        public int? Count { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Metric Metric { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
