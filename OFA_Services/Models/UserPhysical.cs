﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserPhysical
    {
        public int UserPhysicalId { get; set; }
        public int UserId { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
