﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AlertGroup
    {
        public int AlertGroupId { get; set; }
        public int AlertId { get; set; }
        public int AlertGroup1 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Alert Alert { get; set; }
    }
}
