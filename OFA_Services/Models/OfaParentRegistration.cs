﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OFA_Services.Models
{
    public partial class OfaParentRegistration
    {
        public OfaParentRegistration()
        {
            OfaParentPhoneNumber = new HashSet<OfaParentPhoneNumber>();
            OfaParticipant = new HashSet<OfaParticipant>();
        }

        public int ParentId { get; set; }
        public int GranteeId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateofBirth { get; set; }
        public int? Type { get; set; }
        public string ZipCode { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string currentPassword { get; set; }
        [NotMapped]
        public string newPassword { get; set; }
        [NotMapped]
        public string Token { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<OfaParentPhoneNumber> OfaParentPhoneNumber { get; set; }
        public ICollection<OfaParticipant> OfaParticipant { get; set; }
    }
}
