﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantParentGuardian
    {
        public int ParticipantParentGuardianId { get; set; }
        public int ParentGuardianId { get; set; }
        public int ParticipantId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? IsPrimaryParentGuardian { get; set; }
        public bool? DoNotCount { get; set; }
        public int? NumberofFamilyMembers { get; set; }
        public int? ChildRelationshipToParentGuardian { get; set; }
        public string ChildRelationshipToOtherExplain { get; set; }
        public bool? FlagNumberOfFamilyMemberOverride { get; set; }

        public ParentGuardian ParentGuardian { get; set; }
        public Participant Participant { get; set; }
    }
}
