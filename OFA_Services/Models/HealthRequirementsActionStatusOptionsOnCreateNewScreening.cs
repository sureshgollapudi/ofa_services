﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthRequirementsActionStatusOptionsOnCreateNewScreening
    {
        public int HealthRequirementsActionStatusOptionsOnCreateNewScreeningId { get; set; }
        public int GranteeId { get; set; }
        public int ActionStatus { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
    }
}
