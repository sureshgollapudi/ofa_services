﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipantEmergencyContact
    {
        public int ParticipantEmergencyContactId { get; set; }
        public int OfaParticipantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RelationshiptoChild { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber1 { get; set; }
        public int? PhoneNumber1PhoneTypeId { get; set; }
        public string PhoneNumber2 { get; set; }
        public int? PhoneNumber2PhoneTypeId { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParticipant OfaParticipant { get; set; }
        public State State { get; set; }
    }
}
