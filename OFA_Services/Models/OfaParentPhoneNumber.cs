﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParentPhoneNumber
    {
        public int ParentPhoneNumberId { get; set; }
        public int ParentId { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsPrimaryPhone { get; set; }
        public int PhoneTypeId { get; set; }
        public string Notes { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParentRegistration Parent { get; set; }
    }
}
