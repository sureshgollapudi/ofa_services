﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class MigrationHistory
    {
        public string MigrationId { get; set; }
        public byte[] Model { get; set; }
        public string ProductVersion { get; set; }
    }
}
