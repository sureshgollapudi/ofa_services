﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class TbChildPluslErrorRecords
    {
        public int Id { get; set; }
        public int? ChildPlusId { get; set; }
        public string ErrorDescription { get; set; }
        public string Program { get; set; }
        public string Center { get; set; }
        public string AgencyNameGranteeName { get; set; }
        public string ClassName { get; set; }
        public string DataFileName { get; set; }
        public int? RefId { get; set; }
        public long? FileId { get; set; }
        public string ErrorType { get; set; }
    }
}
