﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantImmunization
    {
        public int ParticipantImmunizationId { get; set; }
        public int ParticipantId { get; set; }
        public int GranteeImmunizationId { get; set; }
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public DateTime? ThirdDate { get; set; }
        public DateTime? FourthDate { get; set; }
        public DateTime? FifthDate { get; set; }
        public DateTime? OverrideDate { get; set; }
        public bool? Exempt { get; set; }
        public string ExemptReason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? DueInDays { get; set; }

        public GranteeImmunization GranteeImmunization { get; set; }
        public Participant Participant { get; set; }
    }
}
