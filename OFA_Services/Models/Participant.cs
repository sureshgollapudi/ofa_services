﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Participant
    {
        public Participant()
        {
            Calendar = new HashSet<Calendar>();
            ParticipantAdditionalFamilyMember = new HashSet<ParticipantAdditionalFamilyMember>();
            ParticipantAlert = new HashSet<ParticipantAlert>();
            ParticipantDmhactionPlan = new HashSet<ParticipantDmhactionPlan>();
            ParticipantDmhconcern = new HashSet<ParticipantDmhconcern>();
            ParticipantDmhiep = new HashSet<ParticipantDmhiep>();
            ParticipantDmhnote = new HashSet<ParticipantDmhnote>();
            ParticipantDmhsummary = new HashSet<ParticipantDmhsummary>();
            ParticipantDmhsummaryResult = new HashSet<ParticipantDmhsummaryResult>();
            ParticipantEducationScreeningDocumentation = new HashSet<ParticipantEducationScreeningDocumentation>();
            ParticipantEvaluationCenterprogramyear = new HashSet<ParticipantEvaluationCenterprogramyear>();
            ParticipantFamilyProfile = new HashSet<ParticipantFamilyProfile>();
            ParticipantHealthScreeningDocumentation = new HashSet<ParticipantHealthScreeningDocumentation>();
            ParticipantHealthScreeningReport = new HashSet<ParticipantHealthScreeningReport>();
            ParticipantImmunization = new HashSet<ParticipantImmunization>();
            ParticipantImmunizationPlanning = new HashSet<ParticipantImmunizationPlanning>();
            ParticipantInternalReferral = new HashSet<ParticipantInternalReferral>();
            ParticipantParentGuardian = new HashSet<ParticipantParentGuardian>();
            ParticipantRaceNavigation = new HashSet<ParticipantRace>();
            ParticipantRecord = new HashSet<ParticipantRecord>();
        }

        public int ParticipantId { get; set; }
        public int GranteeId { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public int? Type { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public DateTime? DateofBirth { get; set; }
        public int? Gender { get; set; }
        public int? Ethnicity { get; set; }
        public bool? IsUscitizen { get; set; }
        public bool IsPregnantMother { get; set; }
        public DateTime? ExpectingMotherEstimatedDueDate { get; set; }
        public string RaceOther { get; set; }
        public int? LanguageSpokenAtHome { get; set; }
        public int? ParticipantParentalStatus { get; set; }
        public int? ParentReportDisability { get; set; }
        public string ParentReportDisabilityType { get; set; }
        public int? ParentReportIep { get; set; }
        public int? ParentReportSpecialEducation { get; set; }
        public bool? ParentConcernHearing { get; set; }
        public bool? ParentConcernVision { get; set; }
        public bool? ParentConcernAllergies { get; set; }
        public bool? ParentConcernAsthma { get; set; }
        public bool? ParentConcernDental { get; set; }
        public bool? ParentConcernUnderweight { get; set; }
        public bool? ParentConcernOverweight { get; set; }
        public bool? ParentConcernSeizures { get; set; }
        public bool? ParentConcernAnemia { get; set; }
        public bool? ParentConcernHighLead { get; set; }
        public bool? ParentConcernDiabetes { get; set; }
        public bool? ParentConcernMedDentNutrOther { get; set; }
        public string ParentConcernMedDentNutrOtherType { get; set; }
        public bool? ParentConcernSpeech { get; set; }
        public bool? ParentConcernPhysicalDevelopment { get; set; }
        public bool? ParentConcernBehavior { get; set; }
        public string ParentConcernBehaviorType { get; set; }
        public bool? ParentReportFamilyCrisis { get; set; }
        public bool? ParentReportChildProtection { get; set; }
        public bool? ParentReportFoodStamps { get; set; }
        public bool? ParentReportUnemployment { get; set; }
        public bool? ParentReportUtilityAssist { get; set; }
        public bool? ParentReportFosterCare { get; set; }
        public bool? ParentReportPublicHousing { get; set; }
        public bool? ParentReportChildSupport { get; set; }
        public bool? ParentReportHealthMh { get; set; }
        public bool? ParentReportSection8 { get; set; }
        public bool? ParentReportPrivateHealthInsurance { get; set; }
        public bool? ParentReportStateHealthInsurance { get; set; }
        public bool? ParentReportSubsidizedHousing { get; set; }
        public bool? ParentReportEmergency { get; set; }
        public bool? ParentReportOtherServices { get; set; }
        public string ParentReportOtherServicesType { get; set; }
        public bool? ParentReportTanf { get; set; }
        public bool? ParentReportSsi { get; set; }
        public bool? ParentReportWic { get; set; }
        public bool? ParentReportSnap { get; set; }
        public bool? ParentReportHomeless { get; set; }
        public bool? ParentReportHasVoucher { get; set; }
        public bool? ParentReportPastVoucherApplication { get; set; }
        public bool? ParentReportInterestPrivatePay { get; set; }
        public bool? ParentReportLegalIssues { get; set; }
        public string ParentReportLegalIssuesType { get; set; }
        public bool? ParentReportActiveDutyMilitary { get; set; }
        public bool? ParentReportPriorSchool { get; set; }
        public int? ParentReportPriorSchoolType { get; set; }
        public bool? ParentReportStaffMember { get; set; }
        public bool? ParentReportSiblingEnrolled { get; set; }
        public int? ParentReportReferral { get; set; }
        public string ParentReportReferralOther { get; set; }
        public bool? ParentReportNeedFullDay { get; set; }
        public int? ParentReportOtherCare { get; set; }
        public int? CenterIdpreference1 { get; set; }
        public int? CenterIdpreference2 { get; set; }
        public int? CenterIdpreference3 { get; set; }
        public bool? PreviousFamilyMember { get; set; }
        public int? CreatedByUserId { get; set; }
        public int? SelectionCriteriaPoints { get; set; }
        public int Suffix { get; set; }
        public string LegacyId1Gid { get; set; }
        public string LegacyId1 { get; set; }
        public string LegacyName1 { get; set; }
        public string LegacyId2 { get; set; }
        public string LegacyName2 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string PhotoUrl { get; set; }
        public string SchoolDistrict { get; set; }
        public string School { get; set; }
        public string RaceUnspecified { get; set; }
        public int? ChildPlusFamilyId { get; set; }
        public DateTime? FoundationCardDate { get; set; }
        public int? TransportationType { get; set; }
        public string TransportationTypeOther { get; set; }
        public DateTime? TransportationUpdateDate { get; set; }
        public string TransportationCity { get; set; }
        public string TransportationState { get; set; }
        public string TransportationZipcode { get; set; }
        public bool? IsParticipantreceivingtransportation { get; set; }
        public bool? IsParentGuardianTransportationAddressSameAsParticipantLivingAddress { get; set; }
        public string TransportationAddress1 { get; set; }
        public bool? IsDropOffAddressSameAsPickUpAddress { get; set; }
        public string DropOffTransportationAddress1 { get; set; }
        public string DropOffTransportationCity { get; set; }
        public string DropOffTransportationState { get; set; }
        public string DropOffTransportationZipcode { get; set; }
        public string LanguageOtherSpecify { get; set; }
        public bool? ParentReportOneMemberMilitaryVeteran { get; set; }
        public DateTime? ImmunizationOverAllDueDate { get; set; }
        public int? ParticipantRace { get; set; }
        public string BiracialRace { get; set; }
        public int? PrimaryLanguage { get; set; }
        public string SuffixText { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<Calendar> Calendar { get; set; }
        public ICollection<ParticipantAdditionalFamilyMember> ParticipantAdditionalFamilyMember { get; set; }
        public ICollection<ParticipantAlert> ParticipantAlert { get; set; }
        public ICollection<ParticipantDmhactionPlan> ParticipantDmhactionPlan { get; set; }
        public ICollection<ParticipantDmhconcern> ParticipantDmhconcern { get; set; }
        public ICollection<ParticipantDmhiep> ParticipantDmhiep { get; set; }
        public ICollection<ParticipantDmhnote> ParticipantDmhnote { get; set; }
        public ICollection<ParticipantDmhsummary> ParticipantDmhsummary { get; set; }
        public ICollection<ParticipantDmhsummaryResult> ParticipantDmhsummaryResult { get; set; }
        public ICollection<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentation { get; set; }
        public ICollection<ParticipantEvaluationCenterprogramyear> ParticipantEvaluationCenterprogramyear { get; set; }
        public ICollection<ParticipantFamilyProfile> ParticipantFamilyProfile { get; set; }
        public ICollection<ParticipantHealthScreeningDocumentation> ParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<ParticipantHealthScreeningReport> ParticipantHealthScreeningReport { get; set; }
        public ICollection<ParticipantImmunization> ParticipantImmunization { get; set; }
        public ICollection<ParticipantImmunizationPlanning> ParticipantImmunizationPlanning { get; set; }
        public ICollection<ParticipantInternalReferral> ParticipantInternalReferral { get; set; }
        public ICollection<ParticipantParentGuardian> ParticipantParentGuardian { get; set; }
        public ICollection<ParticipantRace> ParticipantRaceNavigation { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecord { get; set; }
    }
}
