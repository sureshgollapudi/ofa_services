﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DashboardItemMetrics
    {
        public int DashboardItemMetricId { get; set; }
        public string DashboardCategory { get; set; }
        public string DashboardSubCategory { get; set; }
        public int? MetricId { get; set; }
    }
}
