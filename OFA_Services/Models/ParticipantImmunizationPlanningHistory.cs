﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantImmunizationPlanningHistory
    {
        public int ParticipantImmunizationPlanningHistoryId { get; set; }
        public int GranteeImmunizationId { get; set; }
        public int ParticipantImmunizationPlanningId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantImmunizationPlanning ParticipantImmunizationPlanning { get; set; }
    }
}
