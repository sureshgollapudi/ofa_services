﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordSelectionCriteriaCategoryItem
    {
        public int ParticipantRecordSelectionCriteriaCategoryItemId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int SelectionCriteriaCategoryItemId { get; set; }
        public bool? IsChecked { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
        public ParticipantRecordSelectionCriteriaCategoryItem ParticipantRecordSelectionCriteriaCategoryItemNavigation { get; set; }
        public SelectionCriteriaCategoryItem SelectionCriteriaCategoryItem { get; set; }
        public ParticipantRecordSelectionCriteriaCategoryItem InverseParticipantRecordSelectionCriteriaCategoryItemNavigation { get; set; }
    }
}
