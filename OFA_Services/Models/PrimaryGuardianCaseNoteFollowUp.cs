﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianCaseNoteFollowUp
    {
        public int PrimaryGuardianCaseNoteFollowUpId { get; set; }
        public int PrimaryGuardianCaseNoteId { get; set; }
        public DateTime Date { get; set; }
        public int CreatorUserId { get; set; }
        public string Note { get; set; }
        public bool? ReferralGiven { get; set; }
        public bool? ReferralTaken { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedUserId { get; set; }

        public PrimaryGuardianCaseNote PrimaryGuardianCaseNote { get; set; }
    }
}
