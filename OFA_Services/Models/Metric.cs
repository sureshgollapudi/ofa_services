﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Metric
    {
        public Metric()
        {
            MetricProgramType = new HashSet<MetricProgramType>();
            ParticipantRecordMetric = new HashSet<ParticipantRecordMetric>();
        }

        public int MetricId { get; set; }
        public string Name { get; set; }
        public int Granularity { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string Description { get; set; }

        public ICollection<MetricProgramType> MetricProgramType { get; set; }
        public ICollection<ParticipantRecordMetric> ParticipantRecordMetric { get; set; }
    }
}
