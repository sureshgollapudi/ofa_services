﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AssessmentDocumentTag
    {
        public int AssessmentDocumentTagId { get; set; }
        public int AssessmentDocId { get; set; }
        public int CategoryTypeId { get; set; }
        public string Description { get; set; }

        public AssessmentDocument AssessmentDoc { get; set; }
    }
}
