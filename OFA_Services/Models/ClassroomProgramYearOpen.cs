﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ClassroomProgramYearOpen
    {
        public int ClassroomProgramYearOpenId { get; set; }
        public int ClassroomProgramYearId { get; set; }
        public DateTime DateOpen { get; set; }

        public ClassroomProgramYear ClassroomProgramYear { get; set; }
    }
}
