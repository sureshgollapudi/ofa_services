﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhbehaviorPlanType
    {
        public int ParticipantDmhbehaviorPlanTypeId { get; set; }
        public int ParticipantDmhbehaviorPlanId { get; set; }
        public int Type { get; set; }

        public ParticipantDmhbehaviorPlan ParticipantDmhbehaviorPlan { get; set; }
    }
}
