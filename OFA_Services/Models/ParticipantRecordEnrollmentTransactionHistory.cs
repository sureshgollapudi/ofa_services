﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEnrollmentTransactionHistory
    {
        public int ParticipantRecordEnrollmentTransactionHistoryId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int TransactionUserId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TransactionNote { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
        public UserProfile TransactionUser { get; set; }
    }
}
