﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OFA_Services.Models
{
    public class OfaParticipantFileAttachment
    {
        public int FileAttachmentId { get; set; }
        public int OfaParticipantId { get; set; }
        public int DocumentTypeId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string DisplayName { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public OfaParticipant OfaParticipant { get; set; }
        public string Base64String { get; set; }
    }
}
