﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DataListAccess
    {
        public int DataListValueId { get; set; }
        public int? GranteeId { get; set; }
        public int? StateId { get; set; }
        public int? RoleId { get; set; }
        public int? DataListId { get; set; }
        public int? ValueId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }

        public TableColumnValue Value { get; set; }
    }
}
