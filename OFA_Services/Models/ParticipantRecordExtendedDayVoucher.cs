﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordExtendedDayVoucher
    {
        public int ParticipantRecordExtendedDayVoucherId { get; set; }
        public int ParticipantRecordId { get; set; }
        public string IssuerState { get; set; }
        public int? Holder { get; set; }
        public int? CoPayer { get; set; }
        public string Number { get; set; }
        public int Status { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
