﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DmhsummaryNextRequireAction
    {
        public int DmhsummaryNextRequireActionId { get; set; }
        public int DmhsummaryPrimaryResultId { get; set; }
        public int? NextRequireActionId { get; set; }
        public DateTime? NextRequireActionDate { get; set; }
        public int? StatusId { get; set; }
        public int? ProsNoteId { get; set; }

        public DmhsummaryPrimaryResult DmhsummaryPrimaryResult { get; set; }
    }
}
