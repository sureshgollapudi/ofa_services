﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class EvaluationDocument
    {
        public int EvaluationDocId { get; set; }
        public int EvaluationId { get; set; }
        public int AssessmentDocumentId { get; set; }

        public AssessmentDocument AssessmentDocument { get; set; }
        public Evaluation Evaluation { get; set; }
    }
}
