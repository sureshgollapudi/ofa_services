﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianPireducationOrTraining
    {
        public int PrimaryGuardianPireducationOrTrainingId { get; set; }
        public int PrimaryGuardianPirfamilyInformationId { get; set; }
        public int EducationOrTrainingCompletedStatus { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public UserProfile CreatedByNavigation { get; set; }
        public UserProfile ModifiedByNavigation { get; set; }
        public PrimaryGuardianPirfamilyInformation PrimaryGuardianPirfamilyInformation { get; set; }
    }
}
