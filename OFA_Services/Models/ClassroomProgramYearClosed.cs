﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ClassroomProgramYearClosed
    {
        public int ClassroomProgramYearClosedId { get; set; }
        public int ClassroomProgramYearId { get; set; }
        public DateTime DateClosed { get; set; }

        public ClassroomProgramYear ClassroomProgramYear { get; set; }
    }
}
