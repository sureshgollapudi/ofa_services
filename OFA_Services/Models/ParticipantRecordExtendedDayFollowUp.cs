﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordExtendedDayFollowUp
    {
        public int ParticipantRecordExtendedDayFollowUpId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int Issue { get; set; }
        public DateTime TimePeriodtoApplyFollowUpTo { get; set; }
        public int RelatesTo { get; set; }
        public int OwnerUserId { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile OwnerUser { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
