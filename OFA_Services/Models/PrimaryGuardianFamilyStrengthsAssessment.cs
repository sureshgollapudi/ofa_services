﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyStrengthsAssessment
    {
        public int PrimaryGuardianFamilyStrengthsAssessmentId { get; set; }
        public int PrimaryGuardianId { get; set; }
        public bool? Declined { get; set; }
        public DateTime? DeclinedDate { get; set; }
        public int? FamilyLifePracticesCategoryFamilyRoutinesEntryScore { get; set; }
        public int? FamilyLifePracticesCategoryFamilyRoutinesUpdatedScore { get; set; }
        public int? FamilyLifePracticesCategoryExperienceRichEnvironmentEntryScore { get; set; }
        public int? FamilyLifePracticesCategoryExperienceRichEnvironmentUpdatedScore { get; set; }
        public int? FamilyLifePracticesCategoryPromotingLiteracyHomeEntryScore { get; set; }
        public int? FamilyLifePracticesCategoryPromotingLiteracyHomeUpdatedScore { get; set; }
        public int? FamilyLifePracticesCategoryPositiveDisciplineEntryScore { get; set; }
        public int? FamilyLifePracticesCategoryPositiveDisciplineUpdatedScore { get; set; }
        public int? FamilyLifePracticesEntryScoreSubTotal { get; set; }
        public int? FamilyLifePracticesUpdatedScoreSubTotal { get; set; }
        public int? SupportForChildrenCategoryChildrenSpecialNeedsEntryScore { get; set; }
        public int? SupportForChildrenCategoryChildrenSpecialNeedsUpdatedScore { get; set; }
        public int? SupportForChildrenCategoryChildHealthWellnessEntryScore { get; set; }
        public int? SupportForChildrenCategoryChildHealthWellnessUpdatedScore { get; set; }
        public int? SupportForChildrenEntryScoreSubTotal { get; set; }
        public int? SupportForChildrenUpdatedScoreSubTotal { get; set; }
        public int? SelfSufficiencyCategoryHousingCommunityEntryScore { get; set; }
        public int? SelfSufficiencyCategoryHousingCommunityUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryTransportationEntryScore { get; set; }
        public int? SelfSufficiencyCategoryTransportationUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryEmploymentEntryScore { get; set; }
        public int? SelfSufficiencyCategoryEmploymentUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryEducationEntryScore { get; set; }
        public int? SelfSufficiencyCategoryEducationUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryAcculturationEntryScore { get; set; }
        public int? SelfSufficiencyCategoryAcculturationUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryChildCareEntryScore { get; set; }
        public int? SelfSufficiencyCategoryChildCareUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryFamilyFinancesEntryScore { get; set; }
        public int? SelfSufficiencyCategoryFamilyFinancesUpdatedScore { get; set; }
        public int? SelfSufficiencyEntryScoreSubTotal { get; set; }
        public int? SelfSufficiencyUpdatedScoreSubTotal { get; set; }
        public int? SupportForFamilyMembersCategoryFamilyHealthWellnessEntryScore { get; set; }
        public int? SupportForFamilyMembersCategoryFamilyHealthWellnessUpdatedScore { get; set; }
        public int? SupportForFamilyMembersCategoryAccessToResourcesEntryScore { get; set; }
        public int? SupportForFamilyMembersCategoryAccessToResourcesUpdatedScore { get; set; }
        public int? SupportForFamilyMembersCategoryEmotionalSupportEntryScore { get; set; }
        public int? SupportForFamilyMembersCategoryEmotionalSupportUpdatedScore { get; set; }
        public int? SupportForFamilyMembersCategoryFamilyRelationshipsEntryScore { get; set; }
        public int? SupportForFamilyMembersCategoryFamilyRelationshipsUpdatedScore { get; set; }
        public int? SupportForFamilyMembersCategoryAlcoholDrugUseEntryScore { get; set; }
        public int? SupportForFamilyMembersCategoryAlcoholDrugUseUpdatedScore { get; set; }
        public int? SupportForFamilyMembersEntryScoreSubTotal { get; set; }
        public int? SupportForFamilyMembersUpdatedScoreSubTotal { get; set; }
        public int? AllEntryScoreTotal { get; set; }
        public int? AllUpdatedScoreTotal { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? SupportForFamilyMembersCategorySocialNetworksEntryScore { get; set; }
        public int? SupportForFamilyMembersCategorySocialNetworksUpdatedScore { get; set; }
        public int? SupportForChildrenCategoryChildTransitionsEntryScore { get; set; }
        public int? SupportForChildrenCategoryChildTransitionsUpdatedScore { get; set; }
        public int? SelfSufficiencyCategoryAdvocacyandLeadershipEntryScore { get; set; }
        public int? SelfSufficiencyCategoryAdvocacyandLeadershipUpdatedScore { get; set; }

        public ParentGuardian PrimaryGuardian { get; set; }
    }
}
