﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes
    {
        public int PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomesId { get; set; }
        public int PrimaryGuardianFamilyEngagementId { get; set; }
        public int AssociatedFamilyEngagementOutcomes { get; set; }

        public PrimaryGuardianFamilyEngagement PrimaryGuardianFamilyEngagement { get; set; }
    }
}
