﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationEducation
    {
        public int ParticipantRecordEducationEducationId { get; set; }
        public int ParticipantRecordId { get; set; }
        public bool? TransitioningEndofYearOverride { get; set; }
        public int? TransitionPlanAttachment { get; set; }
        public bool? TransitionPlanComplete { get; set; }
        public DateTime? TransitionPlanCompletionDate { get; set; }
        public DateTime? FirstPpvtdate { get; set; }
        public int? FirstPpvtscore { get; set; }
        public DateTime? SecondPpvtdate { get; set; }
        public int? SecondPpvtscore { get; set; }
        public DateTime? FirstWoodcockJohnsonDate { get; set; }
        public int? FirstWoodcockJohnsonScore { get; set; }
        public DateTime? SecondWoodcockJohnsonDate { get; set; }
        public int? SecondWoodcockJohnsonScore { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? User { get; set; }
        public int? TransitioningEndofYear { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
