﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AssessmentLevel
    {
        public AssessmentLevel()
        {
            Evaluation = new HashSet<Evaluation>();
            Rating = new HashSet<Rating>();
        }

        public int AssessmentLevelId { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int AssessmentTypeId { get; set; }
        public int LevelId { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public string Abbreviation { get; set; }
        public int? Category { get; set; }

        public AssessmentType AssessmentType { get; set; }
        public ICollection<Evaluation> Evaluation { get; set; }
        public ICollection<Rating> Rating { get; set; }
    }
}
