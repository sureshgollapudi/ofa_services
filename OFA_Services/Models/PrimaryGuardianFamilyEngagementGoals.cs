﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyEngagementGoals
    {
        public int PrimaryGuardianFamilyEngagementGoalsId { get; set; }
        public int PrimaryGuardianFamilyEngagementId { get; set; }
        public int GoalId { get; set; }

        public PrimaryGuardianFamilyEngagement PrimaryGuardianFamilyEngagement { get; set; }
    }
}
