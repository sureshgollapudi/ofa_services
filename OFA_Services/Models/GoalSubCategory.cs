﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GoalSubCategory
    {
        public GoalSubCategory()
        {
            Goal = new HashSet<Goal>();
        }

        public int GoalSubCategoryId { get; set; }
        public int? GoalCategoryId { get; set; }
        public string Name { get; set; }

        public GoalCategory GoalCategory { get; set; }
        public ICollection<Goal> Goal { get; set; }
    }
}
