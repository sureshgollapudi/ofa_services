﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Evaluation
    {
        public Evaluation()
        {
            EvaluationDocument = new HashSet<EvaluationDocument>();
        }

        public int EvaluationId { get; set; }
        public int LevelId { get; set; }
        public int Score { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ParticipantEvaluationCenterprogramyearId { get; set; }
        public int AssessmentLevelId { get; set; }
        public string Reason { get; set; }

        public AssessmentLevel AssessmentLevel { get; set; }
        public ParticipantEvaluationCenterprogramyear ParticipantEvaluationCenterprogramyear { get; set; }
        public ICollection<EvaluationDocument> EvaluationDocument { get; set; }
    }
}
