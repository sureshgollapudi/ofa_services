﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Program
    {
        public Program()
        {
            Center = new HashSet<Center>();
            ProgramPriority = new HashSet<ProgramPriority>();
            ProgramYear = new HashSet<ProgramYear>();
        }

        public int ProgramId { get; set; }
        public string Name { get; set; }
        public int GranteeId { get; set; }
        public int? SelectionCriteriaId { get; set; }
        public int? ProgramTypeId { get; set; }
        public bool? IsProgramActive { get; set; }
        public string DelegateNumber { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public string DirectorName { get; set; }
        public string DirectorEmail { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string MailingAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZip { get; set; }
        public bool? MailingAddressSameAsAbove { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
        public ProgramType ProgramType { get; set; }
        public SelectionCriteria SelectionCriteria { get; set; }
        public ICollection<Center> Center { get; set; }
        public ICollection<ProgramPriority> ProgramPriority { get; set; }
        public ICollection<ProgramYear> ProgramYear { get; set; }
    }
}
