﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhactionPlanType
    {
        public int ParticipantDmhactionPlanTypeId { get; set; }
        public int ParticipantDmhactionPlanId { get; set; }
        public int Type { get; set; }

        public ParticipantDmhactionPlan ParticipantDmhactionPlan { get; set; }
    }
}
