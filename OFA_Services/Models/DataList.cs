﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DataList
    {
        public int DataListId { get; set; }
        public string DataListName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
    }
}
