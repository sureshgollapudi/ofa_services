﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ProgramPriority
    {
        public int ProgramPriorityId { get; set; }
        public int ProgramId { get; set; }
        public int ProgramTypeId { get; set; }
        public int Priority { get; set; }

        public Program Program { get; set; }
        public ProgramType ProgramType { get; set; }
    }
}
