﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DmhsummaryPrimaryResult
    {
        public DmhsummaryPrimaryResult()
        {
            DmhsummaryAssociatedConcern = new HashSet<DmhsummaryAssociatedConcern>();
            DmhsummaryNextRequireAction = new HashSet<DmhsummaryNextRequireAction>();
        }

        public int DmhsummaryPrimaryResultId { get; set; }
        public int? ParticipantId { get; set; }
        public int? ProcessDocumentId { get; set; }
        public int? MostRecentActionId { get; set; }
        public DateTime? MostRecentActionDate { get; set; }
        public int? ParticipantDmhnoteId { get; set; }

        public ICollection<DmhsummaryAssociatedConcern> DmhsummaryAssociatedConcern { get; set; }
        public ICollection<DmhsummaryNextRequireAction> DmhsummaryNextRequireAction { get; set; }
    }
}
