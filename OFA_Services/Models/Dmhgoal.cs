﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Dmhgoal
    {
        public int DmhgoalId { get; set; }
        public DateTime? GoalStartDate { get; set; }
        public DateTime? GoalUpdatedate { get; set; }
        public DateTime? GoalEndDate { get; set; }
        public string GoalDescription { get; set; }
        public int? CreatedBy { get; set; }
        public int? Modified { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? DmhplanId { get; set; }
        public int? DmhProcessDocumentationId { get; set; }
    }
}
