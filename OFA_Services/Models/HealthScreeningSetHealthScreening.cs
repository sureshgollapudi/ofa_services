﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthScreeningSetHealthScreening
    {
        public int HealthScreeningSetHealthScreeningId { get; set; }
        public int HealthScreeningSetId { get; set; }
        public int HealthScreeningId { get; set; }

        public HealthScreening HealthScreening { get; set; }
        public HealthScreeningSet HealthScreeningSet { get; set; }
    }
}
