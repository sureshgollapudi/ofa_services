﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantConcernGoals
    {
        public int ParticipantConcernGoalsId { get; set; }
        public int HealthconcernId { get; set; }
        public int GoalCategoryId { get; set; }
        public int GoalSubCategoryId { get; set; }
        public string GoalName { get; set; }
    }
}
