﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Center
    {
        public Center()
        {
            CenterProgramYear = new HashSet<CenterProgramYear>();
            Classroom = new HashSet<Classroom>();
            UserCenter = new HashSet<UserCenter>();
        }

        public int CenterId { get; set; }
        public int ProgramId { get; set; }
        public bool? IsCenterActive { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string Director { get; set; }
        public string LicenseNumber { get; set; }
        public bool? ChildCarePartner { get; set; }
        public int? LicensedCapacity { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Program Program { get; set; }
        public ICollection<CenterProgramYear> CenterProgramYear { get; set; }
        public ICollection<Classroom> Classroom { get; set; }
        public ICollection<UserCenter> UserCenter { get; set; }
    }
}
