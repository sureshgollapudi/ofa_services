﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecord
    {
        public ParticipantRecord()
        {
            ParticipantRecordAlert = new HashSet<ParticipantRecordAlert>();
            ParticipantRecordAttendance = new HashSet<ParticipantRecordAttendance>();
            ParticipantRecordCaseNote = new HashSet<ParticipantRecordCaseNote>();
            ParticipantRecordEducationChallengingBehaviour = new HashSet<ParticipantRecordEducationChallengingBehaviour>();
            ParticipantRecordEducationEducation = new HashSet<ParticipantRecordEducationEducation>();
            ParticipantRecordEducationHomeBasedSocialization = new HashSet<ParticipantRecordEducationHomeBasedSocialization>();
            ParticipantRecordEducationHomeBasedVisitDocumentation = new HashSet<ParticipantRecordEducationHomeBasedVisitDocumentation>();
            ParticipantRecordEducationHomeBasedVisits = new HashSet<ParticipantRecordEducationHomeBasedVisits>();
            ParticipantRecordEducationHomeVisitDocumentation = new HashSet<ParticipantRecordEducationHomeVisitDocumentation>();
            ParticipantRecordEducationHomeVisits = new HashSet<ParticipantRecordEducationHomeVisits>();
            ParticipantRecordEducationIncident = new HashSet<ParticipantRecordEducationIncident>();
            ParticipantRecordEligibility = new HashSet<ParticipantRecordEligibility>();
            ParticipantRecordEnrollmentTransactionHistory = new HashSet<ParticipantRecordEnrollmentTransactionHistory>();
            ParticipantRecordExtendedDayBilling = new HashSet<ParticipantRecordExtendedDayBilling>();
            ParticipantRecordExtendedDayCredit = new HashSet<ParticipantRecordExtendedDayCredit>();
            ParticipantRecordExtendedDayFollowUp = new HashSet<ParticipantRecordExtendedDayFollowUp>();
            ParticipantRecordExtendedDayPayment = new HashSet<ParticipantRecordExtendedDayPayment>();
            ParticipantRecordExtendedDayVoucher = new HashSet<ParticipantRecordExtendedDayVoucher>();
            ParticipantRecordFamilyStrengthsAssessment = new HashSet<ParticipantRecordFamilyStrengthsAssessment>();
            ParticipantRecordGoal = new HashSet<ParticipantRecordGoal>();
            ParticipantRecordHealthPlanning = new HashSet<ParticipantRecordHealthPlanning>();
            ParticipantRecordImmunization = new HashSet<ParticipantRecordImmunization>();
            ParticipantRecordImmunizationPlanning = new HashSet<ParticipantRecordImmunizationPlanning>();
            ParticipantRecordImmunizationSetStatus = new HashSet<ParticipantRecordImmunizationSetStatus>();
            ParticipantRecordMetric = new HashSet<ParticipantRecordMetric>();
            ParticipantRecordPir = new HashSet<ParticipantRecordPir>();
            ParticipantRecordPirscreeningGroupStatus = new HashSet<ParticipantRecordPirscreeningGroupStatus>();
            ParticipantRecordPirscreeningStatus = new HashSet<ParticipantRecordPirscreeningStatus>();
            ParticipantRecordSelectionCriteriaCategoryItem = new HashSet<ParticipantRecordSelectionCriteriaCategoryItem>();
        }

        public int ParticipantRecordId { get; set; }
        public int ParticipantId { get; set; }
        public int CenterProgramYearId { get; set; }
        public int? PrimaryOwnerUserId { get; set; }
        public int? SecondaryOwnerUserId { get; set; }
        public int? Status { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public int? FamilyAdvocateUserId { get; set; }
        public DateTime? StatusLastUpdatedDate { get; set; }
        public int? ParticipationYear { get; set; }
        public bool? IsFamilyApplicationComplete { get; set; }
        public int? FamilyApplicationCompleteUserId { get; set; }
        public DateTime? FamilyApplicationCompleteDate { get; set; }
        public bool? IsEligibilityComplete { get; set; }
        public int? EligibilityCompleteUserId { get; set; }
        public DateTime? EligibilityCompleteDate { get; set; }
        public bool? IsSelectionCriteriaComplete { get; set; }
        public int? SelectionCriteriaCompleteUserId { get; set; }
        public DateTime? SelectionCriteriaCompleteDate { get; set; }
        public bool? IsSubmittedforReview { get; set; }
        public int? SubmitforReviewUserId { get; set; }
        public DateTime? SubmitforReviewUserDate { get; set; }
        public bool? IsPreEnrollmentDenied { get; set; }
        public int? PreEnrollmentDeniedReason { get; set; }
        public string PreEnrollmentDeniedReasonOther { get; set; }
        public int? PreEnrollmentDeniedUserId { get; set; }
        public DateTime? PreEnrollmentDeniedDate { get; set; }
        public bool? IsEmergencyContactsComplete { get; set; }
        public int? EmergencyContactsCompleteUserId { get; set; }
        public DateTime? EmergencyContactsCompleteDate { get; set; }
        public bool? IsProofOfResidencyComplete { get; set; }
        public int? ProofOfResidencyCompleteUserId { get; set; }
        public DateTime? ProofOfResidencyCompleteDate { get; set; }
        public bool? IsEmergencyConsentComplete { get; set; }
        public int? EmergencyConsentCompleteUserId { get; set; }
        public DateTime? EmergencyConsentCompleteDate { get; set; }
        public bool? IsPermissionToReleaseAndRequestComplete { get; set; }
        public int? PermissionToReleaseAndRequestCompleteUserId { get; set; }
        public DateTime? PermissionToReleaseAndRequestCompleteDate { get; set; }
        public bool? IsPermissionForProgramActivitiesComplete { get; set; }
        public int? PermissionForProgramActivitiesCompleteUserId { get; set; }
        public DateTime? PermissionForProgramActivitiesCompleteDate { get; set; }
        public bool? IsFamilyInvolvementContractComplete { get; set; }
        public int? FamilyInvolvementContractCompleteUserId { get; set; }
        public DateTime? FamilyInvolvementContractCompleteDate { get; set; }
        public bool? IsFamilyVolunteeringContractComplete { get; set; }
        public int? FamilyVolunteeringContractCompleteUserId { get; set; }
        public DateTime? FamilyVolunteeringContractCompleteDate { get; set; }
        public bool? IsCustomAdmissionForm1Complete { get; set; }
        public int? CustomAdmissionForm1UserId { get; set; }
        public DateTime? CustomAdmissionForm1Date { get; set; }
        public bool? IsCustomAdmissionForm2Complete { get; set; }
        public int? CustomAdmissionForm2UserId { get; set; }
        public DateTime? CustomAdmissionForm2Date { get; set; }
        public bool? IsCustomAdmissionForm3Complete { get; set; }
        public int? CustomAdmissionForm3UserId { get; set; }
        public DateTime? CustomAdmissionForm3Date { get; set; }
        public bool? IsCustomAdmissionForm4Complete { get; set; }
        public int? CustomAdmissionForm4UserId { get; set; }
        public DateTime? CustomAdmissionForm4Date { get; set; }
        public bool? IsCustomAdmissionForm5Complete { get; set; }
        public int? CustomAdmissionForm5UserId { get; set; }
        public DateTime? CustomAdmissionForm5Date { get; set; }
        public bool? IsImmunizationsComplete { get; set; }
        public int? ImmunizationsCompleteUserId { get; set; }
        public DateTime? ImmunizationsCompleteDate { get; set; }
        public bool? IsAdditionalHealthRequirementsComplete { get; set; }
        public int? AdditionalHealthRequirementsCompleteUserId { get; set; }
        public DateTime? AdditionalHealthRequirementsCompleteDate { get; set; }
        public bool? IsExtendedDayComplete { get; set; }
        public int? ExtendedDayCompleteUserId { get; set; }
        public DateTime? ExtendedDayCompleteCompleteDate { get; set; }
        public bool? IsThirdYearEligibilityReadyforReview { get; set; }
        public int? ThirdYearEligibilityReadyforReviewUserId { get; set; }
        public DateTime? ThirdYearEligibilityReadyforReviewDate { get; set; }
        public bool? IsOnPreEnrollmentWaitlist { get; set; }
        public int? PreEnrollmentWaitlistUserId { get; set; }
        public DateTime? PreEnrollmentWaitlistDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? CompletedProgramTerm { get; set; }
        public int? EnrollmentTerminationReason { get; set; }
        public DateTime? EnrollmentTerminationDate { get; set; }
        public bool? IsUnsubmittedForReview { get; set; }
        public int? UnsubmitForReviewUserId { get; set; }
        public DateTime? UnsubmitForReviewUserDate { get; set; }
        public bool? IsInactive { get; set; }
        public int? InactiveUserId { get; set; }
        public DateTime? InactiveDate { get; set; }
        public int? TotalOverrideSelectionPoints { get; set; }
        public bool? IsAccepted { get; set; }
        public int? AcceptedUserId { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public int? SelectionCriteriaOverridedTotal { get; set; }
        public string SelectionCriteriaNotes { get; set; }
        public int? ProgramTypePreference { get; set; }
        public DateTime? TotalAddedDateOverride { get; set; }
        public int? TotalAddedUserIdoverride { get; set; }
        public bool? IsNew { get; set; }
        public string EnrollmentTerminationReasonOther { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? IsConfirmedForNextProgramYear { get; set; }
        public int? SelectionCriteriaId { get; set; }
        public int? PreviousParticipantRecordId { get; set; }
        public bool? IsApplicationIntiatedComplete { get; set; }
        public int? ApplicationIntiatedCompleteUserId { get; set; }
        public DateTime? ApplicationIntiatedCompleteDate { get; set; }
        public int? PreviousApplicationStatus { get; set; }
        public bool? IsBirthdateDocumentationEnteredComplete { get; set; }
        public int? BirthdateDocumentationEnteredCompleteUserId { get; set; }
        public DateTime? BirthdateDocumentationEnteredCompleteDate { get; set; }

        public CenterProgramYear CenterProgramYear { get; set; }
        public UserProfile CreatedByNavigation { get; set; }
        public UserProfile FamilyAdvocateUser { get; set; }
        public UserProfile ModifiedByNavigation { get; set; }
        public Participant Participant { get; set; }
        public UserProfile PrimaryOwnerUser { get; set; }
        public UserProfile SecondaryOwnerUser { get; set; }
        public ParticipantRecordClassroomProgramYear ParticipantRecordClassroomProgramYear { get; set; }
        public ParticipantRecordHealthInfo ParticipantRecordHealthInfo { get; set; }
        public ICollection<ParticipantRecordAlert> ParticipantRecordAlert { get; set; }
        public ICollection<ParticipantRecordAttendance> ParticipantRecordAttendance { get; set; }
        public ICollection<ParticipantRecordCaseNote> ParticipantRecordCaseNote { get; set; }
        public ICollection<ParticipantRecordEducationChallengingBehaviour> ParticipantRecordEducationChallengingBehaviour { get; set; }
        public ICollection<ParticipantRecordEducationEducation> ParticipantRecordEducationEducation { get; set; }
        public ICollection<ParticipantRecordEducationHomeBasedSocialization> ParticipantRecordEducationHomeBasedSocialization { get; set; }
        public ICollection<ParticipantRecordEducationHomeBasedVisitDocumentation> ParticipantRecordEducationHomeBasedVisitDocumentation { get; set; }
        public ICollection<ParticipantRecordEducationHomeBasedVisits> ParticipantRecordEducationHomeBasedVisits { get; set; }
        public ICollection<ParticipantRecordEducationHomeVisitDocumentation> ParticipantRecordEducationHomeVisitDocumentation { get; set; }
        public ICollection<ParticipantRecordEducationHomeVisits> ParticipantRecordEducationHomeVisits { get; set; }
        public ICollection<ParticipantRecordEducationIncident> ParticipantRecordEducationIncident { get; set; }
        public ICollection<ParticipantRecordEligibility> ParticipantRecordEligibility { get; set; }
        public ICollection<ParticipantRecordEnrollmentTransactionHistory> ParticipantRecordEnrollmentTransactionHistory { get; set; }
        public ICollection<ParticipantRecordExtendedDayBilling> ParticipantRecordExtendedDayBilling { get; set; }
        public ICollection<ParticipantRecordExtendedDayCredit> ParticipantRecordExtendedDayCredit { get; set; }
        public ICollection<ParticipantRecordExtendedDayFollowUp> ParticipantRecordExtendedDayFollowUp { get; set; }
        public ICollection<ParticipantRecordExtendedDayPayment> ParticipantRecordExtendedDayPayment { get; set; }
        public ICollection<ParticipantRecordExtendedDayVoucher> ParticipantRecordExtendedDayVoucher { get; set; }
        public ICollection<ParticipantRecordFamilyStrengthsAssessment> ParticipantRecordFamilyStrengthsAssessment { get; set; }
        public ICollection<ParticipantRecordGoal> ParticipantRecordGoal { get; set; }
        public ICollection<ParticipantRecordHealthPlanning> ParticipantRecordHealthPlanning { get; set; }
        public ICollection<ParticipantRecordImmunization> ParticipantRecordImmunization { get; set; }
        public ICollection<ParticipantRecordImmunizationPlanning> ParticipantRecordImmunizationPlanning { get; set; }
        public ICollection<ParticipantRecordImmunizationSetStatus> ParticipantRecordImmunizationSetStatus { get; set; }
        public ICollection<ParticipantRecordMetric> ParticipantRecordMetric { get; set; }
        public ICollection<ParticipantRecordPir> ParticipantRecordPir { get; set; }
        public ICollection<ParticipantRecordPirscreeningGroupStatus> ParticipantRecordPirscreeningGroupStatus { get; set; }
        public ICollection<ParticipantRecordPirscreeningStatus> ParticipantRecordPirscreeningStatus { get; set; }
        public ICollection<ParticipantRecordSelectionCriteriaCategoryItem> ParticipantRecordSelectionCriteriaCategoryItem { get; set; }
    }
}
