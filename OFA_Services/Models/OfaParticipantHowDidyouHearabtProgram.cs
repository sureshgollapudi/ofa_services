﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipantHowDidyouHearabtProgram
    {
        public int ParticipantHowDidyouHearabtProgramId { get; set; }
        public int OfaParticipantId { get; set; }
        public int HowDidyouHearId { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParticipant OfaParticipant { get; set; }
    }
}
