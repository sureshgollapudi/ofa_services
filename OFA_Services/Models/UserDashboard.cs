﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserDashboard
    {
        public int UserDashboardId { get; set; }
        public int UserId { get; set; }
        public int Dashboard { get; set; }

        public UserProfile User { get; set; }
    }
}
