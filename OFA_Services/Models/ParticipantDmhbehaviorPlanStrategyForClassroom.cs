﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhbehaviorPlanStrategyForClassroom
    {
        public int ParticipantDmhbehaviorPlanStrategyForClassroomId { get; set; }
        public int ParticipantDmhbehaviorPlanId { get; set; }
        public int StrategyForClassroom { get; set; }

        public ParticipantDmhbehaviorPlan ParticipantDmhbehaviorPlan { get; set; }
    }
}
