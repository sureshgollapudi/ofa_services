﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DashboardItems
    {
        public int DashboardItemId { get; set; }
        public string Name { get; set; }
        public string ItemName { get; set; }
        public int? DashboardId { get; set; }
        public string Notes { get; set; }
    }
}
