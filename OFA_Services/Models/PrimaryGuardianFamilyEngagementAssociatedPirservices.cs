﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyEngagementAssociatedPirservices
    {
        public int PrimaryGuardianFamilyEngagementAssociatedPirservicesId { get; set; }
        public int PrimaryGuardianFamilyEngagementId { get; set; }
        public int AssociatedPirservices { get; set; }

        public PrimaryGuardianFamilyEngagement PrimaryGuardianFamilyEngagement { get; set; }
    }
}
