﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class MappingBetweenHealthInsurance
    {
        public int Id { get; set; }
        public string SourceHealthInsuranceEnrollment { get; set; }
        public string DesctionationHealthInsuranceEndofEnrollment { get; set; }
        public string ColumnName { get; set; }
        public string HealthInsuranceatTimeOfEnrollmentOther { get; set; }
    }
}
