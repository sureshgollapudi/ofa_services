﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianCaseNote
    {
        public PrimaryGuardianCaseNote()
        {
            PrimaryGuardianCaseNoteFollowUp = new HashSet<PrimaryGuardianCaseNoteFollowUp>();
        }

        public int PrimaryGuardianCaseNoteId { get; set; }
        public int PrimaryGuardianId { get; set; }
        public DateTime? Date { get; set; }
        public int? CreatorUserId { get; set; }
        public int Type { get; set; }
        public string Note { get; set; }
        public int? PirservicesReceived1 { get; set; }
        public int? PirservicesReceived2 { get; set; }
        public int? PirservicesReceived3 { get; set; }
        public bool? ClosureIndicator { get; set; }
        public int? ClosureReason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string ForeignNoteId { get; set; }
        public DateTime? PirservicesReceived1Date { get; set; }
        public DateTime? PirservicesReceived2Date { get; set; }
        public DateTime? PirservicesReceived3Date { get; set; }
        public bool? IsFromAttendance { get; set; }
        public bool? PeeractivityModeling { get; set; }
        public int? NeedType { get; set; }
        public string OtherDescription { get; set; }
        public DateTime? ClosureDate { get; set; }
        public int? LastModifiedUserId { get; set; }
        public int FatherFigureInvolved { get; set; }

        public UserProfile CreatorUser { get; set; }
        public ParentGuardian PrimaryGuardian { get; set; }
        public ICollection<PrimaryGuardianCaseNoteFollowUp> PrimaryGuardianCaseNoteFollowUp { get; set; }
    }
}
