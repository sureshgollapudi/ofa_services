﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteParticipantDmhconcern
    {
        public int ParticipantDmhnoteParticipantDmhconcernId { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ParticipantDmhconcernId { get; set; }
    }
}
