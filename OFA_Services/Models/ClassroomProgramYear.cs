﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ClassroomProgramYear
    {
        public ClassroomProgramYear()
        {
            ClassroomProgramYearClosed = new HashSet<ClassroomProgramYearClosed>();
            ClassroomProgramYearOpen = new HashSet<ClassroomProgramYearOpen>();
            ParticipantRecordAttendance = new HashSet<ParticipantRecordAttendance>();
            ParticipantRecordClassroomProgramYear = new HashSet<ParticipantRecordClassroomProgramYear>();
        }

        public int ClassroomProgramYearId { get; set; }
        public int CenterProgramYearId { get; set; }
        public int ClassroomId { get; set; }
        public int? ProgramYearId { get; set; }
        public int? FundedEnrollment { get; set; }
        public int? LicensedCapacity { get; set; }
        public string ProgramOption { get; set; }
        public bool? IsClassroomProgramYearActive { get; set; }
        public TimeSpan? TimeOpen { get; set; }
        public TimeSpan? TimeClosed { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturday { get; set; }
        public bool? Sunday { get; set; }
        public bool? Breakfast { get; set; }
        public bool? Amsnack { get; set; }
        public bool? Lunch { get; set; }
        public bool? Pmsnack { get; set; }
        public bool? Dinner { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? Iscopied { get; set; }

        public CenterProgramYear CenterProgramYear { get; set; }
        public Classroom Classroom { get; set; }
        public ProgramYear ProgramYear { get; set; }
        public ICollection<ClassroomProgramYearClosed> ClassroomProgramYearClosed { get; set; }
        public ICollection<ClassroomProgramYearOpen> ClassroomProgramYearOpen { get; set; }
        public ICollection<ParticipantRecordAttendance> ParticipantRecordAttendance { get; set; }
        public ICollection<ParticipantRecordClassroomProgramYear> ParticipantRecordClassroomProgramYear { get; set; }
    }
}
