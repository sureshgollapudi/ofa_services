﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class SchoolYear
    {
        public SchoolYear()
        {
            GranteeSchoolYearDefault = new HashSet<GranteeSchoolYearDefault>();
            PrimaryGuardianPirfamilyInformation = new HashSet<PrimaryGuardianPirfamilyInformation>();
            ProgramYear = new HashSet<ProgramYear>();
        }

        public int SchoolYearId { get; set; }
        public string Name { get; set; }
        public DateTime DefaultPirsubmissionDate { get; set; }

        public ICollection<GranteeSchoolYearDefault> GranteeSchoolYearDefault { get; set; }
        public ICollection<PrimaryGuardianPirfamilyInformation> PrimaryGuardianPirfamilyInformation { get; set; }
        public ICollection<ProgramYear> ProgramYear { get; set; }
    }
}
