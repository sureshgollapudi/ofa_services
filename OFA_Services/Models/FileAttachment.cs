﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class FileAttachment
    {
        public int FileAttachmentId { get; set; }
        public int LevelType { get; set; }
        public int LevelTypeIdentifier { get; set; }
        public int Type { get; set; }
        public int? SubType { get; set; }
        public int? SubTypeIdentifier { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? OldLevelType { get; set; }
        public string OldFileName { get; set; }
        public string OldFilePath { get; set; }
        public int? OldLevelTypeIdentifier { get; set; }
        public string DisplayName { get; set; }
        public int? Sequence { get; set; }

        public UserProfile CreatorUser { get; set; }
    }
}
