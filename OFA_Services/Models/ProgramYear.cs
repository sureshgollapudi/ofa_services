﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ProgramYear
    {
        public ProgramYear()
        {
            CenterProgramYear = new HashSet<CenterProgramYear>();
            ClassroomProgramYear = new HashSet<ClassroomProgramYear>();
        }

        public int ProgramYearId { get; set; }
        public int ProgramId { get; set; }
        public int SchoolYearId { get; set; }
        public int? TotalFundedEnrollment { get; set; }
        public int? FundedEnrollmentByOptionCenterBased5DayPerWeekAvailableAsFullWorkingDay { get; set; }
        public int? FundedEnrollmentByOptionCenterBased5DayPerWeekAvailableAsFullWorkingDayAvailableFullCalendarYear { get; set; }
        public int? FundedEnrollmentByOptionCenterBased5DayPerWeekPartDayEnrollment { get; set; }
        public int? FundedEnrollmentByOptionCenterBased5DayPerWeekPartDayEnrollmentOfTheseDoubleSessions { get; set; }
        public int? FundedEnrollmentByOptionCenterBased4DayPerWeekFullDayEnrollment { get; set; }
        public int? FundedEnrollmentByOptionCenterBased4DayPerWeekPartDayEnrollment { get; set; }
        public int? FundedEnrollmentByOptionCenterBased4DayPerWeekPartDayEnrollmentDoubleSessions { get; set; }
        public int? FundedEnrollmentFamilyHomeBasedOption { get; set; }
        public int? FundedEnrollmentCombinationOption { get; set; }
        public int? FundedEnrollmentFamilyChildCareOption { get; set; }
        public int? FundedEnrollmentFamilyChildCareOptionFullDayWorkEnvironment { get; set; }
        public int? FundedEnrollmentFamilyChildCareOptionFullDayWorkEnvironmentAvailableFullWorkingDay { get; set; }
        public int? FundedEnrollmentByOptionLocallyDesignedOption { get; set; }
        public decimal? PovertyLevelBase { get; set; }
        public decimal? PovertyLevelEachAdditionalPerson { get; set; }
        public bool? IsProgramYearActive { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? SelectionCriteriaId { get; set; }
        public int? FundedEnrollmentByOptionCenterBased5DaysFullDay { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal? PreviousPovertyLevelBase { get; set; }
        public decimal? PreviousPovertyLevelEachAdditionalPerson { get; set; }
        public int? Iscopied { get; set; }
        public bool? IsPca { get; set; }
        public int? UpdatedSelectionCriteriaId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? PirsubmissionDate { get; set; }
        public decimal? InitialPovertyLevelBaseOf1FamilyMember { get; set; }
        public decimal? InitialPovertyLevelBaseOf2FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf3FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf4FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf5FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf6FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf7FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseOf8FamilyMembers { get; set; }
        public decimal? InitialPovertyLevelBaseIfMoreThan8FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf1FamilyMember { get; set; }
        public decimal? PovertyLevelBaseOf2FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf3FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf4FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf5FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf6FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf7FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseOf8FamilyMembers { get; set; }
        public decimal? PovertyLevelBaseIfMoreThan8FamilyMembers { get; set; }

        public Program Program { get; set; }
        public SchoolYear SchoolYear { get; set; }
        public SelectionCriteria SelectionCriteria { get; set; }
        public ICollection<CenterProgramYear> CenterProgramYear { get; set; }
        public ICollection<ClassroomProgramYear> ClassroomProgramYear { get; set; }
    }
}
