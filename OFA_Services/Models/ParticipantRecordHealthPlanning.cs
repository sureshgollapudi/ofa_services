﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordHealthPlanning
    {
        public ParticipantRecordHealthPlanning()
        {
            ParticipantRecordHealthPlanningHistory = new HashSet<ParticipantRecordHealthPlanningHistory>();
        }

        public int ParticipantRecordHealthPlanningId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime? PlanningDate { get; set; }
        public int PlanningStatus { get; set; }
        public string PlanningNote { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
        public ICollection<ParticipantRecordHealthPlanningHistory> ParticipantRecordHealthPlanningHistory { get; set; }
    }
}
