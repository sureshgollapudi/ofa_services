﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhactionPlan
    {
        public ParticipantDmhactionPlan()
        {
            ParticipantDmhactionPlanStrategyForChild = new HashSet<ParticipantDmhactionPlanStrategyForChild>();
            ParticipantDmhactionPlanStrategyForClassroom = new HashSet<ParticipantDmhactionPlanStrategyForClassroom>();
            ParticipantDmhactionPlanStrategyForHome = new HashSet<ParticipantDmhactionPlanStrategyForHome>();
            ParticipantDmhactionPlanType = new HashSet<ParticipantDmhactionPlanType>();
        }

        public int ParticipantDmhactionPlanId { get; set; }
        public int ParticipantId { get; set; }
        public bool? BehaviorPlanQuestion { get; set; }
        public bool? BehaviorPlanBasedOnFba { get; set; }
        public DateTime? FbacompletionDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? InputDate { get; set; }
        public string StrategiesForChildDescription { get; set; }
        public string StrategiesForClassroomDescription { get; set; }
        public string StrategiesForHomeDescription { get; set; }
        public string PlanTypeOther { get; set; }
        public string StrategyForChildOther { get; set; }
        public string StrategyForClassroomOther { get; set; }
        public string StrategyForHomeOther { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ActionPlanUpdateFrequency { get; set; }

        public UserProfile CreatedByUser { get; set; }
        public Participant Participant { get; set; }
        public ICollection<ParticipantDmhactionPlanStrategyForChild> ParticipantDmhactionPlanStrategyForChild { get; set; }
        public ICollection<ParticipantDmhactionPlanStrategyForClassroom> ParticipantDmhactionPlanStrategyForClassroom { get; set; }
        public ICollection<ParticipantDmhactionPlanStrategyForHome> ParticipantDmhactionPlanStrategyForHome { get; set; }
        public ICollection<ParticipantDmhactionPlanType> ParticipantDmhactionPlanType { get; set; }
    }
}
