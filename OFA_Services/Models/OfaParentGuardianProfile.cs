﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParentGuardianProfile
    {
        public int ParentGuardianProfileId { get; set; }
        public int ParentRegParentIdFk { get; set; }
        public string RelationshipToChild { get; set; }
        public int? ChildRelationshipToAdultId { get; set; }
        public bool? Custody { get; set; }
        public bool? LivesWithChild { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int? StateIdFk { get; set; }
        public int? PrimaryLanguageSpoken { get; set; }
        public int? PrimaryLanguageRead { get; set; }
        public int? CommunicationLanguageWithChild { get; set; }
        public int? EducationLevel { get; set; }
        public int? EmployementStatus { get; set; }
        public bool? TeenParent { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParentRegistration ParentRegParentIdFkNavigation { get; set; }
        public State StateIdFkNavigation { get; set; }
    }
}
