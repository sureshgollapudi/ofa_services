﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantHealthScreeningDocumentation
    {
        public ParticipantHealthScreeningDocumentation()
        {
            ParticipantHealthConcernParticipantHealthScreeningDocumentation = new HashSet<ParticipantHealthConcernParticipantHealthScreeningDocumentation>();
            ParticipantInternalReferral = new HashSet<ParticipantInternalReferral>();
        }

        public int ParticipantHealthScreeningDocumentationId { get; set; }
        public int ParticipantId { get; set; }
        public int GranteeHealthScreeningId { get; set; }
        public int? ProgramTypeId { get; set; }
        public bool? IsRefused { get; set; }
        public DateTime? RefusedDate { get; set; }
        public bool? IsExempt { get; set; }
        public DateTime? ExemptDate { get; set; }
        public DateTime? ScreeningDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CompletionStatus { get; set; }
        public int? HeightUnit { get; set; }
        public decimal? HeightInches { get; set; }
        public decimal? HeightCentimeters { get; set; }
        public int? HeightAgePercentile { get; set; }
        public int? WeightUnit { get; set; }
        public int? WeightInPounds { get; set; }
        public int? WeightInOunces { get; set; }
        public decimal? WeightInFractionalPounds { get; set; }
        public decimal? WeightInKilograms { get; set; }
        public int? WeightAgePercentile { get; set; }
        public int? WeightHeightPercentile { get; set; }
        public int? BmiagePercentile { get; set; }
        public int? HeadCircumferenceInCentimeters { get; set; }
        public int? HeadCircumferenceAgePercentile { get; set; }
        public int? ResultsStatus { get; set; }
        public string Notes { get; set; }
        public int? ScreeningDocumentationUserId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public bool? IsInternal { get; set; }
        public DateTime? ExternalReceivedDate { get; set; }
        public string ExternalProvider { get; set; }
        public int? NotificationMethod { get; set; }
        public DateTime? NotificationDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? HealthScreeningId { get; set; }
        public string NotificationMethodOther { get; set; }
        public int? CountForPreventiveDentalRequirement { get; set; }
        public int? CreatedBy { get; set; }

        public GranteeHealthScreening GranteeHealthScreening { get; set; }
        public Participant Participant { get; set; }
        public ProgramType ProgramType { get; set; }
        public UserProfile ScreeningDocumentationUser { get; set; }
        public ICollection<ParticipantHealthConcernParticipantHealthScreeningDocumentation> ParticipantHealthConcernParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<ParticipantInternalReferral> ParticipantInternalReferral { get; set; }
    }
}
