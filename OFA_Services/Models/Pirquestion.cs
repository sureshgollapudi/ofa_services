﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Pirquestion
    {
        public Pirquestion()
        {
            GranteeHealthScreeningPirquestion = new HashSet<GranteeHealthScreeningPirquestion>();
            HealthScreeningPirquestion = new HashSet<HealthScreeningPirquestion>();
        }

        public int PirquestionId { get; set; }
        public string Question { get; set; }
        public string ShortDisplayQuestion { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ICollection<GranteeHealthScreeningPirquestion> GranteeHealthScreeningPirquestion { get; set; }
        public ICollection<HealthScreeningPirquestion> HealthScreeningPirquestion { get; set; }
    }
}
