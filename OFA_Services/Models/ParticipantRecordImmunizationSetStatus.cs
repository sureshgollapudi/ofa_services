﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordImmunizationSetStatus
    {
        public int ParticipantRecordImmunizationSetStatusId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? EnrollmentStatusAtEnrollment { get; set; }
        public int? EnrollmentStatusAtEnrollmentOwnerUserId { get; set; }
        public DateTime? EnrollmentStatusAtEnrollmentUpdatedDate { get; set; }
        public int? EnrollmentStatusEndOfEnrollment { get; set; }
        public int? EnrollmentStatusEndOfEnrollmentOwnerUserId { get; set; }
        public DateTime? EnrollmentStatusEndOfEnrollmentUpdatedDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile EnrollmentStatusAtEnrollmentOwnerUser { get; set; }
        public UserProfile EnrollmentStatusEndOfEnrollmentOwnerUser { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
