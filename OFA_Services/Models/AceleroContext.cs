﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace OFA_Services.Models
{
    public partial class AceleroContext : DbContext
    {
        public AceleroContext()
        {
        }

        public AceleroContext(DbContextOptions<AceleroContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AbsentAttendance> AbsentAttendance { get; set; }
        public virtual DbSet<ActivityCard> ActivityCard { get; set; }
        public virtual DbSet<Alert> Alert { get; set; }
        public virtual DbSet<AlertGroup> AlertGroup { get; set; }
        public virtual DbSet<AlertProgramType> AlertProgramType { get; set; }
        public virtual DbSet<AssessmentDocument> AssessmentDocument { get; set; }
        public virtual DbSet<AssessmentDocumentTag> AssessmentDocumentTag { get; set; }
        public virtual DbSet<AssessmentLevel> AssessmentLevel { get; set; }
        public virtual DbSet<AssessmentType> AssessmentType { get; set; }
        public virtual DbSet<Calendar> Calendar { get; set; }
        public virtual DbSet<CalendarItem> CalendarItem { get; set; }
        public virtual DbSet<CaseLoadUser> CaseLoadUser { get; set; }
        public virtual DbSet<Center> Center { get; set; }
        public virtual DbSet<CenterProgramYear> CenterProgramYear { get; set; }
        public virtual DbSet<Classroom> Classroom { get; set; }
        public virtual DbSet<ClassroomAdultMeal> ClassroomAdultMeal { get; set; }
        public virtual DbSet<ClassroomProgramYear> ClassroomProgramYear { get; set; }
        public virtual DbSet<ClassroomProgramYearClosed> ClassroomProgramYearClosed { get; set; }
        public virtual DbSet<ClassroomProgramYearOpen> ClassroomProgramYearOpen { get; set; }
        public virtual DbSet<ClassroonProgramYearTeacher> ClassroonProgramYearTeacher { get; set; }
        public virtual DbSet<DashboardItemMetrics> DashboardItemMetrics { get; set; }
        public virtual DbSet<DashboardItems> DashboardItems { get; set; }
        public virtual DbSet<DataList> DataList { get; set; }
        public virtual DbSet<DataListAccess> DataListAccess { get; set; }
        public virtual DbSet<Dmhgoal> Dmhgoal { get; set; }
        public virtual DbSet<Dmhset> Dmhset { get; set; }
        public virtual DbSet<DmhsummaryAssociatedConcern> DmhsummaryAssociatedConcern { get; set; }
        public virtual DbSet<DmhsummaryNextRequireAction> DmhsummaryNextRequireAction { get; set; }
        public virtual DbSet<DmhsummaryPrimaryResult> DmhsummaryPrimaryResult { get; set; }
        public virtual DbSet<DmhsummaryPriority> DmhsummaryPriority { get; set; }
        public virtual DbSet<EducationScreening> EducationScreening { get; set; }
        public virtual DbSet<EducationScreeningPirquestion> EducationScreeningPirquestion { get; set; }
        public virtual DbSet<EducationScreeningSet> EducationScreeningSet { get; set; }
        public virtual DbSet<EducationStatusCodeDescriptionMapping> EducationStatusCodeDescriptionMapping { get; set; }
        public virtual DbSet<ElmahError> ElmahError { get; set; }
        public virtual DbSet<EmploymentStatusMigrationMapping> EmploymentStatusMigrationMapping { get; set; }
        public virtual DbSet<Evaluation> Evaluation { get; set; }
        public virtual DbSet<EvaluationDocument> EvaluationDocument { get; set; }
        public virtual DbSet<FamilyPracticeArea> FamilyPracticeArea { get; set; }
        public virtual DbSet<FileAttachment> FileAttachment { get; set; }
        public virtual DbSet<FileAttachmentTemp> FileAttachmentTemp { get; set; }
        public virtual DbSet<FileHistory> FileHistory { get; set; }
        public virtual DbSet<FseissueCodeMapping> FseissueCodeMapping { get; set; }
        public virtual DbSet<FundedEnrollmentOption> FundedEnrollmentOption { get; set; }
        public virtual DbSet<Goal> Goal { get; set; }
        public virtual DbSet<GoalCategory> GoalCategory { get; set; }
        public virtual DbSet<GoalSubCategory> GoalSubCategory { get; set; }
        public virtual DbSet<Grantee> Grantee { get; set; }
        public virtual DbSet<GranteeAlert> GranteeAlert { get; set; }
        public virtual DbSet<GranteeHealthScreening> GranteeHealthScreening { get; set; }
        public virtual DbSet<GranteeHealthScreeningPirquestion> GranteeHealthScreeningPirquestion { get; set; }
        public virtual DbSet<GranteeHealthScreeningSet> GranteeHealthScreeningSet { get; set; }
        public virtual DbSet<GranteeHealthScreeningSetGranteeHealthScreening> GranteeHealthScreeningSetGranteeHealthScreening { get; set; }
        public virtual DbSet<GranteeImmunization> GranteeImmunization { get; set; }
        public virtual DbSet<GranteeSchoolYearDefault> GranteeSchoolYearDefault { get; set; }
        public virtual DbSet<GranteeTheme> GranteeTheme { get; set; }
        public virtual DbSet<HealthAndEducationScreenMapping> HealthAndEducationScreenMapping { get; set; }
        public virtual DbSet<HealthRequirementsActionStatusOptionsOnCreateNewScreening> HealthRequirementsActionStatusOptionsOnCreateNewScreening { get; set; }
        public virtual DbSet<HealthScreening> HealthScreening { get; set; }
        public virtual DbSet<HealthScreeningPirquestion> HealthScreeningPirquestion { get; set; }
        public virtual DbSet<HealthScreeningSet> HealthScreeningSet { get; set; }
        public virtual DbSet<HealthScreeningSetHealthScreening> HealthScreeningSetHealthScreening { get; set; }
        public virtual DbSet<Immunization> Immunization { get; set; }
        public virtual DbSet<ImmunizationSet> ImmunizationSet { get; set; }
        public virtual DbSet<JobErrors> JobErrors { get; set; }
        public virtual DbSet<LevelMaster> LevelMaster { get; set; }
        public virtual DbSet<MappingBetweenHealthInsurance> MappingBetweenHealthInsurance { get; set; }
        public virtual DbSet<MasterGoalGrantee> MasterGoalGrantee { get; set; }
        public virtual DbSet<Metric> Metric { get; set; }
        public virtual DbSet<MetricProgramType> MetricProgramType { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<NightlyJobLog> NightlyJobLog { get; set; }
        public virtual DbSet<OfaGranteeTableData> OfaGranteeTableData { get; set; }
        public virtual DbSet<OfaNotification> OfaNotification { get; set; }
        public virtual DbSet<OfaParentPhoneNumber> OfaParentPhoneNumber { get; set; }
        public virtual DbSet<OfaParentRegistration> OfaParentRegistration { get; set; }
        public virtual DbSet<OfaParticipant> OfaParticipant { get; set; }
        public virtual DbSet<OfaParticipantAdditionalMember> OfaParticipantAdditionalMember { get; set; }
        public virtual DbSet<OfaParticipantConcern> OfaParticipantConcern { get; set; }
        public virtual DbSet<OfaParticipantEmergencyContact> OfaParticipantEmergencyContact { get; set; }
        public virtual DbSet<OfaParticipantFamilyServices> OfaParticipantFamilyServices { get; set; }
        public virtual DbSet<OfaParticipantHowDidyouHearabtProgram> OfaParticipantHowDidyouHearabtProgram { get; set; }
        public virtual DbSet<OfaParticipantNotification> OfaParticipantNotification { get; set; }
        public virtual DbSet<OfaParticipantOtherChildCare> OfaParticipantOtherChildCare { get; set; }
        public virtual DbSet<OfaParticipantRace> OfaParticipantRace { get; set; }
        public virtual DbSet<OfaParticipantFileAttachment> OfaParticipantFileAttachment { get; set; }
        public virtual DbSet<ParentGuardian> ParentGuardian { get; set; }
        public virtual DbSet<Participant> Participant { get; set; }
        public virtual DbSet<ParticipantAction> ParticipantAction { get; set; }
        public virtual DbSet<ParticipantAdditionalFamilyMember> ParticipantAdditionalFamilyMember { get; set; }
        public virtual DbSet<ParticipantAlert> ParticipantAlert { get; set; }
        public virtual DbSet<ParticipantConcernGoals> ParticipantConcernGoals { get; set; }
        public virtual DbSet<ParticipantDmhactionPlan> ParticipantDmhactionPlan { get; set; }
        public virtual DbSet<ParticipantDmhactionPlanStrategyForChild> ParticipantDmhactionPlanStrategyForChild { get; set; }
        public virtual DbSet<ParticipantDmhactionPlanStrategyForClassroom> ParticipantDmhactionPlanStrategyForClassroom { get; set; }
        public virtual DbSet<ParticipantDmhactionPlanStrategyForHome> ParticipantDmhactionPlanStrategyForHome { get; set; }
        public virtual DbSet<ParticipantDmhactionPlanType> ParticipantDmhactionPlanType { get; set; }
        public virtual DbSet<ParticipantDmhbehaviorPlan> ParticipantDmhbehaviorPlan { get; set; }
        public virtual DbSet<ParticipantDmhbehaviorPlanStrategyForChild> ParticipantDmhbehaviorPlanStrategyForChild { get; set; }
        public virtual DbSet<ParticipantDmhbehaviorPlanStrategyForClassroom> ParticipantDmhbehaviorPlanStrategyForClassroom { get; set; }
        public virtual DbSet<ParticipantDmhbehaviorPlanStrategyForHome> ParticipantDmhbehaviorPlanStrategyForHome { get; set; }
        public virtual DbSet<ParticipantDmhbehaviorPlanType> ParticipantDmhbehaviorPlanType { get; set; }
        public virtual DbSet<ParticipantDmhconcern> ParticipantDmhconcern { get; set; }
        public virtual DbSet<ParticipantDmhconcernArea> ParticipantDmhconcernArea { get; set; }
        public virtual DbSet<ParticipantDmhiep> ParticipantDmhiep { get; set; }
        public virtual DbSet<ParticipantDmhiepactionPlan> ParticipantDmhiepactionPlan { get; set; }
        public virtual DbSet<ParticipantDmhiepactionPlanStrategyForChild> ParticipantDmhiepactionPlanStrategyForChild { get; set; }
        public virtual DbSet<ParticipantDmhiepactionPlanStrategyForClassroom> ParticipantDmhiepactionPlanStrategyForClassroom { get; set; }
        public virtual DbSet<ParticipantDmhiepactionPlanStrategyForHome> ParticipantDmhiepactionPlanStrategyForHome { get; set; }
        public virtual DbSet<ParticipantDmhiepactionPlanType> ParticipantDmhiepactionPlanType { get; set; }
        public virtual DbSet<ParticipantDmhiepadditionalDiagnosis> ParticipantDmhiepadditionalDiagnosis { get; set; }
        public virtual DbSet<ParticipantDmhiepassociatedConcern> ParticipantDmhiepassociatedConcern { get; set; }
        public virtual DbSet<ParticipantDmhnote> ParticipantDmhnote { get; set; }
        public virtual DbSet<ParticipantDmhnoteAssociation> ParticipantDmhnoteAssociation { get; set; }
        public virtual DbSet<ParticipantDmhnoteParticipantDmhactionPlan> ParticipantDmhnoteParticipantDmhactionPlan { get; set; }
        public virtual DbSet<ParticipantDmhnoteParticipantDmhbehavior> ParticipantDmhnoteParticipantDmhbehavior { get; set; }
        public virtual DbSet<ParticipantDmhnoteParticipantDmhconcern> ParticipantDmhnoteParticipantDmhconcern { get; set; }
        public virtual DbSet<ParticipantDmhnoteParticipantDmhiep> ParticipantDmhnoteParticipantDmhiep { get; set; }
        public virtual DbSet<ParticipantDmhnoteParticipantDmhiepactionPlan> ParticipantDmhnoteParticipantDmhiepactionPlan { get; set; }
        public virtual DbSet<ParticipantDmhsummary> ParticipantDmhsummary { get; set; }
        public virtual DbSet<ParticipantDmhsummaryResult> ParticipantDmhsummaryResult { get; set; }
        public virtual DbSet<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentation { get; set; }
        public virtual DbSet<ParticipantEducationScreeningPlanning> ParticipantEducationScreeningPlanning { get; set; }
        public virtual DbSet<ParticipantEducationScreeningReport> ParticipantEducationScreeningReport { get; set; }
        public virtual DbSet<ParticipantEmergencyContact> ParticipantEmergencyContact { get; set; }
        public virtual DbSet<ParticipantEvaluationCenterprogramyear> ParticipantEvaluationCenterprogramyear { get; set; }
        public virtual DbSet<ParticipantFamilyProfile> ParticipantFamilyProfile { get; set; }
        public virtual DbSet<ParticipantFollowUp> ParticipantFollowUp { get; set; }
        public virtual DbSet<ParticipantHealthConcern> ParticipantHealthConcern { get; set; }
        public virtual DbSet<ParticipantHealthConcernParticipantHealthScreeningDocumentation> ParticipantHealthConcernParticipantHealthScreeningDocumentation { get; set; }
        public virtual DbSet<ParticipantHealthScreeningDocumentation> ParticipantHealthScreeningDocumentation { get; set; }
        public virtual DbSet<ParticipantHealthScreeningPlanning> ParticipantHealthScreeningPlanning { get; set; }
        public virtual DbSet<ParticipantHealthScreeningReport> ParticipantHealthScreeningReport { get; set; }
        public virtual DbSet<ParticipantImmunization> ParticipantImmunization { get; set; }
        public virtual DbSet<ParticipantImmunizationPlanning> ParticipantImmunizationPlanning { get; set; }
        public virtual DbSet<ParticipantImmunizationPlanningHistory> ParticipantImmunizationPlanningHistory { get; set; }
        public virtual DbSet<ParticipantInternalReferral> ParticipantInternalReferral { get; set; }
        public virtual DbSet<ParticipantInternalReferralFollowUp> ParticipantInternalReferralFollowUp { get; set; }
        public virtual DbSet<ParticipantInternalReferralResolutionConcern> ParticipantInternalReferralResolutionConcern { get; set; }
        public virtual DbSet<ParticipantParentGuardian> ParticipantParentGuardian { get; set; }
        public virtual DbSet<ParticipantRace> ParticipantRace { get; set; }
        public virtual DbSet<ParticipantRecord> ParticipantRecord { get; set; }
        public virtual DbSet<ParticipantRecordAlert> ParticipantRecordAlert { get; set; }
        public virtual DbSet<ParticipantRecordAttendance> ParticipantRecordAttendance { get; set; }
        public virtual DbSet<ParticipantRecordCaseNote> ParticipantRecordCaseNote { get; set; }
        public virtual DbSet<ParticipantRecordCaseNoteFollowUp> ParticipantRecordCaseNoteFollowUp { get; set; }
        public virtual DbSet<ParticipantRecordClassroomProgramYear> ParticipantRecordClassroomProgramYear { get; set; }
        public virtual DbSet<ParticipantRecordEducationChallengingBehaviour> ParticipantRecordEducationChallengingBehaviour { get; set; }
        public virtual DbSet<ParticipantRecordEducationEducation> ParticipantRecordEducationEducation { get; set; }
        public virtual DbSet<ParticipantRecordEducationHomeBasedSocialization> ParticipantRecordEducationHomeBasedSocialization { get; set; }
        public virtual DbSet<ParticipantRecordEducationHomeBasedVisitDocumentation> ParticipantRecordEducationHomeBasedVisitDocumentation { get; set; }
        public virtual DbSet<ParticipantRecordEducationHomeBasedVisits> ParticipantRecordEducationHomeBasedVisits { get; set; }
        public virtual DbSet<ParticipantRecordEducationHomeVisitDocumentation> ParticipantRecordEducationHomeVisitDocumentation { get; set; }
        public virtual DbSet<ParticipantRecordEducationHomeVisits> ParticipantRecordEducationHomeVisits { get; set; }
        public virtual DbSet<ParticipantRecordEducationIncident> ParticipantRecordEducationIncident { get; set; }
        public virtual DbSet<ParticipantRecordEligibility> ParticipantRecordEligibility { get; set; }
        public virtual DbSet<ParticipantRecordEnrollmentHistory> ParticipantRecordEnrollmentHistory { get; set; }
        public virtual DbSet<ParticipantRecordEnrollmentTransactionHistory> ParticipantRecordEnrollmentTransactionHistory { get; set; }
        public virtual DbSet<ParticipantRecordExtendedDayBilling> ParticipantRecordExtendedDayBilling { get; set; }
        public virtual DbSet<ParticipantRecordExtendedDayCredit> ParticipantRecordExtendedDayCredit { get; set; }
        public virtual DbSet<ParticipantRecordExtendedDayFollowUp> ParticipantRecordExtendedDayFollowUp { get; set; }
        public virtual DbSet<ParticipantRecordExtendedDayPayment> ParticipantRecordExtendedDayPayment { get; set; }
        public virtual DbSet<ParticipantRecordExtendedDayVoucher> ParticipantRecordExtendedDayVoucher { get; set; }
        public virtual DbSet<ParticipantRecordFamilyAdvocate> ParticipantRecordFamilyAdvocate { get; set; }
        public virtual DbSet<ParticipantRecordFamilyStrengthsAssessment> ParticipantRecordFamilyStrengthsAssessment { get; set; }
        public virtual DbSet<ParticipantRecordFile> ParticipantRecordFile { get; set; }
        public virtual DbSet<ParticipantRecordGoal> ParticipantRecordGoal { get; set; }
        public virtual DbSet<ParticipantRecordGoalFollowUp> ParticipantRecordGoalFollowUp { get; set; }
        public virtual DbSet<ParticipantRecordHealthInfo> ParticipantRecordHealthInfo { get; set; }
        public virtual DbSet<ParticipantRecordHealthPlanning> ParticipantRecordHealthPlanning { get; set; }
        public virtual DbSet<ParticipantRecordHealthPlanningHistory> ParticipantRecordHealthPlanningHistory { get; set; }
        public virtual DbSet<ParticipantRecordImmunization> ParticipantRecordImmunization { get; set; }
        public virtual DbSet<ParticipantRecordImmunizationPlanning> ParticipantRecordImmunizationPlanning { get; set; }
        public virtual DbSet<ParticipantRecordImmunizationSetStatus> ParticipantRecordImmunizationSetStatus { get; set; }
        public virtual DbSet<ParticipantRecordMetric> ParticipantRecordMetric { get; set; }
        public virtual DbSet<ParticipantRecordPir> ParticipantRecordPir { get; set; }
        public virtual DbSet<ParticipantRecordPirscreeningGroupStatus> ParticipantRecordPirscreeningGroupStatus { get; set; }
        public virtual DbSet<ParticipantRecordPirscreeningStatus> ParticipantRecordPirscreeningStatus { get; set; }
        public virtual DbSet<ParticipantRecordPreEnrollmentTransactionHistory> ParticipantRecordPreEnrollmentTransactionHistory { get; set; }
        public virtual DbSet<ParticipantRecordSelectionCriteriaCategoryItem> ParticipantRecordSelectionCriteriaCategoryItem { get; set; }
        public virtual DbSet<Pirmapping> Pirmapping { get; set; }
        public virtual DbSet<Pirquestion> Pirquestion { get; set; }
        public virtual DbSet<PreEnrollmentFilterLog> PreEnrollmentFilterLog { get; set; }
        public virtual DbSet<PresentAttendance> PresentAttendance { get; set; }
        public virtual DbSet<PrimaryGuardianCaseNote> PrimaryGuardianCaseNote { get; set; }
        public virtual DbSet<PrimaryGuardianCaseNoteFollowUp> PrimaryGuardianCaseNoteFollowUp { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyEngagement> PrimaryGuardianFamilyEngagement { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes> PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyEngagementAssociatedPirservices> PrimaryGuardianFamilyEngagementAssociatedPirservices { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyEngagementFlpworkshopActivity> PrimaryGuardianFamilyEngagementFlpworkshopActivity { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyEngagementGoals> PrimaryGuardianFamilyEngagementGoals { get; set; }
        public virtual DbSet<PrimaryGuardianFamilySof> PrimaryGuardianFamilySof { get; set; }
        public virtual DbSet<PrimaryGuardianFamilyStrengthsAssessment> PrimaryGuardianFamilyStrengthsAssessment { get; set; }
        public virtual DbSet<PrimaryGuardianGoal> PrimaryGuardianGoal { get; set; }
        public virtual DbSet<PrimaryGuardianGoalFollowUp> PrimaryGuardianGoalFollowUp { get; set; }
        public virtual DbSet<PrimaryGuardianPireducationOrTraining> PrimaryGuardianPireducationOrTraining { get; set; }
        public virtual DbSet<PrimaryGuardianPirfamilyInformation> PrimaryGuardianPirfamilyInformation { get; set; }
        public virtual DbSet<PrimaryLanguageMappingPirlanguage> PrimaryLanguageMappingPirlanguage { get; set; }
        public virtual DbSet<Program> Program { get; set; }
        public virtual DbSet<ProgramPir> ProgramPir { get; set; }
        public virtual DbSet<ProgramPriority> ProgramPriority { get; set; }
        public virtual DbSet<ProgramType> ProgramType { get; set; }
        public virtual DbSet<ProgramYear> ProgramYear { get; set; }
        public virtual DbSet<Rating> Rating { get; set; }
        public virtual DbSet<RecentUpdate> RecentUpdate { get; set; }
        public virtual DbSet<RoleGrantee> RoleGrantee { get; set; }
        public virtual DbSet<SchoolYear> SchoolYear { get; set; }
        public virtual DbSet<SelectionCriteria> SelectionCriteria { get; set; }
        public virtual DbSet<SelectionCriteriaCategory> SelectionCriteriaCategory { get; set; }
        public virtual DbSet<SelectionCriteriaCategoryItem> SelectionCriteriaCategoryItem { get; set; }
        public virtual DbSet<ShineInsightMigrationMapping> ShineInsightMigrationMapping { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<StatusCodeDescriptionMapping> StatusCodeDescriptionMapping { get; set; }
        public virtual DbSet<SummaryViewParticipantRecord> SummaryViewParticipantRecord { get; set; }
        public virtual DbSet<TableColumnValue> TableColumnValue { get; set; }
        public virtual DbSet<TbChildPluslErrorRecords> TbChildPluslErrorRecords { get; set; }
        public virtual DbSet<TblParticipantStatusesMapping> TblParticipantStatusesMapping { get; set; }
        public virtual DbSet<Themes> Themes { get; set; }
        public virtual DbSet<TimeStamp> TimeStamp { get; set; }
        public virtual DbSet<TraceLog> TraceLog { get; set; }
        public virtual DbSet<UserAdditionalTrainings> UserAdditionalTrainings { get; set; }
        public virtual DbSet<UserAlert> UserAlert { get; set; }
        public virtual DbSet<UserAnnualTrainings> UserAnnualTrainings { get; set; }
        public virtual DbSet<UserAppraisalStaffEvaluation> UserAppraisalStaffEvaluation { get; set; }
        public virtual DbSet<UserCenter> UserCenter { get; set; }
        public virtual DbSet<UserChecksandClearances> UserChecksandClearances { get; set; }
        public virtual DbSet<UserClassroom> UserClassroom { get; set; }
        public virtual DbSet<UserClassScore> UserClassScore { get; set; }
        public virtual DbSet<UserCoaching> UserCoaching { get; set; }
        public virtual DbSet<UserDashboard> UserDashboard { get; set; }
        public virtual DbSet<UserEmergencyContacts> UserEmergencyContacts { get; set; }
        public virtual DbSet<UserEmployeePhoneNumbers> UserEmployeePhoneNumbers { get; set; }
        public virtual DbSet<UserHireInformation> UserHireInformation { get; set; }
        public virtual DbSet<UserImmunizations> UserImmunizations { get; set; }
        public virtual DbSet<UserJobDescriptions> UserJobDescriptions { get; set; }
        public virtual DbSet<UserLanguageProficiency> UserLanguageProficiency { get; set; }
        public virtual DbSet<UserLoginLog> UserLoginLog { get; set; }
        public virtual DbSet<UserPhysical> UserPhysical { get; set; }
        public virtual DbSet<UserPirposition> UserPirposition { get; set; }
        public virtual DbSet<UserProfessional> UserProfessional { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserProfileHistory> UserProfileHistory { get; set; }
        public virtual DbSet<UserRace> UserRace { get; set; }
        public virtual DbSet<UserSuccessRubrics> UserSuccessRubrics { get; set; }
        public virtual DbSet<UserTab> UserTab { get; set; }
        public virtual DbSet<WebpagesMembership> WebpagesMembership { get; set; }
        public virtual DbSet<WebpagesOauthMembership> WebpagesOauthMembership { get; set; }
        public virtual DbSet<WebpagesRoles> WebpagesRoles { get; set; }
        public virtual DbSet<WebpagesUsersInRoles> WebpagesUsersInRoles { get; set; }

        // Unable to generate entity type for table 'dbo.ConcernFollowUpActionStatus'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WhoIsActive_Logs'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DeleteDupParticipant_Jcps'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TempEnrollmentUpdate'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRaceConfiguration'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PovertyBase'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.profiledelet'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantHealthScreeningDocumentation_Backup_'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DMHSUMMARYASSOCIATEDCONCERNSHINE_3665'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DataForELS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DMHSUMMARYNEXTREQUIREACTIONSHINE_3665'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_C#NightlyCalendarJob'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AttendanceMissmatchClassroom'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BMIData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.EducationExamScreeningTypeMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELSDataImport'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DMHSUMMARYPRIMARYRESULTSHINE_3665'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.JCPS_Migration_Log'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.EducationScreeningResultsStatusMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthExamScreeningTypeMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_HealthConcernFUAction'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthExamScreeningTypeMapping_Old'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FileAttachment_Move_BackUp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthScreeningResultsStatusMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ARVAC_User1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ClassroomDataGCAA'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.RolloverWaitlist'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ClassroomGCCSAImport'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordHealthScreeningsOnTimeStatus'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ImmunizationTypeMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.RolloverParticipantRecordList'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.concernfollowupactionmigrate'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblStanfordParentSurveyPIRdataErrorLog1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BBCFEmergencyContactRaw'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblStanfordParentSurveyPIRdataErrorLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.BBCFParticipantEmergencyContactValidation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMP_REPORT'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantEducationScreeningDocumentation_back'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantPrimeTime'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantParentGuardianPrimeTime'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExternalDataAndShineInsightMapping'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Alert_MasterData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantDeleteTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthConcern_Alert_Master'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ToBeMigrateIEPActionPlanTemp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordDeleteTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantHealthScreeningDocumentationTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantHealthScreeningPlanningTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianCaseNoteTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantInternalReferralDMHSummary'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.RolloverDeletetrackingTable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_Dashboard_Results'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianFamilyEngagementAssociatedPIRServicesTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianFamilyEngagementTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_ChildPIRJob'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.participantHealthScreeningDocumentationForSP'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordClassroomProgramYearTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantHealthScreeningReportProcResult'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Files'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordEligibilityTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TempHealthLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordHealthInfoTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordHealthPlanningTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblLongRunningQueries'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DataImportActiveTrack'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordHealthPlanningHistoryTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordImmunizationSetStatusTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordSelectionCriteriaCategoryItemTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianPIREducationOrTrainingTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianPIRFamilyInformationTrackingOutcome'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TrackingLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantRecordEducationScreeningsOnTimeStatus'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianCaseNoteReplica'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianCaseNoteFollowUpReplica'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.OFA_participantFileAttachment'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UsersAlerts_Sheet1'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UsersAlerts_Sheet2'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DataImportTrack'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CenterAndClassRoomMappingForFG'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UserAccess'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthScreeningGroup'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Weekdate'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UserBulkUpdatePermission'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SUP902_UserAlertsData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DoesNotAffectMRAT'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.StanfordPIRData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TotalRecordsForNPS'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblStanfordParentSurveyPIRFamilydataErrorLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TempNotMatchedRecord'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.MatchHVDateRecords'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimaryGuardianPIR'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PrimeTimeEnrollData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UsersList'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblLegacyNameConfiguration'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELS_JOB_1_DifferntCenterProgramyearTimestamp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELS_JOB_2_Programs_Without_Priority'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELS_JOB_3_AssessmentDoc_Without_Strand'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELS_JOB_4_DuplicateEvaluation_forSame_EvaluationDoc'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.MapEnrollmentUpdatetypeNEnrollmentStatus'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ELS_JOB_5_PECPY_Timestamp_Transferred_Participants'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ExceptionLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CSJErrors'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.HealthLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CSJ_DATA_123'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbldataErrorLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblTempHealthScreeningDuplicatesData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TEMPSOURCE'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_DashboardCalendarJob'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParentGuardianOtherLanguagesSpokenOrRead'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblTempEducationScreeningDuplicatesData'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.IncomeAnnualFactors'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_DashboardMetricJob'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tblBenchMarkDatesConfiguration'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_CaseNote'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_Family'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_HealthConcern'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_HealthConcernFollowUp'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tempPrimaryGuardianFamilyEngagement'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AttendanceLogDetails'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.LogsTableMar2018'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Staging_Participant'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Log_DashboardAlertJob'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AttendanceClassroomMissMatchLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ParticipantDataMigrationHistoryLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ChecksumComparision'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AuditTable'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UpdatePRs'. Please see the warning messages.


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
                IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(projectPath)
           .AddJsonFile("appsettings.json")
           .Build();
                string connectionString = configuration.GetConnectionString("DefaultConnection");
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(connectionString);
            
        }
    }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OfaParticipantFileAttachment>().Ignore(c => c.Base64String);

            modelBuilder.Entity<AbsentAttendance>(entity =>
            {
                entity.Property(e => e.AbsentAttendanceId).HasColumnName("AbsentAttendanceID");

                entity.Property(e => e.AttendanceDate).HasColumnType("date");

                entity.Property(e => e.AttendanceStatus).HasMaxLength(50);

                entity.Property(e => e.CenterProgramYear).HasMaxLength(250);

                entity.Property(e => e.ClassroomProgramYear).HasMaxLength(250);

                entity.Property(e => e.LastDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.ParticipantName).HasMaxLength(500);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.StartDate).HasColumnType("date");
            });

            modelBuilder.Entity<ActivityCard>(entity =>
            {
                entity.Property(e => e.CardDescription)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.FamilyPracticeArea)
                    .WithMany(p => p.ActivityCard)
                    .HasForeignKey(d => d.FamilyPracticeAreaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActivityCard_FamilyPracticeArea");
            });

            modelBuilder.Entity<Alert>(entity =>
            {
                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Link).HasMaxLength(1000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<AlertGroup>(entity =>
            {
                entity.Property(e => e.AlertGroupId).HasColumnName("AlertGroupID");

                entity.Property(e => e.AlertGroup1).HasColumnName("AlertGroup");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertGroup)
                    .HasForeignKey(d => d.AlertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlertGroup_Alert");
            });

            modelBuilder.Entity<AlertProgramType>(entity =>
            {
                entity.Property(e => e.AlertProgramTypeId).HasColumnName("AlertProgramTypeID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.AlertProgramType)
                    .HasForeignKey(d => d.AlertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlertProgramType_Alert");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.AlertProgramType)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlertProgramType_ProgramType");
            });

            modelBuilder.Entity<AssessmentDocument>(entity =>
            {
                entity.HasKey(e => e.AssessmentDocId);

                entity.Property(e => e.AnecDote)
                    .IsRequired()
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.FileAttachmentId).HasColumnName("FileAttachmentID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<AssessmentDocumentTag>(entity =>
            {
                entity.HasIndex(e => new { e.CategoryTypeId, e.Description, e.AssessmentDocId })
                    .HasName("_dta_index_AssessmentDocumentTag_5_1001106657__K2_3_4");

                entity.Property(e => e.AssessmentDocumentTagId).HasColumnName("AssessmentDocumentTagID");

                entity.Property(e => e.Description).HasColumnType("nvarchar(max)");

                entity.HasOne(d => d.AssessmentDoc)
                    .WithMany(p => p.AssessmentDocumentTag)
                    .HasForeignKey(d => d.AssessmentDocId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AssessmentDocumentTag_AssessmentDocument");
            });

            modelBuilder.Entity<AssessmentLevel>(entity =>
            {
                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.AssessmentType)
                    .WithMany(p => p.AssessmentLevel)
                    .HasForeignKey(d => d.AssessmentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("AssessmentLevel_AssessmentType");
            });

            modelBuilder.Entity<AssessmentType>(entity =>
            {
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Calendar>(entity =>
            {
                entity.HasIndex(e => new { e.ItemName, e.ParticipantId })
                    .HasName("IX_Calendar_ParticipantID");

                entity.HasIndex(e => new { e.CalendarId, e.CreatedOn, e.ItemName, e.LastModified, e.ParticipantId, e.Date })
                    .HasName("IX_Calendar_Date");

                entity.Property(e => e.CalendarId).HasColumnName("CalendarID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.ItemName).HasColumnType("nvarchar(max)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.Calendar)
                    .HasForeignKey(d => d.AlertId)
                    .HasConstraintName("FK__Calendar__AlertI__6B7B1F28");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.Calendar)
                    .HasForeignKey(d => d.ParticipantId)
                    .HasConstraintName("FK_Calendar_Participant");
            });

            modelBuilder.Entity<CalendarItem>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");
            });

            modelBuilder.Entity<CaseLoadUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.GranteeId)
                    .HasColumnName("GranteeID")
                    .HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.Pirposition).HasColumnName("PIRPosition");

                entity.Property(e => e.PositionDescription).HasMaxLength(100);

                entity.Property(e => e.RoleId).HasMaxLength(100);

                entity.Property(e => e.State).HasMaxLength(50);

                entity.Property(e => e.StreetAddress).HasMaxLength(100);

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(56);

                entity.Property(e => e.ZipCode).HasMaxLength(20);
            });

            modelBuilder.Entity<Center>(entity =>
            {
                entity.HasIndex(e => new { e.ProgramId, e.Name })
                    .HasName("Center_ProgramID_Name");

                entity.Property(e => e.CenterId).HasColumnName("CenterID");

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.County).HasMaxLength(50);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Director).HasMaxLength(50);

                entity.Property(e => e.Extension).HasMaxLength(10);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LicenseExpiration).HasColumnType("date");

                entity.Property(e => e.LicenseNumber).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.Zip).HasMaxLength(50);

                entity.HasOne(d => d.Program)
                    .WithMany(p => p.Center)
                    .HasForeignKey(d => d.ProgramId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Center_Program");
            });

            modelBuilder.Entity<CenterProgramYear>(entity =>
            {
                entity.HasIndex(e => new { e.CenterId, e.ProgramYearId })
                    .HasName("IX_CenterProgramYear")
                    .IsUnique();

                entity.HasIndex(e => new { e.CenterProgramYearId, e.ProgramYearId })
                    .HasName("_dta_index_CenterProgramYear_5_1090102924__K1_K3");

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.CenterId).HasColumnName("CenterID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DmhsetId).HasColumnName("DMHSetID");

                entity.Property(e => e.EducationScreeningSetId).HasColumnName("EducationScreeningSetID");

                entity.Property(e => e.GranteeHealthScreeningSetId).HasColumnName("GranteeHealthScreeningSetID");

                entity.Property(e => e.IsPpvtfieldsActive).HasColumnName("IsPPVTFieldsActive");

                entity.Property(e => e.Iscopied)
                    .HasColumnName("ISCopied")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.SchoolDistrictCutoffDate).HasColumnType("date");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.CenterProgramYear)
                    .HasForeignKey(d => d.CenterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CenterProgramYear_Center");

                entity.HasOne(d => d.EducationScreeningSet)
                    .WithMany(p => p.CenterProgramYear)
                    .HasForeignKey(d => d.EducationScreeningSetId)
                    .HasConstraintName("FK_CenterProgramYear_EducationScreeningSet");

                entity.HasOne(d => d.GranteeHealthScreeningSet)
                    .WithMany(p => p.CenterProgramYear)
                    .HasForeignKey(d => d.GranteeHealthScreeningSetId)
                    .HasConstraintName("FK_CenterProgramYear_HealthScreeningSet");

                entity.HasOne(d => d.ProgramYear)
                    .WithMany(p => p.CenterProgramYear)
                    .HasForeignKey(d => d.ProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CenterProgramYear_ProgramYear");
            });

            modelBuilder.Entity<Classroom>(entity =>
            {
                entity.Property(e => e.ClassroomId).HasColumnName("ClassroomID");

                entity.Property(e => e.CenterId).HasColumnName("CenterID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.Classroom)
                    .HasForeignKey(d => d.CenterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Classroom_Center");
            });

            modelBuilder.Entity<ClassroomAdultMeal>(entity =>
            {
                entity.HasIndex(e => new { e.ClassroomProgramYearId, e.Date })
                    .HasName("UQ_ClassroomProgramYearID_Date")
                    .IsUnique();

                entity.Property(e => e.ClassroomAdultMealId).HasColumnName("ClassroomAdultMealID");

                entity.Property(e => e.Amsnack).HasColumnName("AMSnack");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Pmsnack).HasColumnName("PMSnack");
            });

            modelBuilder.Entity<ClassroomProgramYear>(entity =>
            {
                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.Amsnack).HasColumnName("AMSnack");

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.ClassroomId).HasColumnName("ClassroomID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Iscopied)
                    .HasColumnName("ISCopied")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Pmsnack).HasColumnName("PMSnack");

                entity.Property(e => e.ProgramOption).HasMaxLength(100);

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.TimeClosed).HasColumnType("time(0)");

                entity.Property(e => e.TimeOpen).HasColumnType("time(0)");

                entity.HasOne(d => d.CenterProgramYear)
                    .WithMany(p => p.ClassroomProgramYear)
                    .HasForeignKey(d => d.CenterProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassroomProgramYear_CenterProgramYear");

                entity.HasOne(d => d.Classroom)
                    .WithMany(p => p.ClassroomProgramYear)
                    .HasForeignKey(d => d.ClassroomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassroomProgramYear_Classroom");

                entity.HasOne(d => d.ProgramYear)
                    .WithMany(p => p.ClassroomProgramYear)
                    .HasForeignKey(d => d.ProgramYearId)
                    .HasConstraintName("FK_ClassroomProgramYear_ProgramYear");
            });

            modelBuilder.Entity<ClassroomProgramYearClosed>(entity =>
            {
                entity.HasIndex(e => new { e.ClassroomProgramYearClosedId, e.ClassroomProgramYearId, e.DateClosed })
                    .HasName("_dta_index_ClassroomProgramYearClosed_5_1280723615__K2_K3_1");

                entity.Property(e => e.ClassroomProgramYearClosedId).HasColumnName("ClassroomProgramYearClosedID");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.DateClosed).HasColumnType("date");

                entity.HasOne(d => d.ClassroomProgramYear)
                    .WithMany(p => p.ClassroomProgramYearClosed)
                    .HasForeignKey(d => d.ClassroomProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClassroomProgramYearClosed_ClassroomProgramYear");
            });

            modelBuilder.Entity<ClassroomProgramYearOpen>(entity =>
            {
                entity.Property(e => e.ClassroomProgramYearOpenId).HasColumnName("ClassroomProgramYearOpenID");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.DateOpen).HasColumnType("date");

                entity.HasOne(d => d.ClassroomProgramYear)
                    .WithMany(p => p.ClassroomProgramYearOpen)
                    .HasForeignKey(d => d.ClassroomProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Classroom__Class__66760F55");
            });

            modelBuilder.Entity<ClassroonProgramYearTeacher>(entity =>
            {
                entity.Property(e => e.ClassroonProgramYearTeacherId).HasColumnName("ClassroonProgramYearTeacherID");

                entity.Property(e => e.ClassroomId).HasColumnName("ClassroomID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateAdded).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateRemoved).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.TeacherType).HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<DashboardItemMetrics>(entity =>
            {
                entity.HasKey(e => e.DashboardItemMetricId);

                entity.HasIndex(e => e.MetricId)
                    .HasName("IX_DashboardItemMetrics");

                entity.HasIndex(e => new { e.DashboardCategory, e.DashboardSubCategory })
                    .HasName("IX_DashboardItemMetrics_Category_SubCategory");

                entity.Property(e => e.DashboardItemMetricId).HasColumnName("DashboardItemMetricID");

                entity.Property(e => e.DashboardCategory)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DashboardSubCategory)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DashboardItems>(entity =>
            {
                entity.HasKey(e => e.DashboardItemId);

                entity.HasIndex(e => e.ItemName);

                entity.HasIndex(e => e.Name)
                    .HasName("IX_DashboardItems");

                entity.HasIndex(e => new { e.Name, e.ItemName });

                entity.Property(e => e.DashboardItemId).HasColumnName("DashboardItemID");

                entity.Property(e => e.DashboardId).HasColumnName("DashboardID");

                entity.Property(e => e.ItemName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DataList>(entity =>
            {
                entity.Property(e => e.DataListId).HasColumnName("DataListID");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DataListName).HasMaxLength(100);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModifiedBy).HasMaxLength(100);
            });

            modelBuilder.Entity<DataListAccess>(entity =>
            {
                entity.HasKey(e => e.DataListValueId);

                entity.Property(e => e.DataListValueId).HasColumnName("DataListValueID");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DataListId).HasColumnName("DataListID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModifiedBy).HasMaxLength(100);

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.ValueId).HasColumnName("ValueID");

                entity.HasOne(d => d.Value)
                    .WithMany(p => p.DataListAccess)
                    .HasForeignKey(d => d.ValueId)
                    .HasConstraintName("FK_DataListAccess_TableColumnValue");
            });

            modelBuilder.Entity<Dmhgoal>(entity =>
            {
                entity.ToTable("DMHGoal");

                entity.Property(e => e.DmhgoalId).HasColumnName("DMHGoalId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DmhProcessDocumentationId).HasColumnName("DMhProcessDocumentationId");

                entity.Property(e => e.DmhplanId).HasColumnName("DMHPlanId");

                entity.Property(e => e.GoalDescription).HasMaxLength(4000);

                entity.Property(e => e.GoalEndDate).HasColumnType("datetime");

                entity.Property(e => e.GoalStartDate).HasColumnType("datetime");

                entity.Property(e => e.GoalUpdatedate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<Dmhset>(entity =>
            {
                entity.ToTable("DMHSet");

                entity.Property(e => e.DmhsetId).HasColumnName("DMHSetID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.Iepifspdue).HasColumnName("IEPIFSPDue");

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DmhsummaryAssociatedConcern>(entity =>
            {
                entity.ToTable("DMHSummaryAssociatedConcern");

                entity.HasIndex(e => new { e.DmhsummaryAssociatedConcernId, e.DmhsummaryPrimaryResultId })
                    .HasName("IX_DMHSummaryAssociatedConcern_DMHSummaryPrimaryResultId_Covering");

                entity.Property(e => e.DmhsummaryAssociatedConcernId).HasColumnName("DMHSummaryAssociatedConcernId");

                entity.Property(e => e.DmhsummaryPrimaryResultId).HasColumnName("DMHSummaryPrimaryResultId");

                entity.HasOne(d => d.DmhsummaryPrimaryResult)
                    .WithMany(p => p.DmhsummaryAssociatedConcern)
                    .HasForeignKey(d => d.DmhsummaryPrimaryResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DMHSUMMARYASSOCIATEDCONCERN_DMHSUMMARYPRIMARYRESULT");
            });

            modelBuilder.Entity<DmhsummaryNextRequireAction>(entity =>
            {
                entity.ToTable("DMHSummaryNextRequireAction");

                entity.HasIndex(e => new { e.DmhsummaryNextRequireActionId, e.DmhsummaryPrimaryResultId })
                    .HasName("IX_DMHSummaryNextRequireAction_DMHSummaryPrimaryResultId_Covering");

                entity.Property(e => e.DmhsummaryNextRequireActionId).HasColumnName("DMHSummaryNextRequireActionId");

                entity.Property(e => e.DmhsummaryPrimaryResultId).HasColumnName("DMHSummaryPrimaryResultId");

                entity.Property(e => e.NextRequireActionDate).HasColumnType("date");

                entity.Property(e => e.ProsNoteId).HasColumnName("PROS_NoteID");

                entity.HasOne(d => d.DmhsummaryPrimaryResult)
                    .WithMany(p => p.DmhsummaryNextRequireAction)
                    .HasForeignKey(d => d.DmhsummaryPrimaryResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DMHSUMMARYNEXTREQUIREACTION_DMHSUMMARYPRIMARYRESULT");
            });

            modelBuilder.Entity<DmhsummaryPrimaryResult>(entity =>
            {
                entity.ToTable("DMHSummaryPrimaryResult");

                entity.HasIndex(e => e.ParticipantId);

                entity.Property(e => e.DmhsummaryPrimaryResultId).HasColumnName("DMHSummaryPrimaryResultId");

                entity.Property(e => e.MostRecentActionDate).HasColumnType("date");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<DmhsummaryPriority>(entity =>
            {
                entity.ToTable("DMHSummaryPriority");

                entity.Property(e => e.DmhsummaryPriorityId)
                    .HasColumnName("DMHSummaryPriorityId")
                    .ValueGeneratedNever();

                entity.Property(e => e.DuedateInitial)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NextRequireDueDateDays).HasMaxLength(1000);

                entity.Property(e => e.NextRequireDueDateFeild).HasMaxLength(1000);
            });

            modelBuilder.Entity<EducationScreening>(entity =>
            {
                entity.Property(e => e.EducationScreeningId).HasColumnName("EducationScreeningID");

                entity.Property(e => e.EducationScreeningSetId).HasColumnName("EducationScreeningSetID");

                entity.Property(e => e.IsOnPir).HasColumnName("IsOnPIR");

                entity.HasOne(d => d.EducationScreeningSet)
                    .WithMany(p => p.EducationScreening)
                    .HasForeignKey(d => d.EducationScreeningSetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationScreening_EducationScreeningSet");
            });

            modelBuilder.Entity<EducationScreeningPirquestion>(entity =>
            {
                entity.ToTable("EducationScreeningPIRQuestion");

                entity.Property(e => e.EducationScreeningPirquestionId).HasColumnName("EducationScreeningPIRQuestionID");

                entity.Property(e => e.EducationScreeningId).HasColumnName("EducationScreeningID");

                entity.Property(e => e.PirquestionId).HasColumnName("PIRQuestionID");

                entity.HasOne(d => d.EducationScreening)
                    .WithMany(p => p.EducationScreeningPirquestion)
                    .HasForeignKey(d => d.EducationScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationScreeningPIRQuestion_EducationScreening");
            });

            modelBuilder.Entity<EducationScreeningSet>(entity =>
            {
                entity.Property(e => e.EducationScreeningSetId).HasColumnName("EducationScreeningSetID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.EducationScreeningSet)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EducationScreeningSet_Grantee");
            });

            modelBuilder.Entity<EducationStatusCodeDescriptionMapping>(entity =>
            {
                entity.Property(e => e.ExemptionDate).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(100);

                entity.Property(e => e.RefusalDate).HasMaxLength(100);

                entity.Property(e => e.ScreeningResultsStatus).HasMaxLength(100);

                entity.Property(e => e.StatusCodeDescription).HasMaxLength(100);
            });

            modelBuilder.Entity<ElmahError>(entity =>
            {
                entity.HasKey(e => e.ErrorId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("ELMAH_Error");

                entity.HasIndex(e => new { e.Application, e.TimeUtc, e.Sequence })
                    .HasName("IX_ELMAH_Error_App_Time_Seq");

                entity.Property(e => e.ErrorId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AllXml)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.Application)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Sequence).ValueGeneratedOnAdd();

                entity.Property(e => e.Source)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.TimeUtc).HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EmploymentStatusMigrationMapping>(entity =>
            {
                entity.Property(e => e.DestinationName).HasMaxLength(100);

                entity.Property(e => e.FieldName).HasMaxLength(100);

                entity.Property(e => e.GranteeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobTraining).HasMaxLength(10);

                entity.Property(e => e.SourceName).HasMaxLength(100);
            });

            modelBuilder.Entity<Evaluation>(entity =>
            {
                entity.HasIndex(e => new { e.EvaluationId, e.Score, e.AssessmentLevelId, e.ParticipantEvaluationCenterprogramyearId })
                    .HasName("IX_Evaluation_ParticipantEvaluationCenterprogramyear");

                entity.HasIndex(e => new { e.LevelId, e.Score, e.CreatedBy, e.CreatedOn, e.ModifiedBy, e.ModifiedOn, e.AssessmentLevelId, e.Reason, e.ParticipantEvaluationCenterprogramyearId, e.EvaluationId })
                    .HasName("_dta_index_Evaluation_5_697105574__K8_K1_2_3_4_5_6_7_9_10");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParticipantEvaluationCenterprogramyearId).HasColumnName("ParticipantEvaluationCenterprogramyearID");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.HasOne(d => d.AssessmentLevel)
                    .WithMany(p => p.Evaluation)
                    .HasForeignKey(d => d.AssessmentLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Evaluation_AssessmentLevel");

                entity.HasOne(d => d.ParticipantEvaluationCenterprogramyear)
                    .WithMany(p => p.Evaluation)
                    .HasForeignKey(d => d.ParticipantEvaluationCenterprogramyearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Evaluation_ParticipantEvaluationCenterprogramyear");
            });

            modelBuilder.Entity<EvaluationDocument>(entity =>
            {
                entity.HasKey(e => e.EvaluationDocId);

                entity.HasIndex(e => new { e.EvaluationDocId, e.EvaluationId, e.AssessmentDocumentId })
                    .HasName("_dta_index_EvaluationDocument_5_761105802__K2_K3_1");

                entity.HasOne(d => d.AssessmentDocument)
                    .WithMany(p => p.EvaluationDocument)
                    .HasForeignKey(d => d.AssessmentDocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("EvaluationDocument_AssessmentDocument");

                entity.HasOne(d => d.Evaluation)
                    .WithMany(p => p.EvaluationDocument)
                    .HasForeignKey(d => d.EvaluationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("EvaluationDocument_Evaluation");
            });

            modelBuilder.Entity<FamilyPracticeArea>(entity =>
            {
                entity.Property(e => e.FamilyPracticeAreaText)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<FileAttachment>(entity =>
            {
                entity.HasIndex(e => new { e.DisplayName, e.FilePath, e.OldFilePath, e.LevelType, e.LevelTypeIdentifier, e.FileAttachmentId, e.Type, e.SubType, e.SubTypeIdentifier, e.FileName, e.CreatorUserId, e.CreatedOn, e.LastModified, e.OldLevelType, e.OldFileName, e.OldLevelTypeIdentifier })
                    .HasName("IX_FileAttachment_LevelType");

                entity.Property(e => e.FileAttachmentId).HasColumnName("FileAttachmentID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("displayName")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.OldFileName).HasMaxLength(100);

                entity.Property(e => e.OldFilePath).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Sequence).HasColumnName("sequence");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.FileAttachment)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FileAttachment_UserProfile");
            });

            modelBuilder.Entity<FileAttachmentTemp>(entity =>
            {
                entity.ToTable("FileAttachment_Temp");

                entity.Property(e => e.FileAttachmentTempId).HasColumnName("FileAttachmentTempID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.FileAttachmentTemp)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FileAttachment_Temp_UserProfile");
            });

            modelBuilder.Entity<FileHistory>(entity =>
            {
                entity.Property(e => e.FileHistoryId).HasColumnName("FileHistoryID");

                entity.Property(e => e.DisplayName)
                    .HasColumnName("displayName")
                    .IsUnicode(false);

                entity.Property(e => e.FileAction).HasMaxLength(10);

                entity.Property(e => e.FileAttachmentId).HasColumnName("FileAttachmentID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.RecordType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FseissueCodeMapping>(entity =>
            {
                entity.ToTable("FSEIssueCodeMapping");

                entity.Property(e => e.DestinationGoal).HasMaxLength(100);

                entity.Property(e => e.GranteeName).HasMaxLength(100);

                entity.Property(e => e.SourceEventType).HasMaxLength(100);

                entity.Property(e => e.SourceissueCode).HasMaxLength(100);
            });

            modelBuilder.Entity<FundedEnrollmentOption>(entity =>
            {
                entity.Property(e => e.FundedEnrollmentOptionId)
                    .HasColumnName("FundedEnrollmentOptionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<Goal>(entity =>
            {
                entity.HasIndex(e => new { e.Objective3, e.Objective4, e.Objective5, e.Resource1ContactName, e.Resource1Organization, e.IsGoalActive, e.IsMasterGoal, e.IsMasterGoalActive, e.Name, e.Objective1, e.Objective2, e.Benchmark1Description, e.Benchmark2Description, e.Benchmark3Description, e.Benchmark4Description, e.Description, e.GoalId, e.GranteeId, e.GoalCategoryId, e.GoalSubCategoryId })
                    .HasName("IX_Goal_GranteeID");

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.Benchmark1Description).HasMaxLength(500);

                entity.Property(e => e.Benchmark2Description).HasMaxLength(500);

                entity.Property(e => e.Benchmark3Description).HasMaxLength(500);

                entity.Property(e => e.Benchmark4Description).HasMaxLength(500);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.FamilyEngagementOutcome1).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome2).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome3).HasMaxLength(100);

                entity.Property(e => e.GoalCategoryId).HasColumnName("GoalCategoryID");

                entity.Property(e => e.GoalSubCategoryId).HasColumnName("GoalSubCategoryID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Objective1).HasMaxLength(1000);

                entity.Property(e => e.Objective2).HasMaxLength(1000);

                entity.Property(e => e.Objective3).HasMaxLength(1000);

                entity.Property(e => e.Objective4).HasMaxLength(1000);

                entity.Property(e => e.Objective5).HasMaxLength(1000);

                entity.Property(e => e.PirservicesReceived1)
                    .HasColumnName("PIRServicesReceived1")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived2)
                    .HasColumnName("PIRServicesReceived2")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived3)
                    .HasColumnName("PIRServicesReceived3")
                    .HasMaxLength(100);

                entity.Property(e => e.Resource1Address).HasMaxLength(100);

                entity.Property(e => e.Resource1ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource1Organization).HasMaxLength(100);

                entity.Property(e => e.Resource1Phone).HasMaxLength(30);

                entity.Property(e => e.Resource2Address).HasMaxLength(100);

                entity.Property(e => e.Resource2ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource2Organization).HasMaxLength(100);

                entity.Property(e => e.Resource2Phone).HasMaxLength(30);

                entity.Property(e => e.Resource3Address).HasMaxLength(100);

                entity.Property(e => e.Resource3ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource3Organization).HasMaxLength(100);

                entity.Property(e => e.Resource3Phone).HasMaxLength(30);

                entity.Property(e => e.Resource4Address).HasMaxLength(100);

                entity.Property(e => e.Resource4ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource4Organization).HasMaxLength(100);

                entity.Property(e => e.Resource4Phone).HasMaxLength(30);

                entity.HasOne(d => d.GoalCategory)
                    .WithMany(p => p.Goal)
                    .HasForeignKey(d => d.GoalCategoryId)
                    .HasConstraintName("FK_Goal_GoalCategory");

                entity.HasOne(d => d.GoalSubCategory)
                    .WithMany(p => p.Goal)
                    .HasForeignKey(d => d.GoalSubCategoryId)
                    .HasConstraintName("FK_Goal_GoalSubCategory");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.Goal)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Goal_Grantee");
            });

            modelBuilder.Entity<GoalCategory>(entity =>
            {
                entity.Property(e => e.GoalCategoryId).HasColumnName("GoalCategoryID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<GoalSubCategory>(entity =>
            {
                entity.Property(e => e.GoalSubCategoryId).HasColumnName("GoalSubCategoryID");

                entity.Property(e => e.GoalCategoryId).HasColumnName("GoalCategoryID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.GoalCategory)
                    .WithMany(p => p.GoalSubCategory)
                    .HasForeignKey(d => d.GoalCategoryId)
                    .HasConstraintName("FK_GoalSubCategory_GoalCategory");
            });

            modelBuilder.Entity<Grantee>(entity =>
            {
                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.AgencyAffiliation).HasMaxLength(100);

                entity.Property(e => e.AgencyDescription).HasMaxLength(200);

                entity.Property(e => e.AgencyType).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Duns)
                    .HasColumnName("DUNS")
                    .HasMaxLength(50);

                entity.Property(e => e.Extension).HasMaxLength(10);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.GrantNumber).HasMaxLength(50);

                entity.Property(e => e.IsSof).HasColumnName("IsSOF");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.Website).HasMaxLength(100);
            });

            modelBuilder.Entity<GranteeAlert>(entity =>
            {
                entity.HasIndex(e => new { e.AlertId, e.GranteeId })
                    .HasName("IX_GranteeAlert");

                entity.Property(e => e.GranteeAlertId).HasColumnName("GranteeAlertID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.GranteeAlert)
                    .HasForeignKey(d => d.AlertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeAlert_Alert");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeAlert)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeAlert_Grantee");
            });

            modelBuilder.Entity<GranteeHealthScreening>(entity =>
            {
                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.HealthScreeningGroupId).HasColumnName("HealthScreeningGroupID");

                entity.Property(e => e.HealthScreeningId).HasColumnName("HealthScreeningID");

                entity.Property(e => e.IsOnPir).HasColumnName("IsOnPIR");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeHealthScreening)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreening_Grantee");

                entity.HasOne(d => d.HealthScreening)
                    .WithMany(p => p.GranteeHealthScreening)
                    .HasForeignKey(d => d.HealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreening_HealthScreening");
            });

            modelBuilder.Entity<GranteeHealthScreeningPirquestion>(entity =>
            {
                entity.ToTable("GranteeHealthScreeningPIRQuestion");

                entity.Property(e => e.GranteeHealthScreeningPirquestionId).HasColumnName("GranteeHealthScreeningPIRQuestionID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.PirquestionId).HasColumnName("PIRQuestionID");

                entity.HasOne(d => d.GranteeHealthScreening)
                    .WithMany(p => p.GranteeHealthScreeningPirquestion)
                    .HasForeignKey(d => d.GranteeHealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreeningPIRQuestion_GranteeHealthScreening");

                entity.HasOne(d => d.Pirquestion)
                    .WithMany(p => p.GranteeHealthScreeningPirquestion)
                    .HasForeignKey(d => d.PirquestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreeningPIRQuestion_PIRQuestion");
            });

            modelBuilder.Entity<GranteeHealthScreeningSet>(entity =>
            {
                entity.Property(e => e.GranteeHealthScreeningSetId).HasColumnName("GranteeHealthScreeningSetID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeHealthScreeningSet)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreeningSet_Grantee");
            });

            modelBuilder.Entity<GranteeHealthScreeningSetGranteeHealthScreening>(entity =>
            {
                entity.Property(e => e.GranteeHealthScreeningSetGranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningSetGranteeHealthScreeningID");

                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.GranteeHealthScreeningSetId).HasColumnName("GranteeHealthScreeningSetID");

                entity.HasOne(d => d.GranteeHealthScreening)
                    .WithMany(p => p.GranteeHealthScreeningSetGranteeHealthScreening)
                    .HasForeignKey(d => d.GranteeHealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreeningSetGranteeHealthScreening_GranteeHealthScreening");

                entity.HasOne(d => d.GranteeHealthScreeningSet)
                    .WithMany(p => p.GranteeHealthScreeningSetGranteeHealthScreening)
                    .HasForeignKey(d => d.GranteeHealthScreeningSetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeHealthScreeningSetGranteeHealthScreening_GranteeHealthScreeningSet");
            });

            modelBuilder.Entity<GranteeImmunization>(entity =>
            {
                entity.Property(e => e.GranteeImmunizationId).HasColumnName("GranteeImmunizationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.ImmunizationCdclink)
                    .HasColumnName("ImmunizationCDCLink")
                    .HasMaxLength(2000);

                entity.Property(e => e.ImmunizationId).HasColumnName("ImmunizationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeImmunization)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeImmunization_Grantee");

                entity.HasOne(d => d.Immunization)
                    .WithMany(p => p.GranteeImmunization)
                    .HasForeignKey(d => d.ImmunizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeImmunization_Immunization");
            });

            modelBuilder.Entity<GranteeSchoolYearDefault>(entity =>
            {
                entity.HasIndex(e => new { e.GranteeId, e.SchoolYearId })
                    .HasName("IX_GranteeSchoolYearDefault")
                    .IsUnique();

                entity.Property(e => e.GranteeSchoolYearDefaultId).HasColumnName("GranteeSchoolYearDefaultID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.SchoolYearId).HasColumnName("SchoolYearID");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeSchoolYearDefault)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeSchoolYearDefault_Grantee");

                entity.HasOne(d => d.SchoolYear)
                    .WithMany(p => p.GranteeSchoolYearDefault)
                    .HasForeignKey(d => d.SchoolYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeSchoolYearDefault_SchoolYear");
            });

            modelBuilder.Entity<GranteeTheme>(entity =>
            {
                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.GranteeTheme)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeTheme_Grantee");

                entity.HasOne(d => d.Theme)
                    .WithMany(p => p.GranteeTheme)
                    .HasForeignKey(d => d.ThemeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GranteeTheme_Themes");
            });

            modelBuilder.Entity<HealthAndEducationScreenMapping>(entity =>
            {
                entity.Property(e => e.DestinationEventType).HasMaxLength(100);

                entity.Property(e => e.DestinationGrantee).HasMaxLength(100);

                entity.Property(e => e.DestinationTool).HasMaxLength(100);

                entity.Property(e => e.EventName).HasMaxLength(50);

                entity.Property(e => e.SourceEventType).HasMaxLength(100);

                entity.Property(e => e.SourceGrantee).HasMaxLength(100);
            });

            modelBuilder.Entity<HealthRequirementsActionStatusOptionsOnCreateNewScreening>(entity =>
            {
                entity.Property(e => e.HealthRequirementsActionStatusOptionsOnCreateNewScreeningId).HasColumnName("HealthRequirementsActionStatusOptionsOnCreateNewScreeningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.HealthRequirementsActionStatusOptionsOnCreateNewScreening)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HealthRequirementsActionStatusOptionsOnCreateNewScreening_Grantee");
            });

            modelBuilder.Entity<HealthScreening>(entity =>
            {
                entity.Property(e => e.HealthScreeningId).HasColumnName("HealthScreeningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.HealthScreeningGroupId).HasColumnName("HealthScreeningGroupID");

                entity.Property(e => e.IsOnPir).HasColumnName("IsOnPIR");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<HealthScreeningPirquestion>(entity =>
            {
                entity.ToTable("HealthScreeningPIRQuestion");

                entity.Property(e => e.HealthScreeningPirquestionId).HasColumnName("HealthScreeningPIRQuestionID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.HealthScreeningId).HasColumnName("HealthScreeningID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.PirquestionId).HasColumnName("PIRQuestionID");

                entity.HasOne(d => d.HealthScreening)
                    .WithMany(p => p.HealthScreeningPirquestion)
                    .HasForeignKey(d => d.HealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HealthScreeningPIRQuestion_HealthScreening");

                entity.HasOne(d => d.Pirquestion)
                    .WithMany(p => p.HealthScreeningPirquestion)
                    .HasForeignKey(d => d.PirquestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HealthScreeningPIRQuestion_PIRQuestion");
            });

            modelBuilder.Entity<HealthScreeningSet>(entity =>
            {
                entity.Property(e => e.HealthScreeningSetId).HasColumnName("HealthScreeningSetID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<HealthScreeningSetHealthScreening>(entity =>
            {
                entity.Property(e => e.HealthScreeningSetHealthScreeningId).HasColumnName("HealthScreeningSetHealthScreeningID");

                entity.Property(e => e.HealthScreeningId).HasColumnName("HealthScreeningID");

                entity.Property(e => e.HealthScreeningSetId).HasColumnName("HealthScreeningSetID");

                entity.HasOne(d => d.HealthScreening)
                    .WithMany(p => p.HealthScreeningSetHealthScreening)
                    .HasForeignKey(d => d.HealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HealthScreeningSetHealthScreening_HealthScreening");

                entity.HasOne(d => d.HealthScreeningSet)
                    .WithMany(p => p.HealthScreeningSetHealthScreening)
                    .HasForeignKey(d => d.HealthScreeningSetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HealthScreeningSetHealthScreening_HealthScreeningSet");
            });

            modelBuilder.Entity<Immunization>(entity =>
            {
                entity.Property(e => e.ImmunizationId).HasColumnName("ImmunizationID");

                entity.Property(e => e.AlternateName).HasMaxLength(200);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ImmunizationSet>(entity =>
            {
                entity.Property(e => e.ImmunizationSetId).HasColumnName("ImmunizationSetID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.ImmunizationSet)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ImmunizationSet_Grantee");
            });

            modelBuilder.Entity<JobErrors>(entity =>
            {
                entity.Property(e => e.JobName).HasMaxLength(50);

                entity.Property(e => e.LoggedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<LevelMaster>(entity =>
            {
                entity.HasKey(e => e.LevelId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.HasOne(d => d.AssessmentType)
                    .WithMany(p => p.LevelMaster)
                    .HasForeignKey(d => d.AssessmentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LevelMaster_AssessmentType");
            });

            modelBuilder.Entity<MappingBetweenHealthInsurance>(entity =>
            {
                entity.Property(e => e.ColumnName).HasMaxLength(100);

                entity.Property(e => e.DesctionationHealthInsuranceEndofEnrollment).HasMaxLength(100);

                entity.Property(e => e.HealthInsuranceatTimeOfEnrollmentOther).HasMaxLength(100);

                entity.Property(e => e.SourceHealthInsuranceEnrollment).HasMaxLength(100);
            });

            modelBuilder.Entity<MasterGoalGrantee>(entity =>
            {
                entity.HasIndex(e => new { e.GoalId, e.GranteeId })
                    .HasName("IX_MasterGoalGrantee")
                    .IsUnique();

                entity.Property(e => e.MasterGoalGranteeId).HasColumnName("MasterGoalGranteeID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.HasOne(d => d.Goal)
                    .WithMany(p => p.MasterGoalGrantee)
                    .HasForeignKey(d => d.GoalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterGoalGrantee_Goal");

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.MasterGoalGrantee)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MasterGoalGrantee_Grantee");
            });

            modelBuilder.Entity<Metric>(entity =>
            {
                entity.Property(e => e.MetricId)
                    .HasColumnName("MetricID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<MetricProgramType>(entity =>
            {
                entity.Property(e => e.MetricProgramTypeId).HasColumnName("MetricProgramTypeID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.MetricId).HasColumnName("MetricID");

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.HasOne(d => d.Metric)
                    .WithMany(p => p.MetricProgramType)
                    .HasForeignKey(d => d.MetricId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MetricProgramType_Metric");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.MetricProgramType)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MetricProgramType_ProgramType");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => e.MigrationId);

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId)
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<NightlyJobLog>(entity =>
            {
                entity.HasKey(e => e.StepId);

                entity.Property(e => e.Jobname)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Stepname)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<OfaGranteeTableData>(entity =>
            {
                entity.HasKey(e => e.GranteeTableValueId);

                entity.ToTable("OFA_granteeTableData");

                entity.Property(e => e.GranteeTableValueId).HasColumnName("GranteeTableValueID");

                entity.Property(e => e.DashboardHeader).HasMaxLength(100);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LandingBackgroundPath).HasMaxLength(200);

                entity.Property(e => e.LandpageHeader).HasMaxLength(100);

                entity.Property(e => e.Landpagesubpoint1).HasMaxLength(200);

                entity.Property(e => e.Landpagesubpoint2).HasMaxLength(200);

                entity.Property(e => e.Landpagesubpoint3).HasMaxLength(200);

                entity.Property(e => e.LeftMenuHeader).HasMaxLength(100);

                entity.Property(e => e.LoginHeader).HasMaxLength(100);

                entity.Property(e => e.LogoPath).HasMaxLength(200);

                entity.Property(e => e.ParentRegisterHeader).HasMaxLength(100);

                entity.Property(e => e.RegisterHeader).HasMaxLength(100);

                entity.Property(e => e.SignInHeader).HasMaxLength(100);

                entity.Property(e => e.Urlpath)
                    .HasColumnName("URLpath")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<OfaNotification>(entity =>
            {
                entity.HasKey(e => e.NotificationId);

                entity.ToTable("OFA_Notification");

                entity.Property(e => e.NotificationId).HasColumnName("NotificationID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<OfaParentPhoneNumber>(entity =>
            {
                entity.HasKey(e => e.ParentPhoneNumberId);

                entity.ToTable("OFA_parentPhoneNumber");

                entity.Property(e => e.ParentPhoneNumberId).HasColumnName("ParentPhoneNumberID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.OfaParentPhoneNumber)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_paren__Paren__2B822C52");
            });

            modelBuilder.Entity<OfaParentRegistration>(entity =>
            {
                entity.HasKey(e => e.ParentId);

                entity.ToTable("OFA_parentRegistration");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateofBirth).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordHash).IsRequired();

                entity.Property(e => e.PasswordSalt).IsRequired();

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(56);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.OfaParentRegistration)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_paren__Grant__28A5BFA7");
            });

            modelBuilder.Entity<OfaParticipant>(entity =>
            {
                entity.ToTable("OFA_participant");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.ChildDisabilityDesc).HasMaxLength(500);

                entity.Property(e => e.ChildDisabilityList).HasMaxLength(500);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateofBirth).HasColumnType("date");

                entity.Property(e => e.FirstCenterChoice).HasMaxLength(500);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FullDayChildCareReason).HasMaxLength(500);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.HowDidyouHearabtProgOtherSource).HasMaxLength(500);

                entity.Property(e => e.IsChildIepifsp).HasColumnName("IsChildIEPIFSP");

                entity.Property(e => e.IsChildMotherFatherAttendEhehsprogram).HasColumnName("IsChildMotherFatherAttendEHEHSProgram");

                entity.Property(e => e.IsChildSiblingEnrolledinHsprogram).HasColumnName("IsChildSiblingEnrolledinHSProgram");

                entity.Property(e => e.IsDyfsdcfsdhs).HasColumnName("IsDYFSDCFSDHS");

                entity.Property(e => e.IsSsi).HasColumnName("IsSSI");

                entity.Property(e => e.IsTnaf).HasColumnName("IsTNAF");

                entity.Property(e => e.IsdisplacedHomedue).HasColumnName("ISDisplacedHomedue");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LegalIssuesClarify).HasMaxLength(500);

                entity.Property(e => e.Nationality).HasMaxLength(50);

                entity.Property(e => e.OtherBehaviorConcern).HasMaxLength(500);

                entity.Property(e => e.OtherDevelopmentConcern).HasMaxLength(500);

                entity.Property(e => e.OtherMedicalConcern).HasMaxLength(500);

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.PickupExplaination).HasMaxLength(500);

                entity.Property(e => e.PickupPersonName).HasMaxLength(50);

                entity.Property(e => e.PickupPersonRelationship).HasMaxLength(50);

                entity.Property(e => e.Preferred).HasMaxLength(100);

                entity.Property(e => e.PreviousEnrolledProgram).HasMaxLength(500);

                entity.Property(e => e.RaceOther).HasMaxLength(200);

                entity.Property(e => e.RestrictExplaination).HasMaxLength(500);

                entity.Property(e => e.RestrictPersonName).HasMaxLength(50);

                entity.Property(e => e.RestrictPersonRelationship).HasMaxLength(50);

                entity.Property(e => e.SecondCenterChoice).HasMaxLength(500);

                entity.Property(e => e.SiblingEnrolledEndDate).HasColumnType("datetime");

                entity.Property(e => e.SiblingEnrolledStartDate).HasColumnType("datetime");

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.ThirdCenterChoice).HasMaxLength(500);

                entity.Property(e => e.ZipCode).HasMaxLength(50);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.OfaParticipant)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__Grant__3046E16F");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.OfaParticipant)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__Paren__2E5E98FD");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.OfaParticipant)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__State__2F52BD36");
            });

            modelBuilder.Entity<OfaParticipantAdditionalMember>(entity =>
            {
                entity.HasKey(e => e.ParticipantAdditionalMemberId);

                entity.ToTable("OFA_participantAdditionalMember");

                entity.Property(e => e.ParticipantAdditionalMemberId).HasColumnName("ParticipantAdditionalMemberID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateofBirth).HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.Property(e => e.RealtionshiptoApplicant).HasMaxLength(50);

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantAdditionalMember)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__3E9500C6");
            });

            modelBuilder.Entity<OfaParticipantConcern>(entity =>
            {
                entity.HasKey(e => e.ParticipantConcernId);

                entity.ToTable("OFA_participantConcern");

                entity.Property(e => e.ParticipantConcernId).HasColumnName("ParticipantConcernID");

                entity.Property(e => e.ConcernId).HasColumnName("ConcernID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantConcern)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__35FFBAC5");
            });

            modelBuilder.Entity<OfaParticipantEmergencyContact>(entity =>
            {
                entity.HasKey(e => e.ParticipantEmergencyContactId);

                entity.ToTable("OFA_participantEmergencyContact");

                entity.Property(e => e.ParticipantEmergencyContactId).HasColumnName("ParticipantEmergencyContactID");

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.Property(e => e.PhoneNumber1).HasMaxLength(20);

                entity.Property(e => e.PhoneNumber1PhoneTypeId).HasColumnName("PhoneNumber1PhoneTypeID");

                entity.Property(e => e.PhoneNumber2).HasMaxLength(20);

                entity.Property(e => e.PhoneNumber2PhoneTypeId).HasColumnName("PhoneNumber2PhoneTypeID");

                entity.Property(e => e.RelationshiptoChild).HasMaxLength(50);

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.ZipCode).HasMaxLength(20);

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantEmergencyContact)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__444DDA1C");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.OfaParticipantEmergencyContact)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK__OFA_parti__State__4541FE55");
            });

            modelBuilder.Entity<OfaParticipantFamilyServices>(entity =>
            {
                entity.HasKey(e => e.ParticipantFamilyServicesId);

                entity.ToTable("OFA_participantFamilyServices");

                entity.Property(e => e.ParticipantFamilyServicesId).HasColumnName("ParticipantFamilyServicesID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FamilyServiceId).HasColumnName("FamilyServiceID");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantFamilyServices)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__41716D71");
            });

            modelBuilder.Entity<OfaParticipantHowDidyouHearabtProgram>(entity =>
            {
                entity.HasKey(e => e.ParticipantHowDidyouHearabtProgramId);

                entity.ToTable("OFA_participantHowDidyouHearabtProgram");

                entity.Property(e => e.ParticipantHowDidyouHearabtProgramId).HasColumnName("ParticipantHowDidyouHearabtProgramID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HowDidyouHearId).HasColumnName("HowDidyouHearID");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantHowDidyouHearabtProgram)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__38DC2770");
            });

            modelBuilder.Entity<OfaParticipantNotification>(entity =>
            {
                entity.HasKey(e => e.ParticipantNotificationId);

                entity.ToTable("OFA_ParticipantNotification");

                entity.Property(e => e.ParticipantNotificationId).HasColumnName("ParticipantNotificationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.NotificationId).HasColumnName("NotificationID");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.Notification)
                    .WithMany(p => p.OfaParticipantNotification)
                    .HasForeignKey(d => d.NotificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_Parti__Notif__4BEEFBE4");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantNotification)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_Parti__OFA_P__4AFAD7AB");
            });

            modelBuilder.Entity<OfaParticipantOtherChildCare>(entity =>
            {
                entity.HasKey(e => e.ParticipantOtherChildCareId);

                entity.ToTable("OFA_participantOtherChildCare");

                entity.Property(e => e.ParticipantOtherChildCareId).HasColumnName("ParticipantOtherChildCareID");

                entity.Property(e => e.ChildCareTypeId).HasColumnName("ChildCareTypeID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantOtherChildCare)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__3BB8941B");
            });

            modelBuilder.Entity<OfaParticipantRace>(entity =>
            {
                entity.HasKey(e => e.ParticipantRaceId);

                entity.ToTable("OFA_participantRace");

                entity.Property(e => e.ParticipantRaceId).HasColumnName("ParticipantRaceID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.Property(e => e.RaceId).HasColumnName("RaceID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantRace)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__OFA_parti__OFA_P__33234E1A");
            });
            modelBuilder.Entity<OfaParticipantFileAttachment>(entity =>
            {
                entity.HasKey(e => e.FileAttachmentId);

                entity.ToTable("OFA_participantFileAttachment");

                entity.Property(e => e.FileAttachmentId).HasColumnName("FileAttachmentID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentTypeID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FilePath).IsRequired();

                entity.Property(e => e.LastModifiedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.OfaParticipantId).HasColumnName("OFA_ParticipantID");

                entity.HasOne(d => d.OfaParticipant)
                    .WithMany(p => p.OfaParticipantFileAttachment)
                    .HasForeignKey(d => d.OfaParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OFA_participantFileAttachment_OFA_participant");
            });
            
            modelBuilder.Entity<ParentGuardian>(entity =>
            {
                entity.HasIndex(e => e.ParentGuardianId);

                entity.HasIndex(e => new { e.Gender, e.EducationLevel, e.EmploymentStatus, e.Training, e.ParentGuardianId })
                    .HasName("_dta_index_ParentGuardian_5_2082106458__K1_6_31_32_33");

                entity.Property(e => e.ParentGuardianId).HasColumnName("ParentGuardianID");

                entity.Property(e => e.ChildPlusFamilyId).HasColumnName("ChildPlusFamilyID");

                entity.Property(e => e.ChildPlusId).HasColumnName("ChildPlusID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.EmailAddress).HasMaxLength(100);

                entity.Property(e => e.FamPrimaryStaging).HasColumnName("FamPrimary_Staging");

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.LegacyFamId1)
                    .HasColumnName("LegacyFamID1")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyFamId1Gid)
                    .HasColumnName("LegacyFamID1_GID")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyFamId2)
                    .HasColumnName("LegacyFamID2")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyFamIdname1)
                    .HasColumnName("LegacyFamIDName1")
                    .HasMaxLength(100);

                entity.Property(e => e.LegacyFamIdname2)
                    .HasColumnName("LegacyFamIDName2")
                    .HasMaxLength(100);

                entity.Property(e => e.MailingAddress1).HasMaxLength(100);

                entity.Property(e => e.MailingAddress2).HasMaxLength(100);

                entity.Property(e => e.MailingCity).HasMaxLength(50);

                entity.Property(e => e.MailingState).HasMaxLength(2);

                entity.Property(e => e.MailingZipCode).HasMaxLength(20);

                entity.Property(e => e.MiddleName).HasMaxLength(50);

                entity.Property(e => e.OtherPhoneNumber)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("(N'-')");

                entity.Property(e => e.OtherPhoneNumber2)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("(N'-')");

                entity.Property(e => e.OtherPhoneNumber2Notes).HasMaxLength(500);

                entity.Property(e => e.OtherPhoneNumber3).HasMaxLength(30);

                entity.Property(e => e.OtherPhoneNumber3Notes).HasMaxLength(500);

                entity.Property(e => e.OtherPhoneNumberNotes).HasMaxLength(500);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(30)
                    .HasDefaultValueSql("(N'-')");

                entity.Property(e => e.PhoneNumberNotes).HasMaxLength(500);

                entity.Property(e => e.Suffix).HasMaxLength(10);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.ParentGuardian)
                    .HasForeignKey(d => d.GranteeId)
                    .HasConstraintName("FK_ParentGuardian_Grantee");
            });

            modelBuilder.Entity<Participant>(entity =>
            {
                entity.HasIndex(e => new { e.LegacyId1, e.GranteeId, e.LegacyName1 })
                    .HasName("Participant_GranteeID_LegacyName1");

                entity.HasIndex(e => new { e.ParentReportHomeless, e.ParentReportActiveDutyMilitary, e.ParentReportOneMemberMilitaryVeteran, e.ParticipantId })
                    .HasName("_dta_index_Participant_5_2114106572__K1_63_69_115");

                entity.HasIndex(e => new { e.Type, e.FirstName, e.LastName, e.DateofBirth, e.PhotoUrl, e.GranteeId, e.ParticipantId })
                    .HasName("IX_ParticipantId_Covering");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.Address1).HasMaxLength(100);

                entity.Property(e => e.Address2).HasMaxLength(100);

                entity.Property(e => e.ApplicationDate).HasColumnType("date");

                entity.Property(e => e.BiracialRace).HasMaxLength(500);

                entity.Property(e => e.CenterIdpreference1).HasColumnName("CenterIDPreference1");

                entity.Property(e => e.CenterIdpreference2).HasColumnName("CenterIDPreference2");

                entity.Property(e => e.CenterIdpreference3).HasColumnName("CenterIDPreference3");

                entity.Property(e => e.ChildPlusFamilyId).HasColumnName("ChildPlusFamilyID");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateofBirth).HasColumnType("date");

                entity.Property(e => e.DropOffTransportationAddress1).HasMaxLength(500);

                entity.Property(e => e.DropOffTransportationCity).HasMaxLength(500);

                entity.Property(e => e.DropOffTransportationState).HasMaxLength(20);

                entity.Property(e => e.DropOffTransportationZipcode).HasMaxLength(100);

                entity.Property(e => e.ExpectingMotherEstimatedDueDate).HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.ImmunizationOverAllDueDate).HasColumnType("date");

                entity.Property(e => e.IsUscitizen).HasColumnName("IsUSCitizen");

                entity.Property(e => e.LanguageOtherSpecify).HasMaxLength(500);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyId1)
                    .HasColumnName("LegacyID1")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyId1Gid)
                    .HasColumnName("LegacyID1_GID")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyId2)
                    .HasColumnName("LegacyID2")
                    .HasMaxLength(50);

                entity.Property(e => e.LegacyName1).HasMaxLength(100);

                entity.Property(e => e.LegacyName2).HasMaxLength(100);

                entity.Property(e => e.MiddleName).HasMaxLength(50);

                entity.Property(e => e.ParentConcernBehaviorType).HasMaxLength(500);

                entity.Property(e => e.ParentConcernMedDentNutrOtherType).HasMaxLength(500);

                entity.Property(e => e.ParentReportDisabilityType).HasMaxLength(500);

                entity.Property(e => e.ParentReportHealthMh).HasColumnName("ParentReportHealthMH");

                entity.Property(e => e.ParentReportIep).HasColumnName("ParentReportIEP");

                entity.Property(e => e.ParentReportLegalIssuesType).HasMaxLength(500);

                entity.Property(e => e.ParentReportOtherServicesType).HasMaxLength(500);

                entity.Property(e => e.ParentReportReferralOther).HasMaxLength(500);

                entity.Property(e => e.ParentReportSnap).HasColumnName("ParentReportSNAP");

                entity.Property(e => e.ParentReportSsi).HasColumnName("ParentReportSSI");

                entity.Property(e => e.ParentReportTanf).HasColumnName("ParentReportTANF");

                entity.Property(e => e.ParentReportWic).HasColumnName("ParentReportWIC");

                entity.Property(e => e.PhotoUrl)
                    .HasColumnName("photoUrl")
                    .HasMaxLength(1000);

                entity.Property(e => e.RaceOther).HasMaxLength(100);

                entity.Property(e => e.RaceUnspecified).HasMaxLength(100);

                entity.Property(e => e.School).HasMaxLength(100);

                entity.Property(e => e.SchoolDistrict).HasMaxLength(100);

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.SuffixText)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationAddress1).HasMaxLength(500);

                entity.Property(e => e.TransportationCity).HasMaxLength(500);

                entity.Property(e => e.TransportationState).HasMaxLength(20);

                entity.Property(e => e.TransportationTypeOther).HasMaxLength(500);

                entity.Property(e => e.TransportationUpdateDate).HasColumnType("date");

                entity.Property(e => e.TransportationZipcode).HasMaxLength(100);

                entity.Property(e => e.Zipcode).HasMaxLength(20);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.Participant)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Participant_Grantee");
            });

            modelBuilder.Entity<ParticipantAction>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantFollowUpId, e.StatusId, e.StatusDate })
                    .HasName("IX_ParticipantAction_StatusID_StatusDate_Covering");

                entity.Property(e => e.ParticipantActionId).HasColumnName("ParticipantActionID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(4000);

                entity.Property(e => e.ParticipantFollowUpId).HasColumnName("ParticipantFollowUpID");

                entity.Property(e => e.StatusDate).HasColumnType("date");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.UserInputDate).HasColumnType("date");

                entity.Property(e => e.UserInputId).HasColumnName("UserInputID");

                entity.HasOne(d => d.ParticipantFollowUp)
                    .WithMany(p => p.ParticipantAction)
                    .HasForeignKey(d => d.ParticipantFollowUpId)
                    .HasConstraintName("FK_ParticipantAction_ParticipantFollowUp");
            });

            modelBuilder.Entity<ParticipantAdditionalFamilyMember>(entity =>
            {
                entity.HasIndex(e => new { e.ChildRelationshipToParentGuardian, e.CreatedOn, e.DateOfBirth, e.FirstName, e.Gender, e.LastModified, e.LastName, e.ParticipantAdditionalFamilyMemberId, e.ParticipantId })
                    .HasName("IX_ParticipantAdditionalFamilyMember_ParticipantID");

                entity.Property(e => e.ParticipantAdditionalFamilyMemberId).HasColumnName("ParticipantAdditionalFamilyMemberID");

                entity.Property(e => e.AdditionalFamilyMemberId).HasColumnName("AdditionalFamilyMemberID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantAdditionalFamilyMember)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantAdditionalFamilyMember_Participant");
            });

            modelBuilder.Entity<ParticipantAlert>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantAlertId, e.AlertId })
                    .HasName("IX_ParticipantAlert");

                entity.HasIndex(e => new { e.ParticipantId, e.AlertId })
                    .HasName("_dta_index_ParticipantAlert_5_62623266__K2_K3");

                entity.HasIndex(e => new { e.AlertSetDate, e.CreatedOn, e.LastModified, e.ParticipantAlertId, e.ParticipantId, e.Points, e.AlertId })
                    .HasName("IX_ParticipantAlert_Points");

                entity.Property(e => e.ParticipantAlertId).HasColumnName("ParticipantAlertID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.AlertSetDate).HasColumnType("datetime2(0)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Alert)
                    .WithMany(p => p.ParticipantAlert)
                    .HasForeignKey(d => d.AlertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantAlert_Alert");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantAlert)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantAlert_Participant1");
            });

            modelBuilder.Entity<ParticipantConcernGoals>(entity =>
            {
                entity.Property(e => e.ParticipantConcernGoalsId).HasColumnName("ParticipantConcernGoalsID");

                entity.Property(e => e.GoalCategoryId).HasColumnName("GoalCategoryID");

                entity.Property(e => e.GoalName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.GoalSubCategoryId).HasColumnName("GoalSubCategoryID");

                entity.Property(e => e.HealthconcernId).HasColumnName("HealthconcernID");
            });

            modelBuilder.Entity<ParticipantDmhactionPlan>(entity =>
            {
                entity.ToTable("ParticipantDMHActionPlan");

                entity.HasIndex(e => e.ParticipantId);

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.Property(e => e.BehaviorPlanBasedOnFba).HasColumnName("BehaviorPlanBasedOnFBA");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.FbacompletionDate)
                    .HasColumnName("FBACompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.InputDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PlanTypeOther).HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StrategiesForChildDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForClassroomDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForHomeDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategyForChildOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForClassroomOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForHomeOther).HasMaxLength(100);

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.ParticipantDmhactionPlan)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_ParticipantDMHActionPlan_UserProfile");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhactionPlan)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHActionPlan_Participant");
            });

            modelBuilder.Entity<ParticipantDmhactionPlanStrategyForChild>(entity =>
            {
                entity.ToTable("ParticipantDMHActionPlanStrategyForChild");

                entity.Property(e => e.ParticipantDmhactionPlanStrategyForChildId).HasColumnName("ParticipantDMHActionPlanStrategyForChildID");

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.HasOne(d => d.ParticipantDmhactionPlan)
                    .WithMany(p => p.ParticipantDmhactionPlanStrategyForChild)
                    .HasForeignKey(d => d.ParticipantDmhactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHActionPlanStrategyForChild_ParticipantDMHActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhactionPlanStrategyForClassroom>(entity =>
            {
                entity.ToTable("ParticipantDMHActionPlanStrategyForClassroom");

                entity.Property(e => e.ParticipantDmhactionPlanStrategyForClassroomId).HasColumnName("ParticipantDMHActionPlanStrategyForClassroomID");

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.HasOne(d => d.ParticipantDmhactionPlan)
                    .WithMany(p => p.ParticipantDmhactionPlanStrategyForClassroom)
                    .HasForeignKey(d => d.ParticipantDmhactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHActionPlanStrategyForClassroom_ParticipantDMHActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhactionPlanStrategyForHome>(entity =>
            {
                entity.ToTable("ParticipantDMHActionPlanStrategyForHome");

                entity.Property(e => e.ParticipantDmhactionPlanStrategyForHomeId).HasColumnName("ParticipantDMHActionPlanStrategyForHomeID");

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.HasOne(d => d.ParticipantDmhactionPlan)
                    .WithMany(p => p.ParticipantDmhactionPlanStrategyForHome)
                    .HasForeignKey(d => d.ParticipantDmhactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHActionPlanStrategyForHome_ParticipantDMHActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhactionPlanType>(entity =>
            {
                entity.ToTable("ParticipantDMHActionPlanType");

                entity.Property(e => e.ParticipantDmhactionPlanTypeId).HasColumnName("ParticipantDMHActionPlanTypeID");

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.HasOne(d => d.ParticipantDmhactionPlan)
                    .WithMany(p => p.ParticipantDmhactionPlanType)
                    .HasForeignKey(d => d.ParticipantDmhactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHActionPlanType_ParticipantDMHActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhbehaviorPlan>(entity =>
            {
                entity.ToTable("ParticipantDMHBehaviorPlan");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.FbacompletionDate)
                    .HasColumnName("FBACompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.InputDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PlanTypeOther).HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StrategiesForChildDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForClassroomDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForHomeDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategyForChildOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForClassroomOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForHomeOther).HasMaxLength(100);
            });

            modelBuilder.Entity<ParticipantDmhbehaviorPlanStrategyForChild>(entity =>
            {
                entity.ToTable("ParticipantDMHBehaviorPlanStrategyForChild");

                entity.Property(e => e.ParticipantDmhbehaviorPlanStrategyForChildId).HasColumnName("ParticipantDMHBehaviorPlanStrategyForChildID");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.HasOne(d => d.ParticipantDmhbehaviorPlan)
                    .WithMany(p => p.ParticipantDmhbehaviorPlanStrategyForChild)
                    .HasForeignKey(d => d.ParticipantDmhbehaviorPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHBehaviorPlanStrategyForChild_ParticipantDMHBehaviorPlan");
            });

            modelBuilder.Entity<ParticipantDmhbehaviorPlanStrategyForClassroom>(entity =>
            {
                entity.ToTable("ParticipantDMHBehaviorPlanStrategyForClassroom");

                entity.Property(e => e.ParticipantDmhbehaviorPlanStrategyForClassroomId).HasColumnName("ParticipantDMHBehaviorPlanStrategyForClassroomID");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.HasOne(d => d.ParticipantDmhbehaviorPlan)
                    .WithMany(p => p.ParticipantDmhbehaviorPlanStrategyForClassroom)
                    .HasForeignKey(d => d.ParticipantDmhbehaviorPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHBehaviorPlanStrategyForClassroom_ParticipantDMHBehaviorPlan");
            });

            modelBuilder.Entity<ParticipantDmhbehaviorPlanStrategyForHome>(entity =>
            {
                entity.ToTable("ParticipantDMHBehaviorPlanStrategyForHome");

                entity.Property(e => e.ParticipantDmhbehaviorPlanStrategyForHomeId).HasColumnName("ParticipantDMHBehaviorPlanStrategyForHomeID");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.HasOne(d => d.ParticipantDmhbehaviorPlan)
                    .WithMany(p => p.ParticipantDmhbehaviorPlanStrategyForHome)
                    .HasForeignKey(d => d.ParticipantDmhbehaviorPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHBehaviorPlanStrategyForHome_ParticipantDMHBehaviorPlan");
            });

            modelBuilder.Entity<ParticipantDmhbehaviorPlanType>(entity =>
            {
                entity.ToTable("ParticipantDMHBehaviorPlanType");

                entity.Property(e => e.ParticipantDmhbehaviorPlanTypeId).HasColumnName("ParticipantDMHBehaviorPlanTypeID");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.HasOne(d => d.ParticipantDmhbehaviorPlan)
                    .WithMany(p => p.ParticipantDmhbehaviorPlanType)
                    .HasForeignKey(d => d.ParticipantDmhbehaviorPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHBehaviorPlanType_ParticipantDMHBehaviorPlan");
            });

            modelBuilder.Entity<ParticipantDmhconcern>(entity =>
            {
                entity.ToTable("ParticipantDMHConcern");

                entity.HasIndex(e => new { e.ParticipantId, e.ConcernArea });

                entity.Property(e => e.ParticipantDmhconcernId).HasColumnName("ParticipantDMHConcernID");

                entity.Property(e => e.ConcernAreaOtherDescription).HasMaxLength(2000);

                entity.Property(e => e.ConcernInitiatedDate).HasColumnType("datetime");

                entity.Property(e => e.ConcernNote).HasMaxLength(4000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.UserInputDate).HasColumnType("date");

                entity.Property(e => e.UserModifiedDate).HasColumnType("date");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.ParticipantDmhconcern)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_ParticipantDMHConcern_UserProfile");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhconcern)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHConcern_Participant");
            });

            modelBuilder.Entity<ParticipantDmhconcernArea>(entity =>
            {
                entity.ToTable("ParticipantDMHConcernArea");

                entity.Property(e => e.ParticipantDmhconcernAreaId).HasColumnName("ParticipantDMHConcernAreaID");

                entity.Property(e => e.ParticipantDmhconcernId).HasColumnName("ParticipantDMHConcernID");

                entity.HasOne(d => d.ParticipantDmhconcern)
                    .WithMany(p => p.ParticipantDmhconcernArea)
                    .HasForeignKey(d => d.ParticipantDmhconcernId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHConcernArea_ParticipantDMHConcern");
            });

            modelBuilder.Entity<ParticipantDmhiep>(entity =>
            {
                entity.ToTable("ParticipantDMHIEP");

                entity.HasIndex(e => new { e.ParticipantId, e.PlanType })
                    .HasName("IX_ParticipantDMHIEP_PlanType_Covering");

                entity.Property(e => e.ParticipantDmhiepid).HasColumnName("ParticipantDMHIEPID");

                entity.Property(e => e.AdditionalDiagnosisOther).HasMaxLength(100);

                entity.Property(e => e.AssociatedConcernOther).HasMaxLength(4000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DiagnosisDate).HasColumnType("date");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.InputDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TransitionDate).HasColumnType("date");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.ParticipantDmhiep)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_ParticipantDMHIEP_UserProfile");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhiep)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEP_Participant");
            });

            modelBuilder.Entity<ParticipantDmhiepactionPlan>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPActionPlan");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.IepactionPlanUpdateFrequency).HasColumnName("IEPActionPlanUpdateFrequency");

                entity.Property(e => e.InputDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PlanTypeOther).HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StrategiesForChildDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForClassroomDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategiesForHomeDescription).HasMaxLength(4000);

                entity.Property(e => e.StrategyForChildOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForClassroomOther).HasMaxLength(100);

                entity.Property(e => e.StrategyForHomeOther).HasMaxLength(100);
            });

            modelBuilder.Entity<ParticipantDmhiepactionPlanStrategyForChild>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPActionPlanStrategyForChild");

                entity.Property(e => e.ParticipantDmhiepactionPlanStrategyForChildId).HasColumnName("ParticipantDMHIEPActionPlanStrategyForChildID");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.HasOne(d => d.ParticipantDmhiepactionPlan)
                    .WithMany(p => p.ParticipantDmhiepactionPlanStrategyForChild)
                    .HasForeignKey(d => d.ParticipantDmhiepactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPActionPlanStrategyForChild_ParticipantDMHIEPActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhiepactionPlanStrategyForClassroom>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPActionPlanStrategyForClassroom");

                entity.Property(e => e.ParticipantDmhiepactionPlanStrategyForClassroomId).HasColumnName("ParticipantDMHIEPActionPlanStrategyForClassroomID");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.HasOne(d => d.ParticipantDmhiepactionPlan)
                    .WithMany(p => p.ParticipantDmhiepactionPlanStrategyForClassroom)
                    .HasForeignKey(d => d.ParticipantDmhiepactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPActionPlanStrategyForClassroom_ParticipantDMHIEPActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhiepactionPlanStrategyForHome>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPActionPlanStrategyForHome");

                entity.Property(e => e.ParticipantDmhiepactionPlanStrategyForHomeId).HasColumnName("ParticipantDMHIEPActionPlanStrategyForHomeID");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.HasOne(d => d.ParticipantDmhiepactionPlan)
                    .WithMany(p => p.ParticipantDmhiepactionPlanStrategyForHome)
                    .HasForeignKey(d => d.ParticipantDmhiepactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPActionPlanStrategyForHome_ParticipantDMHIEPActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhiepactionPlanType>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPActionPlanType");

                entity.Property(e => e.ParticipantDmhiepactionPlanTypeId).HasColumnName("ParticipantDMHIEPActionPlanTypeID");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.HasOne(d => d.ParticipantDmhiepactionPlan)
                    .WithMany(p => p.ParticipantDmhiepactionPlanType)
                    .HasForeignKey(d => d.ParticipantDmhiepactionPlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPActionPlanType_ParticipantDMHIEPActionPlan");
            });

            modelBuilder.Entity<ParticipantDmhiepadditionalDiagnosis>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPAdditionalDiagnosis");

                entity.Property(e => e.ParticipantDmhiepadditionalDiagnosisId).HasColumnName("ParticipantDMHIEPAdditionalDiagnosisID");

                entity.Property(e => e.ParticipantDmhiepid).HasColumnName("ParticipantDMHIEPID");

                entity.HasOne(d => d.ParticipantDmhiep)
                    .WithMany(p => p.ParticipantDmhiepadditionalDiagnosis)
                    .HasForeignKey(d => d.ParticipantDmhiepid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPAdditionalDiagnosis_ParticipantDMHIEP");
            });

            modelBuilder.Entity<ParticipantDmhiepassociatedConcern>(entity =>
            {
                entity.ToTable("ParticipantDMHIEPAssociatedConcern");

                entity.Property(e => e.ParticipantDmhiepassociatedConcernId).HasColumnName("ParticipantDMHIEPAssociatedConcernID");

                entity.Property(e => e.ParticipantDmhiepid).HasColumnName("ParticipantDMHIEPID");

                entity.HasOne(d => d.ParticipantDmhiep)
                    .WithMany(p => p.ParticipantDmhiepassociatedConcern)
                    .HasForeignKey(d => d.ParticipantDmhiepid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHIEPAssociatedConcern_ParticipantDMHIEP");
            });

            modelBuilder.Entity<ParticipantDmhnote>(entity =>
            {
                entity.ToTable("ParticipantDMHNote");

                entity.HasIndex(e => new { e.ParticipantDmhnoteId, e.NoteType, e.ParticipantId })
                    .HasName("IX_ParticipantDMHNote_NoteType");

                entity.HasIndex(e => new { e.ParticipantId, e.NoteDate, e.PirservicesReceived1, e.PirservicesReceived2, e.PirservicesReceived3 })
                    .HasName("_dta_index_ParticipantDMHNote_5_1741249258__K2_K4_K7_K8_K9");

                entity.HasIndex(e => new { e.LastModified, e.NoteType, e.ParticipantDmhnoteId, e.PirservicesReceived1, e.PirservicesReceived2, e.PirservicesReceived3, e.CreatedOn, e.Description, e.IsMentalHealth, e.UserInputDate, e.ParticipantId, e.CreatedByUserId, e.NoteDate })
                    .HasName("IX_ParticipantDMHNote_CreatedByUserId");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Description).HasMaxLength(4000);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.NoteDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PirservicesReceived1).HasColumnName("PIRServicesReceived1");

                entity.Property(e => e.PirservicesReceived2).HasColumnName("PIRServicesReceived2");

                entity.Property(e => e.PirservicesReceived3).HasColumnName("PIRServicesReceived3");

                entity.Property(e => e.UserInputDate).HasColumnType("date");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.ParticipantDmhnote)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_ParticipantDMHNote_UserProfile");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhnote)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHNote_Participant");
            });

            modelBuilder.Entity<ParticipantDmhnoteAssociation>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteAssociation");

                entity.Property(e => e.ParticipantDmhnoteAssociationId).HasColumnName("ParticipantDMHNoteAssociationID");

                entity.Property(e => e.ForeignKeyId).HasColumnName("ForeignKeyID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");

                entity.HasOne(d => d.ParticipantDmhnote)
                    .WithMany(p => p.ParticipantDmhnoteAssociation)
                    .HasForeignKey(d => d.ParticipantDmhnoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHNoteAssociation_ParticipantDMHNote");
            });

            modelBuilder.Entity<ParticipantDmhnoteParticipantDmhactionPlan>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteParticipantDMHActionPlan");

                entity.Property(e => e.ParticipantDmhnoteParticipantDmhactionPlanId).HasColumnName("ParticipantDMHNoteParticipantDMHActionPlanID");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ParticipantDmhactionPlanId).HasColumnName("ParticipantDMHActionPlanID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<ParticipantDmhnoteParticipantDmhbehavior>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteParticipantDMHBehavior");

                entity.Property(e => e.ParticipantDmhnoteParticipantDmhbehaviorId).HasColumnName("ParticipantDMHNoteParticipantDMHBehaviorID");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ParticipantDmhbehaviorPlanId).HasColumnName("ParticipantDMHBehaviorPlanID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<ParticipantDmhnoteParticipantDmhconcern>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteParticipantDMHConcern");

                entity.Property(e => e.ParticipantDmhnoteParticipantDmhconcernId).HasColumnName("ParticipantDMHNoteParticipantDMHConcernID");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ParticipantDmhconcernId).HasColumnName("ParticipantDMHConcernID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<ParticipantDmhnoteParticipantDmhiep>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteParticipantDMHIEP");

                entity.Property(e => e.ParticipantDmhnoteParticipantDmhiepid).HasColumnName("ParticipantDMHNoteParticipantDMHIEPID");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ParticipantDmhiepid).HasColumnName("ParticipantDMHIEPID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<ParticipantDmhnoteParticipantDmhiepactionPlan>(entity =>
            {
                entity.ToTable("ParticipantDMHNoteParticipantDMHIEPActionPlan");

                entity.Property(e => e.ParticipantDmhnoteParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHNoteParticipantDMHIEPActionPlanID");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationID");

                entity.Property(e => e.ParticipantDmhiepactionPlanId).HasColumnName("ParticipantDMHIEPActionPlanID");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");
            });

            modelBuilder.Entity<ParticipantDmhsummary>(entity =>
            {
                entity.ToTable("ParticipantDMHSummary");

                entity.Property(e => e.ParticipantDmhsummaryId).HasColumnName("ParticipantDMHSummaryID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DisabilitiesServicesMostRecentActionDate).HasColumnType("date");

                entity.Property(e => e.DisabilitiesServicesMostRecentActionTaken).HasMaxLength(256);

                entity.Property(e => e.DisabilitiesServicesStatusLastModified).HasColumnType("date");

                entity.Property(e => e.DisabilitiesServicesStepInProcessLastModified).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.MentalHealthServicesStatusDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhsummary)
                    .HasForeignKey(d => d.ParticipantId)
                    .HasConstraintName("FK_ParticipantDMHSummary_Participant");
            });

            modelBuilder.Entity<ParticipantDmhsummaryResult>(entity =>
            {
                entity.ToTable("ParticipantDMHSummaryResult");

                entity.Property(e => e.ParticipantDmhsummaryResultId).HasColumnName("ParticipantDMHSummaryResultID");

                entity.Property(e => e.AssociatedConcernId).HasColumnName("AssociatedConcernID");

                entity.Property(e => e.MostRecentActionDate).HasColumnType("datetime2(0)");

                entity.Property(e => e.NextRequireDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantDmhsummaryResult)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantDMHSummaryResult_Participant");
            });

            modelBuilder.Entity<ParticipantEducationScreeningDocumentation>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantId, e.EducationScreeningId, e.ResultsStatus })
                    .HasName("ParticipantEducationScreeningDocumentation_ParticipantID_EducationScreeningID_ResultsStatus");

                entity.Property(e => e.ParticipantEducationScreeningDocumentationId).HasColumnName("ParticipantEducationScreeningDocumentationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.EducationScreeningId).HasColumnName("EducationScreeningID");

                entity.Property(e => e.ExemptDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.NotificationDate).HasColumnType("date");

                entity.Property(e => e.NotificationMethodOther).HasMaxLength(500);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.Property(e => e.RefusedDate).HasColumnType("date");

                entity.Property(e => e.RescreenDueDate).HasColumnType("date");

                entity.Property(e => e.Results).HasMaxLength(500);

                entity.Property(e => e.ScreeningDate).HasColumnType("date");

                entity.Property(e => e.UserInputDate).HasColumnType("datetime");

                entity.HasOne(d => d.EducationScreening)
                    .WithMany(p => p.ParticipantEducationScreeningDocumentation)
                    .HasForeignKey(d => d.EducationScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantEducationScreeningDocumentation_EducationScreening");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantEducationScreeningDocumentation)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantEducationScreeningDocumentation_Participant");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.ParticipantEducationScreeningDocumentation)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .HasConstraintName("FK_ParticipantEducationScreeningDocumentation_ProgramType");

                entity.HasOne(d => d.Screener)
                    .WithMany(p => p.ParticipantEducationScreeningDocumentationScreener)
                    .HasForeignKey(d => d.ScreenerId)
                    .HasConstraintName("FK_ParticipantEducationScreeningDocumentation_UserProfile1");

                entity.HasOne(d => d.ScreeningDocumentationUser)
                    .WithMany(p => p.ParticipantEducationScreeningDocumentationScreeningDocumentationUser)
                    .HasForeignKey(d => d.ScreeningDocumentationUserId)
                    .HasConstraintName("FK_ParticipantEducationScreeningDocumentation_UserProfile");
            });

            modelBuilder.Entity<ParticipantEducationScreeningPlanning>(entity =>
            {
                entity.Property(e => e.ParticipantEducationScreeningPlanningId).HasColumnName("ParticipantEducationScreeningPlanningID");

                entity.Property(e => e.AttemptDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.EducationScreeningId).HasColumnName("EducationScreeningID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.PlanningUser)
                    .WithMany(p => p.ParticipantEducationScreeningPlanning)
                    .HasForeignKey(d => d.PlanningUserId)
                    .HasConstraintName("FK_ParticipantEducationScreeningPlanning_UserProfile");
            });

            modelBuilder.Entity<ParticipantEducationScreeningReport>(entity =>
            {
                entity.HasIndex(e => e.ParticipantId);

                entity.Property(e => e.ParticipantEducationScreeningReportId).HasColumnName("ParticipantEducationScreeningReportID");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.EducationScreeningId).HasColumnName("EducationScreeningID");

                entity.Property(e => e.ParticipantEducationScreeningDocumentationId).HasColumnName("ParticipantEducationScreeningDocumentationID");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.RescreenDueDate).HasColumnType("date");

                entity.Property(e => e.RescreenStatusId).HasColumnName("RescreenStatusID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<ParticipantEmergencyContact>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EmergencyContactCity).HasMaxLength(50);

                entity.Property(e => e.EmergencyContactEmail).HasMaxLength(100);

                entity.Property(e => e.EmergencyContactFirstName).HasMaxLength(50);

                entity.Property(e => e.EmergencyContactLastName).HasMaxLength(50);

                entity.Property(e => e.EmergencyContactPhone1).HasMaxLength(20);

                entity.Property(e => e.EmergencyContactPhone2).HasMaxLength(20);

                entity.Property(e => e.EmergencyContactPickUpNotes).HasMaxLength(1000);

                entity.Property(e => e.EmergencyContactState).HasMaxLength(50);

                entity.Property(e => e.EmergencyContactStreetAddress).HasMaxLength(100);

                entity.Property(e => e.EmergencyContactZipcode).HasMaxLength(20);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ParticipantFamilyProfile)
                    .WithMany(p => p.ParticipantEmergencyContact)
                    .HasForeignKey(d => d.ParticipantFamilyProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantEmergencyContact_ParticipantFamilyProfile");
            });

            modelBuilder.Entity<ParticipantEvaluationCenterprogramyear>(entity =>
            {
                entity.HasIndex(e => e.ParticipantId);

                entity.HasIndex(e => new { e.ParticipantId, e.TimeStampId });

                entity.HasIndex(e => new { e.ParticipantId, e.ParticipantEvaluationCenterprogramyearId, e.TimeStampId, e.CenterProgramYearId })
                    .HasName("_dta_index_ParticipantEvaluationCenterprogr_5_633105346__K2_K1_K3_K4");

                entity.Property(e => e.ParticipantEvaluationCenterprogramyearId).HasColumnName("ParticipantEvaluationCenterprogramyearID");

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantEvaluationCenterprogramyear)
                    .HasForeignKey(d => d.ParticipantId)
                    .HasConstraintName("FK_ParticipantEvaluationCenterprogramyear_Participant");

                entity.HasOne(d => d.TimeStamp)
                    .WithMany(p => p.ParticipantEvaluationCenterprogramyear)
                    .HasForeignKey(d => d.TimeStampId)
                    .HasConstraintName("FK_ParticipantEvaluationCenterprogramyear_TimeStamp");
            });

            modelBuilder.Entity<ParticipantFamilyProfile>(entity =>
            {
                entity.Property(e => e.ParticipantFamilyProfileId).HasColumnName("ParticipantFamilyProfileID");

                entity.Property(e => e.ApplicationCustomFormNotes1).HasMaxLength(1000);

                entity.Property(e => e.ApplicationCustomFormNotes2).HasMaxLength(1000);

                entity.Property(e => e.ApplicationCustomFormNotes3).HasMaxLength(1000);

                entity.Property(e => e.ApplicationCustomFormNotes4).HasMaxLength(1000);

                entity.Property(e => e.ApplicationCustomFormNotes5).HasMaxLength(1000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.EmergencyConsentContactEmergencyResourcesBeforeParents).HasDefaultValueSql("((1))");

                entity.Property(e => e.EmergencyConsentEmergencyMeasures).HasDefaultValueSql("((1))");

                entity.Property(e => e.EmergencyConsentEvacuated).HasDefaultValueSql("((1))");

                entity.Property(e => e.EmergencyConsentTransported).HasDefaultValueSql("((1))");

                entity.Property(e => e.EmergencyConsentXrays)
                    .HasColumnName("EmergencyConsentXRays")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ExtendedDayHasVoucher).HasMaxLength(10);

                entity.Property(e => e.ExtendedDayInterest).HasMaxLength(10);

                entity.Property(e => e.ExtendedDayVoucherApplicationSubmitted).HasMaxLength(10);

                entity.Property(e => e.FamilyInvolvementContractBringChildOnTime).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyInvolvementContractParticipateInParentMeetingsCenterMeetingsEvents).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyInvolvementContractReadEveryNight).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyInvolvementContractVolunteer24HoursPerProgramYear).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyInvolvementContractVolunteerCustom1Title).HasMaxLength(500);

                entity.Property(e => e.FamilyInvolvementContractVolunteerCustom2Title).HasMaxLength(500);

                entity.Property(e => e.FamilyInvolvementContractVolunteerCustom3Title).HasMaxLength(500);

                entity.Property(e => e.FamilyVolunteeringContractAdvisoryCommittee).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractContributeSkills).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractContributeSkillsNotes).HasMaxLength(1000);

                entity.Property(e => e.FamilyVolunteeringContractParentCommittee).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractPolicyCouncil).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractRegularClassroomVolunteer).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractSpecialClassroomVolunteer).HasDefaultValueSql("((1))");

                entity.Property(e => e.FamilyVolunteeringContractVolunteerOnFieldTrips).HasDefaultValueSql("((1))");

                entity.Property(e => e.GuardianSignatureDate).HasColumnType("datetime");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationAttachFile).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationChildDevelopmentandEducationInformation).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationChildHealthInformation).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationChildsBirthCertificate).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationFamilyContactInfo).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationFamilyServices).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionToReleaseAndRequestInformationRegardingSpecialNeeds).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforActivitiesPermissionToText).HasColumnName("permissionforActivitiesPermissionToText");

                entity.Property(e => e.PermissionforProgramActivitiesClassroomScreenings).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesCustom1Title).HasMaxLength(500);

                entity.Property(e => e.PermissionforProgramActivitiesCustom2Title).HasMaxLength(500);

                entity.Property(e => e.PermissionforProgramActivitiesCustom3Title).HasMaxLength(500);

                entity.Property(e => e.PermissionforProgramActivitiesCustom4Title).HasMaxLength(500);

                entity.Property(e => e.PermissionforProgramActivitiesCustom5Title).HasMaxLength(500);

                entity.Property(e => e.PermissionforProgramActivitiesDevelopmentalScreenings).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesHealthScreenings).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesSocialEmotionalScreenings).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesSpeechScreening).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesUseofInterperter).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramActivitiesWalkingTrips).HasDefaultValueSql("((1))");

                entity.Property(e => e.PermissionforProgramChildActivitiesPhotographed).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantFamilyProfile)
                    .HasForeignKey(d => d.ParticipantId)
                    .HasConstraintName("FK_ParticipantFamilyProfile_Participant");
            });

            modelBuilder.Entity<ParticipantFollowUp>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantHealthConcernId, e.ParticipantFollowUpId })
                    .HasName("IX_ParticipantFollowUp_ParticipantFollowUpID");

                entity.HasIndex(e => new { e.ParticipantHealthConcernId, e.ParticipantFollowUpId, e.NameId })
                    .HasName("IX_ParticipantFollowUp_NameID");

                entity.HasIndex(e => new { e.DueDate, e.OngoingActionRequired, e.ParticipantFollowUpId, e.DaysWithoutAction, e.ParticipantHealthConcernId, e.NameId, e.StatusId })
                    .HasName("IX_ParticipantFollowUp_ParticipantHealthConcernID");

                entity.Property(e => e.ParticipantFollowUpId).HasColumnName("ParticipantFollowUpID");

                entity.Property(e => e.CompletionDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DentalTreatmentDiagnosisDate).HasColumnType("date");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.ForeignConcernFustepId)
                    .HasColumnName("Foreign_ConcernFUStepID")
                    .HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.NameId).HasColumnName("NameID");

                entity.Property(e => e.OwnerId).HasColumnName("OwnerID");

                entity.Property(e => e.ParticipantHealthConcernId).HasColumnName("ParticipantHealthConcernID");

                entity.Property(e => e.StatusDate).HasColumnType("date");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.UserInputDate).HasColumnType("date");

                entity.Property(e => e.UserInputId).HasColumnName("UserInputID");

                entity.HasOne(d => d.ParticipantHealthConcern)
                    .WithMany(p => p.ParticipantFollowUp)
                    .HasForeignKey(d => d.ParticipantHealthConcernId)
                    .HasConstraintName("FK_ParticipantFollowUp_ParticipantHealthConcern");
            });

            modelBuilder.Entity<ParticipantHealthConcern>(entity =>
            {
                entity.HasIndex(e => e.ParticipantId);

                entity.HasIndex(e => new { e.ParticipantId, e.NameId })
                    .HasName("IX_ParticipantHealthConcern_NameID");

                entity.HasIndex(e => new { e.ParticipantId, e.ParticipantHealthConcernId, e.NameId })
                    .HasName("IX_ParticipantHealthConcern_ParticipantHealthConcernID");

                entity.Property(e => e.ParticipantHealthConcernId).HasColumnName("ParticipantHealthConcernID");

                entity.Property(e => e.AssociatedPrimaryGuardianGoalId).HasColumnName("AssociatedPrimaryGuardianGoalID");

                entity.Property(e => e.ChronicConditionDiagnosedByHealthCareProfessionalId).HasColumnName("ChronicConditionDiagnosedByHealthCareProfessionalID");

                entity.Property(e => e.ChronicConditionDiagnosisDate).HasColumnType("date");

                entity.Property(e => e.ChronicConditionDiagnosisId).HasColumnName("ChronicConditionDiagnosisID");

                entity.Property(e => e.ChronicConditionDiagnosisOther).HasMaxLength(1000);

                entity.Property(e => e.ChronicOrAcuteConditionDeterminationId).HasColumnName("ChronicOrAcuteConditionDeterminationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.ForeignConcernId)
                    .HasColumnName("Foreign_ConcernID")
                    .HasMaxLength(50);

                entity.Property(e => e.GoalRequiredId).HasColumnName("GoalRequiredID");

                entity.Property(e => e.InitiatedFromId).HasColumnName("InitiatedFromID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.NameId).HasColumnName("NameID");

                entity.Property(e => e.OtherDescription).HasMaxLength(50);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.UserInputDate).HasColumnType("date");

                entity.Property(e => e.UserInputId).HasColumnName("UserInputID");

                entity.HasOne(d => d.AssociatedPrimaryGuardianGoal)
                    .WithMany(p => p.ParticipantHealthConcern)
                    .HasForeignKey(d => d.AssociatedPrimaryGuardianGoalId)
                    .HasConstraintName("FK_ParticipantHealthConcern_PrimaryGuardianGoal");
            });

            modelBuilder.Entity<ParticipantHealthConcernParticipantHealthScreeningDocumentation>(entity =>
            {
                entity.Property(e => e.ParticipantHealthConcernParticipantHealthScreeningDocumentationId).HasColumnName("ParticipantHealthConcernParticipantHealthScreeningDocumentationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantHealthConcernId).HasColumnName("ParticipantHealthConcernID");

                entity.Property(e => e.ParticipantHealthScreeningDocumentationId).HasColumnName("ParticipantHealthScreeningDocumentationID");

                entity.HasOne(d => d.ParticipantHealthConcern)
                    .WithMany(p => p.ParticipantHealthConcernParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.ParticipantHealthConcernId)
                    .HasConstraintName("FK_ParticipantHealthConcernParticipantHealthScreeningDocumentation_ParticipantHealthConcern");

                entity.HasOne(d => d.ParticipantHealthScreeningDocumentation)
                    .WithMany(p => p.ParticipantHealthConcernParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.ParticipantHealthScreeningDocumentationId)
                    .HasConstraintName("FK_ParticipantHealthConcernParticipantHealthScreeningDocumentation_ParticipantHealthScreeningDocumentation");
            });

            modelBuilder.Entity<ParticipantHealthScreeningDocumentation>(entity =>
            {
                entity.HasIndex(e => e.GranteeHealthScreeningId);

                entity.HasIndex(e => e.HealthScreeningId)
                    .HasName("ParticipantHealthScreeningDocumentation_HealthScreeningID");

                entity.HasIndex(e => new { e.GranteeHealthScreeningId, e.ScreeningDate, e.ParticipantId, e.BmiagePercentile })
                    .HasName("IX_ParticipantHealthScreeningDocumentation_ParticipantID_BMIAgePercentile_Covering");

                entity.HasIndex(e => new { e.ParticipantId, e.GranteeHealthScreeningId, e.ScreeningDate, e.BmiagePercentile })
                    .HasName("IX_ParticipantHealthScreeningDocumentation_BMIAgePercentile_Covering");

                entity.Property(e => e.ParticipantHealthScreeningDocumentationId).HasColumnName("ParticipantHealthScreeningDocumentationID");

                entity.Property(e => e.BmiagePercentile).HasColumnName("BMIAgePercentile");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.ExemptDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.ExternalProvider).HasMaxLength(500);

                entity.Property(e => e.ExternalReceivedDate).HasColumnType("date");

                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.HealthScreeningId).HasColumnName("HealthScreeningID");

                entity.Property(e => e.HeightCentimeters).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.HeightInches).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.NotificationDate).HasColumnType("date");

                entity.Property(e => e.NotificationMethodOther).HasMaxLength(500);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.Property(e => e.RefusedDate).HasColumnType("date");

                entity.Property(e => e.ScreeningDate).HasColumnType("date");

                entity.Property(e => e.UserInputDate).HasColumnType("datetime");

                entity.Property(e => e.WeightInFractionalPounds).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.WeightInKilograms).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.GranteeHealthScreening)
                    .WithMany(p => p.ParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.GranteeHealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantHealthScreeningDocumentation_GranteeHealthScreening");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantHealthScreeningDocumentation_Participant");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.ParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .HasConstraintName("FK_ParticipantHealthScreeningDocumentation_ProgramType");

                entity.HasOne(d => d.ScreeningDocumentationUser)
                    .WithMany(p => p.ParticipantHealthScreeningDocumentation)
                    .HasForeignKey(d => d.ScreeningDocumentationUserId)
                    .HasConstraintName("FK_ParticipantHealthScreeningDocumentation_UserProfile");
            });

            modelBuilder.Entity<ParticipantHealthScreeningPlanning>(entity =>
            {
                entity.Property(e => e.ParticipantHealthScreeningPlanningId).HasColumnName("ParticipantHealthScreeningPlanningID");

                entity.Property(e => e.AttemptDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.PlanningUser)
                    .WithMany(p => p.ParticipantHealthScreeningPlanning)
                    .HasForeignKey(d => d.PlanningUserId)
                    .HasConstraintName("FK_ParticipantHealthScreeningPlanning_UserProfile");
            });

            modelBuilder.Entity<ParticipantHealthScreeningReport>(entity =>
            {
                entity.HasIndex(e => e.ParticipantId);

                entity.HasIndex(e => new { e.ParticipantId, e.ParticipantHealthScreeningReportId, e.GranteeHealthScreeningId, e.DueDate })
                    .HasName("_dta_index_ParticipantHealthScreeningReport_5_1120723045__K2_K1_K3_K5");

                entity.Property(e => e.ParticipantHealthScreeningReportId).HasColumnName("ParticipantHealthScreeningReportID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.GranteeHealthScreeningId).HasColumnName("GranteeHealthScreeningID");

                entity.Property(e => e.LastModified).HasColumnType("datetime");

                entity.Property(e => e.ParticipantHealthScreeningDocumentationId).HasColumnName("ParticipantHealthScreeningDocumentationID");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.GranteeHealthScreening)
                    .WithMany(p => p.ParticipantHealthScreeningReport)
                    .HasForeignKey(d => d.GranteeHealthScreeningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantHealthScreeningReport_GranteeHealthScreening");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantHealthScreeningReport)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantHealthScreeningReport_Participant");
            });

            modelBuilder.Entity<ParticipantImmunization>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantId, e.FirstDate, e.OverrideDate })
                    .HasName("IX_ParticipantImmunization_FirstDate");

                entity.Property(e => e.ParticipantImmunizationId).HasColumnName("ParticipantImmunizationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.ExemptReason).HasMaxLength(1000);

                entity.Property(e => e.FifthDate).HasColumnType("date");

                entity.Property(e => e.FirstDate).HasColumnType("date");

                entity.Property(e => e.FourthDate).HasColumnType("date");

                entity.Property(e => e.GranteeImmunizationId).HasColumnName("GranteeImmunizationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.OverrideDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.SecondDate).HasColumnType("date");

                entity.Property(e => e.ThirdDate).HasColumnType("date");

                entity.HasOne(d => d.GranteeImmunization)
                    .WithMany(p => p.ParticipantImmunization)
                    .HasForeignKey(d => d.GranteeImmunizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantImmunization_GranteeImmunization");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantImmunization)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantImmunization_Participant");
            });

            modelBuilder.Entity<ParticipantImmunizationPlanning>(entity =>
            {
                entity.Property(e => e.ParticipantImmunizationPlanningId).HasColumnName("ParticipantImmunizationPlanningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantImmunizationPlanning)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantImmunizationPlanning_Participant");
            });

            modelBuilder.Entity<ParticipantImmunizationPlanningHistory>(entity =>
            {
                entity.Property(e => e.ParticipantImmunizationPlanningHistoryId).HasColumnName("ParticipantImmunizationPlanningHistoryID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.GranteeImmunizationId).HasColumnName("GranteeImmunizationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantImmunizationPlanningId).HasColumnName("ParticipantImmunizationPlanningID");

                entity.HasOne(d => d.ParticipantImmunizationPlanning)
                    .WithMany(p => p.ParticipantImmunizationPlanningHistory)
                    .HasForeignKey(d => d.ParticipantImmunizationPlanningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantImmunizationPlanningHistory_ParticipantImmunizationPlanning");
            });

            modelBuilder.Entity<ParticipantInternalReferral>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Notes)
                    .IsRequired()
                    .HasMaxLength(4000);

                entity.Property(e => e.ParticipantEducationScreeningDocumentationId).HasColumnName("ParticipantEducationScreeningDocumentationID");

                entity.Property(e => e.ParticipantHealthScreeningDocumentationId).HasColumnName("ParticipantHealthScreeningDocumentationID");

                entity.Property(e => e.ReferralDate).HasColumnType("datetime");

                entity.Property(e => e.ResolvedDate).HasColumnType("datetime");

                entity.HasOne(d => d.ParticipantEducationScreeningDocumentation)
                    .WithMany(p => p.ParticipantInternalReferral)
                    .HasForeignKey(d => d.ParticipantEducationScreeningDocumentationId)
                    .HasConstraintName("FK_ParticipantInternalReferral_ParticipantEducationScreeningDocumentation");

                entity.HasOne(d => d.ParticipantHealthScreeningDocumentation)
                    .WithMany(p => p.ParticipantInternalReferral)
                    .HasForeignKey(d => d.ParticipantHealthScreeningDocumentationId)
                    .HasConstraintName("FK_ParticipantInternalReferral_ParticipantHealthScreeningDocumentation");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantInternalReferral)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantInternalReferral_Participant");
            });

            modelBuilder.Entity<ParticipantInternalReferralFollowUp>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FollowUpDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Notes)
                    .IsRequired()
                    .HasMaxLength(4000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ParticipantInternalReferralResolutionConcern>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DmhprocessDocumentationId).HasColumnName("DMHProcessDocumentationId");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParticipantDmhnoteId).HasColumnName("ParticipantDMHNoteID");

                entity.HasOne(d => d.ParticipantInternalReferral)
                    .WithMany(p => p.ParticipantInternalReferralResolutionConcern)
                    .HasForeignKey(d => d.ParticipantInternalReferralId)
                    .HasConstraintName("FK_ParticipantInternalReferralResolutionConcern_ParticipantInternalReferral");
            });

            modelBuilder.Entity<ParticipantParentGuardian>(entity =>
            {
                entity.HasIndex(e => e.ParentGuardianId);

                entity.HasIndex(e => new { e.ParticipantId, e.IsPrimaryParentGuardian })
                    .HasName("IX_ParticipantParentGuardian")
                    .IsUnique();

                entity.HasIndex(e => new { e.ParentGuardianId, e.ParticipantId, e.IsPrimaryParentGuardian })
                    .HasName("IX_ParticipantParentGuardian_IsPrimaryParentGuardian_Covering");

                entity.HasIndex(e => new { e.ParentGuardianId, e.ParticipantId, e.ParticipantParentGuardianId })
                    .HasName("_dta_index_ParticipantParentGuardian_5_350624292__K2_K3_K1");

                entity.HasIndex(e => new { e.ParticipantId, e.ParticipantParentGuardianId, e.ParentGuardianId })
                    .HasName("IX_ParticipantParentGuardian_ParticipantID");

                entity.HasIndex(e => new { e.ChildRelationshipToOtherExplain, e.ParentGuardianId, e.IsPrimaryParentGuardian, e.ParticipantParentGuardianId })
                    .HasName("_dta_index_ParticipantParentGuardian_5_350624292__K2_K6_K1_10");

                entity.HasIndex(e => new { e.IsPrimaryParentGuardian, e.ParticipantId, e.ParticipantParentGuardianId, e.ParentGuardianId })
                    .HasName("IX_ParticipantParentGuardian_ParticipantParentGuardianID");

                entity.HasIndex(e => new { e.ParticipantId, e.IsPrimaryParentGuardian, e.ParticipantParentGuardianId, e.ParentGuardianId })
                    .HasName("IX_ParticipantParentGuardian_IsPrimaryParentGuardian");

                entity.HasIndex(e => new { e.ParticipantParentGuardianId, e.ParticipantId, e.ParentGuardianId, e.IsPrimaryParentGuardian })
                    .HasName("_dta_index_ParticipantParentGuardian_5_350624292__K2_K6_1_3");

                entity.HasIndex(e => new { e.IsPrimaryParentGuardian, e.DoNotCount, e.ChildRelationshipToParentGuardian, e.ParticipantId, e.ParticipantParentGuardianId, e.ParentGuardianId })
                    .HasName("_dta_index_ParticipantParentGuardian_5_350624292__K3_K1_K2_6_7_9");

                entity.Property(e => e.ParticipantParentGuardianId).HasColumnName("ParticipantParentGuardianID");

                entity.Property(e => e.ChildRelationshipToOtherExplain).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.FlagNumberOfFamilyMemberOverride).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParentGuardianId).HasColumnName("ParentGuardianID");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.ParentGuardian)
                    .WithMany(p => p.ParticipantParentGuardian)
                    .HasForeignKey(d => d.ParentGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantParentGuardian_ParentGuardian");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantParentGuardian)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantParentGuardian_Participant");
            });

            modelBuilder.Entity<ParticipantRace>(entity =>
            {
                entity.HasIndex(e => new { e.Race, e.ParticipantId })
                    .HasName("IX_ParticipantRace_ParticipantIDp");

                entity.Property(e => e.ParticipantRaceId).HasColumnName("ParticipantRaceID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantRaceNavigation)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRace_Participant");
            });

            modelBuilder.Entity<ParticipantRecord>(entity =>
            {
                entity.HasIndex(e => e.CenterProgramYearId);

                entity.HasIndex(e => e.FamilyAdvocateUserId);

                entity.HasIndex(e => new { e.FamilyAdvocateUserId, e.Status })
                    .HasName("IX_ParticipantRecord_Status_Include");

                entity.HasIndex(e => new { e.ParticipantId, e.Status });

                entity.HasIndex(e => new { e.ParticipantId, e.CenterProgramYearId, e.Status })
                    .HasName("IX_ParticipantRecord_Status_Covering1");

                entity.HasIndex(e => new { e.ParticipantId, e.EntryDate, e.Status });

                entity.HasIndex(e => new { e.ParticipantRecordId, e.CenterProgramYearId, e.Status })
                    .HasName("IX_ParticipantRecord_Status_Covering");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.ParticipantId, e.IsNew })
                    .HasName("IX_ParticipantRecord_IsNew_Covering");

                entity.HasIndex(e => new { e.EnrollmentTerminationDate, e.ParticipantId, e.ParticipantRecordId, e.Status, e.CenterProgramYearId })
                    .HasName("_dta_index_ParticipantRecord_5_414624520__K2_K1_K6_K3_96");

                entity.HasIndex(e => new { e.EntryDate, e.PrimaryOwnerUserId, e.SecondaryOwnerUserId, e.Status, e.CenterProgramYearId, e.EnrollmentDate, e.ParticipantId, e.ParticipantRecordId })
                    .HasName("IX_ParticipantRecord_PrimaryOwnerUserId");

                entity.HasIndex(e => new { e.EnrollmentTerminationReason, e.EnrollmentTerminationDate, e.Status, e.EnrollmentDate, e.EntryDate, e.FamilyAdvocateUserId, e.StatusLastUpdatedDate, e.ParticipationYear, e.ParticipantRecordId, e.ParticipantId })
                    .HasName("IX_ParticipantId_Covering");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.AcceptedDate).HasColumnType("date");

                entity.Property(e => e.AdditionalHealthRequirementsCompleteDate).HasColumnType("date");

                entity.Property(e => e.ApplicationIntiatedCompleteDate).HasColumnType("date");

                entity.Property(e => e.BirthdateDocumentationEnteredCompleteDate).HasColumnType("date");

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.CustomAdmissionForm1Date).HasColumnType("date");

                entity.Property(e => e.CustomAdmissionForm2Date).HasColumnType("date");

                entity.Property(e => e.CustomAdmissionForm3Date).HasColumnType("date");

                entity.Property(e => e.CustomAdmissionForm4Date).HasColumnType("date");

                entity.Property(e => e.CustomAdmissionForm5Date).HasColumnType("date");

                entity.Property(e => e.EligibilityCompleteDate).HasColumnType("date");

                entity.Property(e => e.EmergencyConsentCompleteDate).HasColumnType("date");

                entity.Property(e => e.EmergencyContactsCompleteDate).HasColumnType("date");

                entity.Property(e => e.EnrollmentDate).HasColumnType("date");

                entity.Property(e => e.EnrollmentTerminationDate).HasColumnType("datetime");

                entity.Property(e => e.EnrollmentTerminationReasonOther).HasMaxLength(100);

                entity.Property(e => e.EntryDate).HasColumnType("date");

                entity.Property(e => e.ExtendedDayCompleteCompleteDate).HasColumnType("date");

                entity.Property(e => e.FamilyApplicationCompleteDate).HasColumnType("date");

                entity.Property(e => e.FamilyInvolvementContractCompleteDate).HasColumnType("date");

                entity.Property(e => e.FamilyVolunteeringContractCompleteDate).HasColumnType("date");

                entity.Property(e => e.ImmunizationsCompleteDate).HasColumnType("date");

                entity.Property(e => e.InactiveDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.PermissionForProgramActivitiesCompleteDate).HasColumnType("date");

                entity.Property(e => e.PermissionToReleaseAndRequestCompleteDate).HasColumnType("date");

                entity.Property(e => e.PreEnrollmentDeniedDate).HasColumnType("date");

                entity.Property(e => e.PreEnrollmentDeniedReasonOther).HasMaxLength(1000);

                entity.Property(e => e.PreEnrollmentWaitlistDate).HasColumnType("date");

                entity.Property(e => e.ProofOfResidencyCompleteDate).HasColumnType("date");

                entity.Property(e => e.SelectionCriteriaCompleteDate).HasColumnType("date");

                entity.Property(e => e.SelectionCriteriaNotes).IsUnicode(false);

                entity.Property(e => e.StatusLastUpdatedDate).HasColumnType("date");

                entity.Property(e => e.SubmitforReviewUserDate).HasColumnType("date");

                entity.Property(e => e.ThirdYearEligibilityReadyforReviewDate).HasColumnType("date");

                entity.Property(e => e.TotalAddedDateOverride).HasColumnType("datetime");

                entity.Property(e => e.TotalAddedUserIdoverride).HasColumnName("TotalAddedUserIDOverride");

                entity.Property(e => e.UnsubmitForReviewUserDate).HasColumnType("date");

                entity.HasOne(d => d.CenterProgramYear)
                    .WithMany(p => p.ParticipantRecord)
                    .HasForeignKey(d => d.CenterProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecord_CenterProgramYear");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ParticipantRecordCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__Participa__Creat__100234CD");

                entity.HasOne(d => d.FamilyAdvocateUser)
                    .WithMany(p => p.ParticipantRecordFamilyAdvocateUser)
                    .HasForeignKey(d => d.FamilyAdvocateUserId)
                    .HasConstraintName("FK_ParticipantRecord_UserProfile2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ParticipantRecordModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK__Participa__Modif__10F65906");

                entity.HasOne(d => d.Participant)
                    .WithMany(p => p.ParticipantRecord)
                    .HasForeignKey(d => d.ParticipantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecord_Participant");

                entity.HasOne(d => d.PrimaryOwnerUser)
                    .WithMany(p => p.ParticipantRecordPrimaryOwnerUser)
                    .HasForeignKey(d => d.PrimaryOwnerUserId)
                    .HasConstraintName("FK_ParticipantRecord_UserProfile");

                entity.HasOne(d => d.SecondaryOwnerUser)
                    .WithMany(p => p.ParticipantRecordSecondaryOwnerUser)
                    .HasForeignKey(d => d.SecondaryOwnerUserId)
                    .HasConstraintName("FK_ParticipantRecord_UserProfile1");
            });

            modelBuilder.Entity<ParticipantRecordAlert>(entity =>
            {
                entity.Property(e => e.ParticipantRecordAlertId).HasColumnName("ParticipantRecordAlertID");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.AlertSetDate).HasColumnType("datetime2(0)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordAlert)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantAlert_Participant");
            });

            modelBuilder.Entity<ParticipantRecordAttendance>(entity =>
            {
                entity.HasIndex(e => new { e.ClassroomProgramYearId, e.Date })
                    .HasName("IX_ParticipantRecordAttendance_Date");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.ClassroomProgramYearId, e.Date })
                    .HasName("IX_ParticipantRecordAttendance")
                    .IsUnique();

                entity.HasIndex(e => new { e.Date, e.ClassroomProgramYearId, e.ParticipantRecordAttendanceId, e.ParticipantRecordId })
                    .HasName("_dta_index_ParticipantRecordAttendance_5_478624748__K4_K3_K1_K2");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.AttendanceCode, e.ParticipantRecordAttendanceId, e.Date })
                    .HasName("IX_ParticipantRecordAttendance_AttendanceCode");

                entity.Property(e => e.ParticipantRecordAttendanceId).HasColumnName("ParticipantRecordAttendanceID");

                entity.Property(e => e.AbsenceReasonNotes)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Amsnack).HasColumnName("AMSnack");

                entity.Property(e => e.ArrivalTime).HasColumnType("time(0)");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.DepartureTime).HasColumnType("time(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.Pmsnack).HasColumnName("PMSnack");

                entity.HasOne(d => d.ClassroomProgramYear)
                    .WithMany(p => p.ParticipantRecordAttendance)
                    .HasForeignKey(d => d.ClassroomProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordAttendance_ClassroomProgramYear");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordAttendance)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordAttendance_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordCaseNote>(entity =>
            {
                entity.Property(e => e.ParticipantRecordCaseNoteId).HasColumnName("ParticipantRecordCaseNoteID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("datetime2(0)");

                entity.Property(e => e.ForeignNoteId)
                    .HasColumnName("Foreign_NoteID")
                    .HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PirservicesReceived1).HasColumnName("PIRServicesReceived1");

                entity.Property(e => e.PirservicesReceived2).HasColumnName("PIRServicesReceived2");

                entity.Property(e => e.PirservicesReceived3).HasColumnName("PIRServicesReceived3");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.ParticipantRecordCaseNote)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CaseNote_UserProfile");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordCaseNote)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordCaseNote_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordCaseNoteFollowUp>(entity =>
            {
                entity.Property(e => e.ParticipantRecordCaseNoteFollowUpId).HasColumnName("ParticipantRecordCaseNoteFollowUpID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.ParticipantRecordCaseNoteId).HasColumnName("ParticipantRecordCaseNoteID");

                entity.HasOne(d => d.ParticipantRecordCaseNote)
                    .WithMany(p => p.ParticipantRecordCaseNoteFollowUp)
                    .HasForeignKey(d => d.ParticipantRecordCaseNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CaseNoteFollwUp_UserProfile");
            });

            modelBuilder.Entity<ParticipantRecordClassroomProgramYear>(entity =>
            {
                entity.HasIndex(e => e.ClassroomProgramYearId);

                entity.HasIndex(e => e.ParticipantRecordId)
                    .HasName("IX_ParticipantRecordClassroomProgramYear")
                    .IsUnique();

                entity.HasIndex(e => new { e.ClassroomProgramYearId, e.ParticipantRecordId })
                    .HasName("_dta_index_ParticipantRecordClassroomProgra_5_574625090__K3_K2");

                entity.HasIndex(e => new { e.ParticipantRecordClassroomProgramYearId, e.ParticipantRecordId, e.ClassroomProgramYearId })
                    .HasName("UniqueInsertColumns")
                    .IsUnique();

                entity.HasIndex(e => new { e.ParticipantRecordId, e.ParticipantRecordClassroomProgramYearId, e.ClassroomProgramYearId })
                    .HasName("_dta_index_ParticipantRecordClassroomProgra_5_574625090__K2_K1_K3");

                entity.HasIndex(e => new { e.ApplicationDate, e.FirstDayInNewClassroom, e.ParticipantRecordId, e.ParticipantRecordClassroomProgramYearId, e.ClassroomProgramYearId })
                    .HasName("_dta_index_ParticipantRecordClassroomProgra_5_574625090__K2_K1_K3_5_9");

                entity.Property(e => e.ParticipantRecordClassroomProgramYearId).HasColumnName("ParticipantRecordClassroomProgramYearID");

                entity.Property(e => e.ApplicationDate).HasColumnType("date");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryDate).HasColumnType("date");

                entity.Property(e => e.FirstDayInNewClassroom).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PreviousClassroomProgramYearId).HasColumnName("PreviousClassroomProgramYearID");

                entity.HasOne(d => d.ClassroomProgramYear)
                    .WithMany(p => p.ParticipantRecordClassroomProgramYear)
                    .HasForeignKey(d => d.ClassroomProgramYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordClassroomProgramYear_ClassroomProgramYear");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ParticipantRecordClassroomProgramYearCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__Participa__Creat__3A2E75E4");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ParticipantRecordClassroomProgramYearModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK__Participa__Modif__3B229A1D");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithOne(p => p.ParticipantRecordClassroomProgramYear)
                    .HasForeignKey<ParticipantRecordClassroomProgramYear>(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordClassroomProgramYear_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationChallengingBehaviour>(entity =>
            {
                entity.HasIndex(e => e.ParticipantRecordId)
                    .HasName("IX_ParticipantRecordEducationChallengingBehaviour_ParticipantRecordId");

                entity.Property(e => e.ParticipantRecordEducationChallengingBehaviourId).HasColumnName("ParticipantRecordEducationChallengingBehaviourID");

                entity.Property(e => e.ChallengingBehaviour)
                    .IsRequired()
                    .HasMaxLength(130);

                entity.Property(e => e.ChallengingBehaviourOther).HasMaxLength(20);

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.DirectorStrategiesOther).HasMaxLength(25);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LocationOther).HasMaxLength(20);

                entity.Property(e => e.ParentInput)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ParentNotificationViaOther).HasMaxLength(25);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PrecedingBehaviour)
                    .IsRequired()
                    .HasMaxLength(130);

                entity.Property(e => e.PrecedingBehaviourOther).HasMaxLength(20);

                entity.Property(e => e.Strategies).HasMaxLength(2000);

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationChallengingBehaviour)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationChallengingBehaviour_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationEducation>(entity =>
            {
                entity.HasIndex(e => new { e.TransitioningEndofYearOverride, e.TransitioningEndofYear, e.ParticipantRecordId })
                    .HasName("IX_ParticipantRecordEducationEducation_ParticipantRecordID_Covering");

                entity.Property(e => e.ParticipantRecordEducationEducationId).HasColumnName("ParticipantRecordEducationEducationID");

                entity.Property(e => e.FirstPpvtdate).HasColumnName("FirstPPVTDate");

                entity.Property(e => e.FirstPpvtscore).HasColumnName("FirstPPVTScore");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.SecondPpvtdate).HasColumnName("SecondPPVTDate");

                entity.Property(e => e.SecondPpvtscore).HasColumnName("SecondPPVTScore");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationEducation)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationEducation_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationHomeBasedSocialization>(entity =>
            {
                entity.HasIndex(e => new { e.FatherFigureInvolved, e.HomeBasedSocializationDate, e.ParticipantRecordId })
                    .HasName("_dta_index_ParticipantRecordEducationHomeBa_5_670625432__K34_K3_K2");

                entity.Property(e => e.ParticipantRecordEducationHomeBasedSocializationId).HasColumnName("ParticipantRecordEducationHomeBasedSocializationID");

                entity.Property(e => e.HomeBasedSocializationDate).HasColumnType("datetime2(0)");

                entity.Property(e => e.HomeBasedSocializationNotes).HasMaxLength(2000);

                entity.Property(e => e.HomeBasedSocializationPirservices)
                    .HasColumnName("HomeBasedSocializationPIRServices")
                    .HasMaxLength(150);

                entity.Property(e => e.HomeBasedSocializationPirservicesAdultEducation).HasColumnName("HomeBasedSocializationPIRServices_AdultEducation");

                entity.Property(e => e.HomeBasedSocializationPirservicesAssetBuilding).HasColumnName("HomeBasedSocializationPIRServices_AssetBuilding");

                entity.Property(e => e.HomeBasedSocializationPirservicesAssistanceToFamiliesOfIncarceratedIndividuals).HasColumnName("HomeBasedSocializationPIRServices_AssistanceToFamiliesOfIncarceratedIndividuals");

                entity.Property(e => e.HomeBasedSocializationPirservicesChildAbuseAndNeglectServices).HasColumnName("HomeBasedSocializationPIRServices_ChildAbuseAndNeglectServices");

                entity.Property(e => e.HomeBasedSocializationPirservicesChildSupportAssistance).HasColumnName("HomeBasedSocializationPIRServices_ChildSupportAssistance");

                entity.Property(e => e.HomeBasedSocializationPirservicesDomesticViolenceServices).HasColumnName("HomeBasedSocializationPIRServices_DomesticViolenceServices");

                entity.Property(e => e.HomeBasedSocializationPirservicesEmergencyOrCrisisIntervention).HasColumnName("HomeBasedSocializationPIRServices_EmergencyOrCrisisIntervention");

                entity.Property(e => e.HomeBasedSocializationPirservicesEsltraining).HasColumnName("HomeBasedSocializationPIRServices_ESLTraining");

                entity.Property(e => e.HomeBasedSocializationPirservicesHealthEducation).HasColumnName("HomeBasedSocializationPIRServices_HealthEducation");

                entity.Property(e => e.HomeBasedSocializationPirservicesHousingAssistance).HasColumnName("HomeBasedSocializationPIRServices_HousingAssistance");

                entity.Property(e => e.HomeBasedSocializationPirservicesJobTraining).HasColumnName("HomeBasedSocializationPIRServices_JobTraining");

                entity.Property(e => e.HomeBasedSocializationPirservicesMentalHealthServices).HasColumnName("HomeBasedSocializationPIRServices_MentalHealthServices");

                entity.Property(e => e.HomeBasedSocializationPirservicesParentingEducation).HasColumnName("HomeBasedSocializationPIRServices_ParentingEducation");

                entity.Property(e => e.HomeBasedSocializationPirservicesRelationshipOrMarriageEducation).HasColumnName("HomeBasedSocializationPIRServices_RelationshipOrMarriageEducation");

                entity.Property(e => e.HomeBasedSocializationPirservicesSubstanceAbusePrevention).HasColumnName("HomeBasedSocializationPIRServices_SubstanceAbusePrevention");

                entity.Property(e => e.HomeBasedSocializationPirservicesSubstanceAbuseTreatment).HasColumnName("HomeBasedSocializationPIRServices_SubstanceAbuseTreatment");

                entity.Property(e => e.HomeBasedSocializationPregnantWomenPirservicesInformationOnBenefitsOfBreastfeeding).HasColumnName("HomeBasedSocializationPregnantWomenPIRServices_InformationOnBenefitsOfBreastfeeding");

                entity.Property(e => e.HomeBasedSocializationPregnantWomenPirservicesPostpartumHealthCare).HasColumnName("HomeBasedSocializationPregnantWomenPIRServices_PostpartumHealthCare");

                entity.Property(e => e.HomeBasedSocializationPregnantWomenPirservicesPrenantalHealthCare).HasColumnName("HomeBasedSocializationPregnantWomenPIRServices_PrenantalHealthCare");

                entity.Property(e => e.HomeBasedSocializationPregnantWomenPirservicesPrenatalEducationOnFetalDevelopment).HasColumnName("HomeBasedSocializationPregnantWomenPIRServices_PrenatalEducationOnFetalDevelopment");

                entity.Property(e => e.HomeBasedSocializationPregnantWomenServices).HasMaxLength(75);

                entity.Property(e => e.HomeBasedSocializationTheme)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationHomeBasedSocialization)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationHomeBasedSocialization_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationHomeBasedVisitDocumentation>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantRecordId, e.HomeBasedVisitDate });

                entity.HasIndex(e => new { e.FatherFigureInvolved, e.HomeBasedVisitDate, e.ParticipantRecordId })
                    .HasName("_dta_index_ParticipantRecordEducationHomeBa_5_702625546__K45_K3_K2");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.HomeBasedVisitPirservices, e.HomeBasedVisitDate })
                    .HasName("IX_ParticipantRecordEducationHomeBasedVisitDocumentation_HomeBasedVisitDate_Covering");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.HomeBasedVisitDate, e.ParticipantRecordEducationHomeBasedVisitDocumentationId, e.FatherFigureInvolved })
                    .HasName("_dta_index_ParticipantRecordEducationHomeBa_5_702625546__K2_K3_K1_K45");

                entity.Property(e => e.ParticipantRecordEducationHomeBasedVisitDocumentationId).HasColumnName("ParticipantRecordEducationHomeBasedVisitDocumentationID");

                entity.Property(e => e.HomeBasedVisitDate).HasColumnType("datetime2(0)");

                entity.Property(e => e.HomeBasedVisitDocumentationNotes).HasMaxLength(2000);

                entity.Property(e => e.HomeBasedVisitObjectivesObserved).HasMaxLength(2000);

                entity.Property(e => e.HomeBasedVisitParentInvolvement).HasMaxLength(2000);

                entity.Property(e => e.HomeBasedVisitPirservices)
                    .HasColumnName("HomeBasedVisitPIRServices")
                    .HasMaxLength(150);

                entity.Property(e => e.HomeBasedVisitPirservicesAdultEducation).HasColumnName("HomeBasedVisitPIRServices_AdultEducation");

                entity.Property(e => e.HomeBasedVisitPirservicesAssetBuilding).HasColumnName("HomeBasedVisitPIRServices_AssetBuilding");

                entity.Property(e => e.HomeBasedVisitPirservicesAssistanceToFamiliesOfIncarceratedIndividuals).HasColumnName("HomeBasedVisitPIRServices_AssistanceToFamiliesOfIncarceratedIndividuals");

                entity.Property(e => e.HomeBasedVisitPirservicesChildAbuseAndNeglectServices).HasColumnName("HomeBasedVisitPIRServices_ChildAbuseAndNeglectServices");

                entity.Property(e => e.HomeBasedVisitPirservicesChildSupportAssistance).HasColumnName("HomeBasedVisitPIRServices_ChildSupportAssistance");

                entity.Property(e => e.HomeBasedVisitPirservicesDomesticViolenceServices).HasColumnName("HomeBasedVisitPIRServices_DomesticViolenceServices");

                entity.Property(e => e.HomeBasedVisitPirservicesEmergencyOrCrisisIntervention).HasColumnName("HomeBasedVisitPIRServices_EmergencyOrCrisisIntervention");

                entity.Property(e => e.HomeBasedVisitPirservicesEsltraining).HasColumnName("HomeBasedVisitPIRServices_ESLTraining");

                entity.Property(e => e.HomeBasedVisitPirservicesHealthEducation).HasColumnName("HomeBasedVisitPIRServices_HealthEducation");

                entity.Property(e => e.HomeBasedVisitPirservicesHousingAssistance).HasColumnName("HomeBasedVisitPIRServices_HousingAssistance");

                entity.Property(e => e.HomeBasedVisitPirservicesJobTraining).HasColumnName("HomeBasedVisitPIRServices_JobTraining");

                entity.Property(e => e.HomeBasedVisitPirservicesMentalHealthServices).HasColumnName("HomeBasedVisitPIRServices_MentalHealthServices");

                entity.Property(e => e.HomeBasedVisitPirservicesParentingEducation).HasColumnName("HomeBasedVisitPIRServices_ParentingEducation");

                entity.Property(e => e.HomeBasedVisitPirservicesRelationshipOrMarriageEducation).HasColumnName("HomeBasedVisitPIRServices_RelationshipOrMarriageEducation");

                entity.Property(e => e.HomeBasedVisitPirservicesSubstanceAbusePrevention).HasColumnName("HomeBasedVisitPIRServices_SubstanceAbusePrevention");

                entity.Property(e => e.HomeBasedVisitPirservicesSubstanceAbuseTreatment).HasColumnName("HomeBasedVisitPIRServices_SubstanceAbuseTreatment");

                entity.Property(e => e.HomeBasedVisitPregnantWomenPirservicesInformationOnBenefitsOfBreastfeeding).HasColumnName("HomeBasedVisitPregnantWomenPIRServices_InformationOnBenefitsOfBreastfeeding");

                entity.Property(e => e.HomeBasedVisitPregnantWomenPirservicesPostpartumHealthCare).HasColumnName("HomeBasedVisitPregnantWomenPIRServices_PostpartumHealthCare");

                entity.Property(e => e.HomeBasedVisitPregnantWomenPirservicesPrenantalHealthCare).HasColumnName("HomeBasedVisitPregnantWomenPIRServices_PrenantalHealthCare");

                entity.Property(e => e.HomeBasedVisitPregnantWomenPirservicesPrenataleducationOnFetalDevelopment).HasColumnName("HomeBasedVisitPregnantWomenPIRServices_PrenataleducationOnFetalDevelopment");

                entity.Property(e => e.HomeBasedVisitPregnantWomenServices).HasMaxLength(75);

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomain).HasMaxLength(50);

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsApproachesToLearningAndPlay).HasColumnName("HomeBasedVisitSchoolReadinessDomains_ApproachesToLearningAndPlay");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsLanguageAndLiteracy).HasColumnName("HomeBasedVisitSchoolReadinessDomains_LanguageAndLiteracy");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsMathAndScience).HasColumnName("HomeBasedVisitSchoolReadinessDomains_MathAndScience");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsPhysical).HasColumnName("HomeBasedVisitSchoolReadinessDomains_Physical");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsSocialEmotional).HasColumnName("HomeBasedVisitSchoolReadinessDomains_SocialEmotional");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsSocialStudies).HasColumnName("HomeBasedVisitSchoolReadinessDomains_SocialStudies");

                entity.Property(e => e.HomeBasedVisitSchoolReadinessDomainsTheArts).HasColumnName("HomeBasedVisitSchoolReadinessDomains_TheArts");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.Twoweekpostpartumvisit).HasColumnName("twoweekpostpartumvisit");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationHomeBasedVisitDocumentation)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationHomeBasedVisitDocumentation_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationHomeBasedVisits>(entity =>
            {
                entity.HasIndex(e => e.ParticipantRecordId);

                entity.Property(e => e.ParticipantRecordEducationHomeBasedVisitsId).HasColumnName("ParticipantRecordEducationHomeBasedVisitsID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PlanningHomeBasedVisitNotes).HasMaxLength(2000);

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationHomeBasedVisits)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationHomeBasedVisits_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationHomeVisitDocumentation>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantRecordId, e.FatherFigureInvolved, e.EducationHvptcdocumentationDate })
                    .HasName("_dta_index_ParticipantRecordEducationHomeVi_5_766625774__K2_K13_K5");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.EducationHvptcdocumentationDate, e.ParticipantRecordEducationHomeVisitDocumentationId, e.FatherFigureInvolved })
                    .HasName("_dta_index_ParticipantRecordEducationHomeVi_5_766625774__K2_K5_K1_K13");

                entity.HasIndex(e => new { e.EducationHvptcdocumentationLocation, e.EducationHvptcdocumentationOtherDescription, e.HomeVisit, e.LastModified, e.ParticipantRecordEducationHomeVisitDocumentationId, e.User, e.CreatedOn, e.EducationDocumentationHvptcnotes, e.EducationHvptcdocumentation, e.EducationHvptcdocumentationDate, e.ParticipantRecordId })
                    .HasName("IX_ParticipantRecordEducationHomeVisitDocumentation_ParticipantRecordID");

                entity.Property(e => e.ParticipantRecordEducationHomeVisitDocumentationId).HasColumnName("ParticipantRecordEducationHomeVisitDocumentationID");

                entity.Property(e => e.EducationDocumentationHvptcnotes)
                    .HasColumnName("EducationDocumentationHVPTCNotes")
                    .HasMaxLength(2000);

                entity.Property(e => e.EducationHvptcdocumentation).HasColumnName("EducationHVPTCDocumentation");

                entity.Property(e => e.EducationHvptcdocumentationDate).HasColumnName("EducationHVPTCDocumentationDate");

                entity.Property(e => e.EducationHvptcdocumentationLocation).HasColumnName("EducationHVPTCDocumentationLocation");

                entity.Property(e => e.EducationHvptcdocumentationOtherDescription)
                    .HasColumnName("EducationHVPTCDocumentationOtherDescription")
                    .HasMaxLength(30);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PeeractivityModeling).HasColumnName("PEERActivityModeling");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationHomeVisitDocumentation)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationHomeVisitDocumentation_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationHomeVisits>(entity =>
            {
                entity.Property(e => e.ParticipantRecordEducationHomeVisitsId).HasColumnName("ParticipantRecordEducationHomeVisitsID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PlanningEducationHvptcattemptDate).HasColumnName("PlanningEducationHVPTCAttemptDate");

                entity.Property(e => e.PlanningEducationHvptcattemptReason).HasColumnName("PlanningEducationHVPTCAttemptReason");

                entity.Property(e => e.PlanningEducationHvptcnotes)
                    .HasColumnName("PlanningEducationHVPTCNotes")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PlanningEducationHvptcstatus).HasColumnName("PlanningEducationHVPTCStatus");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationHomeVisits)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationHomeVisits_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEducationIncident>(entity =>
            {
                entity.HasIndex(e => new { e.AdministrationNotified, e.CreatedOn, e.LastModified, e.ParentNotificationDateTime, e.ParentNotificationVia, e.ParentNotificationViaOther, e.ParticipantRecordId })
                    .HasName("IX_ParticipantRecordEducationIncident");

                entity.Property(e => e.ParticipantRecordEducationIncidentId).HasColumnName("ParticipantRecordEducationIncidentID");

                entity.Property(e => e.ActionTaken).HasMaxLength(2000);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateOfEvent).HasColumnType("datetime2(0)");

                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LocationOther).HasMaxLength(20);

                entity.Property(e => e.ParentNotificationDateTime).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParentNotificationViaOther).HasMaxLength(50);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.Symptoms).HasMaxLength(200);

                entity.Property(e => e.SymptomsOther).HasMaxLength(100);

                entity.Property(e => e.WitnessOther1).HasMaxLength(25);

                entity.Property(e => e.WitnessOther2).HasMaxLength(25);

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEducationIncident)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEducationIncident_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordEligibility>(entity =>
            {
                entity.HasIndex(e => new { e.AgeAtApplication, e.CategoricallyEligibleType, e.PrimaryParentGuardianIncomeNoneConfirmation, e.PrimaryParentGuardianIncomeNoneExplanation, e.PrimaryParentGuardianIncomeNotes, e.PrimaryParentGuardianIncomeSource, e.PrimaryParentGuardianIncomeSourceDate, e.HomelessStatus, e.LivesInServiceArea, e.ParticipantRecordEligibilityId, e.PrimaryParentGuardianIncome, e.PrimaryParentGuardianIncomeAnnualized, e.PrimaryParentGuardianIncomeFrequency, e.ParticipantRecordId, e.SignerUserId })
                    .HasName("IX_ParticipantRecordEligibility_SignerUserId");

                entity.Property(e => e.ParticipantRecordEligibilityId).HasColumnName("ParticipantRecordEligibilityID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FamilyAdvocateSupervisorSignOffDate).HasColumnType("date");

                entity.Property(e => e.IncomeNoneExplanation).HasMaxLength(1000);

                entity.Property(e => e.IncomePercentageFederalPoverty).HasDefaultValueSql("((0.00))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.OtherParentGuardianIncomeNoneExplanation1).HasMaxLength(500);

                entity.Property(e => e.OtherParentGuardianIncomeNoneExplanation2).HasMaxLength(500);

                entity.Property(e => e.OtherParentGuardianIncomeNotes1).HasMaxLength(1000);

                entity.Property(e => e.OtherParentGuardianIncomeNotes2).HasMaxLength(1000);

                entity.Property(e => e.OtherParentGuardianIncomeSourceDate1).HasColumnType("date");

                entity.Property(e => e.OtherParentGuardianIncomeSourceDate2).HasColumnType("date");

                entity.Property(e => e.OtherParentGuardianIncomeSourceOther1)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.OtherParentGuardianIncomeSourceOther2)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ParentGuardianSignatureDate).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PrimaryParentGuardianIncomeNoneExplanation).HasMaxLength(500);

                entity.Property(e => e.PrimaryParentGuardianIncomeNotes).HasMaxLength(1000);

                entity.Property(e => e.PrimaryParentGuardianIncomeSourceDate).HasColumnType("date");

                entity.Property(e => e.PrimaryParentGuardianIncomeSourceOther)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryParentGuardianIncomeNoneExplanation).HasMaxLength(500);

                entity.Property(e => e.SecondaryParentGuardianIncomeNotes).HasMaxLength(1000);

                entity.Property(e => e.SecondaryParentGuardianIncomeSourceDate).HasColumnType("date");

                entity.Property(e => e.SecondaryParentGuardianIncomeSourceOther)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ThirdYearFamilyAdvocateSupervisorSignOffDate).HasColumnType("date");

                entity.Property(e => e.UserSignOffDate).HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEligibility)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEligibility_ParticipantRecord");

                entity.HasOne(d => d.SignerUser)
                    .WithMany(p => p.ParticipantRecordEligibility)
                    .HasForeignKey(d => d.SignerUserId)
                    .HasConstraintName("FK_ParticipantRecordEligibility_UserProfile");
            });

            modelBuilder.Entity<ParticipantRecordEnrollmentHistory>(entity =>
            {
                entity.Property(e => e.ParticipantRecordEnrollmentHistoryId).HasColumnName("ParticipantRecordEnrollmentHistoryID");

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.EnrollmentDate).HasColumnType("date");

                entity.Property(e => e.EnrollmentTerminationDate).HasColumnType("datetime");

                entity.Property(e => e.EnrollmentTerminationReasonId).HasColumnName("EnrollmentTerminationReasonID");

                entity.Property(e => e.EnrollmentTerminationReasonIdchangeIndicator).HasColumnName("EnrollmentTerminationReasonIDChangeIndicator");

                entity.Property(e => e.EnrollmentTerminationReasonOther).HasMaxLength(100);

                entity.Property(e => e.EntryDate).HasColumnType("date");

                entity.Property(e => e.FirstDayInNewClassroom).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.StatusIdchangeIndicator).HasColumnName("StatusIDChangeIndicator");

                entity.Property(e => e.TransactionDateAndTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ParticipantRecordEnrollmentTransactionHistory>(entity =>
            {
                entity.HasIndex(e => e.ParticipantRecordId);

                entity.Property(e => e.ParticipantRecordEnrollmentTransactionHistoryId).HasColumnName("ParticipantRecordEnrollmentTransactionHistoryID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.TransactionDate).HasColumnType("date");

                entity.Property(e => e.TransactionNote).HasMaxLength(500);

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordEnrollmentTransactionHistory)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEnrollmentTransactionHistory_ParticipantRecord");

                entity.HasOne(d => d.TransactionUser)
                    .WithMany(p => p.ParticipantRecordEnrollmentTransactionHistory)
                    .HasForeignKey(d => d.TransactionUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordEnrollmentTransactionHistory_UserProfile");
            });

            modelBuilder.Entity<ParticipantRecordExtendedDayBilling>(entity =>
            {
                entity.Property(e => e.ParticipantRecordExtendedDayBillingId).HasColumnName("ParticipantRecordExtendedDayBillingID");

                entity.Property(e => e.AmountPerInterval).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordExtendedDayBilling)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayBilling_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordExtendedDayCredit>(entity =>
            {
                entity.Property(e => e.ParticipantRecordExtendedDayCreditId).HasColumnName("ParticipantRecordExtendedDayCreditID");

                entity.Property(e => e.AmountToApply).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.TimePeriodtoApplyCreditTo).HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordExtendedDayCredit)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayCredit_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordExtendedDayFollowUp>(entity =>
            {
                entity.Property(e => e.ParticipantRecordExtendedDayFollowUpId).HasColumnName("ParticipantRecordExtendedDayFollowUpID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.TimePeriodtoApplyFollowUpTo).HasColumnType("date");

                entity.HasOne(d => d.OwnerUser)
                    .WithMany(p => p.ParticipantRecordExtendedDayFollowUp)
                    .HasForeignKey(d => d.OwnerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayFollowUp_UserProfile");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordExtendedDayFollowUp)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayFollowUp_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordExtendedDayPayment>(entity =>
            {
                entity.Property(e => e.ParticipantRecordExtendedDayPaymentId).HasColumnName("ParticipantRecordExtendedDayPaymentID");

                entity.Property(e => e.AmountToApply).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Notes).HasMaxLength(1000);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.TimePeriodtoApplyPaymentTo).HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordExtendedDayPayment)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayPayment_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordExtendedDayVoucher>(entity =>
            {
                entity.Property(e => e.ParticipantRecordExtendedDayVoucherId).HasColumnName("ParticipantRecordExtendedDayVoucherID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.IssuerState)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordExtendedDayVoucher)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordExtendedDayVoucher_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordFamilyAdvocate>(entity =>
            {
                entity.Property(e => e.ParticipantRecordFamilyAdvocateId).HasColumnName("ParticipantRecordFamilyAdvocateID");

                entity.Property(e => e.AdvocateAssignmentDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.AdvocateAssignmentEnteredByUser)
                    .WithMany(p => p.ParticipantRecordFamilyAdvocateAdvocateAssignmentEnteredByUser)
                    .HasForeignKey(d => d.AdvocateAssignmentEnteredByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordFamilyAdvocate_UserProfile1");

                entity.HasOne(d => d.FamilyAdvocateUser)
                    .WithMany(p => p.ParticipantRecordFamilyAdvocateFamilyAdvocateUser)
                    .HasForeignKey(d => d.FamilyAdvocateUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordFamilyAdvocate_UserProfile");
            });

            modelBuilder.Entity<ParticipantRecordFamilyStrengthsAssessment>(entity =>
            {
                entity.Property(e => e.ParticipantRecordFamilyStrengthsAssessmentId).HasColumnName("ParticipantRecordFamilyStrengthsAssessmentID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DeclinedDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordFamilyStrengthsAssessment)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FamilyStrengthsAssessment_Participant");
            });

            modelBuilder.Entity<ParticipantRecordFile>(entity =>
            {
                entity.Property(e => e.ParticipantRecordFileId).HasColumnName("ParticipantRecordFileID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Idtype).HasColumnName("IDType");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.ParticipantRecordFile)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantFile_Participant");
            });

            modelBuilder.Entity<ParticipantRecordGoal>(entity =>
            {
                entity.Property(e => e.ParticipantRecordGoalId).HasColumnName("ParticipantRecordGoalID");

                entity.Property(e => e.Benchmark1CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark2CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark3CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark4CompletedDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FamilyEngagementOutcome1).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome2).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome3).HasMaxLength(100);

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.InitiatedDate).HasColumnType("date");

                entity.Property(e => e.IsActiveDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Objective1).HasMaxLength(1000);

                entity.Property(e => e.Objective1TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective2).HasMaxLength(1000);

                entity.Property(e => e.Objective2TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective3).HasMaxLength(1000);

                entity.Property(e => e.Objective3TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective4).HasMaxLength(1000);

                entity.Property(e => e.Objective4TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective5).HasMaxLength(1000);

                entity.Property(e => e.Objective5TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther1).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther1TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther2).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther2TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther3).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther3TargetDate).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PirservicesReceived1)
                    .HasColumnName("PIRServicesReceived1")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived1Date)
                    .HasColumnName("PIRServicesReceived1Date")
                    .HasColumnType("date");

                entity.Property(e => e.PirservicesReceived2)
                    .HasColumnName("PIRServicesReceived2")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived2Date)
                    .HasColumnName("PIRServicesReceived2Date")
                    .HasColumnType("date");

                entity.Property(e => e.PirservicesReceived3)
                    .HasColumnName("PIRServicesReceived3")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived3Date)
                    .HasColumnName("PIRServicesReceived3Date")
                    .HasColumnType("date");

                entity.Property(e => e.Resource1Address).HasMaxLength(100);

                entity.Property(e => e.Resource1ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource1GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource1Organization).HasMaxLength(100);

                entity.Property(e => e.Resource1Phone).HasMaxLength(30);

                entity.Property(e => e.Resource1TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource2Address).HasMaxLength(100);

                entity.Property(e => e.Resource2ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource2GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource2Organization).HasMaxLength(100);

                entity.Property(e => e.Resource2Phone).HasMaxLength(30);

                entity.Property(e => e.Resource2TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource3Address).HasMaxLength(100);

                entity.Property(e => e.Resource3ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource3GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource3Organization).HasMaxLength(100);

                entity.Property(e => e.Resource3Phone).HasMaxLength(30);

                entity.Property(e => e.Resource3TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource4Address).HasMaxLength(100);

                entity.Property(e => e.Resource4ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource4GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource4Organization).HasMaxLength(100);

                entity.Property(e => e.Resource4Phone).HasMaxLength(30);

                entity.Property(e => e.Resource4TakenDate).HasColumnType("date");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.ParticipantRecordGoal)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantGoal_UserProfile");

                entity.HasOne(d => d.Goal)
                    .WithMany(p => p.ParticipantRecordGoal)
                    .HasForeignKey(d => d.GoalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantGoal_Goal");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordGoal)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantGoal_Participant");
            });

            modelBuilder.Entity<ParticipantRecordGoalFollowUp>(entity =>
            {
                entity.Property(e => e.ParticipantRecordGoalFollowUpId).HasColumnName("ParticipantRecordGoalFollowUpID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.ParticipantRecordGoalId).HasColumnName("ParticipantRecordGoalID");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.ParticipantRecordGoalFollowUp)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantGoalFollowUp_UserProfile");

                entity.HasOne(d => d.ParticipantRecordGoal)
                    .WithMany(p => p.ParticipantRecordGoalFollowUp)
                    .HasForeignKey(d => d.ParticipantRecordGoalId)
                    .HasConstraintName("FK_ParticipantGoalFollowUp_ParticipantGoal");
            });

            modelBuilder.Entity<ParticipantRecordHealthInfo>(entity =>
            {
                entity.HasIndex(e => e.ParticipantRecordId)
                    .HasName("IX_ParticipantRecordHealthInfo")
                    .IsUnique();

                entity.Property(e => e.ParticipantRecordHealthInfoId).HasColumnName("ParticipantRecordHealthInfoID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DentalHome).HasMaxLength(500);

                entity.Property(e => e.DentalHomeDateofDetermination).HasColumnType("date");

                entity.Property(e => e.DentalHomeGoalRecentDate).HasColumnType("date");

                entity.Property(e => e.FamilyInsuranceDateofDetermination).HasColumnType("date");

                entity.Property(e => e.FamilyInsuranceGoalRecentDate).HasColumnType("date");

                entity.Property(e => e.HealthInsuranceDateofDetermination).HasColumnType("date");

                entity.Property(e => e.HealthInsuranceEndOfEnrollment).HasDefaultValueSql("(N'-')");

                entity.Property(e => e.HealthInsuranceEndOfEnrollmentOther).HasMaxLength(500);

                entity.Property(e => e.HealthInsuranceEnrollment).HasDefaultValueSql("(N'-')");

                entity.Property(e => e.HealthInsuranceEnrollmentOther).HasMaxLength(500);

                entity.Property(e => e.HealthInsuranceGoalRecentDate).HasColumnType("date");

                entity.Property(e => e.InsuranceNo).HasMaxLength(100);

                entity.Property(e => e.InsuranceProvider).HasMaxLength(100);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.MedicalHome).HasMaxLength(500);

                entity.Property(e => e.MedicalHomeDateofDetermination).HasColumnType("date");

                entity.Property(e => e.MedicalHomeGoalRecentDate).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PregnancyActualDeliveryDate).HasColumnType("date");

                entity.Property(e => e.PregnancyExpectedDueDate).HasColumnType("date");

                entity.Property(e => e.PreventiveDental).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithOne(p => p.ParticipantRecordHealthInfo)
                    .HasForeignKey<ParticipantRecordHealthInfo>(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordHealthInfo_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordHealthPlanning>(entity =>
            {
                entity.HasIndex(e => e.ParticipantRecordId);

                entity.Property(e => e.ParticipantRecordHealthPlanningId).HasColumnName("ParticipantRecordHealthPlanningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.PlanningDate).HasColumnType("date");

                entity.Property(e => e.PlanningNote).HasMaxLength(2000);

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordHealthPlanning)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordHealthPlanning_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordHealthPlanningHistory>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantRecordHealthPlanningHistoryId, e.ParticipantRecordHealthPlanningId })
                    .HasName("IX_ParticipantRecordHealthPlanningHistory_ParticipantRecordHealthPlanningID_Include");

                entity.Property(e => e.ParticipantRecordHealthPlanningHistoryId).HasColumnName("ParticipantRecordHealthPlanningHistoryID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.HealthInformationId).HasColumnName("HealthInformationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordHealthPlanningId).HasColumnName("ParticipantRecordHealthPlanningID");

                entity.HasOne(d => d.ParticipantRecordHealthPlanning)
                    .WithMany(p => p.ParticipantRecordHealthPlanningHistory)
                    .HasForeignKey(d => d.ParticipantRecordHealthPlanningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordHealthPlanningHistory_ParticipantRecordHealthPlanning");
            });

            modelBuilder.Entity<ParticipantRecordImmunization>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantRecordId, e.ImmunizationId })
                    .HasName("IX_ParticipantRecordImmunization")
                    .IsUnique();

                entity.Property(e => e.ParticipantRecordImmunizationId).HasColumnName("ParticipantRecordImmunizationID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.ExemptReason).HasMaxLength(1000);

                entity.Property(e => e.FifthDate).HasColumnType("date");

                entity.Property(e => e.FirstDate).HasColumnType("date");

                entity.Property(e => e.FourthDate).HasColumnType("date");

                entity.Property(e => e.ImmunizationId).HasColumnName("ImmunizationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.OverrideDate).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.SecondDate).HasColumnType("date");

                entity.Property(e => e.ThirdDate).HasColumnType("date");

                entity.HasOne(d => d.Immunization)
                    .WithMany(p => p.ParticipantRecordImmunization)
                    .HasForeignKey(d => d.ImmunizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordImmunization_Immunization");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordImmunization)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordImmunization_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordImmunizationPlanning>(entity =>
            {
                entity.Property(e => e.ParticipantRecordImmunizationPlanningId).HasColumnName("ParticipantRecordImmunizationPlanningID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.ImmunizationId).HasColumnName("ImmunizationID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.HasOne(d => d.Immunization)
                    .WithMany(p => p.ParticipantRecordImmunizationPlanning)
                    .HasForeignKey(d => d.ImmunizationId)
                    .HasConstraintName("FK_ParticipantRecordImmunizationPlanning_Immunization");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordImmunizationPlanning)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordImmunizationPlanning_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordImmunizationSetStatus>(entity =>
            {
                entity.HasIndex(e => new { e.EnrollmentStatusAtEnrollment, e.ParticipantRecordId })
                    .HasName("IX_ParticipantRecordImmunizationSetStatus_ParticipantRecordID_Covering");

                entity.Property(e => e.ParticipantRecordImmunizationSetStatusId).HasColumnName("ParticipantRecordImmunizationSetStatusID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.EnrollmentStatusAtEnrollmentUpdatedDate).HasColumnType("date");

                entity.Property(e => e.EnrollmentStatusEndOfEnrollmentUpdatedDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.EnrollmentStatusAtEnrollmentOwnerUser)
                    .WithMany(p => p.ParticipantRecordImmunizationSetStatusEnrollmentStatusAtEnrollmentOwnerUser)
                    .HasForeignKey(d => d.EnrollmentStatusAtEnrollmentOwnerUserId)
                    .HasConstraintName("FK_ParticipantRecordImmunizationSetStatus_UserProfile");

                entity.HasOne(d => d.EnrollmentStatusEndOfEnrollmentOwnerUser)
                    .WithMany(p => p.ParticipantRecordImmunizationSetStatusEnrollmentStatusEndOfEnrollmentOwnerUser)
                    .HasForeignKey(d => d.EnrollmentStatusEndOfEnrollmentOwnerUserId)
                    .HasConstraintName("FK_ParticipantRecordImmunizationSetStatus_UserProfile1");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordImmunizationSetStatus)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordImmunizationSetStatus_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordMetric>(entity =>
            {
                entity.HasIndex(e => e.MetricId)
                    .HasName("IX_ParticipantRecordMetric_1");

                entity.HasIndex(e => e.ParticipantRecordId);

                entity.HasIndex(e => new { e.ParticipantRecordId, e.MetricId })
                    .HasName("IX_ParticipantRecordMetric")
                    .IsUnique();

                entity.Property(e => e.ParticipantRecordMetricId).HasColumnName("ParticipantRecordMetricID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.MetricId).HasColumnName("MetricID");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.Metric)
                    .WithMany(p => p.ParticipantRecordMetric)
                    .HasForeignKey(d => d.MetricId)
                    .HasConstraintName("FK_ParticipantRecordMetric_Metric");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordMetric)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordMetric_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordPir>(entity =>
            {
                entity.HasKey(e => e.ParticipantRecordPirid)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("ParticipantRecordPIR");

                entity.HasIndex(e => e.ParticipantRecordId);

                entity.HasIndex(e => e.ParticipantRecordPirid);

                entity.HasIndex(e => e.ProgramYearId);

                entity.HasIndex(e => new { e.ParticipantRecordId, e.SystemParticipantRecordIncludedInPiryear })
                    .HasName("_dta_index_ParticipantRecordPIR_5_1494348438__K2_K10");

                entity.Property(e => e.ParticipantRecordPirid).HasColumnName("ParticipantRecordPIRID");

                entity.Property(e => e.A12OverrideAgeofChildatEnrollment).HasColumnName("A12_Override_AgeofChildatEnrollment");

                entity.Property(e => e.A12SystemAgeofChildatEnrollment).HasColumnName("A12_System_AgeofChildatEnrollment");

                entity.Property(e => e.A13ParticipantEnrolledasPregnantMom).HasColumnName("A13_ParticipantEnrolledasPregnantMom");

                entity.Property(e => e.A15SystemTypeofEligibility).HasColumnName("A15_System_TypeofEligibility");

                entity.Property(e => e.A17OverridePriorEnrollment).HasColumnName("A17_Override_PriorEnrollment");

                entity.Property(e => e.A17SystemPriorEnrollment).HasColumnName("A17_System_PriorEnrollment");

                entity.Property(e => e.A18DisplayCheckHeadStartTransitionedOutofProgram).HasColumnName("A18_DisplayCheck_HeadStartTransitionedOutofProgram");

                entity.Property(e => e.A18OverrideHeadStartTransitionedOutofProgram).HasColumnName("A18_Override_HeadStartTransitionedOutofProgram");

                entity.Property(e => e.A18SystemHeadStartTransitionedOutofProgram).HasColumnName("A18_System_HeadStartTransitionedOutofProgram");

                entity.Property(e => e.A18aOverrideHeadStartTransitionedOutofPrograminlessthan45days).HasColumnName("A18a_Override_HeadStartTransitionedOutofPrograminlessthan45days");

                entity.Property(e => e.A18aSystemHeadStartTransitionedOutofPrograminlessthan45days).HasColumnName("A18a_System_HeadStartTransitionedOutofPrograminlessthan45days");

                entity.Property(e => e.A18bDisplayCheckHeadStartProjectedToEnterKindergarten).HasColumnName("A18b_DisplayCheck_HeadStartProjectedToEnterKindergarten");

                entity.Property(e => e.A18bSystemHeadStartProjectedToEnterKindergarten).HasColumnName("A18b_System_HeadStartProjectedToEnterKindergarten");

                entity.Property(e => e.A19DisplayCheckEhstransitionedOutOfProgram).HasColumnName("A19_DisplayCheck_EHSTransitionedOutOfProgram");

                entity.Property(e => e.A19OverrideEhstransitionedOutOfProgram).HasColumnName("A19_Override_EHSTransitionedOutOfProgram");

                entity.Property(e => e.A19SystemEhstransitionedOutOfProgram).HasColumnName("A19_System_EHSTransitionedOutOfProgram");

                entity.Property(e => e.A19aOverrideEhstransitionedOutLessthan45Days).HasColumnName("A19a_Override_EHSTransitionedOutLessthan45Days");

                entity.Property(e => e.A19aSystemEhstransitionedOutLessthan45Days).HasColumnName("A19a_System_EHSTransitionedOutLessthan45Days");

                entity.Property(e => e.A19b1OverrideEhstransitionedOutEnteredHeadStart).HasColumnName("A19b1_Override_EHSTransitionedOutEnteredHeadStart");

                entity.Property(e => e.A19b1SystemEhstransitionedOutEnteredHeadStart).HasColumnName("A19b1_System_EHSTransitionedOutEnteredHeadStart");

                entity.Property(e => e.A19b2OverrideEhstransitionedOuttoAnotherChildhoodProgram).HasColumnName("A19b2_Override_EHSTransitionedOuttoAnotherChildhoodProgram");

                entity.Property(e => e.A19b2SystemEhstransitionedOuttoAnotherChildhoodProgram).HasColumnName("A19b2_System_EHSTransitionedOuttoAnotherChildhoodProgram");

                entity.Property(e => e.A19b3OverrideEhstransitionedOutNotToAnotherChildhood).HasColumnName("A19b3_Override_EHSTransitionedOut_NOT_toAnotherChildhood");

                entity.Property(e => e.A19b3SystemEhstransitionedOutNotToAnotherChildhood).HasColumnName("A19b3_System_EHSTransitionedOut_NOT_toAnotherChildhood");

                entity.Property(e => e.A19bOverrideEhstransitionedOutbyAge).HasColumnName("A19b_Override_EHSTransitionedOutbyAge");

                entity.Property(e => e.A19bSystemEhstransitionedOutbyAge).HasColumnName("A19b_System_EHSTransitionedOutbyAge");

                entity.Property(e => e.A20A21OverridePregnantMomEnrolledOrLeftBeforeBirth).HasColumnName("A20_A21_Override_PregnantMomEnrolledOrLeftBeforeBirth");

                entity.Property(e => e.A20A21SystemPregnantMomEnrolledOrLeftBeforeBirth).HasColumnName("A20_A21_System_PregnantMomEnrolledOrLeftBeforeBirth");

                entity.Property(e => e.A20b13OverrideAgedOutReason).HasColumnName("A20b13_Override_AgedOutReason");

                entity.Property(e => e.A20b13SystemAgedOutReason).HasColumnName("A20b13_System_AgedOutReason");

                entity.Property(e => e.A21OverridePregnantMomEnrolledAtDelivery).HasColumnName("A21_Override_PregnantMomEnrolledAtDelivery");

                entity.Property(e => e.A21SystemPregnantMomEnrolledAtDelivery).HasColumnName("A21_System_PregnantMomEnrolledAtDelivery");

                entity.Property(e => e.A21aA21bOverridePregnantMomEnrolledInfantEnrolledNotEnrolled).HasColumnName("A21a_A21b_Override_PregnantMomEnrolledInfantEnrolledNotEnrolled");

                entity.Property(e => e.A21aA21bSystemPregnantMomEnrolledInfantEnrolledNotEnrolled).HasColumnName("A21a_A21b_System_PregnantMomEnrolledInfantEnrolledNotEnrolled");

                entity.Property(e => e.A22DisplayCheckMigrantTransitionedOutofProgram).HasColumnName("A22_DisplayCheck_MigrantTransitionedOutofProgram");

                entity.Property(e => e.A22OverrideMigrantTransitionedOutofProgram).HasColumnName("A22_Override_MigrantTransitionedOutofProgram");

                entity.Property(e => e.A22SystemMigrantTransitionedOutofProgram).HasColumnName("A22_System_MigrantTransitionedOutofProgram");

                entity.Property(e => e.A22aOverrideMigrantTransitionedOutofPrograminlessthan45days).HasColumnName("A22a_Override_MigrantTransitionedOutofPrograminlessthan45days");

                entity.Property(e => e.A22aSystemMigrantTransitionedOutofPrograminlessthan45days).HasColumnName("A22a_System_MigrantTransitionedOutofPrograminlessthan45days");

                entity.Property(e => e.A22bDisplayCheckMigrantProjectedToEnterKindergarten).HasColumnName("A22b_DisplayCheck_MigrantProjectedToEnterKindergarten");

                entity.Property(e => e.A22bOverrideMigrantProjectedToEnterKindergarten).HasColumnName("A22b_Override_MigrantProjectedToEnterKindergarten");

                entity.Property(e => e.A22bSystemMigrantProjectedToEnterKindergarten).HasColumnName("A22b_System_MigrantProjectedToEnterKindergarten");

                entity.Property(e => e.A23OverrideReceivedChildCare).HasColumnName("A23_Override_ReceivedChildCare");

                entity.Property(e => e.A23SystemReceivedChildCare).HasColumnName("A23_System_ReceivedChildCare");

                entity.Property(e => e.A24OverrideEthnicity).HasColumnName("A24_Override_Ethnicity");

                entity.Property(e => e.A24SystemEthnicity).HasColumnName("A24_System_Ethnicity");

                entity.Property(e => e.A25OverrideRace).HasColumnName("A25_Override_Race");

                entity.Property(e => e.A25SystemRace).HasColumnName("A25_System_Race");

                entity.Property(e => e.A25g1OverrideRaceOtherNotes)
                    .HasColumnName("A25g1_Override_RaceOtherNotes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A25g1SystemRaceOtherNotes)
                    .HasColumnName("A25g1_System_RaceOtherNotes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A25h1OverrideRaceUnspecifiedNotes)
                    .HasColumnName("A25h1_Override_RaceUnspecifiedNotes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A25h1SystemRaceUnspecifiedNotes)
                    .HasColumnName("A25h1_System_RaceUnspecifiedNotes")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A26OverridePrimaryLanguageofFamily).HasColumnName("A26_Override_PrimaryLanguageofFamily");

                entity.Property(e => e.A26OverridePrimaryLanguageofFamilyOther)
                    .HasColumnName("A26_Override_Primary_LanguageofFamily_Other")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A26SystemPrimaryLanguageofFamily).HasColumnName("A26_System_PrimaryLanguageofFamily");

                entity.Property(e => e.A26SystemPrimaryLanguageofFamilyOther)
                    .HasColumnName("A26_System_Primary_LanguageofFamily_Other")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.A27OverrideParticipantReceivedTransportationServices).HasColumnName("A27_Override_ParticipantReceivedTransportationServices");

                entity.Property(e => e.A27SystemParticipantReceivedTransportationServices).HasColumnName("A27_System_ParticipantReceivedTransportationServices");

                entity.Property(e => e.C10adDisplayCheckChildBmi).HasColumnName("C10ad_DisplayCheck_ChildBMI");

                entity.Property(e => e.C10adOverrideChildBmi).HasColumnName("C10ad_Override_ChildBMI");

                entity.Property(e => e.C10adSystemChildBmi).HasColumnName("C10ad_System_ChildBMI");

                entity.Property(e => e.C1113OverrideChildImmunizationAtEnrollment).HasColumnName("C1113_Override_ChildImmunizationAtEnrollment");

                entity.Property(e => e.C1113OverrideChildImmunizationEndOfYear).HasColumnName("C1113_Override_ChildImmunizationEndOfYear");

                entity.Property(e => e.C1113SystemChildImmunizationAtEnrollment).HasColumnName("C1113_System_ChildImmunizationAtEnrollment");

                entity.Property(e => e.C1113SystemChildImmunizationEndOfYear).HasColumnName("C1113_System_ChildImmunizationEndOfYear");

                entity.Property(e => e.C14aOverridePregnantWomanPrenatalHealthCare).HasColumnName("C14a_Override_PregnantWomanPrenatalHealthCare");

                entity.Property(e => e.C14aSystemPregnantWomanPrenatalHealthCare).HasColumnName("C14a_System_PregnantWomanPrenatalHealthCare");

                entity.Property(e => e.C14bOverridePregnantWomanPostpartumHealthCare).HasColumnName("C14b_Override_PregnantWomanPostpartumHealthCare");

                entity.Property(e => e.C14bSystemPregnantWomanPostpartumHealthCare).HasColumnName("C14b_System_PregnantWomanPostpartumHealthCare");

                entity.Property(e => e.C14cOverridePregnantWomanMentalHealthServices).HasColumnName("C14c_Override_PregnantWomanMentalHealthServices");

                entity.Property(e => e.C14cSystemPregnantWomanMentalHealthServices).HasColumnName("C14c_System_PregnantWomanMentalHealthServices");

                entity.Property(e => e.C14dOverridePregnantWomanSubstanceAbusePrevention).HasColumnName("C14d_Override_PregnantWomanSubstanceAbusePrevention");

                entity.Property(e => e.C14dSystemPregnantWomanSubstanceAbusePrevention).HasColumnName("C14d_System_PregnantWomanSubstanceAbusePrevention");

                entity.Property(e => e.C14eOverridePregnantWomanSustanceAbuseTreatment).HasColumnName("C14e_Override_PregnantWomanSustanceAbuseTreatment");

                entity.Property(e => e.C14eSystemPregnantWomanSustanceAbuseTreatment).HasColumnName("C14e_System_PregnantWomanSustanceAbuseTreatment");

                entity.Property(e => e.C14fOverridePregnantWomanPrenatalEducationFetalDevelopment).HasColumnName("C14f_Override_PregnantWomanPrenatalEducationFetalDevelopment");

                entity.Property(e => e.C14fSystemPregnantWomanPrenatalEducationFetalDevelopment).HasColumnName("C14f_System_PregnantWomanPrenatalEducationFetalDevelopment");

                entity.Property(e => e.C14gOverridePregnantWomanBreastfeedingBenefitsInfo).HasColumnName("C14g_Override_PregnantWomanBreastfeedingBenefitsInfo");

                entity.Property(e => e.C14gSystemPregnantWomanBreastfeedingBenefitsInfo).HasColumnName("C14g_System_PregnantWomanBreastfeedingBenefitsInfo");

                entity.Property(e => e.C15aOverridePregnantWoman1stTrimesterServices).HasColumnName("C15a_Override_PregnantWoman1stTrimesterServices");

                entity.Property(e => e.C15aSystemPregnantWoman1stTrimesterServices).HasColumnName("C15a_System_PregnantWoman1stTrimesterServices");

                entity.Property(e => e.C15abcOverridePregnantWomanTrimesterServices).HasColumnName("C15abc_Override_PregnantWomanTrimesterServices");

                entity.Property(e => e.C15abcSystemPregnantWomanTrimesterServices).HasColumnName("C15abc_System_PregnantWomanTrimesterServices");

                entity.Property(e => e.C15bOverridePregnantWoman2ndTrimesterServices).HasColumnName("C15b_Override_PregnantWoman2ndTrimesterServices");

                entity.Property(e => e.C15bSystemPregnantWoman2ndTrimesterServices).HasColumnName("C15b_System_PregnantWoman2ndTrimesterServices");

                entity.Property(e => e.C15cOverridePregnantWoman3rdTrimesterServices).HasColumnName("C15c_Override_PregnantWoman3rdTrimesterServices");

                entity.Property(e => e.C15cSystemPregnantWoman3rdTrimesterServices).HasColumnName("C15c_System_PregnantWoman3rdTrimesterServices");

                entity.Property(e => e.C16OverridePregnantWomanHighRisk).HasColumnName("C16_Override_PregnantWomanHighRisk");

                entity.Property(e => e.C16SystemPregnantWomanHighRisk).HasColumnName("C16_System_PregnantWomanHighRisk");

                entity.Property(e => e.C17OverrideChildSourceOfDentalCareAtEnrollment).HasColumnName("C17_Override_ChildSourceOfDentalCareAtEnrollment");

                entity.Property(e => e.C17OverrideChildSourceOfDentalCareEndofYear).HasColumnName("C17_Override_ChildSourceOfDentalCareEndofYear");

                entity.Property(e => e.C17SystemChildSourceOfDentalCareAtEnrollment).HasColumnName("C17_System_ChildSourceOfDentalCareAtEnrollment");

                entity.Property(e => e.C17SystemChildSourceOfDentalCareEndofYear).HasColumnName("C17_System_ChildSourceOfDentalCareEndofYear");

                entity.Property(e => e.C18OverrideChildPreventiveDental).HasColumnName("C18_Override_ChildPreventiveDental");

                entity.Property(e => e.C18SystemChildPreventiveDental).HasColumnName("C18_System_ChildPreventiveDental");

                entity.Property(e => e.C19OverrideChildProfessionalDentalExam).HasColumnName("C19_Override_ChildProfessionalDentalExam");

                entity.Property(e => e.C19SystemChildProfessionalDentalExam).HasColumnName("C19_System_ChildProfessionalDentalExam");

                entity.Property(e => e.C19a1OverrideChildDentalTreatmentReceived).HasColumnName("C19a1_Override_ChildDentalTreatmentReceived");

                entity.Property(e => e.C19a1SystemChildDentalTreatmentReceived).HasColumnName("C19a1_System_ChildDentalTreatmentReceived");

                entity.Property(e => e.C19aOverrideChildDentalTreatmentDiagnosis).HasColumnName("C19a_Override_ChildDentalTreatmentDiagnosis");

                entity.Property(e => e.C19aSystemChildDentalTreatmentDiagnosis).HasColumnName("C19a_System_ChildDentalTreatmentDiagnosis");

                entity.Property(e => e.C19b19OverrideChildDentalReasonNoTreatmentReceived).HasColumnName("C19b19_Override_ChildDentalReasonNoTreatmentReceived");

                entity.Property(e => e.C19b19SystemChildDentalReasonNoTreatmentReceived).HasColumnName("C19b19_System_ChildDentalReasonNoTreatmentReceived");

                entity.Property(e => e.C19b9OverrideChildDentalReasonNoTreatmentReceivedOther)
                    .HasColumnName("C19b9_Override_ChildDentalReasonNoTreatmentReceivedOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C19b9SystemChildDentalReasonNoTreatmentReceivedOther)
                    .HasColumnName("C19b9_System_ChildDentalReasonNoTreatmentReceivedOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C1OverrideChildHealthInsuranceEndOfYear).HasColumnName("C1_Override_ChildHealthInsuranceEndOfYear");

                entity.Property(e => e.C1OverrideChildHealthInsuranceEnrollment).HasColumnName("C1_Override_ChildHealthInsuranceEnrollment");

                entity.Property(e => e.C1SystemChildHealthInsuranceEndOfYear).HasColumnName("C1_System_ChildHealthInsuranceEndOfYear");

                entity.Property(e => e.C1SystemChildHealthInsuranceEnrollment).HasColumnName("C1_System_ChildHealthInsuranceEnrollment");

                entity.Property(e => e.C1adOverrideChildHealthInsuranceTypeEndofEnrollment).HasColumnName("C1ad_Override_ChildHealthInsuranceTypeEndofEnrollment");

                entity.Property(e => e.C1adOverrideChildHealthInsuranceTypeEnrollment).HasColumnName("C1ad_Override_ChildHealthInsuranceTypeEnrollment");

                entity.Property(e => e.C1adSystemChildHealthInsuranceTypeEndofEnrollment).HasColumnName("C1ad_System_ChildHealthInsuranceTypeEndofEnrollment");

                entity.Property(e => e.C1adSystemChildHealthInsuranceTypeEnrollment).HasColumnName("C1ad_System_ChildHealthInsuranceTypeEnrollment");

                entity.Property(e => e.C1d1OverrideChildHealthInsuranceTypeEndofEnrollmentOther)
                    .HasColumnName("C1d1_Override_ChildHealthInsuranceTypeEndofEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C1d1OverrideChildHealthInsuranceTypeEnrollmentOther)
                    .HasColumnName("C1d1_Override_ChildHealthInsuranceTypeEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C1d1SystemChildHealthInsuranceTypeEndofEnrollmentOther)
                    .HasColumnName("C1d1_System_ChildHealthInsuranceTypeEndofEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C1d1SystemChildHealthInsuranceTypeEnrollmentOther)
                    .HasColumnName("C1d1_System_ChildHealthInsuranceTypeEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C20DisplayCheckEhsmigrantChildUpToDateOnDentalEpsdtendOfYear).HasColumnName("C20_DisplayCheck_EHSMigrantChildUpToDateOnDentalEPSDTEndOfYear");

                entity.Property(e => e.C20OverrideEhsmigrantChildUpToDateOnDentalEpsdtendOfYear).HasColumnName("C20_Override_EHSMigrantChildUpToDateOnDentalEPSDTEndOfYear");

                entity.Property(e => e.C20SystemEhsmigrantChildUpToDateOnDentalEpsdtendOfYear).HasColumnName("C20_System_EHSMigrantChildUpToDateOnDentalEPSDTEndOfYear");

                entity.Property(e => e.C21OverridePregnantWomanDentalExam).HasColumnName("C21_Override_PregnantWomanDentalExam");

                entity.Property(e => e.C21SystemPregnantWomanDentalExam).HasColumnName("C21_System_PregnantWomanDentalExam");

                entity.Property(e => e.C23a1OverrideChildMhprofessionalStaffConsultation3Plus).HasColumnName("C23a1_Override_ChildMHProfessionalStaffConsultation3Plus");

                entity.Property(e => e.C23a1SystemChildMhprofessionalStaffConsultation3Plus).HasColumnName("C23a1_System_ChildMHProfessionalStaffConsultation3Plus");

                entity.Property(e => e.C23aOverrideChildMhprofessionalStaffConsultation).HasColumnName("C23a_Override_ChildMHProfessionalStaffConsultation");

                entity.Property(e => e.C23aSystemChildMhprofessionalStaffConsultation).HasColumnName("C23a_System_ChildMHProfessionalStaffConsultation");

                entity.Property(e => e.C23b1OverrideChildMhprofessionalParentGuardianConsultation3Plus).HasColumnName("C23b1_Override_ChildMHProfessionalParentGuardianConsultation3Plus");

                entity.Property(e => e.C23b1SystemChildMhprofessionalParentGuardianConsultation3Plus).HasColumnName("C23b1_System_ChildMHProfessionalParentGuardianConsultation3Plus");

                entity.Property(e => e.C23bOverrideChildMhprofessionalParentGuardianConsultation).HasColumnName("C23b_Override_ChildMHProfessionalParentGuardianConsultation");

                entity.Property(e => e.C23bSystemChildMhprofessionalParentGuardianConsultation).HasColumnName("C23b_System_ChildMHProfessionalParentGuardianConsultation");

                entity.Property(e => e.C23cOverrideChildMhprofessionalMhassessment).HasColumnName("C23c_Override_ChildMHProfessionalMHAssessment");

                entity.Property(e => e.C23cSystemChildMhprofessionalMhassessment).HasColumnName("C23c_System_ChildMHProfessionalMHAssessment");

                entity.Property(e => e.C23dOverrideChildMhprofessionalMhreferral).HasColumnName("C23d_Override_ChildMHProfessionalMHReferral");

                entity.Property(e => e.C23dSystemChildMhprofessionalMhreferral).HasColumnName("C23d_System_ChildMHProfessionalMHReferral");

                entity.Property(e => e.C24OverrideChildProgramMhexternalReferralThisYear).HasColumnName("C24_Override_ChildProgramMHExternalReferralThisYear");

                entity.Property(e => e.C24SystemChildProgramMhexternalReferralThisYear).HasColumnName("C24_System_ChildProgramMHExternalReferralThisYear");

                entity.Property(e => e.C24aOverrideChildProgramMhexternalReferralServicesReceivedThisYear).HasColumnName("C24a_Override_ChildProgramMHExternalReferralServicesReceivedThisYear");

                entity.Property(e => e.C24aSystemChildProgramMhexternalReferralServicesReceivedThisYear).HasColumnName("C24a_System_ChildProgramMHExternalReferralServicesReceivedThisYear");

                entity.Property(e => e.C25DisplayCheckChildIepthisYear).HasColumnName("C25_DisplayCheck_ChildIEPThisYear");

                entity.Property(e => e.C25OverrideChildIepthisYear).HasColumnName("C25_Override_ChildIEPThisYear");

                entity.Property(e => e.C25SystemChildIepthisYear).HasColumnName("C25_System_ChildIEPThisYear");

                entity.Property(e => e.C25a1OverrideChildIepservicesReceivedPriorToEnrollmentYear).HasColumnName("C25a1_Override_ChildIEPServicesReceivedPriorToEnrollmentYear");

                entity.Property(e => e.C25a1SystemChildIepservicesReceivedPriorToEnrollmentYear).HasColumnName("C25a1_System_ChildIEPServicesReceivedPriorToEnrollmentYear");

                entity.Property(e => e.C25a2OverrideChildIepservicesReceivedDuringEnrollmentYear).HasColumnName("C25a2_Override_ChildIEPServicesReceivedDuringEnrollmentYear");

                entity.Property(e => e.C25a2SystemChildIepservicesReceivedDuringEnrollmentYear).HasColumnName("C25a2_System_ChildIEPServicesReceivedDuringEnrollmentYear");

                entity.Property(e => e.C25bOverrideChidlIepservicesNotReceived).HasColumnName("C25b_Override_ChidlIEPServicesNotReceived");

                entity.Property(e => e.C25bSystemChidlIepservicesNotReceived).HasColumnName("C25b_System_ChidlIEPServicesNotReceived");

                entity.Property(e => e.C26DisplayCheckChildIfspthisYear).HasColumnName("C26_DisplayCheck_ChildIFSPThisYear");

                entity.Property(e => e.C26OverrideChildIfspthisYear).HasColumnName("C26_Override_ChildIFSPThisYear");

                entity.Property(e => e.C26SystemChildIfspthisYear).HasColumnName("C26_System_ChildIFSPThisYear");

                entity.Property(e => e.C26a1OverrideChildIfspservicesReceivedPriorToEnrollmentYear).HasColumnName("C26a1_Override_ChildIFSPServicesReceivedPriorToEnrollmentYear");

                entity.Property(e => e.C26a1SystemChildIfspservicesReceivedPriorToEnrollmentYear).HasColumnName("C26a1_System_ChildIFSPServicesReceivedPriorToEnrollmentYear");

                entity.Property(e => e.C26a2OverrideChildIfspservicesReceivedDuringEnrollmentYear).HasColumnName("C26a2_Override_ChildIFSPServicesReceivedDuringEnrollmentYear");

                entity.Property(e => e.C26a2SystemChildIfspservicesReceivedDuringEnrollmentYear).HasColumnName("C26a2_System_ChildIFSPServicesReceivedDuringEnrollmentYear");

                entity.Property(e => e.C26bOverrideChidlIfspservicesNotReceived).HasColumnName("C26b_Override_ChidlIFSPServicesNotReceived");

                entity.Property(e => e.C26bSystemChidlIfspservicesNotReceived).HasColumnName("C26b_System_ChidlIFSPServicesNotReceived");

                entity.Property(e => e.C27amOverrideChildIepprimaryDiagnosis)
                    .HasColumnName("C27am_Override_ChildIEPPrimaryDiagnosis")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C27amOverrideChildIepservicesReceived).HasColumnName("C27am_Override_ChildIEPServicesReceived");

                entity.Property(e => e.C27amSystemChildIepprimaryDiagnosis)
                    .HasColumnName("C27am_System_ChildIEPPrimaryDiagnosis")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C27amSystemChildIepservicesReceived).HasColumnName("C27am_System_ChildIEPServicesReceived");

                entity.Property(e => e.C28OverrideChildNewlyEnrolledThisYear).HasColumnName("C28_Override_ChildNewlyEnrolledThisYear");

                entity.Property(e => e.C28SystemChildNewlyEnrolledThisYear).HasColumnName("C28_System_ChildNewlyEnrolledThisYear");

                entity.Property(e => e.C29OverrideChildDevSensoryBehavioralScreeningsWithin45Days).HasColumnName("C29_Override_ChildDevSensoryBehavioralScreeningsWithin45Days");

                entity.Property(e => e.C29SystemChildDevSensoryBehavioralScreeningsWithin45Days).HasColumnName("C29_System_ChildDevSensoryBehavioralScreeningsWithin45Days");

                entity.Property(e => e.C29aOverrideChildDevSensoryBehavioralScreeningsFollowUpRequired).HasColumnName("C29a_Override_ChildDevSensoryBehavioralScreeningsFollowUpRequired");

                entity.Property(e => e.C29aSystemChildDevSensoryBehavioralScreeningsFollowUpRequired).HasColumnName("C29a_System_ChildDevSensoryBehavioralScreeningsFollowUpRequired");

                entity.Property(e => e.C3OverridePregnantWomanHealthInsuranceEndOfYear).HasColumnName("C3_Override_PregnantWomanHealthInsuranceEndOfYear");

                entity.Property(e => e.C3OverridePregnantWomanHealthInsuranceEnrollment).HasColumnName("C3_Override_PregnantWomanHealthInsuranceEnrollment");

                entity.Property(e => e.C3SystemPregnantWomanHealthInsuranceEndOfYear).HasColumnName("C3_System_PregnantWomanHealthInsuranceEndOfYear");

                entity.Property(e => e.C3SystemPregnantWomanHealthInsuranceEnrollment).HasColumnName("C3_System_PregnantWomanHealthInsuranceEnrollment");

                entity.Property(e => e.C3adOverridePregnantWomanHealthInsuranceTypeEndofEnrollment).HasColumnName("C3ad_Override_PregnantWomanHealthInsuranceTypeEndofEnrollment");

                entity.Property(e => e.C3adOverridePregnantWomanHealthInsuranceTypeEnrollment).HasColumnName("C3ad_Override_PregnantWomanHealthInsuranceTypeEnrollment");

                entity.Property(e => e.C3adSystemPregnantWomanHealthInsuranceTypeEndofEnrollment).HasColumnName("C3ad_System_PregnantWomanHealthInsuranceTypeEndofEnrollment");

                entity.Property(e => e.C3adSystemPregnantWomanHealthInsuranceTypeEnrollment).HasColumnName("C3ad_System_PregnantWomanHealthInsuranceTypeEnrollment");

                entity.Property(e => e.C3d1OverridePregnantWomanHealthInsuranceTypeEndofEnrollmentOther)
                    .HasColumnName("C3d1_Override_PregnantWomanHealthInsuranceTypeEndofEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C3d1OverridePregnantWomanHealthInsuranceTypeEnrollmentOther)
                    .HasColumnName("C3d1_Override_PregnantWomanHealthInsuranceTypeEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C3d1SystemPregnantWomanHealthInsuranceTypeEndofEnrollmentOther)
                    .HasColumnName("C3d1_System_PregnantWomanHealthInsuranceTypeEndofEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C3d1SystemPregnantWomanHealthInsuranceTypeEnrollmentOther)
                    .HasColumnName("C3d1_System_PregnantWomanHealthInsuranceTypeEnrollmentOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C50OverrideChildHomeless).HasColumnName("C50_Override_ChildHomeless");

                entity.Property(e => e.C50SystemChildHomeless).HasColumnName("C50_System_ChildHomeless");

                entity.Property(e => e.C52OverrideChildFosterCareThisYear).HasColumnName("C52_Override_ChildFosterCareThisYear");

                entity.Property(e => e.C52SystemChildFosterCareThisYear).HasColumnName("C52_System_ChildFosterCareThisYear");

                entity.Property(e => e.C53OverrideChildWelfareAgencyReferral).HasColumnName("C53_Override_ChildWelfareAgencyReferral");

                entity.Property(e => e.C53SystemChildWelfareAgencyReferral).HasColumnName("C53_System_ChildWelfareAgencyReferral");

                entity.Property(e => e.C5OverrideChildSourceOfHealthCareEndOfYear).HasColumnName("C5_Override_ChildSourceOfHealthCareEndOfYear");

                entity.Property(e => e.C5OverrideChildSourceOfHealthCareEnrollment).HasColumnName("C5_Override_ChildSourceOfHealthCareEnrollment");

                entity.Property(e => e.C5SystemChildSourceOfHealthCareEndOfYear).HasColumnName("C5_System_ChildSourceOfHealthCareEndOfYear");

                entity.Property(e => e.C5SystemChildSourceOfHealthCareEnrollment).HasColumnName("C5_System_ChildSourceOfHealthCareEnrollment");

                entity.Property(e => e.C6OverrideChildIndianHealthServiceEndOfYear).HasColumnName("C6_Override_ChildIndianHealthServiceEndOfYear");

                entity.Property(e => e.C6OverrideChildIndianHealthServiceEnrollment).HasColumnName("C6_Override_ChildIndianHealthServiceEnrollment");

                entity.Property(e => e.C6SystemChildIndianHealthServiceEndOfYear).HasColumnName("C6_System_ChildIndianHealthServiceEndOfYear");

                entity.Property(e => e.C6SystemChildIndianHealthServiceEnrollment).HasColumnName("C6_System_ChildIndianHealthServiceEnrollment");

                entity.Property(e => e.C7OverrideChildMigrantCommunityHealthServiceEndOfYear).HasColumnName("C7_Override_ChildMigrantCommunityHealthServiceEndOfYear");

                entity.Property(e => e.C7OverrideChildMigrantCommunityHealthServiceEnrollment).HasColumnName("C7_Override_ChildMigrantCommunityHealthServiceEnrollment");

                entity.Property(e => e.C7SystemChildMigrantCommunityHealthServiceEndOfYear).HasColumnName("C7_System_ChildMigrantCommunityHealthServiceEndOfYear");

                entity.Property(e => e.C7SystemChildMigrantCommunityHealthServiceEnrollment).HasColumnName("C7_System_ChildMigrantCommunityHealthServiceEnrollment");

                entity.Property(e => e.C8OverrideChildUpToDateOnEpsdtendOfYear).HasColumnName("C8_Override_ChildUpToDateOnEPSDTEndOfYear");

                entity.Property(e => e.C8OverrideChildUpToDateOnEpsdtenrollment).HasColumnName("C8_Override_ChildUpToDateOnEPSDTEnrollment");

                entity.Property(e => e.C8SystemChildUpToDateOnEpsdtasOfToday).HasColumnName("C8_System_ChildUpToDateOnEPSDTAsOfToday");

                entity.Property(e => e.C8SystemChildUpToDateOnEpsdtendOfYear).HasColumnName("C8_System_ChildUpToDateOnEPSDTEndOfYear");

                entity.Property(e => e.C8SystemChildUpToDateOnEpsdtenrollment).HasColumnName("C8_System_ChildUpToDateOnEPSDTEnrollment");

                entity.Property(e => e.C8a1OverrideChildChronicConditionReasonNoTreatmentReceived).HasColumnName("C8a1_Override_ChildChronicConditionReasonNoTreatmentReceived");

                entity.Property(e => e.C8a1OverrideChildChronicConditionTreatmentReceived).HasColumnName("C8a1_Override_ChildChronicConditionTreatmentReceived");

                entity.Property(e => e.C8a1SystemChildChronicConditionReasonNoTreatmentReceived)
                    .HasColumnName("C8a1_System_ChildChronicConditionReasonNoTreatmentReceived")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C8a1SystemChildChronicConditionTreatmentReceived).HasColumnName("C8a1_System_ChildChronicConditionTreatmentReceived");

                entity.Property(e => e.C8aOverrideChildChronicConditionDiagnosis).HasColumnName("C8a_Override_ChildChronicConditionDiagnosis");

                entity.Property(e => e.C8aSystemChildChronicConditionDiagnosis).HasColumnName("C8a_System_ChildChronicConditionDiagnosis");

                entity.Property(e => e.C8b18OverrideChildChronicConditionReasonNoTreatmentReceivedOther)
                    .HasColumnName("C8b18_Override_ChildChronicConditionReasonNoTreatmentReceivedOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C8b18SystemChildChronicConditionReasonNoTreatmentReceivedOther)
                    .HasColumnName("C8b18_System_ChildChronicConditionReasonNoTreatmentReceivedOther")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C9afDisplayCheckChildChronicConditionTreatmentSpecification).HasColumnName("C9af_DisplayCheck_ChildChronicConditionTreatmentSpecification");

                entity.Property(e => e.C9afOverrideChildChronicConditionTreatmentSpecification)
                    .HasColumnName("C9af_Override_ChildChronicConditionTreatmentSpecification")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.C9afSystemChildChronicConditionTreatmentSpecification)
                    .HasColumnName("C9af_System_ChildChronicConditionTreatmentSpecification")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.IsFrozen)
                    .HasColumnName("isFrozen")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantDateOfBirth).HasColumnType("date");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.ParticipantType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PirsubmissionDate)
                    .HasColumnName("PIRSubmissionDate")
                    .HasColumnType("date");

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.SavedFromUi).HasColumnName("savedFromUI");

                entity.Property(e => e.SystemParticipantPiryearEndDate)
                    .HasColumnName("System_ParticipantPIRYearEndDate")
                    .HasColumnType("date");

                entity.Property(e => e.SystemParticipantRecordIncludedInPiryear).HasColumnName("System_ParticipantRecordIncludedInPIRYear");

                entity.Property(e => e.SystemParticipantRecordPirprogramType).HasColumnName("System_ParticipantRecordPIRProgramType");

                entity.Property(e => e.SystemParticipantRecordPiryearStartDate)
                    .HasColumnName("System_ParticipantRecordPIRYearStartDate")
                    .HasColumnType("date");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordPir)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPIR_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordPirscreeningGroupStatus>(entity =>
            {
                entity.ToTable("ParticipantRecordPIRScreeningGroupStatus");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.C29typeStatus });

                entity.Property(e => e.ParticipantRecordPirscreeningGroupStatusId).HasColumnName("ParticipantRecordPIRScreeningGroupStatusId");

                entity.Property(e => e.C29typeStatus).HasColumnName("C29TypeStatus");

                entity.Property(e => e.C8typeAsOfTodayStatus).HasColumnName("C8TypeAsOfTodayStatus");

                entity.Property(e => e.C8typeAtEndOfEnrollmentStatus).HasColumnName("C8TypeAtEndOfEnrollmentStatus");

                entity.Property(e => e.C8typeAtEnrollmentStatus).HasColumnName("C8TypeAtEnrollmentStatus");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsFrozen).HasColumnName("isFrozen");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ParticipantRecordPirscreeningGroupStatusCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningGroupStatus_UserProfile");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ParticipantRecordPirscreeningGroupStatusModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningGroupStatus_UserProfile1");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordPirscreeningGroupStatus)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningGroupStatus_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordPirscreeningStatus>(entity =>
            {
                entity.ToTable("ParticipantRecordPIRScreeningStatus");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.ScreeningType });

                entity.Property(e => e.ParticipantRecordPirscreeningStatusId).HasColumnName("ParticipantRecordPIRScreeningStatusID");

                entity.Property(e => e.C29typeStatus).HasColumnName("C29TypeStatus");

                entity.Property(e => e.C8typeAsOfTodayStatus).HasColumnName("C8TypeAsOfTodayStatus");

                entity.Property(e => e.C8typeAtEndOfEnrollmentStatus).HasColumnName("C8TypeAtEndOfEnrollmentStatus");

                entity.Property(e => e.C8typeAtEnrollmentStatus).HasColumnName("C8TypeAtEnrollmentStatus");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HealthOrEducationScreeningId).HasColumnName("HealthOrEducationScreeningID");

                entity.Property(e => e.IsFrozen).HasColumnName("isFrozen");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.ScreeningId).HasColumnName("ScreeningID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ParticipantRecordPirscreeningStatusCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningStatus_UserProfile");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ParticipantRecordPirscreeningStatusModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningStatus_UserProfile1");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordPirscreeningStatus)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPIRScreeningStatus_ParticipantRecord");
            });

            modelBuilder.Entity<ParticipantRecordPreEnrollmentTransactionHistory>(entity =>
            {
                entity.Property(e => e.ParticipantRecordPreEnrollmentTransactionHistoryId).HasColumnName("ParticipantRecordPreEnrollmentTransactionHistoryID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.TransactionDate).HasColumnType("date");

                entity.Property(e => e.TransactionNote).HasMaxLength(500);

                entity.HasOne(d => d.TransactionUser)
                    .WithMany(p => p.ParticipantRecordPreEnrollmentTransactionHistory)
                    .HasForeignKey(d => d.TransactionUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordPreEnrollmentTransactionHistory_UserProfile");
            });

            modelBuilder.Entity<ParticipantRecordSelectionCriteriaCategoryItem>(entity =>
            {
                entity.HasIndex(e => new { e.ParticipantRecordId, e.IsChecked });

                entity.HasIndex(e => new { e.ParticipantRecordId, e.SelectionCriteriaCategoryItemId, e.IsChecked })
                    .HasName("IX_ParticipantRecordSelectionCriteriaCategoryItem_SelectionCriteriaCategoryItemID_IsChecked_Convering");

                entity.Property(e => e.ParticipantRecordSelectionCriteriaCategoryItemId)
                    .HasColumnName("ParticipantRecordSelectionCriteriaCategoryItemID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.SelectionCriteriaCategoryItemId).HasColumnName("SelectionCriteriaCategoryItemID");

                entity.HasOne(d => d.ParticipantRecord)
                    .WithMany(p => p.ParticipantRecordSelectionCriteriaCategoryItem)
                    .HasForeignKey(d => d.ParticipantRecordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordSelectionCriteriaCategoryItem_ParticipantRecord");

                entity.HasOne(d => d.ParticipantRecordSelectionCriteriaCategoryItemNavigation)
                    .WithOne(p => p.InverseParticipantRecordSelectionCriteriaCategoryItemNavigation)
                    .HasForeignKey<ParticipantRecordSelectionCriteriaCategoryItem>(d => d.ParticipantRecordSelectionCriteriaCategoryItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordSelectionCriteriaCategoryItem_ParticipantRecordSelectionCriteriaCategoryItem");

                entity.HasOne(d => d.SelectionCriteriaCategoryItem)
                    .WithMany(p => p.ParticipantRecordSelectionCriteriaCategoryItem)
                    .HasForeignKey(d => d.SelectionCriteriaCategoryItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ParticipantRecordSelectionCriteriaCategoryItem_SelectionCriteriaCategoryItem");
            });

            modelBuilder.Entity<Pirmapping>(entity =>
            {
                entity.ToTable("PIRMapping");

                entity.Property(e => e.PirmappingId)
                    .HasColumnName("PIRMappingId")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OverrideColumnName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SystemColumnName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TableName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Pirquestion>(entity =>
            {
                entity.ToTable("PIRQuestion");

                entity.Property(e => e.PirquestionId)
                    .HasColumnName("PIRQuestionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.ShortDisplayQuestion).HasMaxLength(500);
            });

            modelBuilder.Entity<PreEnrollmentFilterLog>(entity =>
            {
                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.SearchText).HasMaxLength(1024);

                entity.Property(e => e.SortOrderKey).HasMaxLength(50);

                entity.Property(e => e.SortOrderValue).HasMaxLength(50);

                entity.Property(e => e.StartTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<PresentAttendance>(entity =>
            {
                entity.Property(e => e.PresentAttendanceId).HasColumnName("PresentAttendanceID");

                entity.Property(e => e.AttendanceDate).HasColumnType("date");

                entity.Property(e => e.AttendanceStatus).HasMaxLength(50);

                entity.Property(e => e.CenterProgramYear).HasMaxLength(250);

                entity.Property(e => e.ClassroomProgramYear).HasMaxLength(250);

                entity.Property(e => e.LastDate).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.ParticipantName).HasMaxLength(500);

                entity.Property(e => e.ParticipantRecordId).HasColumnName("ParticipantRecordID");

                entity.Property(e => e.StartDate).HasColumnType("date");
            });

            modelBuilder.Entity<PrimaryGuardianCaseNote>(entity =>
            {
                entity.HasIndex(e => new { e.PrimaryGuardianId, e.PrimaryGuardianCaseNoteId })
                    .HasName("IX_PrimaryGuardianCaseNote_PrimaryGuardianCaseNoteID");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.PrimaryGuardianCaseNoteId, e.Type, e.Date, e.ClosureIndicator })
                    .HasName("IX_PrimaryGuardianCaseNote_PrimaryGuardianID");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.Type, e.Date, e.FatherFigureInvolved, e.PrimaryGuardianCaseNoteId })
                    .HasName("_dta_index_PrimaryGuardianCaseNote_5_1534628510__K2_K5_K3_K24_K1");

                entity.Property(e => e.PrimaryGuardianCaseNoteId).HasColumnName("PrimaryGuardianCaseNoteID");

                entity.Property(e => e.ClosureDate).HasColumnType("datetime");

                entity.Property(e => e.ClosureIndicator).HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("datetime2(0)");

                entity.Property(e => e.ForeignNoteId)
                    .HasColumnName("Foreign_NoteID")
                    .HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.OtherDescription).HasMaxLength(50);

                entity.Property(e => e.PeeractivityModeling).HasColumnName("PEERActivityModeling");

                entity.Property(e => e.PirservicesReceived1).HasColumnName("PIRServicesReceived1");

                entity.Property(e => e.PirservicesReceived1Date)
                    .HasColumnName("PIRServicesReceived1Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PirservicesReceived2).HasColumnName("PIRServicesReceived2");

                entity.Property(e => e.PirservicesReceived2Date)
                    .HasColumnName("PIRServicesReceived2Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PirservicesReceived3).HasColumnName("PIRServicesReceived3");

                entity.Property(e => e.PirservicesReceived3Date)
                    .HasColumnName("PIRServicesReceived3Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PrimaryGuardianId).HasColumnName("PrimaryGuardianID");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.PrimaryGuardianCaseNote)
                    .HasForeignKey(d => d.CreatorUserId)
                    .HasConstraintName("FK_CaseNote_UserProfile_1");

                entity.HasOne(d => d.PrimaryGuardian)
                    .WithMany(p => p.PrimaryGuardianCaseNote)
                    .HasForeignKey(d => d.PrimaryGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianCaseNote_PrimaryGuardian_1");
            });

            modelBuilder.Entity<PrimaryGuardianCaseNoteFollowUp>(entity =>
            {
                entity.HasIndex(e => e.PrimaryGuardianCaseNoteId);

                entity.HasIndex(e => new { e.PrimaryGuardianCaseNoteId, e.PrimaryGuardianCaseNoteFollowUpId, e.Date })
                    .HasName("IX_PrimaryGuardianCaseNoteFollowUp_Date");

                entity.Property(e => e.PrimaryGuardianCaseNoteFollowUpId).HasColumnName("PrimaryGuardianCaseNoteFollowUpID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryGuardianCaseNoteId).HasColumnName("PrimaryGuardianCaseNoteID");

                entity.Property(e => e.ReferralGiven).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReferralTaken).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.PrimaryGuardianCaseNote)
                    .WithMany(p => p.PrimaryGuardianCaseNoteFollowUp)
                    .HasForeignKey(d => d.PrimaryGuardianCaseNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CaseNoteFollwUp_UserProfile_1");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyEngagement>(entity =>
            {
                entity.HasIndex(e => e.PrimaryGuardianId);

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.Type, e.Date })
                    .HasName("_dta_index_PrimaryGuardianFamilyEngagement_5_1213247377__K2_K3_K4");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.Type, e.FatherFigureInvolved, e.Date, e.PrimaryGuardianFamilyEngagementId })
                    .HasName("_dta_index_PrimaryGuardianFamilyEngagement_5_1213247377__K2_K3_K11_K4_K1");

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.Date).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerName).HasMaxLength(200);

                entity.Property(e => e.PrimaryGuardianId).HasColumnName("PrimaryGuardianID");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagement)
                    .HasForeignKey(d => d.CreatorUserId)
                    .HasConstraintName("FK_FamilyEngagement_UserProfile");

                entity.HasOne(d => d.PrimaryGuardian)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagement)
                    .HasForeignKey(d => d.PrimaryGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianFamilyEngagement_PrimaryGuardian");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes>(entity =>
            {
                entity.HasIndex(e => e.PrimaryGuardianFamilyEngagementId);

                entity.Property(e => e.PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomesId).HasColumnName("PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomesID");

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.HasOne(d => d.PrimaryGuardianFamilyEngagement)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes)
                    .HasForeignKey(d => d.PrimaryGuardianFamilyEngagementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes_PrimaryGuardianFamilyEngagement");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyEngagementAssociatedPirservices>(entity =>
            {
                entity.ToTable("PrimaryGuardianFamilyEngagementAssociatedPIRServices");

                entity.HasIndex(e => e.PrimaryGuardianFamilyEngagementId);

                entity.Property(e => e.PrimaryGuardianFamilyEngagementAssociatedPirservicesId).HasColumnName("PrimaryGuardianFamilyEngagementAssociatedPIRServicesID");

                entity.Property(e => e.AssociatedPirservices).HasColumnName("AssociatedPIRServices");

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.HasOne(d => d.PrimaryGuardianFamilyEngagement)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagementAssociatedPirservices)
                    .HasForeignKey(d => d.PrimaryGuardianFamilyEngagementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianFamilyEngagementAssociatedPIRServices_PrimaryGuardianFamilyEngagement");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyEngagementFlpworkshopActivity>(entity =>
            {
                entity.ToTable("PrimaryGuardianFamilyEngagementFLPWorkshopActivity");

                entity.HasIndex(e => e.PrimaryGuardianFamilyEngagementId);

                entity.Property(e => e.PrimaryGuardianFamilyEngagementFlpworkshopActivityId).HasColumnName("PrimaryGuardianFamilyEngagementFLPWorkshopActivityID");

                entity.Property(e => e.FlpworkshopActivity).HasColumnName("FLPWorkshopActivity");

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.HasOne(d => d.PrimaryGuardianFamilyEngagement)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagementFlpworkshopActivity)
                    .HasForeignKey(d => d.PrimaryGuardianFamilyEngagementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianFamilyEngagementFLPWorkshopActivity_PrimaryGuardianFamilyEngagement");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyEngagementGoals>(entity =>
            {
                entity.HasIndex(e => e.PrimaryGuardianFamilyEngagementId);

                entity.Property(e => e.PrimaryGuardianFamilyEngagementGoalsId).HasColumnName("PrimaryGuardianFamilyEngagementGoalsID");

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.HasOne(d => d.PrimaryGuardianFamilyEngagement)
                    .WithMany(p => p.PrimaryGuardianFamilyEngagementGoals)
                    .HasForeignKey(d => d.PrimaryGuardianFamilyEngagementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianFamilyEngagementGoals_PrimaryGuardianFamilyEngagement");
            });

            modelBuilder.Entity<PrimaryGuardianFamilySof>(entity =>
            {
                entity.ToTable("PrimaryGuardianFamilySOF");

                entity.Property(e => e.PrimaryGuardianFamilySofid).HasColumnName("PrimaryGuardianFamilySOFId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<PrimaryGuardianFamilyStrengthsAssessment>(entity =>
            {
                entity.HasIndex(e => new { e.AllEntryScoreTotal, e.AllUpdatedScoreTotal, e.PrimaryGuardianId })
                    .HasName("IX_PrimaryGuardianFamilyStrengthsAssessment_PrimaryGuardianID");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SelfSufficiencyCategoryAcculturationUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K30_K1");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SelfSufficiencyCategoryHousingCommunityUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K22_K1");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SupportForFamilyMembersCategoryAlcoholDrugUseUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K46_K1");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SupportForFamilyMembersCategoryEmotionalSupportUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K42_K1");

                entity.HasIndex(e => new { e.SelfSufficiencyCategoryAcculturationUpdatedScore, e.PrimaryGuardianId, e.SupportForChildrenCategoryChildHealthWellnessUpdatedScore, e.SupportForFamilyMembersCategoryFamilyHealthWellnessUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K18_K38_K1_30");

                entity.HasIndex(e => new { e.SelfSufficiencyCategoryHousingCommunityUpdatedScore, e.SupportForFamilyMembersCategoryAlcoholDrugUseUpdatedScore, e.PrimaryGuardianId, e.SelfSufficiencyCategoryEducationUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K28_K1_22_46");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.FamilyLifePracticesCategoryFamilyRoutinesUpdatedScore, e.FamilyLifePracticesCategoryExperienceRichEnvironmentUpdatedScore, e.FamilyLifePracticesCategoryPromotingLiteracyHomeUpdatedScore, e.FamilyLifePracticesCategoryPositiveDisciplineUpdatedScore, e.SupportForChildrenCategoryChildrenSpecialNeedsUpdatedScore, e.PrimaryGuardianFamilyStrengthsAssessmentId })
                    .HasName("_dta_index_PrimaryGuardianFamilyStrengthsAs_5_1598628738__K2_K6_K8_K10_K12_K16_K1");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SupportForChildrenCategoryChildHealthWellnessEntryScore, e.FamilyLifePracticesCategoryPositiveDisciplineEntryScore, e.FamilyLifePracticesCategoryPromotingLiteracyHomeEntryScore, e.FamilyLifePracticesCategoryFamilyRoutinesEntryScore, e.SupportForChildrenCategoryChildrenSpecialNeedsEntryScore, e.FamilyLifePracticesCategoryExperienceRichEnvironmentEntryScore })
                    .HasName("IX_PrimaryGuardianFamilyStrengthsAssessment_SupportForChildrenCategoryChildHealthWellnessEntryScore");

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SupportForChildrenCategoryChildHealthWellnessEntryScore, e.FamilyLifePracticesCategoryPositiveDisciplineEntryScore, e.FamilyLifePracticesCategoryPromotingLiteracyHomeEntryScore, e.FamilyLifePracticesCategoryFamilyRoutinesEntryScore, e.SupportForChildrenCategoryChildrenSpecialNeedsEntryScore, e.FamilyLifePracticesCategoryExperienceRichEnvironmentEntryScore, e.SelfSufficiencyCategoryAcculturationEntryScore, e.SelfSufficiencyCategoryChildCareEntryScore, e.SelfSufficiencyCategoryEducationEntryScore, e.SelfSufficiencyCategoryEmploymentEntryScore, e.SelfSufficiencyCategoryFamilyFinancesEntryScore, e.SelfSufficiencyCategoryHousingCommunityEntryScore, e.SelfSufficiencyCategoryTransportationEntryScore })
                    .HasName("IX_PrimaryGuardianFamilyStrengthsAssessment_SelfSufficiencyCategoryTransportationEntryScore");

                entity.Property(e => e.PrimaryGuardianFamilyStrengthsAssessmentId).HasColumnName("PrimaryGuardianFamilyStrengthsAssessmentID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DeclinedDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.PrimaryGuardianId).HasColumnName("PrimaryGuardianID");

                entity.HasOne(d => d.PrimaryGuardian)
                    .WithMany(p => p.PrimaryGuardianFamilyStrengthsAssessment)
                    .HasForeignKey(d => d.PrimaryGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FamilyStrengthsAssessment_Participant_1");
            });

            modelBuilder.Entity<PrimaryGuardianGoal>(entity =>
            {
                entity.HasIndex(e => e.GoalId);

                entity.HasIndex(e => e.PrimaryGuardianId);

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.IsActive, e.FatherFigureInvolved, e.PrimaryGuardianGoalId })
                    .HasName("_dta_index_PrimaryGuardianGoal_5_1630628852__K2_K5_K63_K1");

                entity.Property(e => e.PrimaryGuardianGoalId).HasColumnName("PrimaryGuardianGoalID");

                entity.Property(e => e.Benchmark0CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark1CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark2CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark3CompletedDate).HasColumnType("date");

                entity.Property(e => e.Benchmark4CompletedDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FamilyEngagementOutcome1).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome2).HasMaxLength(100);

                entity.Property(e => e.FamilyEngagementOutcome3).HasMaxLength(100);

                entity.Property(e => e.GoalId).HasColumnName("GoalID");

                entity.Property(e => e.InitiatedDate).HasColumnType("date");

                entity.Property(e => e.IsActiveDate).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Objective1).HasMaxLength(1000);

                entity.Property(e => e.Objective1TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective2).HasMaxLength(1000);

                entity.Property(e => e.Objective2TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective3).HasMaxLength(1000);

                entity.Property(e => e.Objective3TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective4).HasMaxLength(1000);

                entity.Property(e => e.Objective4TargetDate).HasColumnType("date");

                entity.Property(e => e.Objective5).HasMaxLength(1000);

                entity.Property(e => e.Objective5TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther1).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther1TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther2).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther2TargetDate).HasColumnType("date");

                entity.Property(e => e.ObjectiveOther3).HasMaxLength(1000);

                entity.Property(e => e.ObjectiveOther3TargetDate).HasColumnType("date");

                entity.Property(e => e.PirservicesReceived1)
                    .HasColumnName("PIRServicesReceived1")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived1Date)
                    .HasColumnName("PIRServicesReceived1Date")
                    .HasColumnType("date");

                entity.Property(e => e.PirservicesReceived2)
                    .HasColumnName("PIRServicesReceived2")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived2Date)
                    .HasColumnName("PIRServicesReceived2Date")
                    .HasColumnType("date");

                entity.Property(e => e.PirservicesReceived3)
                    .HasColumnName("PIRServicesReceived3")
                    .HasMaxLength(100);

                entity.Property(e => e.PirservicesReceived3Date)
                    .HasColumnName("PIRServicesReceived3Date")
                    .HasColumnType("date");

                entity.Property(e => e.PrimaryGuardianId).HasColumnName("PrimaryGuardianID");

                entity.Property(e => e.Resource1Address).HasMaxLength(100);

                entity.Property(e => e.Resource1ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource1GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource1Organization).HasMaxLength(100);

                entity.Property(e => e.Resource1Phone).HasMaxLength(30);

                entity.Property(e => e.Resource1TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource2Address).HasMaxLength(100);

                entity.Property(e => e.Resource2ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource2GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource2Organization).HasMaxLength(100);

                entity.Property(e => e.Resource2Phone).HasMaxLength(30);

                entity.Property(e => e.Resource2TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource3Address).HasMaxLength(100);

                entity.Property(e => e.Resource3ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource3GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource3Organization).HasMaxLength(100);

                entity.Property(e => e.Resource3Phone).HasMaxLength(30);

                entity.Property(e => e.Resource3TakenDate).HasColumnType("date");

                entity.Property(e => e.Resource4Address).HasMaxLength(100);

                entity.Property(e => e.Resource4ContactName).HasMaxLength(100);

                entity.Property(e => e.Resource4GivenDate).HasColumnType("date");

                entity.Property(e => e.Resource4Organization).HasMaxLength(100);

                entity.Property(e => e.Resource4Phone).HasMaxLength(30);

                entity.Property(e => e.Resource4TakenDate).HasColumnType("date");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.PrimaryGuardianGoal)
                    .HasForeignKey(d => d.CreatorUserId)
                    .HasConstraintName("FK_PrimaryGuardian_UserProfile");

                entity.HasOne(d => d.Goal)
                    .WithMany(p => p.PrimaryGuardianGoal)
                    .HasForeignKey(d => d.GoalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardian_Goal");

                entity.HasOne(d => d.PrimaryGuardian)
                    .WithMany(p => p.PrimaryGuardianGoal)
                    .HasForeignKey(d => d.PrimaryGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardian");
            });

            modelBuilder.Entity<PrimaryGuardianGoalFollowUp>(entity =>
            {
                entity.HasIndex(e => e.PrimaryGuardianGoalId)
                    .HasName("IX_PrimaryGuardianGoalFollowUp");

                entity.HasIndex(e => new { e.CreatedOn, e.Date, e.FollowUpDate, e.LastModified, e.Note, e.PrimaryGuardianGoalFollowUpId, e.PrimaryGuardianGoalId, e.CreatorUserId })
                    .HasName("IX_PrimaryGuardianGoalFollowUp_PrimaryGuardianGoalID");

                entity.Property(e => e.PrimaryGuardianGoalFollowUpId).HasColumnName("PrimaryGuardianGoalFollowUpID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.FollowUpDate).HasColumnType("datetime");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.PrimaryGuardianFamilyEngagementId).HasColumnName("PrimaryGuardianFamilyEngagementID");

                entity.Property(e => e.PrimaryGuardianGoalId).HasColumnName("PrimaryGuardianGoalID");

                entity.HasOne(d => d.CreatorUser)
                    .WithMany(p => p.PrimaryGuardianGoalFollowUp)
                    .HasForeignKey(d => d.CreatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianGoalFollowUp_UserProfile");

                entity.HasOne(d => d.ParticipantAction)
                    .WithMany(p => p.PrimaryGuardianGoalFollowUp)
                    .HasForeignKey(d => d.ParticipantActionId)
                    .HasConstraintName("FK_PrimaryGuardianGoalFollowUp_ParticipantAction");

                entity.HasOne(d => d.ParticipantHealthConcern)
                    .WithMany(p => p.PrimaryGuardianGoalFollowUp)
                    .HasForeignKey(d => d.ParticipantHealthConcernId)
                    .HasConstraintName("FK_PrimaryGuardianGoalFollowUp_ParticipantHealthConcern");

                entity.HasOne(d => d.PrimaryGuardianGoal)
                    .WithMany(p => p.PrimaryGuardianGoalFollowUp)
                    .HasForeignKey(d => d.PrimaryGuardianGoalId)
                    .HasConstraintName("FK_PrimaryGuardianGoalFollowUp_PrimaryGuardianGoal");
            });

            modelBuilder.Entity<PrimaryGuardianPireducationOrTraining>(entity =>
            {
                entity.ToTable("PrimaryGuardianPIREducationOrTraining");

                entity.HasIndex(e => e.PrimaryGuardianPirfamilyInformationId);

                entity.HasIndex(e => new { e.EducationOrTrainingCompletedStatus, e.PrimaryGuardianPirfamilyInformationId, e.PrimaryGuardianPireducationOrTrainingId })
                    .HasName("_dta_index_PrimaryGuardianPIREducationOrTra_5_1895729856__K3_K2_K1");

                entity.Property(e => e.PrimaryGuardianPireducationOrTrainingId).HasColumnName("PrimaryGuardianPIREducationOrTrainingId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PrimaryGuardianPirfamilyInformationId).HasColumnName("PrimaryGuardianPIRFamilyInformationId");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PrimaryGuardianPireducationOrTrainingCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianPIREducationOrTraining_UserProfile");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.PrimaryGuardianPireducationOrTrainingModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_PrimaryGuardianPIREducationOrTraining_UserProfile1");

                entity.HasOne(d => d.PrimaryGuardianPirfamilyInformation)
                    .WithMany(p => p.PrimaryGuardianPireducationOrTraining)
                    .HasForeignKey(d => d.PrimaryGuardianPirfamilyInformationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianPIREducationOrTraining_PrimaryGuardianPIRFamilyInformation");
            });

            modelBuilder.Entity<PrimaryGuardianPirfamilyInformation>(entity =>
            {
                entity.ToTable("PrimaryGuardianPIRFamilyInformation");

                entity.HasIndex(e => e.PrimaryGuardianId);

                entity.HasIndex(e => new { e.PrimaryGuardianId, e.SchoolYearId, e.PrimaryGuardianPirfamilyInformationId })
                    .HasName("_dta_index_PrimaryGuardianPIRFamilyInformat_5_1815729571__K2_K3_K1");

                entity.Property(e => e.PrimaryGuardianPirfamilyInformationId).HasColumnName("PrimaryGuardianPIRFamilyInformationId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SnapatEndOfEnrollment).HasColumnName("SNAPAtEndOfEnrollment");

                entity.Property(e => e.SnapatEnrollment).HasColumnName("SNAPAtEnrollment");

                entity.Property(e => e.SsiatEndOfEnrollment).HasColumnName("SSIAtEndOfEnrollment");

                entity.Property(e => e.SsiatEnrollment).HasColumnName("SSIAtEnrollment");

                entity.Property(e => e.TanfatEndOfEnrollment).HasColumnName("TANFAtEndOfEnrollment");

                entity.Property(e => e.TanfatEnrollment).HasColumnName("TANFAtEnrollment");

                entity.Property(e => e.WicatEndOfEnrollment).HasColumnName("WICAtEndOfEnrollment");

                entity.Property(e => e.WicatEnrollment).HasColumnName("WICAtEnrollment");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PrimaryGuardianPirfamilyInformation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianPIRFamilyInformation_UserProfile");

                entity.HasOne(d => d.PrimaryGuardian)
                    .WithMany(p => p.PrimaryGuardianPirfamilyInformation)
                    .HasForeignKey(d => d.PrimaryGuardianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianPIRFamilyInformation_ParentGuardian");

                entity.HasOne(d => d.SchoolYear)
                    .WithMany(p => p.PrimaryGuardianPirfamilyInformation)
                    .HasForeignKey(d => d.SchoolYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PrimaryGuardianPIRFamilyInformation_SchoolYear");
            });

            modelBuilder.Entity<PrimaryLanguageMappingPirlanguage>(entity =>
            {
                entity.ToTable("PrimaryLanguageMappingPIRLanguage");

                entity.Property(e => e.PrimaryLanguageMappingPirlanguageId).HasColumnName("PrimaryLanguageMappingPIRLanguageID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.PirlanguageId).HasColumnName("PIRLanguageID");

                entity.Property(e => e.PrimaryLanguageId).HasColumnName("PrimaryLanguageID");
            });

            modelBuilder.Entity<Program>(entity =>
            {
                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DelegateNumber).HasMaxLength(50);

                entity.Property(e => e.DirectorEmail).HasMaxLength(100);

                entity.Property(e => e.DirectorName).HasMaxLength(50);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.IsProgramActive).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.MailingAddress).HasMaxLength(50);

                entity.Property(e => e.MailingCity).HasMaxLength(50);

                entity.Property(e => e.MailingState).HasMaxLength(2);

                entity.Property(e => e.MailingZip).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.Property(e => e.SelectionCriteriaId).HasColumnName("SelectionCriteriaID");

                entity.Property(e => e.Source).HasMaxLength(200);

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.Zip).HasMaxLength(50);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.Program)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Program_Grantee");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.Program)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .HasConstraintName("FK_Program_ProgramType");

                entity.HasOne(d => d.SelectionCriteria)
                    .WithMany(p => p.Program)
                    .HasForeignKey(d => d.SelectionCriteriaId)
                    .HasConstraintName("FK_Program_SelectionCriteria");
            });

            modelBuilder.Entity<ProgramPir>(entity =>
            {
                entity.ToTable("ProgramPIR");

                entity.Property(e => e.ProgramPirid).HasColumnName("ProgramPIRID");

                entity.Property(e => e.A10OverrideChildCarePartnersFundedEnrollment).HasColumnName("A10_Override_ChildCarePartnersFundedEnrollment");

                entity.Property(e => e.A10SystemChildCarePartnersFundedEnrollment).HasColumnName("A10_System_ChildCarePartnersFundedEnrollment");

                entity.Property(e => e.A11OverrideNumberofClassroomsOperated).HasColumnName("A11_Override_NumberofClassroomsOperated");

                entity.Property(e => e.A11SystemNumberofClassroomsOperated).HasColumnName("A11_System_NumberofClassroomsOperated");

                entity.Property(e => e.A11aOverrideNumberofDoubleSessionClassrooms).HasColumnName("A11a_Override_NumberofDoubleSessionClassrooms");

                entity.Property(e => e.A11aSystemNumberofDoubleSessionClassrooms).HasColumnName("A11a_System_NumberofDoubleSessionClassrooms");

                entity.Property(e => e.A12aOverrideNumberChildrenEnrollmentByAgeUnder1YearAtEnrollment).HasColumnName("A12a_Override_NumberChildrenEnrollmentByAgeUnder1YearAtEnrollment");

                entity.Property(e => e.A12aSystemNumberChildrenEnrollmentByAgeUnder1YearAtEnrollment).HasColumnName("A12a_System_NumberChildrenEnrollmentByAgeUnder1YearAtEnrollment");

                entity.Property(e => e.A12bOverrideNumberChildrenEnrollmentByAge1YearAtEnrollment).HasColumnName("A12b_Override_NumberChildrenEnrollmentByAge1YearAtEnrollment");

                entity.Property(e => e.A12bSystemNumberChildrenEnrollmentByAge1YearAtEnrollment).HasColumnName("A12b_System_NumberChildrenEnrollmentByAge1YearAtEnrollment");

                entity.Property(e => e.A12cOverrideNumberChildrenEnrollmentByAge2YearAtEnrollment).HasColumnName("A12c_Override_NumberChildrenEnrollmentByAge2YearAtEnrollment");

                entity.Property(e => e.A12cSystemNumberChildrenEnrollmentByAge2YearAtEnrollment).HasColumnName("A12c_System_NumberChildrenEnrollmentByAge2YearAtEnrollment");

                entity.Property(e => e.A12dOverrideNumberChildrenEnrollmentByAge3YearAtEnrollment).HasColumnName("A12d_Override_NumberChildrenEnrollmentByAge3YearAtEnrollment");

                entity.Property(e => e.A12dSystemNumberChildrenEnrollmentByAge3YearAtEnrollment).HasColumnName("A12d_System_NumberChildrenEnrollmentByAge3YearAtEnrollment");

                entity.Property(e => e.A12eOverrideNumberChildrenEnrollmentByAge4YearAtEnrollment).HasColumnName("A12e_Override_NumberChildrenEnrollmentByAge4YearAtEnrollment");

                entity.Property(e => e.A12eSystemNumberChildrenEnrollmentByAge4YearAtEnrollment).HasColumnName("A12e_System_NumberChildrenEnrollmentByAge4YearAtEnrollment");

                entity.Property(e => e.A12fOverrideNumberChildrenEnrollmentByAge5YearsAndOlderAtEnrollment).HasColumnName("A12f_Override_NumberChildrenEnrollmentByAge5YearsAndOlderAtEnrollment");

                entity.Property(e => e.A12fSystemNumberChildrenEnrollmentByAge5YearsAndOlderAtEnrollment).HasColumnName("A12f_System_NumberChildrenEnrollmentByAge5YearsAndOlderAtEnrollment");

                entity.Property(e => e.A13OverridePregnantWomenEnrollment).HasColumnName("A13_Override_PregnantWomenEnrollment");

                entity.Property(e => e.A13SystemPregnantWomenEnrollment).HasColumnName("A13_System_PregnantWomenEnrollment");

                entity.Property(e => e.A14OverrideTotalCumulativeEnrollment).HasColumnName("A14_Override_TotalCumulativeEnrollment");

                entity.Property(e => e.A14SystemTotalCumulativeEnrollment).HasColumnName("A14_System_TotalCumulativeEnrollment");

                entity.Property(e => e.A15aOverridePrimaryTypeOfEligibilityIncomeBelow100PercentFederalPoverty).HasColumnName("A15a_Override_PrimaryTypeOfEligibilityIncomeBelow100PercentFederalPoverty");

                entity.Property(e => e.A15aSystemPrimaryTypeOfEligibilityIncomeBelow100PercentFederalPoverty).HasColumnName("A15a_System_PrimaryTypeOfEligibilityIncomeBelow100PercentFederalPoverty");

                entity.Property(e => e.A15bOverridePrimaryTypeOfEligibilityReceiptOfPublicAssistanceSuchAsTanfssi).HasColumnName("A15b_OVerride_PrimaryTypeOfEligibilityReceiptOfPublicAssistanceSuchAsTANFSSI");

                entity.Property(e => e.A15bSystemPrimaryTypeOfEligibilityReceiptOfPublicAssistanceSuchAsTanfssi).HasColumnName("A15b_System_PrimaryTypeOfEligibilityReceiptOfPublicAssistanceSuchAsTANFSSI");

                entity.Property(e => e.A15cOverridePrimaryTypeOfEligibilityFosterChild).HasColumnName("A15c_Override_PrimaryTypeOfEligibilityFosterChild");

                entity.Property(e => e.A15cSystemPrimaryTypeOfEligibilityFosterChild).HasColumnName("A15c_System_PrimaryTypeOfEligibilityFosterChild");

                entity.Property(e => e.A15dOverridePrimaryTypeOfEligibilityHomeless).HasColumnName("A15d_Override_PrimaryTypeOfEligibilityHomeless");

                entity.Property(e => e.A15dSystemPrimaryTypeOfEligibilityHomeless).HasColumnName("A15d_System_PrimaryTypeOfEligibilityHomeless");

                entity.Property(e => e.A15eOverridePrimaryTypeOfEligibilityOverIncome).HasColumnName("A15e_Override_PrimaryTypeOfEligibilityOverIncome");

                entity.Property(e => e.A15eSystemPrimaryTypeOfEligibilityOverIncome).HasColumnName("A15e_System_PrimaryTypeOfEligibilityOverIncome");

                entity.Property(e => e.A15fOverridePrimaryTypeOfEligibilityOverIncome100PercentTo130Percent).HasColumnName("A15f_Override_PrimaryTypeOfEligibilityOverIncome100PercentTo130Percent");

                entity.Property(e => e.A15fSystemPrimaryTypeOfEligibilityOverIncome100PercentTo130Percent).HasColumnName("A15f_System_PrimaryTypeOfEligibilityOverIncome100PercentTo130Percent");

                entity.Property(e => e.A16OverrideDemonstrationOfIncomeEligibleChildrenInAreaBeingServed).HasColumnName("A16_Override_DemonstrationOfIncomeEligibleChildrenInAreaBeingServed");

                entity.Property(e => e.A17aOverrideHeadStartNumberOfChildrenEnrolledForTheSecondYear).HasColumnName("A17a_Override_HeadStartNumberOfChildrenEnrolledForTheSecondYear");

                entity.Property(e => e.A17aSystemHeadStartNumberOfChildrenEnrolledForTheSecondYear).HasColumnName("A17a_System_HeadStartNumberOfChildrenEnrolledForTheSecondYear");

                entity.Property(e => e.A17bOverrideHeadStartNumberOfChildrenEnrolledForThreeOrMoreYears).HasColumnName("A17b_Override_HeadStartNumberOfChildrenEnrolledForThreeOrMoreYears");

                entity.Property(e => e.A17bSystemHeadStartNumberOfChildrenEnrolledForThreeOrMoreYears).HasColumnName("A17b_System_HeadStartNumberOfChildrenEnrolledForThreeOrMoreYears");

                entity.Property(e => e.A18OverrideHeadStartNumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A18_Override_HeadStartNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A18SystemHeadStartNumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A18_System_HeadStartNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A18aOverrideHeadStartNumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A18a_Override_HeadStartNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A18aSystemHeadStartNumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A18a_System_HeadStartNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A18bOverrideHeadStartNumberOfChildrenProjectedToEnterK).HasColumnName("A18b_Override_HeadStartNumberOfChildrenProjectedToEnterK");

                entity.Property(e => e.A18bSystemHeadStartNumberOfChildrenProjectedToEnterK).HasColumnName("A18b_System_HeadStartNumberOfChildrenProjectedToEnterK");

                entity.Property(e => e.A19OverrideEhsnumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A19_Override_EHSNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A19SystemEhsnumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A19_System_EHSNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A19aOverrideEhsnumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A19a_Override_EHSNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A19aSystemEhsnumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A19a_System_EHSNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A19b1OverrideEhsnumberOfChildrenAgedOutEnteredHeadStart).HasColumnName("A19b1_Override_EHSNumberOfChildrenAgedOutEnteredHeadStart");

                entity.Property(e => e.A19b1SystemEhsnumberOfChildrenAgedOutEnteredHeadStart).HasColumnName("A19b1_System_EHSNumberOfChildrenAgedOutEnteredHeadStart");

                entity.Property(e => e.A19b2OverrideEhsnumberOfChildrenAgedOutEnteredOtherEarlyChildhoodProgram).HasColumnName("A19b2_Override_EHSNumberOfChildrenAgedOutEnteredOtherEarlyChildhoodProgram");

                entity.Property(e => e.A19b2SystemEhsnumberOfChildrenAgedOutEnteredOtherEarlyChildhoodProgram).HasColumnName("A19b2_System_EHSNumberOfChildrenAgedOutEnteredOtherEarlyChildhoodProgram");

                entity.Property(e => e.A19b3OverrideEhsnumberOfChildrenAgedOutDidNotEnterOtherEarlyChildhoodProgram).HasColumnName("A19b3_Override_EHSNumberOfChildrenAgedOutDidNotEnterOtherEarlyChildhoodProgram");

                entity.Property(e => e.A19b3SystemEhsnumberOfChildrenAgedOutDidNotEnterOtherEarlyChildhoodProgram).HasColumnName("A19b3_System_EHSNumberOfChildrenAgedOutDidNotEnterOtherEarlyChildhoodProgram");

                entity.Property(e => e.A19bOverrideEhsnumberOfChildrenAgedOut).HasColumnName("A19b_OVerride_EHSNumberOfChildrenAgedOut");

                entity.Property(e => e.A19bSystemEhsnumberOfChildrenAgedOut).HasColumnName("A19b_System_EHSNumberOfChildrenAgedOut");

                entity.Property(e => e.A1aSystemPirenrollmentYearStartDate)
                    .HasColumnName("A1a_System_PIREnrollmentYearStartDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.A1bSystemPirenrollmentYearEndDate)
                    .HasColumnName("A1b_System_PIREnrollmentYearEndDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.A20OverridePregnantWomenNumberWhoLeftBeforeBirthAndDidNotReEnroll).HasColumnName("A20_Override_PregnantWomenNumberWhoLeftBeforeBirthAndDidNotReEnroll");

                entity.Property(e => e.A20SystemPregnantWomenNumberWhoLeftBeforeBirthAndDidNotReEnroll).HasColumnName("A20_System_PregnantWomenNumberWhoLeftBeforeBirthAndDidNotReEnroll");

                entity.Property(e => e.A21OverridePregnantWomenNumberReceivingEhsservicesAtTimeOfInfantBirth).HasColumnName("A21_Override_PregnantWomenNumberReceivingEHSServicesAtTimeOfInfantBirth");

                entity.Property(e => e.A21SystemPregnantWomenNumberReceivingEhsservicesAtTimeOfInfantBirth).HasColumnName("A21_System_PregnantWomenNumberReceivingEHSServicesAtTimeOfInfantBirth");

                entity.Property(e => e.A21aOverridePregnantWomenNumberOfInfantEnrolled).HasColumnName("A21a_Override_PregnantWomenNumberOfInfantEnrolled");

                entity.Property(e => e.A21aSystemPregnantWomenNumberOfInfantEnrolled).HasColumnName("A21a_System_PregnantWomenNumberOfInfantEnrolled");

                entity.Property(e => e.A21bOverridePregnantWomenNumberOfInfantNotEnrolled).HasColumnName("A21b_Override_PregnantWomenNumberOfInfantNotEnrolled");

                entity.Property(e => e.A21bSystemPregnantWomenNumberOfInfantNotEnrolled).HasColumnName("A21b_System_PregnantWomenNumberOfInfantNotEnrolled");

                entity.Property(e => e.A22OverrideMigrantProgramNumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A22_Override_MigrantProgramNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A22SystemMigrantProgramNumberOfChildrenWhoLeftAndDidNotReEnroll).HasColumnName("A22_System_MigrantProgramNumberOfChildrenWhoLeftAndDidNotReEnroll");

                entity.Property(e => e.A22aOverrideMigrantProgramNumberOfChildrenProjectedToEnterK).HasColumnName("A22a_Override_MigrantProgramNumberOfChildrenProjectedToEnterK");

                entity.Property(e => e.A22aOverrideMigrantProgramNumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A22a_Override_MigrantProgramNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A22aSystemMigrantProgramNumberOfChildrenProjectedToEnterK).HasColumnName("A22a_System_MigrantProgramNumberOfChildrenProjectedToEnterK");

                entity.Property(e => e.A22aSystemMigrantProgramNumberOfChildrenWhoLeftEnrolledLessThan45Days).HasColumnName("A22a_System_MigrantProgramNumberOfChildrenWhoLeftEnrolledLessThan45Days");

                entity.Property(e => e.A23OverrideNumberOfChildrenReceivedAchildCareSubsidy).HasColumnName("A23_Override_NumberOfChildrenReceivedAChildCareSubsidy");

                entity.Property(e => e.A23SystemNumberOfChildrenReceivedAchildCareSubsidy).HasColumnName("A23_System_NumberOfChildrenReceivedAChildCareSubsidy");

                entity.Property(e => e.A24aOverrideNumberOfParticipantsHispanicOrLatinoOrigin).HasColumnName("A24a_Override_NumberOfParticipantsHispanicOrLatinoOrigin");

                entity.Property(e => e.A24aSystemNumberOfParticipantsHispanicOrLatinoOrigin).HasColumnName("A24a_System_NumberOfParticipantsHispanicOrLatinoOrigin");

                entity.Property(e => e.A24bOverrideNumberOfParticipantsNonHispanicOrNonLatinoOrigin).HasColumnName("A24b_Override_NumberOfParticipantsNonHispanicOrNonLatinoOrigin");

                entity.Property(e => e.A24bSystemNumberOfParticipantsNonHispanicOrNonLatinoOrigin).HasColumnName("A24b_System_NumberOfParticipantsNonHispanicOrNonLatinoOrigin");

                entity.Property(e => e.A25aOverrideNumberOfParticipantsRaceAmericanIndianorAlaskaNative).HasColumnName("A25a_Override_NumberOfParticipantsRaceAmericanIndianorAlaskaNative");

                entity.Property(e => e.A25aSystemNumberOfParticipantsRaceAmericanIndianorAlaskaNative).HasColumnName("A25a_System_NumberOfParticipantsRaceAmericanIndianorAlaskaNative");

                entity.Property(e => e.A25bOverrideNumberOfParticipantsRaceAsian).HasColumnName("A25b_Override_NumberOfParticipantsRaceAsian");

                entity.Property(e => e.A25bSystemNumberOfParticipantsRaceAsian).HasColumnName("A25b_System_NumberOfParticipantsRaceAsian");

                entity.Property(e => e.A25cOverrideNumberOfParticipantsRaceBlackOrAfricanAmerican).HasColumnName("A25c_Override_NumberOfParticipantsRaceBlackOrAfricanAmerican");

                entity.Property(e => e.A25cSystemNumberOfParticipantsRaceBlackOrAfricanAmerican).HasColumnName("A25c_System_NumberOfParticipantsRaceBlackOrAfricanAmerican");

                entity.Property(e => e.A25dOverrideNumberOfParticipantsRaceNativeHawaiianOrOtherPacificIslander).HasColumnName("A25d_Override_NumberOfParticipantsRaceNativeHawaiianOrOtherPacificIslander");

                entity.Property(e => e.A25dSystemNumberOfParticipantsRaceNativeHawaiianOrOtherPacificIslander).HasColumnName("A25d_System_NumberOfParticipantsRaceNativeHawaiianOrOtherPacificIslander");

                entity.Property(e => e.A25eOverrideNumberOfParticipantsRaceWhite).HasColumnName("A25e_Override_NumberOfParticipantsRaceWhite");

                entity.Property(e => e.A25eSystemNumberOfParticipantsRaceWhite).HasColumnName("A25e_System_NumberOfParticipantsRaceWhite");

                entity.Property(e => e.A25fOverrideNumberOfParticipantsRaceBiracialMultiRacial).HasColumnName("A25f_Override_NumberOfParticipantsRaceBiracialMultiRacial");

                entity.Property(e => e.A25fSystemNumberOfParticipantsRaceBiracialMultiRacial).HasColumnName("A25f_System_NumberOfParticipantsRaceBiracialMultiRacial");

                entity.Property(e => e.A25g1OverrideNumberOfParticipantsRaceOtherExplain).HasColumnName("A25g1_Override_NumberOfParticipantsRaceOtherExplain");

                entity.Property(e => e.A25g1SystemNumberOfParticipantsRaceOtherExplain).HasColumnName("A25g1_System_NumberOfParticipantsRaceOtherExplain");

                entity.Property(e => e.A25gOverrideNumberOfParticipantsRaceOther).HasColumnName("A25g_Override_NumberOfParticipantsRaceOther");

                entity.Property(e => e.A25gSystemNumberOfParticipantsRaceOther).HasColumnName("A25g_System_NumberOfParticipantsRaceOther");

                entity.Property(e => e.A25h1OverrideNumberOfParticipantsRaceUnspecifiedExplain).HasColumnName("A25h1_Override_NumberOfParticipantsRaceUnspecifiedExplain");

                entity.Property(e => e.A25h1SystemNumberOfParticipantsRaceUnspecifiedExplain).HasColumnName("A25h1_System_NumberOfParticipantsRaceUnspecifiedExplain");

                entity.Property(e => e.A25hOverrideNumberOfParticipantsRaceUnspecified).HasColumnName("A25h_Override_NumberOfParticipantsRaceUnspecified");

                entity.Property(e => e.A25hSystemNumberOfParticipantsRaceUnspecified).HasColumnName("A25h_System_NumberOfParticipantsRaceUnspecified");

                entity.Property(e => e.A26aOverrideNumberOfParticipantsLanguageAtHomeEnglish).HasColumnName("A26a_Override_NumberOfParticipantsLanguageAtHomeEnglish");

                entity.Property(e => e.A26aSystemNumberOfParticipantsLanguageAtHomeEnglish).HasColumnName("A26a_System_NumberOfParticipantsLanguageAtHomeEnglish");

                entity.Property(e => e.A26bOverrideNumberOfParticipantsLanguageAtHomeSpanish).HasColumnName("A26b_Override_NumberOfParticipantsLanguageAtHomeSpanish");

                entity.Property(e => e.A26bSystemNumberOfParticipantsLanguageAtHomeSpanish).HasColumnName("A26b_System_NumberOfParticipantsLanguageAtHomeSpanish");

                entity.Property(e => e.A26cOverrideNumberOfParticipantsLanguageAtHomeNativeCentralSouthAmericanMexican).HasColumnName("A26c_Override_NumberOfParticipantsLanguageAtHomeNativeCentralSouthAmericanMexican");

                entity.Property(e => e.A26cSystemNumberOfParticipantsLanguageAtHomeNativeCentralSouthAmericanMexican).HasColumnName("A26c_System_NumberOfParticipantsLanguageAtHomeNativeCentralSouthAmericanMexican");

                entity.Property(e => e.A26dOverrideNumberOfParticipantsLanguageAtHomeCaribbeanLanguages).HasColumnName("A26d_Override_NumberOfParticipantsLanguageAtHomeCaribbeanLanguages");

                entity.Property(e => e.A26dSystemNumberOfParticipantsLanguageAtHomeCaribbeanLanguages).HasColumnName("A26d_System_NumberOfParticipantsLanguageAtHomeCaribbeanLanguages");

                entity.Property(e => e.A26eOverrideNumberOfParticipantsLanguageAtHomeMiddleEasternandSouthAsianLanguages).HasColumnName("A26e_Override_NumberOfParticipantsLanguageAtHomeMiddleEasternandSouthAsianLanguages");

                entity.Property(e => e.A26eSystemNumberOfParticipantsLanguageAtHomeMiddleEasternandSouthAsianLanguages).HasColumnName("A26e_System_NumberOfParticipantsLanguageAtHomeMiddleEasternandSouthAsianLanguages");

                entity.Property(e => e.A26fOverrideNumberOfParticipantsLanguageAtHomeEastAsianLanguages).HasColumnName("A26f_Override_NumberOfParticipantsLanguageAtHomeEastAsianLanguages");

                entity.Property(e => e.A26fSystemNumberOfParticipantsLanguageAtHomeEastAsianLanguages).HasColumnName("A26f_System_NumberOfParticipantsLanguageAtHomeEastAsianLanguages");

                entity.Property(e => e.A26gOverrideNumberOfParticipantsLanguageNativeNorthAmericanAlaskaNativeLanguages).HasColumnName("A26g_Override_NumberOfParticipantsLanguageNativeNorthAmericanAlaskaNativeLanguages");

                entity.Property(e => e.A26gSystemNumberOfParticipantsLanguageNativeNorthAmericanAlaskaNativeLanguages).HasColumnName("A26g_System_NumberOfParticipantsLanguageNativeNorthAmericanAlaskaNativeLanguages");

                entity.Property(e => e.A26hOverrideNumberOfParticipantsLanguagePacificIslandLanguages).HasColumnName("A26h_Override_NumberOfParticipantsLanguagePacificIslandLanguages");

                entity.Property(e => e.A26hSystemNumberOfParticipantsLanguagePacificIslandLanguages).HasColumnName("A26h_System_NumberOfParticipantsLanguagePacificIslandLanguages");

                entity.Property(e => e.A26iOverrideNumberOfParticipantsLanguageEuropeanSlavicLanguages).HasColumnName("A26i_Override_NumberOfParticipantsLanguageEuropeanSlavicLanguages");

                entity.Property(e => e.A26iSystemNumberOfParticipantsLanguageEuropeanSlavicLanguages).HasColumnName("A26i_System_NumberOfParticipantsLanguageEuropeanSlavicLanguages");

                entity.Property(e => e.A26jOverrideNumberOfParticipantsLanguageAfricanLanguages).HasColumnName("A26j_Override_NumberOfParticipantsLanguageAfricanLanguages");

                entity.Property(e => e.A26jSystemNumberOfParticipantsLanguageAfricanLanguages).HasColumnName("A26j_System_NumberOfParticipantsLanguageAfricanLanguages");

                entity.Property(e => e.A26k1OverrideNumberOfParticipantsLanguageOtherExplain).HasColumnName("A26k1_Override_NumberOfParticipantsLanguageOtherExplain");

                entity.Property(e => e.A26k1SystemNumberOfParticipantsLanguageOtherExplain).HasColumnName("A26k1_System_NumberOfParticipantsLanguageOtherExplain");

                entity.Property(e => e.A26kOverrideNumberOfParticipantsLanguageOther).HasColumnName("A26k_Override_NumberOfParticipantsLanguageOther");

                entity.Property(e => e.A26kSystemNumberOfParticipantsLanguageOther).HasColumnName("A26k_System_NumberOfParticipantsLanguageOther");

                entity.Property(e => e.A27SystemTransportation).HasColumnName("A27_System_Transportation");

                entity.Property(e => e.A27aSystemTransportationNumberofChildren).HasColumnName("A27a_System_TransportationNumberofChildren");

                entity.Property(e => e.A28SystemNumberOfBusesOwned).HasColumnName("A28_System_NumberOfBusesOwned");

                entity.Property(e => e.A28aSystemNumberOfBusesPurchasedSinceLastYear).HasColumnName("A28a_System_NumberOfBusesPurchasedSinceLastYear");

                entity.Property(e => e.A29SystemLeasedBuses).HasColumnName("A29_System_LeasedBuses");

                entity.Property(e => e.A29aSystemNumberOfBusesLeased).HasColumnName("A29a_System_NumberOfBusesLeased");

                entity.Property(e => e.A2aOverrideFundedEnrollmentHeadStartEarlyHeadStart).HasColumnName("A2a_Override_FundedEnrollmentHeadStartEarlyHeadStart");

                entity.Property(e => e.A2aOverrideFundedEnrollmentMiechvgrantProgram).HasColumnName("A2a_Override_FundedEnrollmentMIECHVGrantProgram");

                entity.Property(e => e.A2aOverrideFundedEnrollmentNonFederalSources).HasColumnName("A2a_Override_FundedEnrollmentNonFederalSources");

                entity.Property(e => e.A2aSystemFundedEnrollmentHeadStartEarlyHeadStart).HasColumnName("A2a_System_FundedEnrollmentHeadStartEarlyHeadStart");

                entity.Property(e => e.A2aSystemFundedEnrollmentMiechvgrantProgram).HasColumnName("A2a_System_FundedEnrollmentMIECHVGrantProgram");

                entity.Property(e => e.A2aSystemFundedEnrollmentNonFederalSources).HasColumnName("A2a_System_FundedEnrollmentNonFederalSources");

                entity.Property(e => e.A30SystemManagementInformationSystem).HasColumnName("A30_System_ManagementInformationSystem");

                entity.Property(e => e.A30a1SystemManagementInformationSystemLocallyDesigned1).HasColumnName("A30a1_System_ManagementInformationSystemLocallyDesigned1");

                entity.Property(e => e.A30a1SystemManagementInformationSystemNameTitle1).HasColumnName("A30a1_System_ManagementInformationSystemNameTitle1");

                entity.Property(e => e.A30a2SystemManagementInformationSystemWebBased1).HasColumnName("A30a2_System_ManagementInformationSystemWebBased1");

                entity.Property(e => e.A30b1SystemManagementInformationSystemNameTitle2).HasColumnName("A30b1_System_ManagementInformationSystemNameTitle2");

                entity.Property(e => e.A30b2SystemManagementInformationSystemLocallyDesigned2).HasColumnName("A30b2_System_ManagementInformationSystemLocallyDesigned2");

                entity.Property(e => e.A30b3SystemManagementInformationSystemWebBased2).HasColumnName("A30b3_System_ManagementInformationSystemWebBased2");

                entity.Property(e => e.A30c1SystemManagementInformationSystemNameTitle3).HasColumnName("A30c1_System_ManagementInformationSystemNameTitle3");

                entity.Property(e => e.A30c2SystemManagementInformationSystemLocallyDesigned3).HasColumnName("A30c2_System_ManagementInformationSystemLocallyDesigned3");

                entity.Property(e => e.A30c3SystemManagementInformationSystemWebBased3).HasColumnName("A30c3_System_ManagementInformationSystemWebBased3");

                entity.Property(e => e.A3a1OverrideCenterBasedOption5DaysPerWeekFullWorkingDayFundedEnrollment).HasColumnName("A3a1_Override_CenterBasedOption5DaysPerWeekFullWorkingDayFundedEnrollment");

                entity.Property(e => e.A3a1SystemCenterBasedOption5DaysPerWeekFullWorkingDayFundedEnrollment).HasColumnName("A3a1_System_CenterBasedOption5DaysPerWeekFullWorkingDayFundedEnrollment");

                entity.Property(e => e.A3a1aOverrideCenterBasedOption5DaysPerWeekFullWorkingDayFullCalendarYearFundedEnrollment).HasColumnName("A3a1a_Override_CenterBasedOption5DaysPerWeekFullWorkingDayFullCalendarYearFundedEnrollment");

                entity.Property(e => e.A3a1aSystemCenterBasedOption5DaysPerWeekFullWorkingDayFullCalendarYearFundedEnrollment).HasColumnName("A3a1a_System_CenterBasedOption5DaysPerWeekFullWorkingDayFullCalendarYearFundedEnrollment");

                entity.Property(e => e.A3aOverrideCenterBasedOption5DaysPerWeekFullDayFundedEnrollment).HasColumnName("A3a_Override_CenterBasedOption5DaysPerWeekFullDayFundedEnrollment");

                entity.Property(e => e.A3aSystemCenterBasedOption5DaysPerWeekFullDayFundedEnrollment).HasColumnName("A3a_System_CenterBasedOption5DaysPerWeekFullDayFundedEnrollment");

                entity.Property(e => e.A3b1OverrideCenterBasedOption5DaysPerWeekPartDayDoubleSessionFundedEnrollment).HasColumnName("A3b1_Override_CenterBasedOption5DaysPerWeekPartDayDoubleSessionFundedEnrollment");

                entity.Property(e => e.A3b1SystemCenterBasedOption5DaysPerWeekPartDayDoubleSessionFundedEnrollment).HasColumnName("A3b1_System_CenterBasedOption5DaysPerWeekPartDayDoubleSessionFundedEnrollment");

                entity.Property(e => e.A3bOverrideCenterBasedOption5DaysPerWeekPartDayFundedEnrollment).HasColumnName("A3b_Override_CenterBasedOption5DaysPerWeekPartDayFundedEnrollment");

                entity.Property(e => e.A3bSystemCenterBasedOption5DaysPerWeekPartDayFundedEnrollment).HasColumnName("A3b_System_CenterBasedOption5DaysPerWeekPartDayFundedEnrollment");

                entity.Property(e => e.A4aOverrideCenterBasedOption4DaysPerWeekFullDayFundedEnrollment).HasColumnName("A4a_Override_CenterBasedOption4DaysPerWeekFullDayFundedEnrollment");

                entity.Property(e => e.A4aSystemCenterBasedOption4DaysPerWeekFullDayFundedEnrollment).HasColumnName("A4a_System_CenterBasedOption4DaysPerWeekFullDayFundedEnrollment");

                entity.Property(e => e.A4b1OverrideCenterBasedOption4DaysPerWeekPartDayDoubleSessionFundedEnrollment).HasColumnName("A4b1_Override_CenterBasedOption4DaysPerWeekPartDayDoubleSessionFundedEnrollment");

                entity.Property(e => e.A4b1SystemCenterBasedOption4DaysPerWeekPartDayDoubleSessionFundedEnrollment).HasColumnName("A4b1_System_CenterBasedOption4DaysPerWeekPartDayDoubleSessionFundedEnrollment");

                entity.Property(e => e.A4bOverrideCenterBasedOption4DaysPerWeekPartDayFundedEnrollment).HasColumnName("A4b_Override_CenterBasedOption4DaysPerWeekPartDayFundedEnrollment");

                entity.Property(e => e.A4bSystemCenterBasedOption4DaysPerWeekPartDayFundedEnrollment).HasColumnName("A4b_System_CenterBasedOption4DaysPerWeekPartDayFundedEnrollment");

                entity.Property(e => e.A5OverrideHomeBasedOptionFundedEnrollment).HasColumnName("A5_Override_HomeBasedOptionFundedEnrollment");

                entity.Property(e => e.A5SystemHomeBasedOptionFundedEnrollment).HasColumnName("A5_System_HomeBasedOptionFundedEnrollment");

                entity.Property(e => e.A6OverrideCombinationOptionFundedEnrollment).HasColumnName("A6_Override_CombinationOptionFundedEnrollment");

                entity.Property(e => e.A6SystemCombinationOptionFundedEnrollment).HasColumnName("A6_System_CombinationOptionFundedEnrollment");

                entity.Property(e => e.A7OverrideFamilyChildCareOptionFundedEnrollment).HasColumnName("A7_Override_FamilyChildCareOptionFundedEnrollment");

                entity.Property(e => e.A7SystemFamilyChildCareOptionFundedEnrollment).HasColumnName("A7_System_FamilyChildCareOptionFundedEnrollment");

                entity.Property(e => e.A7a1OverrideFamilyChildCareFullWorkingDayFullCalendarYearFundedEnrollment).HasColumnName("A7a1_Override_FamilyChildCareFullWorkingDayFullCalendarYearFundedEnrollment");

                entity.Property(e => e.A7a1SystemFamilyChildCareFullWorkingDayFullCalendarYearFundedEnrollment).HasColumnName("A7a1_System_FamilyChildCareFullWorkingDayFullCalendarYearFundedEnrollment");

                entity.Property(e => e.A7aOverrideFamilyChildCareFullWorkingDayFundedEnrollment).HasColumnName("A7a_Override_FamilyChildCareFullWorkingDayFundedEnrollment");

                entity.Property(e => e.A7aSystemFamilyChildCareFullWorkingDayFundedEnrollment).HasColumnName("A7a_System_FamilyChildCareFullWorkingDayFundedEnrollment");

                entity.Property(e => e.A8OverrideLocallyDesignedOptionFundedEnrollment).HasColumnName("A8_Override_LocallyDesignedOptionFundedEnrollment");

                entity.Property(e => e.A8SystemLocallyDesignedOptionFundedEnrollment).HasColumnName("A8_System_LocallyDesignedOptionFundedEnrollment");

                entity.Property(e => e.A9OverridePregnantWomenFundedEnrollment).HasColumnName("A9_Override_PregnantWomenFundedEnrollment");

                entity.Property(e => e.A9SystemPregnantWomenFundedEnrollment).HasColumnName("A9_System_PregnantWomenFundedEnrollment");

                entity.Property(e => e.C10aOverrideNumberChildrenUnderweightAtEnrollment).HasColumnName("C10a_Override_NumberChildrenUnderweightAtEnrollment");

                entity.Property(e => e.C10aSystemNumberChildrenUnderweightAtEnrollment).HasColumnName("C10a_System_NumberChildrenUnderweightAtEnrollment");

                entity.Property(e => e.C10bOverrideNumberChildrenHealthyWeightAtEnrollment).HasColumnName("C10b_Override_NumberChildrenHealthyWeightAtEnrollment");

                entity.Property(e => e.C10bSystemNumberChildrenHealthyWeightAtEnrollment).HasColumnName("C10b_System_NumberChildrenHealthyWeightAtEnrollment");

                entity.Property(e => e.C10cOverrideNumberChildrenOverweightAtEnrollment).HasColumnName("C10c_Override_NumberChildrenOverweightAtEnrollment");

                entity.Property(e => e.C10cSystemNumberChildrenOverweightAtEnrollment).HasColumnName("C10c_System_NumberChildrenOverweightAtEnrollment");

                entity.Property(e => e.C10dOverrideNumberChildrenObeseAtEnrollment).HasColumnName("C10d_Override_NumberChildrenObeseAtEnrollment");

                entity.Property(e => e.C10dSystemNumberChildrenObeseAtEnrollment).HasColumnName("C10d_System_NumberChildrenObeseAtEnrollment");

                entity.Property(e => e.C111OverrideNumberChildrenImmunizationsUpToDateAtEnrollment).HasColumnName("C11_1_Override_NumberChildrenImmunizationsUpToDateAtEnrollment");

                entity.Property(e => e.C111SystemNumberChildrenImmunizationsUpToDateAtEnrollment).HasColumnName("C11_1_System_NumberChildrenImmunizationsUpToDateAtEnrollment");

                entity.Property(e => e.C112OverrideNumberChildrenImmunizationsUpToDateAtEndOfEnrollment).HasColumnName("C11_2_Override_NumberChildrenImmunizationsUpToDateAtEndOfEnrollment");

                entity.Property(e => e.C112SystemNumberChildrenImmunizationsUpToDateAtEndOfEnrollment).HasColumnName("C11_2_System_NumberChildrenImmunizationsUpToDateAtEndOfEnrollment");

                entity.Property(e => e.C11OverrideNumberChildrenWithHealthInsuranceAtEnrollment).HasColumnName("C1_1_Override_NumberChildrenWithHealthInsuranceAtEnrollment");

                entity.Property(e => e.C11SystemNumberChildrenWithHealthInsuranceAtEnrollment).HasColumnName("C1_1_System_NumberChildrenWithHealthInsuranceAtEnrollment");

                entity.Property(e => e.C121OverrideNumberChildrenImmunizationsAllPossibleAtTiemAtEnrollmentOverride).HasColumnName("C12_1_Override_NumberChildrenImmunizationsAllPossibleAtTiemAtEnrollmentOverride");

                entity.Property(e => e.C121SystemNumberChildrenImmunizationsAllPossibleAtTiemAtEnrollment).HasColumnName("C12_1_System_NumberChildrenImmunizationsAllPossibleAtTiemAtEnrollment");

                entity.Property(e => e.C122OverrideNumberChildrenImmunizationsAllPossibleAtTiemAtEndOfEnrollment).HasColumnName("C12_2_Override_NumberChildrenImmunizationsAllPossibleAtTiemAtEndOfEnrollment");

                entity.Property(e => e.C122SystemNumberChildrenImmunizationsAllPossibleAtTiemAtEndOfEnrollment).HasColumnName("C12_2_System_NumberChildrenImmunizationsAllPossibleAtTiemAtEndOfEnrollment");

                entity.Property(e => e.C12OverrideNumberChildrenWithHealthInsuranceAtEndOfEnrollment).HasColumnName("C1_2_Override_NumberChildrenWithHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C12SystemNumberChildrenWithHealthInsuranceAtEndOfEnrollment).HasColumnName("C1_2_System_NumberChildrenWithHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C131OverrideNumberChildrenImmunizationsExemptAtEnrollment).HasColumnName("C13_1_Override_NumberChildrenImmunizationsExemptAtEnrollment");

                entity.Property(e => e.C131SystemNumberChildrenImmunizationsExemptAtEnrollment).HasColumnName("C13_1_System_NumberChildrenImmunizationsExemptAtEnrollment");

                entity.Property(e => e.C132OverrideNumberChildrenImmunizationsExemptAtEndOfEnrollment).HasColumnName("C13_2_Override_NumberChildrenImmunizationsExemptAtEndOfEnrollment");

                entity.Property(e => e.C132SystemNumberChildrenImmunizationsExemptAtEndOfEnrollment).HasColumnName("C13_2_System_NumberChildrenImmunizationsExemptAtEndOfEnrollment");

                entity.Property(e => e.C14aOverrideNumberPregnantWomenPrenatalHealthCare).HasColumnName("C14a_Override_NumberPregnantWomenPrenatalHealthCare");

                entity.Property(e => e.C14aSystemNumberPregnantWomenPrenatalHealthCare).HasColumnName("C14a_System_NumberPregnantWomenPrenatalHealthCare");

                entity.Property(e => e.C14bOverrideNumberPregnantWomenPostpartumHealthCare).HasColumnName("C14b_Override_NumberPregnantWomenPostpartumHealthCare");

                entity.Property(e => e.C14bSystemNumberPregnantWomenPostpartumHealthCare).HasColumnName("C14b_System_NumberPregnantWomenPostpartumHealthCare");

                entity.Property(e => e.C14cOverrideNumberPregnantWomenMentalHealthInterventions).HasColumnName("C14c_Override_NumberPregnantWomenMentalHealthInterventions");

                entity.Property(e => e.C14cSystemNumberPregnantWomenMentalHealthInterventions).HasColumnName("C14c_System_NumberPregnantWomenMentalHealthInterventions");

                entity.Property(e => e.C14dOverrideNumberPregnantWomenSubstanceAbusePrevention).HasColumnName("C14d_Override_NumberPregnantWomenSubstanceAbusePrevention");

                entity.Property(e => e.C14dSystemNumberPregnantWomenSubstanceAbusePrevention).HasColumnName("C14d_System_NumberPregnantWomenSubstanceAbusePrevention");

                entity.Property(e => e.C14eOverrideNumberPregnantWomenSubstanceAbuseTreatment).HasColumnName("C14e_Override_NumberPregnantWomenSubstanceAbuseTreatment");

                entity.Property(e => e.C14eSystemNumberPregnantWomenSubstanceAbuseTreatment).HasColumnName("C14e_System_NumberPregnantWomenSubstanceAbuseTreatment");

                entity.Property(e => e.C14fOverrideNumberPregnantWomenPrenatalEducationOnFetalDevelopment).HasColumnName("C14f_Override_NumberPregnantWomenPrenatalEducationOnFetalDevelopment");

                entity.Property(e => e.C14fSystemNumberPregnantWomenPrenatalEducationOnFetalDevelopment).HasColumnName("C14f_System_NumberPregnantWomenPrenatalEducationOnFetalDevelopment");

                entity.Property(e => e.C14gOverrideNumberPregnantWomenBreastfeedingBenefitsInfo).HasColumnName("C14g_Override_NumberPregnantWomenBreastfeedingBenefitsInfo");

                entity.Property(e => e.C14gSystemNumberPregnantWomenBreastfeedingBenefitsInfo).HasColumnName("C14g_System_NumberPregnantWomenBreastfeedingBenefitsInfo");

                entity.Property(e => e.C15aOverrideNumberPregnantWomenEnrolled1stTrimester).HasColumnName("C15a_Override_NumberPregnantWomenEnrolled1stTrimester");

                entity.Property(e => e.C15aSystemNumberPregnantWomenEnrolled1stTrimester).HasColumnName("C15a_System_NumberPregnantWomenEnrolled1stTrimester");

                entity.Property(e => e.C15bOverrideNumberPregnantWomenEnrolled2ndTrimester).HasColumnName("C15b_Override_NumberPregnantWomenEnrolled2ndTrimester");

                entity.Property(e => e.C15bSystemNumberPregnantWomenEnrolled2ndTrimester).HasColumnName("C15b_System_NumberPregnantWomenEnrolled2ndTrimester");

                entity.Property(e => e.C15cOverrideNumberPregnantWomenEnrolled3rdTrimester).HasColumnName("C15c_Override_NumberPregnantWomenEnrolled3rdTrimester");

                entity.Property(e => e.C15cSystemNumberPregnantWomenEnrolled3rdTrimester).HasColumnName("C15c_System_NumberPregnantWomenEnrolled3rdTrimester");

                entity.Property(e => e.C16OverrideNumberPregnantWomenHighRisk).HasColumnName("C16_Override_NumberPregnantWomenHighRisk");

                entity.Property(e => e.C16SystemNumberPregnantWomenHighRisk).HasColumnName("C16_System_NumberPregnantWomenHighRisk");

                entity.Property(e => e.C171OverrideNumberChildrenWithDentalCareSourceAtEnrollment).HasColumnName("C17_1_Override_NumberChildrenWithDentalCareSourceAtEnrollment");

                entity.Property(e => e.C171SystemNumberChildrenWithDentalCareSourceAtEnrollment).HasColumnName("C17_1_System_NumberChildrenWithDentalCareSourceAtEnrollment");

                entity.Property(e => e.C172OverrideNumberChildrenWithDentalCareSourceAtEndOfEnrollment).HasColumnName("C17_2_Override_NumberChildrenWithDentalCareSourceAtEndOfEnrollment");

                entity.Property(e => e.C172SystemNumberChildrenWithDentalCareSourceAtEndOfEnrollment).HasColumnName("C17_2_System_NumberChildrenWithDentalCareSourceAtEndOfEnrollment");

                entity.Property(e => e.C18OverrideNumberChildrenReceivedPreventiveDentalCareAtEndOfEnrollment).HasColumnName("C18_Override_NumberChildrenReceivedPreventiveDentalCareAtEndOfEnrollment");

                entity.Property(e => e.C18SystemNumberChildrenReceivedPreventiveDentalCareAtEndOfEnrollment).HasColumnName("C18_System_NumberChildrenReceivedPreventiveDentalCareAtEndOfEnrollment");

                entity.Property(e => e.C19OverrideNumberChildrenCompletedDentalExamEndOfEnrollment).HasColumnName("C19_Override_NumberChildrenCompletedDentalExamEndOfEnrollment");

                entity.Property(e => e.C19SystemNumberChildrenCompletedDentalExamEndOfEnrollment).HasColumnName("C19_System_NumberChildrenCompletedDentalExamEndOfEnrollment");

                entity.Property(e => e.C19a1OverrideNumberChildrenDentalTreatmentReceivedEndOfEnrollmentOverride).HasColumnName("C19a1_Override_NumberChildrenDentalTreatmentReceivedEndOfEnrollmentOverride");

                entity.Property(e => e.C19a1SystemNumberChildrenDentalTreatmentReceivedEndOfEnrollment).HasColumnName("C19a1_System_NumberChildrenDentalTreatmentReceivedEndOfEnrollment");

                entity.Property(e => e.C19aOverrideNumberChildrenDentalTreatmentRequiredEndOfEnrollment).HasColumnName("C19a_Override_NumberChildrenDentalTreatmentRequiredEndOfEnrollment");

                entity.Property(e => e.C19aSystemNumberChildrenDentalTreatmentRequiredEndOfEnrollment).HasColumnName("C19a_System_NumberChildrenDentalTreatmentRequiredEndOfEnrollment");

                entity.Property(e => e.C19b1OverrideNumberChildrenDentalTreatmentRequiredNotReceivedNoDentalCoverageEndOfEnrollment).HasColumnName("C19b1_Override_NumberChildrenDentalTreatmentRequiredNotReceivedNoDentalCoverageEndOfEnrollment");

                entity.Property(e => e.C19b1SystemNumberChildrenDentalTreatmentRequiredNotReceivedNoDentalCoverageEndOfEnrollment).HasColumnName("C19b1_System_NumberChildrenDentalTreatmentRequiredNotReceivedNoDentalCoverageEndOfEnrollment");

                entity.Property(e => e.C19b2OverrideNumberChildrenDentalTreatmentRequiredNotReceivedDentalCareUnavailableEndOfEnrollmentOverride).HasColumnName("C19b2_Override_NumberChildrenDentalTreatmentRequiredNotReceivedDentalCareUnavailableEndOfEnrollmentOverride");

                entity.Property(e => e.C19b2SystemNumberChildrenDentalTreatmentRequiredNotReceivedDentalCareUnavailableEndOfEnrollment).HasColumnName("C19b2_System_NumberChildrenDentalTreatmentRequiredNotReceivedDentalCareUnavailableEndOfEnrollment");

                entity.Property(e => e.C19b3OverrideNumberChildrenDentalTreatmentRequiredNotReceivedMedicaidNotAcceptedEndOfEnrollment).HasColumnName("C19b3_Override_NumberChildrenDentalTreatmentRequiredNotReceivedMedicaidNotAcceptedEndOfEnrollment");

                entity.Property(e => e.C19b3SystemNumberChildrenDentalTreatmentRequiredNotReceivedMedicaidNotAcceptedEndOfEnrollment).HasColumnName("C19b3_System_NumberChildrenDentalTreatmentRequiredNotReceivedMedicaidNotAcceptedEndOfEnrollment");

                entity.Property(e => e.C19b4OverrideNumberChildrenDentalTreatmentRequiredNotReceivedDentistsDoNotTreat3To5YearOldsEndOfEnrollment).HasColumnName("C19b4_Override_NumberChildrenDentalTreatmentRequiredNotReceivedDentistsDoNotTreat3To5YearOldsEndOfEnrollment");

                entity.Property(e => e.C19b4SystemNumberChildrenDentalTreatmentRequiredNotReceivedDentistsDoNotTreat3To5YearOldsEndOfEnrollment).HasColumnName("C19b4_System_NumberChildrenDentalTreatmentRequiredNotReceivedDentistsDoNotTreat3To5YearOldsEndOfEnrollment");

                entity.Property(e => e.C19b5OverrideNumberChildrenDentalTreatmentRequiredNotReceivedParentsDidNotKeepMakeAppointmentEndOfEnrollment).HasColumnName("C19b5_Override_NumberChildrenDentalTreatmentRequiredNotReceivedParentsDidNotKeepMakeAppointmentEndOfEnrollment");

                entity.Property(e => e.C19b5SystemNumberChildrenDentalTreatmentRequiredNotReceivedParentsDidNotKeepMakeAppointmentEndOfEnrollment).HasColumnName("C19b5_System_NumberChildrenDentalTreatmentRequiredNotReceivedParentsDidNotKeepMakeAppointmentEndOfEnrollment");

                entity.Property(e => e.C19b6OverrideNumberChildrenDentalTreatmentRequiredNotReceivedLeftBeforeAppointmentEndOfEnrollment).HasColumnName("C19b6_Override_NumberChildrenDentalTreatmentRequiredNotReceivedLeftBeforeAppointmentEndOfEnrollment");

                entity.Property(e => e.C19b6SystemNumberChildrenDentalTreatmentRequiredNotReceivedLeftBeforeAppointmentEndOfEnrollment).HasColumnName("C19b6_System_NumberChildrenDentalTreatmentRequiredNotReceivedLeftBeforeAppointmentEndOfEnrollment");

                entity.Property(e => e.C19b7OverrideNumberChildrenDentalTreatmentRequiredNotReceivedAppointmentScheduledForFutureEndOfEnrollment).HasColumnName("C19b7_Override_NumberChildrenDentalTreatmentRequiredNotReceivedAppointmentScheduledForFutureEndOfEnrollment");

                entity.Property(e => e.C19b7SystemNumberChildrenDentalTreatmentRequiredNotReceivedAppointmentScheduledForFutureEndOfEnrollment).HasColumnName("C19b7_System_NumberChildrenDentalTreatmentRequiredNotReceivedAppointmentScheduledForFutureEndOfEnrollment");

                entity.Property(e => e.C19b8OverrideNumberChildrenDentalTreatmentRequiredNotReceivedNoTransportationEndOfEnrollment).HasColumnName("C19b8_Override_NumberChildrenDentalTreatmentRequiredNotReceivedNoTransportationEndOfEnrollment");

                entity.Property(e => e.C19b8SystemNumberChildrenDentalTreatmentRequiredNotReceivedNoTransportationEndOfEnrollment).HasColumnName("C19b8_System_NumberChildrenDentalTreatmentRequiredNotReceivedNoTransportationEndOfEnrollment");

                entity.Property(e => e.C19b9OverrideNumberChildrenDentalTreatmentRequiredNotReceivedOtherEndOfEnrollment).HasColumnName("C19b9_Override_NumberChildrenDentalTreatmentRequiredNotReceivedOtherEndOfEnrollment");

                entity.Property(e => e.C19b9SystemNumberChildrenDentalTreatmentRequiredNotReceivedOtherEndOfEnrollment).HasColumnName("C19b9_System_NumberChildrenDentalTreatmentRequiredNotReceivedOtherEndOfEnrollment");

                entity.Property(e => e.C1a1OverrideNumberChildrenEnrolledinMedicaidChipatEnrollment).HasColumnName("C1a_1_Override_NumberChildrenEnrolledinMedicaidCHIPAtEnrollment");

                entity.Property(e => e.C1a1SystemNumberChildrenEnrolledinMedicaidChipatEnrollment).HasColumnName("C1a_1_System_NumberChildrenEnrolledinMedicaidCHIPAtEnrollment");

                entity.Property(e => e.C1a2OverrideNumberChildrenEnrolledinMedicaidChipatEndOfEnrollment).HasColumnName("C1a_2_Override_NumberChildrenEnrolledinMedicaidCHIPAtEndOfEnrollment");

                entity.Property(e => e.C1a2SystemNumberChildrenEnrolledinMedicaidChipatEndOfEnrollment).HasColumnName("C1a_2_System_NumberChildrenEnrolledinMedicaidCHIPAtEndOfEnrollment");

                entity.Property(e => e.C1b1OverrideNumberChildrenEnrolledinStateFundedInsuranceAtEnrollment).HasColumnName("C1b_1_Override_NumberChildrenEnrolledinStateFundedInsuranceAtEnrollment");

                entity.Property(e => e.C1b1SystemNumberChildrenEnrolledinStateFundedInsuranceAtEnrollment).HasColumnName("C1b_1_System_NumberChildrenEnrolledinStateFundedInsuranceAtEnrollment");

                entity.Property(e => e.C1b2OverrideNumberChildrenEnrolledinStateFundedInsuranceAtEndOfEnrollment).HasColumnName("C1b_2_Override_NumberChildrenEnrolledinStateFundedInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C1b2SystemNumberChildrenEnrolledinStateFundedInsuranceAtEndOfEnrollment).HasColumnName("C1b_2_System_NumberChildrenEnrolledinStateFundedInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C1c1OverrideNumberChildrenEnrolledinPrivateInsuranceAtEnrollment).HasColumnName("C1c_1_Override_NumberChildrenEnrolledinPrivateInsuranceAtEnrollment");

                entity.Property(e => e.C1c1OverrideNumberPregnantWomenrenEnrolledinPrivateInsuranceAtEnrollment).HasColumnName("C1c_1_Override_NumberPregnantWomenrenEnrolledinPrivateInsuranceAtEnrollment");

                entity.Property(e => e.C1c1SystemNumberChildrenEnrolledinPrivateInsuranceAtEnrollment).HasColumnName("C1c_1_System_NumberChildrenEnrolledinPrivateInsuranceAtEnrollment");

                entity.Property(e => e.C1c1SystemNumberPregnantWomenrenEnrolledinPrivateInsuranceAtEnrollment).HasColumnName("C1c_1_System_NumberPregnantWomenrenEnrolledinPrivateInsuranceAtEnrollment");

                entity.Property(e => e.C1c2OverrideNumberChildrenEnrolledinPrivateInsuranceAtEndOfEnrollment).HasColumnName("C1c_2_Override_NumberChildrenEnrolledinPrivateInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C1c2SystemNumberChildrenEnrolledinPrivateInsuranceAtEndOfEnrollment).HasColumnName("C1c_2_System_NumberChildrenEnrolledinPrivateInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C1d11OverrideNumberChildrenEnrolledinOtherDescriptionAtEnrollment).HasColumnName("C1d1_1_Override_NumberChildrenEnrolledinOtherDescriptionAtEnrollment");

                entity.Property(e => e.C1d11SystemNumberChildrenEnrolledinOtherDescriptionAtEnrollment).HasColumnName("C1d1_1_System_NumberChildrenEnrolledinOtherDescriptionAtEnrollment");

                entity.Property(e => e.C1d12OverrideNumberChildrenEnrolledinOtherDescriptionAtEndOfEnrollment).HasColumnName("C1d1_2_Override_NumberChildrenEnrolledinOtherDescriptionAtEndOfEnrollment");

                entity.Property(e => e.C1d12SystemNumberChildrenEnrolledinOtherDescriptionAtEndOfEnrollment).HasColumnName("C1d1_2_System_NumberChildrenEnrolledinOtherDescriptionAtEndOfEnrollment");

                entity.Property(e => e.C1d1OverrideNumberChildrenEnrolledinOtherInsuranceAtEnrollment).HasColumnName("C1d_1_Override_NumberChildrenEnrolledinOtherInsuranceAtEnrollment");

                entity.Property(e => e.C1d1SystemNumberChildrenEnrolledinOtherInsuranceAtEnrollment).HasColumnName("C1d_1_System_NumberChildrenEnrolledinOtherInsuranceAtEnrollment");

                entity.Property(e => e.C1d2OverrideNumberChildrenEnrolledinOtherInsuranceEndOfEnrollment).HasColumnName("C1d_2_Override_NumberChildrenEnrolledinOtherInsuranceEndOfEnrollment");

                entity.Property(e => e.C1d2SystemNumberChildrenEnrolledinOtherInsuranceEndOfEnrollment).HasColumnName("C1d_2_System_NumberChildrenEnrolledinOtherInsuranceEndOfEnrollment");

                entity.Property(e => e.C20OverrideNumberEhschildrenUpToDateDentalEpsdtendOfEnrollmentOverride).HasColumnName("C20_Override_NumberEHSChildrenUpToDateDentalEPSDTEndOfEnrollmentOverride");

                entity.Property(e => e.C20SystemNumberEhschildrenUpToDateDentalEpsdtendOfEnrollment).HasColumnName("C20_System_NumberEHSChildrenUpToDateDentalEPSDTEndOfEnrollment");

                entity.Property(e => e.C21OverrideNumberChildrenWithNoHealthInsuranceAtEnrollment).HasColumnName("C2_1_Override_NumberChildrenWithNoHealthInsuranceAtEnrollment");

                entity.Property(e => e.C21OverrideNumberPregnantWomenUpToDateDentalExamsTreatmentEndOfEnrollment).HasColumnName("C21_Override_NumberPregnantWomenUpToDateDentalExamsTreatmentEndOfEnrollment");

                entity.Property(e => e.C21SystemNumberChildrenWithNoHealthInsuranceAtEnrollment).HasColumnName("C2_1_System_NumberChildrenWithNoHealthInsuranceAtEnrollment");

                entity.Property(e => e.C21SystemNumberPregnantWomenUpToDateDentalExamsTreatmentEndOfEnrollment).HasColumnName("C21_System_NumberPregnantWomenUpToDateDentalExamsTreatmentEndOfEnrollment");

                entity.Property(e => e.C22OverrideNumberChildrenWithNoHealthInsuranceAtEndOfEnrollment).HasColumnName("C2_2_Override_NumberChildrenWithNoHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C22SystemNumberChildrenWithNoHealthInsuranceAtEndOfEnrollment).HasColumnName("C2_2_System_NumberChildrenWithNoHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C22SystemTotalHoursPerMonthMentalHealthProfessionalOnSite).HasColumnName("C22_System_TotalHoursPerMonthMentalHealthProfessionalOnSite");

                entity.Property(e => e.C23a1OverrideNumberChildrenMhprofessionalStaffConsultation3PlusEndOfEnrollment).HasColumnName("C23a1_Override_NumberChildrenMHProfessionalStaffConsultation3PlusEndOfEnrollment");

                entity.Property(e => e.C23a1SystemNumberChildrenMhprofessionalStaffConsultation3PlusEndOfEnrollment).HasColumnName("C23a1_System_NumberChildrenMHProfessionalStaffConsultation3PlusEndOfEnrollment");

                entity.Property(e => e.C23aOverrideNumberChildrenwithMhprofessionalStaffConsultationEndOfEnrollment).HasColumnName("C23a_Override_NumberChildrenwithMHProfessionalStaffConsultationEndOfEnrollment");

                entity.Property(e => e.C23aSystemNumberChildrenwithMhprofessionalStaffConsultationEndOfEnrollment).HasColumnName("C23a_System_NumberChildrenwithMHProfessionalStaffConsultationEndOfEnrollment");

                entity.Property(e => e.C23b1OverrideNumberChildrenMhprofessionalParentGuardianConsultation3PlusEndOfEnrollment).HasColumnName("C23b1_Override_NumberChildrenMHProfessionalParentGuardianConsultation3PlusEndOfEnrollment");

                entity.Property(e => e.C23b1SystemNumberChildrenMhprofessionalParentGuardianConsultation3PlusEndOfEnrollment).HasColumnName("C23b1_System_NumberChildrenMHProfessionalParentGuardianConsultation3PlusEndOfEnrollment");

                entity.Property(e => e.C23bOverrideNumberChildrenMhprofessionalParentGuardianConsultationEndOfEnrollment).HasColumnName("C23b_Override_NumberChildrenMHProfessionalParentGuardianConsultationEndOfEnrollment");

                entity.Property(e => e.C23bSystemNumberChildrenMhprofessionalParentGuardianConsultationEndOfEnrollment).HasColumnName("C23b_System_NumberChildrenMHProfessionalParentGuardianConsultationEndOfEnrollment");

                entity.Property(e => e.C23cOverrideNumberChildrenMhprofessionalMhassessmentEndOfEnrollment).HasColumnName("C23c_Override_NumberChildrenMHProfessionalMHAssessmentEndOfEnrollment");

                entity.Property(e => e.C23cSystemNumberChildrenMhprofessionalMhassessmentEndOfEnrollment).HasColumnName("C23c_System_NumberChildrenMHProfessionalMHAssessmentEndOfEnrollment");

                entity.Property(e => e.C23dOverrideNumberChildrenMhprofessionalMhreferralEndOfEnrollment).HasColumnName("C23d_Override_NumberChildrenMHProfessionalMHReferralEndOfEnrollment");

                entity.Property(e => e.C23dSystemNumberChildrenMhprofessionalMhreferralEndOfEnrollment).HasColumnName("C23d_System_NumberChildrenMHProfessionalMHReferralEndOfEnrollment");

                entity.Property(e => e.C24OverrideNumberChildrenProgramMhexternalReferralThisYearEndOfEnrollment).HasColumnName("C24_Override_NumberChildrenProgramMHExternalReferralThisYearEndOfEnrollment");

                entity.Property(e => e.C24SystemNumberChildrenProgramMhexternalReferralThisYearEndOfEnrollment).HasColumnName("C24_System_NumberChildrenProgramMHExternalReferralThisYearEndOfEnrollment");

                entity.Property(e => e.C24aOverrideNumberChildrenMhexternalReferralServicesReceivedEndOfEnrollment).HasColumnName("C24a_Override_NumberChildrenMHExternalReferralServicesReceivedEndOfEnrollment");

                entity.Property(e => e.C24aSystemNumberChildrenProgramMhexternalReferralServicesReceivedEndOfEnrollment).HasColumnName("C24a_System_NumberChildrenProgramMHExternalReferralServicesReceivedEndOfEnrollment");

                entity.Property(e => e.C25OverrideNumberChildrenWithIepthisEnrollmentYear).HasColumnName("C25_Override_NumberChildrenWithIEPThisEnrollmentYear");

                entity.Property(e => e.C25SystemNumberChildrenWithIepthisEnrollmentYear).HasColumnName("C25_System_NumberChildrenWithIEPThisEnrollmentYear");

                entity.Property(e => e.C25a1OverrideNumberChildrenWithIeppriorToEnrollmentYear).HasColumnName("C25a1_Override_NumberChildrenWithIEPPriorToEnrollmentYear");

                entity.Property(e => e.C25a1SystemNumberChildrenWithIeppriorToEnrollmentYear)
                    .HasColumnName("C25a1_System_NumberChildrenWithIEPPriorToEnrollmentYear")
                    .HasMaxLength(10);

                entity.Property(e => e.C25a2OverrideNumberChildrenWithIeppriorToEnrollmentYear).HasColumnName("C25a2_Override_NumberChildrenWithIEPPriorToEnrollmentYear");

                entity.Property(e => e.C25a2SystemNumberChildrenWithIepduringEnrollmentYear).HasColumnName("C25a2_System_NumberChildrenWithIEPDuringEnrollmentYear");

                entity.Property(e => e.C25bOverrideNumberChildrenWithIepservicesNotReceived).HasColumnName("C25b_Override_NumberChildrenWithIEPServicesNotReceived");

                entity.Property(e => e.C25bSystemNumberChildrenWithIepservicesNotReceived).HasColumnName("C25b_System_NumberChildrenWithIEPServicesNotReceived");

                entity.Property(e => e.C26OverrideNumberChildrenWithIfspthisEnrollmentYear).HasColumnName("C26_Override_NumberChildrenWithIFSPThisEnrollmentYear");

                entity.Property(e => e.C26SystemNumberChildrenWithIfspthisEnrollmentYear).HasColumnName("C26_System_NumberChildrenWithIFSPThisEnrollmentYear");

                entity.Property(e => e.C26a1OverrideNumberChildrenWithIfsppriorToEnrollmentYear).HasColumnName("C26a1_Override_NumberChildrenWithIFSPPriorToEnrollmentYear");

                entity.Property(e => e.C26a1SystemNumberChildrenWithIfsppriorToEnrollmentYear)
                    .HasColumnName("C26a1_System_NumberChildrenWithIFSPPriorToEnrollmentYear")
                    .HasMaxLength(10);

                entity.Property(e => e.C26a2OverrideNumberChildrenWithIfsppriorToEnrollmentYear).HasColumnName("C26a2_Override_NumberChildrenWithIFSPPriorToEnrollmentYear");

                entity.Property(e => e.C26a2SystemNumberChildrenWithIfspduringEnrollmentYear).HasColumnName("C26a2_System_NumberChildrenWithIFSPDuringEnrollmentYear");

                entity.Property(e => e.C26bOverrideNumberChildrenWithIfspservicesNotReceived).HasColumnName("C26b_Override_NumberChildrenWithIFSPServicesNotReceived");

                entity.Property(e => e.C26bSystemNumberChildrenWithIfspservicesNotReceived).HasColumnName("C26b_System_NumberChildrenWithIFSPServicesNotReceived");

                entity.Property(e => e.C27a1NumberChildrenHealthImpairmentDiagnosedPrimaryDisability).HasColumnName("C27a_1_NumberChildrenHealthImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27a1OverrideNumberChildrenHealthImpairmentDiagnosedPrimaryDisability).HasColumnName("C27a_1_Override_NumberChildrenHealthImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27a2OverrideNumberChildrenHealthImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27a_2_Override_NumberChildrenHealthImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27a2SystemNumberChildrenHealthImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27a_2_System_NumberChildrenHealthImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27b1OverrideNumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisability).HasColumnName("C27b_1_Override_NumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisability");

                entity.Property(e => e.C27b1SystemNumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisability).HasColumnName("C27b_1_System_NumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisability");

                entity.Property(e => e.C27b2OverrideNumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27b_2_Override_NumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27b2SystemNumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27b_2_System_NumberChildrenEmotionalDisturbanceDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27c1OverrideNumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisability).HasColumnName("C27c_1_Override_NumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisability");

                entity.Property(e => e.C27c1SystemNumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisability).HasColumnName("C27c_1_System_NumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisability");

                entity.Property(e => e.C27c2OverrideNumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27c_2_Override_NumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27c2SystemNumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27c_2_System_NumberChildrenSpeechLanguageImpairmentsDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27d1OverrideNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27d_1_Override_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27d1SystemNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27d_1_System_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27d2OverrideNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27d_2_Override_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27d2SystemNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27d_2_System_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27e1OverrideNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27e_1_Override_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27e1SystemNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27e_1_System_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27e2OverrideNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27e_2_Override_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27e2SystemNumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27e_2_System_NumberChildrenIntellectualDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27f1OverrideNumberChildrenHearingImpairmentDiagnosedPrimaryDisability).HasColumnName("C27f_1_Override_NumberChildrenHearingImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27f1SystemNumberChildrenHearingImpairmentDiagnosedPrimaryDisability).HasColumnName("C27f_1_System_NumberChildrenHearingImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27f2OverrideNumberChildrenHearingImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27f_2_Override_NumberChildrenHearingImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27f2SystemNumberChildrenHearingImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27f_2_System_NumberChildrenHearingImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27g1OverrideNumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisability).HasColumnName("C27g_1_Override_NumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27g1SystemNumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisability).HasColumnName("C27g_1_System_NumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27g2OverrideNumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27g_2_Override_NumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27g2SystemNumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27g_2_System_NumberChildrenOrthopedicImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27h1OverrideNumberChildrenVisualImpairmentDiagnosedPrimaryDisability).HasColumnName("C27h_1_Override_NumberChildrenVisualImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27h1SystemNumberChildrenVisualImpairmentDiagnosedPrimaryDisability).HasColumnName("C27h_1_System_NumberChildrenVisualImpairmentDiagnosedPrimaryDisability");

                entity.Property(e => e.C27h2OverrideNumberChildrenVisualImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27h_2_Override_NumberChildrenVisualImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27h2SystemNumberChildrenVisualImpairmentDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27h_2_System_NumberChildrenVisualImpairmentDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27i1OverrideNumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisability).HasColumnName("C27i_1_Override_NumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisability");

                entity.Property(e => e.C27i1SystemNumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisability).HasColumnName("C27i_1_System_NumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisability");

                entity.Property(e => e.C27i2OverrideNumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27i_2_Override_NumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27i2SystemNumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27i_2_System_NumberChildrenSpecificLearningDisabilityDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27j1OverrideNumberChildrenAutismDiagnosedPrimaryDisability).HasColumnName("C27j_1_Override_NumberChildrenAutismDiagnosedPrimaryDisability");

                entity.Property(e => e.C27j1SystemNumberChildrenAutismDiagnosedPrimaryDisability).HasColumnName("C27j_1_System_NumberChildrenAutismDiagnosedPrimaryDisability");

                entity.Property(e => e.C27j2OverrideNumberChildrenAutismDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27j_2_Override_NumberChildrenAutismDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27j2SystemNumberChildrenAutismDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27j_2_System_NumberChildrenAutismDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27k1OverrideNumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisability).HasColumnName("C27k_1_Override_NumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisability");

                entity.Property(e => e.C27k1SystemNumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisability).HasColumnName("C27k_1_System_NumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisability");

                entity.Property(e => e.C27k2OverrideNumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27k_2_Override_NumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27k2SystemNumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27k_2_System_NumberChildrenTraumaticBrainInjuryDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27l1OverrideNumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisability).HasColumnName("C27l_1_Override_NumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisability");

                entity.Property(e => e.C27l1SystemNumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisability).HasColumnName("C27l_1_System_NumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisability");

                entity.Property(e => e.C27l2OverrideNumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27l_2_Override_NumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27l2SystemNumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27l_2_System_NumberChildrenNonCategoricalDevelopmentalDelayDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27m1OverrideNumberChildrenDeafBlindDiagnosedPrimaryDisability).HasColumnName("C27m_1_Override_NumberChildrenDeafBlindDiagnosedPrimaryDisability");

                entity.Property(e => e.C27m1OverrideNumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27m_1_Override_NumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27m1SystemNumberChildrenDeafBlindDiagnosedPrimaryDisability).HasColumnName("C27m_1_System_NumberChildrenDeafBlindDiagnosedPrimaryDisability");

                entity.Property(e => e.C27m1SystemNumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisability).HasColumnName("C27m_1_System_NumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisability");

                entity.Property(e => e.C27m2OverrideNumberChildrenDeafBlindDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27m_2_Override_NumberChildrenDeafBlindDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27m2OverrideNumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27m_2_Override_NumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27m2SystemNumberChildrenDeafBlindDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27m_2_System_NumberChildrenDeafBlindDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C27m2SystemNumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisabilityReceivingServices).HasColumnName("C27m_2_System_NumberChildrenMultipleDisabilitiesDiagnosedPrimaryDisabilityReceivingServices");

                entity.Property(e => e.C28OverrideNumberChildrenNewlyEnrolledSinceLastPir).HasColumnName("C28_Override_NumberChildrenNewlyEnrolledSinceLastPIR");

                entity.Property(e => e.C28SystemNumberChildrenNewlyEnrolledSinceLastPir).HasColumnName("C28_System_NumberChildrenNewlyEnrolledSinceLastPIR");

                entity.Property(e => e.C29OverrideNumberChildrenDevSensoryBehavioralScreeningsWithin45Days).HasColumnName("C29_Override_NumberChildrenDevSensoryBehavioralScreeningsWithin45Days");

                entity.Property(e => e.C29SystemNumberChildrenDevSensoryBehavioralScreeningsWithin45Days).HasColumnName("C29_System_NumberChildrenDevSensoryBehavioralScreeningsWithin45Days");

                entity.Property(e => e.C29aOverrideNumberChildrenDevSensoryBehavioralScreeningsFollowUpRequired).HasColumnName("C29a_Override_NumberChildrenDevSensoryBehavioralScreeningsFollowUpRequired");

                entity.Property(e => e.C29aSystemNumberChildrenDevSensoryBehavioralScreeningsFollowUpRequired).HasColumnName("C29a_System_NumberChildrenDevSensoryBehavioralScreeningsFollowUpRequired");

                entity.Property(e => e.C31OverrideNumberPregnantWomenrenWithHealthInsuranceAtEnrollment).HasColumnName("C3_1_Override_NumberPregnantWomenrenWithHealthInsuranceAtEnrollment");

                entity.Property(e => e.C31SystemNumberPregnantWomenrenWithHealthInsuranceAtEnrollment).HasColumnName("C3_1_System_NumberPregnantWomenrenWithHealthInsuranceAtEnrollment");

                entity.Property(e => e.C32OverrideNumberPregnantWomenrenWithHealthInsuranceAtEndOfEnrollment).HasColumnName("C3_2_Override_NumberPregnantWomenrenWithHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C32SystemNumberPregnantWomenrenWithHealthInsuranceAtEndOfEnrollment).HasColumnName("C3_2_System_NumberPregnantWomenrenWithHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C35OverrideNumberFamilies).HasColumnName("C35_Override_NumberFamilies");

                entity.Property(e => e.C35SystemNumberFamilies).HasColumnName("C35_System_NumberFamilies");

                entity.Property(e => e.C35aOverrideNumberTwoParentFamilies).HasColumnName("C35a_Override_NumberTwoParentFamilies");

                entity.Property(e => e.C35aSystemNumberTwoParentFamilies).HasColumnName("C35a_System_NumberTwoParentFamilies");

                entity.Property(e => e.C35bOverrideNumberSingleParentFamilies).HasColumnName("C35b_Override_NumberSingleParentFamilies");

                entity.Property(e => e.C35bSystemNumberSingleParentFamilies).HasColumnName("C35b_System_NumberSingleParentFamilies");

                entity.Property(e => e.C36aOverrideNumberTwoParentFamiliesBothParentsEmployed).HasColumnName("C36a_Override_NumberTwoParentFamiliesBothParentsEmployed");

                entity.Property(e => e.C36aSystemNumberTwoParentFamiliesBothParentsEmployed).HasColumnName("C36a_System_NumberTwoParentFamiliesBothParentsEmployed");

                entity.Property(e => e.C36bOverrideNumberTwoParentFamiliesOneParentEmployed).HasColumnName("C36b_Override_NumberTwoParentFamiliesOneParentEmployed");

                entity.Property(e => e.C36bSystemNumberTwoParentFamiliesOneParentEmployed).HasColumnName("C36b_System_NumberTwoParentFamiliesOneParentEmployed");

                entity.Property(e => e.C36cOverrideNumberTwoParentFamiliesBothParentsUnemployed).HasColumnName("C36c_Override_NumberTwoParentFamiliesBothParentsUnemployed");

                entity.Property(e => e.C36cSystemNumberTwoParentFamiliesBothParentsUnemployed).HasColumnName("C36c_System_NumberTwoParentFamiliesBothParentsUnemployed");

                entity.Property(e => e.C37aOverrideNumberSingleParentFamiliesOneParentEmployed).HasColumnName("C37a_Override_NumberSingleParentFamiliesOneParentEmployed");

                entity.Property(e => e.C37aSystemNumberSingleParentFamiliesOneParentEmployed).HasColumnName("C37a_System_NumberSingleParentFamiliesOneParentEmployed");

                entity.Property(e => e.C37bOverrideNumberSingleParentFamiliesBothParentsUnemployed).HasColumnName("C37b_Override_NumberSingleParentFamiliesBothParentsUnemployed");

                entity.Property(e => e.C37bSystemNumberSingleParentFamiliesBothParentsUnemployed).HasColumnName("C37b_System_NumberSingleParentFamiliesBothParentsUnemployed");

                entity.Property(e => e.C38OverrideNumberFamiliesAtLeastOneParentIsActiveMilitaryMember).HasColumnName("C38_Override_NumberFamiliesAtLeastOneParentIsActiveMilitaryMember");

                entity.Property(e => e.C38SystemNumberFamiliesAtLeastOneParentIsActiveMilitaryMember).HasColumnName("C38_System_NumberFamiliesAtLeastOneParentIsActiveMilitaryMember");

                entity.Property(e => e.C39OverrideNumberFamiliesReceivingTanf).HasColumnName("C39_Override_NumberFamiliesReceivingTANF");

                entity.Property(e => e.C39SystemNumberFamiliesReceivingTanf).HasColumnName("C39_System_NumberFamiliesReceivingTANF");

                entity.Property(e => e.C3a1OverrideNumberPregnantWomenrenEnrolledinMedicaidAtEnrollment).HasColumnName("C3a_1_Override_NumberPregnantWomenrenEnrolledinMedicaidAtEnrollment");

                entity.Property(e => e.C3a1SystemNumberPregnantWomenrenEnrolledinMedicaidAtEnrollment).HasColumnName("C3a_1_System_NumberPregnantWomenrenEnrolledinMedicaidAtEnrollment");

                entity.Property(e => e.C3a2OverrideNumberPregnantWomenrenEnrolledinMedicaidAtEndOfEnrollment).HasColumnName("C3a_2_Override_NumberPregnantWomenrenEnrolledinMedicaidAtEndOfEnrollment");

                entity.Property(e => e.C3a2SystemNumberPregnantWomenrenEnrolledinMedicaidAtEndOfEnrollment).HasColumnName("C3a_2_System_NumberPregnantWomenrenEnrolledinMedicaidAtEndOfEnrollment");

                entity.Property(e => e.C3b1OverrideNumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEnrollment).HasColumnName("C3b_1_Override_NumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEnrollment");

                entity.Property(e => e.C3b1SystemNumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEnrollment).HasColumnName("C3b_1_System_NumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEnrollment");

                entity.Property(e => e.C3b2OverrideNumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEndOfEnrollment).HasColumnName("C3b_2_Override_NumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C3b2SystemNumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEndOfEnrollment).HasColumnName("C3b_2_System_NumberPregnantWomenrenEnrolledinPublicallyFundedInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C3c2OverrideNumberPregnantWomenrenEnrolledinPrivateInsuranceAtEndOfEnrollment).HasColumnName("C3c_2_Override_NumberPregnantWomenrenEnrolledinPrivateInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C3c2SystemNumberPregnantWomenrenEnrolledinPrivateInsuranceAtEndOfEnrollment).HasColumnName("C3c_2_System_NumberPregnantWomenrenEnrolledinPrivateInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C3d11OverrideNumberPregnantWomenrenEnrolledinOtherDescriptionAtEnrollment).HasColumnName("C3d1_1_Override_NumberPregnantWomenrenEnrolledinOtherDescriptionAtEnrollment");

                entity.Property(e => e.C3d11SystemNumberPregnantWomenrenEnrolledinOtherDescriptionAtEnrollment).HasColumnName("C3d1_1_System_NumberPregnantWomenrenEnrolledinOtherDescriptionAtEnrollment");

                entity.Property(e => e.C3d12OverrideNumberPregnantWomenrenEnrolledinOtherDescriptionAtEndOfEnrollment).HasColumnName("C3d1_2_Override_NumberPregnantWomenrenEnrolledinOtherDescriptionAtEndOfEnrollment");

                entity.Property(e => e.C3d12SystemNumberPregnantWomenrenEnrolledinOtherDescriptionAtEndOfEnrollment).HasColumnName("C3d1_2_System_NumberPregnantWomenrenEnrolledinOtherDescriptionAtEndOfEnrollment");

                entity.Property(e => e.C3d1OverrideNumberPregnantWomenrenEnrolledinOtherInsuranceAtEnrollment).HasColumnName("C3d_1_Override_NumberPregnantWomenrenEnrolledinOtherInsuranceAtEnrollment");

                entity.Property(e => e.C3d1SystemNumberPregnantWomenrenEnrolledinOtherInsuranceAtEnrollment).HasColumnName("C3d_1_System_NumberPregnantWomenrenEnrolledinOtherInsuranceAtEnrollment");

                entity.Property(e => e.C3d2OverrideNumberPregnantWomenrenEnrolledinOtherInsuranceEndOfEnrollment).HasColumnName("C3d_2_Override_NumberPregnantWomenrenEnrolledinOtherInsuranceEndOfEnrollment");

                entity.Property(e => e.C3d2SystemNumberPregnantWomenrenEnrolledinOtherInsuranceEndOfEnrollment).HasColumnName("C3d_2_System_NumberPregnantWomenrenEnrolledinOtherInsuranceEndOfEnrollment");

                entity.Property(e => e.C40OverrideNumberFamiliesReceivingSsi).HasColumnName("C40_Override_NumberFamiliesReceivingSSI");

                entity.Property(e => e.C40SystemNumberFamiliesReceivingSsi).HasColumnName("C40_System_NumberFamiliesReceivingSSI");

                entity.Property(e => e.C41OverrideNumberFamiliesReceivingWic).HasColumnName("C41_Override_NumberFamiliesReceivingWIC");

                entity.Property(e => e.C41OverrideNumberPregnantWomenrenWithNoHealthInsuranceAtEnrollment).HasColumnName("C4_1_Override_NumberPregnantWomenrenWithNoHealthInsuranceAtEnrollment");

                entity.Property(e => e.C41SystemNumberFamiliesReceivingWic).HasColumnName("C41_System_NumberFamiliesReceivingWIC");

                entity.Property(e => e.C41SystemNumberPregnantWomenrenWithNoHealthInsuranceAtEnrollment).HasColumnName("C4_1_System_NumberPregnantWomenrenWithNoHealthInsuranceAtEnrollment");

                entity.Property(e => e.C42OverrideNumberFamiliesReceivingSnap).HasColumnName("C42_Override_NumberFamiliesReceivingSNAP");

                entity.Property(e => e.C42OverrideNumberPregnantWomenrenWithNoHealthInsuranceAtEndOfEnrollment).HasColumnName("C4_2_Override_NumberPregnantWomenrenWithNoHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C42SystemNumberFamiliesReceivingSnap).HasColumnName("C42_System_NumberFamiliesReceivingSNAP");

                entity.Property(e => e.C42SystemNumberPregnantWomenrenWithNoHealthInsuranceAtEndOfEnrollment).HasColumnName("C4_2_System_NumberPregnantWomenrenWithNoHealthInsuranceAtEndOfEnrollment");

                entity.Property(e => e.C43aOverrideNumberTwoParentFamiliesBothParentsInJobTrainingOrSchool).HasColumnName("C43a_Override_NumberTwoParentFamiliesBothParentsInJobTrainingOrSchool");

                entity.Property(e => e.C43aSystemNumberTwoParentFamiliesBothParentsInJobTrainingOrSchool).HasColumnName("C43a_System_NumberTwoParentFamiliesBothParentsInJobTrainingOrSchool");

                entity.Property(e => e.C43bOverrideNumberTwoParentFamiliesOneParentInJobTrainingOrSchool).HasColumnName("C43b_Override_NumberTwoParentFamiliesOneParentInJobTrainingOrSchool");

                entity.Property(e => e.C43bSystemNumberTwoParentFamiliesOneParentInJobTrainingOrSchool).HasColumnName("C43b_System_NumberTwoParentFamiliesOneParentInJobTrainingOrSchool");

                entity.Property(e => e.C43cOverrideNumberTwoParentFamiliesNeitherParentInJobTrainingOrSchool).HasColumnName("C43c_Override_NumberTwoParentFamiliesNeitherParentInJobTrainingOrSchool");

                entity.Property(e => e.C43cSystemNumberTwoParentFamiliesNeitherParentInJobTrainingOrSchool).HasColumnName("C43c_System_NumberTwoParentFamiliesNeitherParentInJobTrainingOrSchool");

                entity.Property(e => e.C44aOverrideNumberSingleParentFamiliesParentInJobTrainingOrSchool).HasColumnName("C44a_Override_NumberSingleParentFamiliesParentInJobTrainingOrSchool");

                entity.Property(e => e.C44aSystemNumberSingleParentFamiliesParentInJobTrainingOrSchool).HasColumnName("C44a_System_NumberSingleParentFamiliesParentInJobTrainingOrSchool");

                entity.Property(e => e.C44bOverrideNumberSingleParentFamiliesParentNotInJobTrainingOrSchool).HasColumnName("C44b_Override_NumberSingleParentFamiliesParentNotInJobTrainingOrSchool");

                entity.Property(e => e.C44bSystemNumberSingleParentFamiliesParentNotInJobTrainingOrSchool).HasColumnName("C44b_System_NumberSingleParentFamiliesParentNotInJobTrainingOrSchool");

                entity.Property(e => e.C45aOverrideNumberFamiliesHighestEducationAdvancedOrBaccalaureateDegree).HasColumnName("C45a_Override_NumberFamiliesHighestEducationAdvancedOrBaccalaureateDegree");

                entity.Property(e => e.C45aSystemNumberFamiliesHighestEducationAdvancedOrBaccalaureateDegree).HasColumnName("C45a_System_NumberFamiliesHighestEducationAdvancedOrBaccalaureateDegree");

                entity.Property(e => e.C45bOverrideNumberFamiliesHighestEducationAssociateDegreeVocationalOrSomeCollege).HasColumnName("C45b_Override_NumberFamiliesHighestEducationAssociateDegreeVocationalOrSomeCollege");

                entity.Property(e => e.C45bSystemNumberFamiliesHighestEducationAssociateDegreeVocationalOrSomeCollege).HasColumnName("C45b_System_NumberFamiliesHighestEducationAssociateDegreeVocationalOrSomeCollege");

                entity.Property(e => e.C45cOverrideNumberFamiliesHighestEducationHighSchoolGraduateOrGed).HasColumnName("C45c_Override_NumberFamiliesHighestEducationHighSchoolGraduateOrGED");

                entity.Property(e => e.C45cSystemNumberFamiliesHighestEducationHighSchoolGraduateOrGed).HasColumnName("C45c_System_NumberFamiliesHighestEducationHighSchoolGraduateOrGED");

                entity.Property(e => e.C45dOverrideNumberFamiliesHighestEducationLessThanHighSchoolGraduate).HasColumnName("C45d_Override_NumberFamiliesHighestEducationLessThanHighSchoolGraduate");

                entity.Property(e => e.C45dSystemNumberFamiliesHighestEducationLessThanHighSchoolGraduate).HasColumnName("C45d_System_NumberFamiliesHighestEducationLessThanHighSchoolGraduate");

                entity.Property(e => e.C46aOverrideNumberFamiliesReceivedEmergencyCrisisServices).HasColumnName("C46a_Override_NumberFamiliesReceivedEmergencyCrisisServices");

                entity.Property(e => e.C46aSystemNumberFamiliesReceivedEmergencyCrisisServices).HasColumnName("C46a_System_NumberFamiliesReceivedEmergencyCrisisServices");

                entity.Property(e => e.C46bOverrideNumberFamiliesReceivedHousingAssistance).HasColumnName("C46b_Override_NumberFamiliesReceivedHousingAssistance");

                entity.Property(e => e.C46bSystemNumberFamiliesReceivedHousingAssistance).HasColumnName("C46b_System_NumberFamiliesReceivedHousingAssistance");

                entity.Property(e => e.C46cOverrideNumberFamiliesReceivedMentalHealthServices).HasColumnName("C46c_Override_NumberFamiliesReceivedMentalHealthServices");

                entity.Property(e => e.C46cSystemNumberFamiliesReceivedMentalHealthServices).HasColumnName("C46c_System_NumberFamiliesReceivedMentalHealthServices");

                entity.Property(e => e.C46dOverrideNumberFamiliesReceivedEslservices).HasColumnName("C46d_Override_NumberFamiliesReceivedESLServices");

                entity.Property(e => e.C46dSystemNumberFamiliesReceivedEslservices).HasColumnName("C46d_System_NumberFamiliesReceivedESLServices");

                entity.Property(e => e.C46eOverrideNumberFamiliesReceivedAdultEducation).HasColumnName("C46e_Override_NumberFamiliesReceivedAdultEducation");

                entity.Property(e => e.C46eSystemNumberFamiliesReceivedAdultEducation).HasColumnName("C46e_System_NumberFamiliesReceivedAdultEducation");

                entity.Property(e => e.C46fNumberFamiliesReceivedJobTraining).HasColumnName("C46f_NumberFamiliesReceivedJobTraining");

                entity.Property(e => e.C46fOverrideNumberFamiliesReceivedJobTraining).HasColumnName("C46f_Override_NumberFamiliesReceivedJobTraining");

                entity.Property(e => e.C46gOverrideNumberFamiliesReceivedSubstanceAbusePreventionServices).HasColumnName("C46g_Override_NumberFamiliesReceivedSubstanceAbusePreventionServices");

                entity.Property(e => e.C46gSystemNumberFamiliesReceivedSubstanceAbusePreventionServices).HasColumnName("C46g_System_NumberFamiliesReceivedSubstanceAbusePreventionServices");

                entity.Property(e => e.C46hOverrideNumberFamiliesReceivedSubstanceAbuseTreatmentServices).HasColumnName("C46h_Override_NumberFamiliesReceivedSubstanceAbuseTreatmentServices");

                entity.Property(e => e.C46hSystemNumberFamiliesReceivedSubstanceAbuseTreatmentServices).HasColumnName("C46h_System_NumberFamiliesReceivedSubstanceAbuseTreatmentServices");

                entity.Property(e => e.C46iOverrideNumberFamiliesReceivedChildAbuseNeglectTreatmentServices).HasColumnName("C46i_Override_NumberFamiliesReceivedChildAbuseNeglectTreatmentServices");

                entity.Property(e => e.C46iSystemNumberFamiliesReceivedChildAbuseNeglectServices).HasColumnName("C46i_System_NumberFamiliesReceivedChildAbuseNeglectServices");

                entity.Property(e => e.C46jNumberFamiliesReceivedDomesticViolenceServicesOverride).HasColumnName("C46j_NumberFamiliesReceivedDomesticViolenceServicesOverride");

                entity.Property(e => e.C46jSystemNumberFamiliesReceivedDomesticViolenceServices).HasColumnName("C46j_System_NumberFamiliesReceivedDomesticViolenceServices");

                entity.Property(e => e.C46kOverrideNumberFamiliesReceivedChildSupportServices).HasColumnName("C46k_Override_NumberFamiliesReceivedChildSupportServices");

                entity.Property(e => e.C46kSystemNumberFamiliesReceivedChildSupportServices).HasColumnName("C46k_System_NumberFamiliesReceivedChildSupportServices");

                entity.Property(e => e.C46lOverrideNumberFamiliesReceivedHealthEducation).HasColumnName("C46l_Override_NumberFamiliesReceivedHealthEducation");

                entity.Property(e => e.C46lSystemNumberFamiliesReceivedHealthEducation).HasColumnName("C46l_System_NumberFamiliesReceivedHealthEducation");

                entity.Property(e => e.C46mOverrideNumberFamiliesReceivedServicesForIncarceratedFamilyMember).HasColumnName("C46m_Override_NumberFamiliesReceivedServicesForIncarceratedFamilyMember");

                entity.Property(e => e.C46mSystemNumberFamiliesReceivedServicesForIncarceratedFamilyMember).HasColumnName("C46m_System_NumberFamiliesReceivedServicesForIncarceratedFamilyMember");

                entity.Property(e => e.C46nOverrideNumberFamiliesReceivedParentingEducation).HasColumnName("C46n_Override_NumberFamiliesReceivedParentingEducation");

                entity.Property(e => e.C46nSystemNumberFamiliesReceivedParentingEducation).HasColumnName("C46n_System_NumberFamiliesReceivedParentingEducation");

                entity.Property(e => e.C46oOverrideNumberFamiliesReceivedRelationshipMarriageEducation).HasColumnName("C46o_Override_NumberFamiliesReceivedRelationshipMarriageEducation");

                entity.Property(e => e.C46oSystemNumberFamiliesReceivedRelationshipMarriageEducation).HasColumnName("C46o_System_NumberFamiliesReceivedRelationshipMarriageEducation");

                entity.Property(e => e.C47OverrideNumberFamiliesReceivedAtLeastOneService).HasColumnName("C47_Override_NumberFamiliesReceivedAtLeastOneService");

                entity.Property(e => e.C47SystemNumberFamiliesReceivedAtLeastOneService).HasColumnName("C47_System_NumberFamiliesReceivedAtLeastOneService");

                entity.Property(e => e.C48SystemFatherInvolvementRegularActivities).HasColumnName("C48_System_FatherInvolvementRegularActivities");

                entity.Property(e => e.C48aOverrideNumberChildrenWhoseFathersParticipated).HasColumnName("C48a_Override_NumberChildrenWhoseFathersParticipated");

                entity.Property(e => e.C48aSystemNumberChildrenWhoseFathersParticipated).HasColumnName("C48a_System_NumberChildrenWhoseFathersParticipated");

                entity.Property(e => e.C49OverrideNumberFamiliesExperiencingHomelessness).HasColumnName("C49_Override_NumberFamiliesExperiencingHomelessness");

                entity.Property(e => e.C49SystemNumberFamiliesExperiencingHomelessness).HasColumnName("C49_System_NumberFamiliesExperiencingHomelessness");

                entity.Property(e => e.C50OverrideNumberChildrenExperiencingHomelessness).HasColumnName("C50_Override_NumberChildrenExperiencingHomelessness");

                entity.Property(e => e.C50SystemNumberChildrenExperiencingHomelessness).HasColumnName("C50_System_NumberChildrenExperiencingHomelessness");

                entity.Property(e => e.C51OverrideNumberChildrenWithHealthCareSourceAtEnrollment).HasColumnName("C5_1_Override_NumberChildrenWithHealthCareSourceAtEnrollment");

                entity.Property(e => e.C51OverrideNumberFamiliesExperiencingHomelessnessAquiredHousing).HasColumnName("C51_Override_NumberFamiliesExperiencingHomelessnessAquiredHousing");

                entity.Property(e => e.C51SystemNumberChildrenWithHealthCareSourceAtEnrollment).HasColumnName("C5_1_System_NumberChildrenWithHealthCareSourceAtEnrollment");

                entity.Property(e => e.C51SystemNumberFamiliesExperiencingHomelessnessAquiredHousing).HasColumnName("C51_System_NumberFamiliesExperiencingHomelessnessAquiredHousing");

                entity.Property(e => e.C52OverrideNumberChildrenWithHealthCareSourceAtEndOfEnrollment).HasColumnName("C5_2_Override_NumberChildrenWithHealthCareSourceAtEndOfEnrollment");

                entity.Property(e => e.C52OverrideNumberChildreninFosterCare).HasColumnName("C52_Override_NumberChildreninFosterCare");

                entity.Property(e => e.C52SystemNumberChildrenWithHealthCareSourceAtEndOfEnrollment).HasColumnName("C5_2_System_NumberChildrenWithHealthCareSourceAtEndOfEnrollment");

                entity.Property(e => e.C52SystemNumberChildreninFosterCare).HasColumnName("C52_System_NumberChildreninFosterCare");

                entity.Property(e => e.C53OverrideNumberChildrenReferredByWelfareAgency).HasColumnName("C53_Override_NumberChildrenReferredByWelfareAgency");

                entity.Property(e => e.C53SystemNumberChildrenReferredByWelfareAgency).HasColumnName("C53_System_NumberChildrenReferredByWelfareAgency");

                entity.Property(e => e.C61OverrideNumberChildrenReceivingIndianHealthServicesAtEnrollment).HasColumnName("C6_1_Override_NumberChildrenReceivingIndianHealthServicesAtEnrollment");

                entity.Property(e => e.C61SystemNumberChildrenReceivingIndianHealthServicesAtEnrollment).HasColumnName("C6_1_System_NumberChildrenReceivingIndianHealthServicesAtEnrollment");

                entity.Property(e => e.C62OverrideNumberChildrenReceivingIndianHealthServicesAtEndOfEnrollment).HasColumnName("C6_2_Override_NumberChildrenReceivingIndianHealthServicesAtEndOfEnrollment");

                entity.Property(e => e.C62SystemNumberChildrenReceivingIndianHealthServicesAtEndOfEnrollment).HasColumnName("C6_2_System_NumberChildrenReceivingIndianHealthServicesAtEndOfEnrollment");

                entity.Property(e => e.C71OverrideNumberChildrenReceivingMigrantCommunityHealthServicesAtEnrollment).HasColumnName("C7_1_Override_NumberChildrenReceivingMigrantCommunityHealthServicesAtEnrollment");

                entity.Property(e => e.C71SystemNumberChildrenReceivingMigrantCommunityHealthServicesAtEnrollment).HasColumnName("C7_1_System_NumberChildrenReceivingMigrantCommunityHealthServicesAtEnrollment");

                entity.Property(e => e.C72OverrideNumberChildrenReceivingMigrantCommunityHealthServicesAtEndOfEnrollment).HasColumnName("C7_2_Override_NumberChildrenReceivingMigrantCommunityHealthServicesAtEndOfEnrollment");

                entity.Property(e => e.C72SystemNumberChildrenReceivingMigrantCommunityHealthServicesAtEndOfEnrollment).HasColumnName("C7_2_System_NumberChildrenReceivingMigrantCommunityHealthServicesAtEndOfEnrollment");

                entity.Property(e => e.C81OverrideNumberChildrenUpToDateEpsdtatEnrollmentOverride).HasColumnName("C8_1_Override_NumberChildrenUpToDateEPSDTAtEnrollmentOverride");

                entity.Property(e => e.C81SystemNumberChildrenUpToDateEpsdtatEnrollment).HasColumnName("C8_1_System_NumberChildrenUpToDateEPSDTAtEnrollment");

                entity.Property(e => e.C82OverrideNumberChildrenUpToDateEpsdtatEndOfEnrollment).HasColumnName("C8_2_Override_NumberChildrenUpToDateEPSDTAtEndOfEnrollment");

                entity.Property(e => e.C82SystemNumberChildrenUpToDateEpsdtatEndOfEnrollment).HasColumnName("C8_2_System_NumberChildrenUpToDateEPSDTAtEndOfEnrollment");

                entity.Property(e => e.C8a12OverrideNumberChildrenDiagnosedWithTreatment).HasColumnName("C8a1_2_Override_NumberChildrenDiagnosedWithTreatment");

                entity.Property(e => e.C8a12SystemNumberChildrenDiagnosedWithTreatment).HasColumnName("C8a1_2_System_NumberChildrenDiagnosedWithTreatment");

                entity.Property(e => e.C8a2OverrideNumberChildrenDiagnosedWithChronicCondition).HasColumnName("C8a_2_Override_NumberChildrenDiagnosedWithChronicCondition");

                entity.Property(e => e.C8a2SystemNumberChildrenDiagnosedWithChronicCondition).HasColumnName("C8a_2_System_NumberChildrenDiagnosedWithChronicCondition");

                entity.Property(e => e.C8b12OverrideNumberChildrenDiagnosedWithoutTreatmentNoHealthInsurance).HasColumnName("C8b1_2_OverrideNumberChildrenDiagnosedWithoutTreatmentNoHealthInsurance");

                entity.Property(e => e.C8b12SystemNumberChildrenDiagnosedWithoutTreatmentNoHealthInsurance).HasColumnName("C8b1_2_System_NumberChildrenDiagnosedWithoutTreatmentNoHealthInsurance");

                entity.Property(e => e.C8b22OverrideNumberChildrenDiagnosedWithoutTreatmentNoCareAvailable).HasColumnName("C8b2_2_Override_NumberChildrenDiagnosedWithoutTreatmentNoCareAvailable");

                entity.Property(e => e.C8b22SystemNumberChildrenDiagnosedWithoutTreatmentNoCareAvailable).HasColumnName("C8b2_2_System_NumberChildrenDiagnosedWithoutTreatmentNoCareAvailable");

                entity.Property(e => e.C8b32OverrideNumberChildrenDiagnosedWithoutTreatmentMedicardNotAccepted).HasColumnName("C8b3_2_Override_NumberChildrenDiagnosedWithoutTreatmentMedicardNotAccepted");

                entity.Property(e => e.C8b32SystemNumberChildrenDiagnosedWithoutTreatmentMedicardNotAccepted).HasColumnName("C8b3_2_System_NumberChildrenDiagnosedWithoutTreatmentMedicardNotAccepted");

                entity.Property(e => e.C8b42OverrideNumberChildrenDiagnosedWithoutTreatmentParentsDidNotKeepMakeAppointment).HasColumnName("C8b4_2_Override_NumberChildrenDiagnosedWithoutTreatmentParentsDidNotKeepMakeAppointment");

                entity.Property(e => e.C8b42SystemNumberChildrenDiagnosedWithoutTreatmentParentsDidNotKeepMakeAppointment).HasColumnName("C8b4_2_System_NumberChildrenDiagnosedWithoutTreatmentParentsDidNotKeepMakeAppointment");

                entity.Property(e => e.C8b52OverrideNumberChildrenDiagnosedWithoutTreatmentChildrenLeftProgramBeforeAppointment).HasColumnName("C8b5_2_Override_NumberChildrenDiagnosedWithoutTreatmentChildrenLeftProgramBeforeAppointment");

                entity.Property(e => e.C8b52SystemNumberChildrenDiagnosedWithoutTreatmentChildrenLeftProgramBeforeAppointment).HasColumnName("C8b5_2_System_NumberChildrenDiagnosedWithoutTreatmentChildrenLeftProgramBeforeAppointment");

                entity.Property(e => e.C8b62OverrideNumberChildrenDiagnosedWithoutTreatmentAppointmentScheduledForFuture).HasColumnName("C8b6_2_Override_NumberChildrenDiagnosedWithoutTreatmentAppointmentScheduledForFuture");

                entity.Property(e => e.C8b62SystemNumberChildrenDiagnosedWithoutTreatmentAppointmentScheduledForFuture).HasColumnName("C8b6_2_System_NumberChildrenDiagnosedWithoutTreatmentAppointmentScheduledForFuture");

                entity.Property(e => e.C8b72OverrideNumberChildrenDiagnosedWithoutTreatmentNoTransportation).HasColumnName("C8b7_2_Override_NumberChildrenDiagnosedWithoutTreatmentNoTransportation");

                entity.Property(e => e.C8b72SystemNumberChildrenDiagnosedWithoutTreatmentNoTransportation).HasColumnName("C8b7_2_System_NumberChildrenDiagnosedWithoutTreatmentNoTransportation");

                entity.Property(e => e.C8b82OverrideNumberChildrenDiagnosedWithoutTreatmentOtherReason).HasColumnName("C8b8_2_Override_NumberChildrenDiagnosedWithoutTreatmentOtherReason");

                entity.Property(e => e.C8b82SystemNumberChildrenDiagnosedWithoutTreatmentOtherReason).HasColumnName("C8b8_2_System_NumberChildrenDiagnosedWithoutTreatmentOtherReason");

                entity.Property(e => e.C9aOverrideNumberChildrenChronicConditionAnemia).HasColumnName("C9a_Override_NumberChildrenChronicConditionAnemia");

                entity.Property(e => e.C9aSystemNumberChildrenChronicConditionAnemia).HasColumnName("C9a_System_NumberChildrenChronicConditionAnemia");

                entity.Property(e => e.C9bOverrideNumberChildrenChronicConditionAsthma).HasColumnName("C9b_Override_NumberChildrenChronicConditionAsthma");

                entity.Property(e => e.C9bSystemNumberChildrenChronicConditionAsthma).HasColumnName("C9b_System_NumberChildrenChronicConditionAsthma");

                entity.Property(e => e.C9cOverrideNumberChildrenChronicConditionHearingDifficulties).HasColumnName("C9c_Override_NumberChildrenChronicConditionHearingDifficulties");

                entity.Property(e => e.C9cSystemNumberChildrenChronicConditionHearingDifficulties).HasColumnName("C9c_System_NumberChildrenChronicConditionHearingDifficulties");

                entity.Property(e => e.C9dOverrideNumberChildrenChronicConditionVisionProblem).HasColumnName("C9d_Override_NumberChildrenChronicConditionVisionProblem");

                entity.Property(e => e.C9dSystemNumberChildrenChronicConditionVisionProblems).HasColumnName("C9d_System_NumberChildrenChronicConditionVisionProblems");

                entity.Property(e => e.C9eOverrideNumberChildrenChronicConditionHighLeadLevels).HasColumnName("C9e_Override_NumberChildrenChronicConditionHighLeadLevels");

                entity.Property(e => e.C9eSystemNumberChildrenChronicConditionHighLeadLevels).HasColumnName("C9e_System_NumberChildrenChronicConditionHighLeadLevels");

                entity.Property(e => e.C9fOverrideNumberChildrenChronicConditionDiabetes).HasColumnName("C9f_Override_NumberChildrenChronicConditionDiabetes");

                entity.Property(e => e.C9fSystemNumberChildrenChronicConditionDiabetes).HasColumnName("C9f_System_NumberChildrenChronicConditionDiabetes");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).IsUnicode(false);

                entity.Property(e => e.IsFrozen).HasColumnName("isFrozen");

                entity.Property(e => e.LastModified).HasColumnType("datetime");

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.StartDate).IsUnicode(false);
            });

            modelBuilder.Entity<ProgramPriority>(entity =>
            {
                entity.Property(e => e.ProgramPriorityId).HasColumnName("ProgramPriorityID");

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.HasOne(d => d.Program)
                    .WithMany(p => p.ProgramPriority)
                    .HasForeignKey(d => d.ProgramId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProgramPriority_Program");

                entity.HasOne(d => d.ProgramType)
                    .WithMany(p => p.ProgramPriority)
                    .HasForeignKey(d => d.ProgramTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProgramPriority_ProgramType");
            });

            modelBuilder.Entity<ProgramType>(entity =>
            {
                entity.Property(e => e.ProgramTypeId)
                    .HasColumnName("ProgramTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<ProgramYear>(entity =>
            {
                entity.HasIndex(e => new { e.ProgramId, e.SchoolYearId })
                    .HasName("IX_ProgramYear")
                    .IsUnique();

                entity.HasIndex(e => new { e.ProgramYearId, e.SchoolYearId })
                    .HasName("_dta_index_ProgramYear_5_1758629308__K1_K3");

                entity.HasIndex(e => new { e.EndDate, e.ProgramYearId, e.SchoolYearId, e.BeginDate })
                    .HasName("_dta_index_ProgramYear_5_1758629308__K1_K3_K21_22");

                entity.Property(e => e.ProgramYearId).HasColumnName("ProgramYearID");

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.InitialPovertyLevelBaseIfMoreThan8FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf1FamilyMember).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf2FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf3FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf4FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf5FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf6FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf7FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.InitialPovertyLevelBaseOf8FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.IsPca).HasColumnName("IsPCA");

                entity.Property(e => e.Iscopied)
                    .HasColumnName("ISCopied")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.PirsubmissionDate)
                    .HasColumnName("PIRSubmissionDate")
                    .HasColumnType("date");

                entity.Property(e => e.PovertyLevelBase).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseIfMoreThan8FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf1FamilyMember).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf2FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf3FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf4FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf5FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf6FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf7FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelBaseOf8FamilyMembers).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PovertyLevelEachAdditionalPerson).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PreviousPovertyLevelBase).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.PreviousPovertyLevelEachAdditionalPerson).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.SchoolYearId).HasColumnName("SchoolYearID");

                entity.Property(e => e.SelectionCriteriaId).HasColumnName("SelectionCriteriaID");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.UpdatedSelectionCriteriaId).HasColumnName("UpdatedSelectionCriteriaID");

                entity.HasOne(d => d.Program)
                    .WithMany(p => p.ProgramYear)
                    .HasForeignKey(d => d.ProgramId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProgramYear_Program");

                entity.HasOne(d => d.SchoolYear)
                    .WithMany(p => p.ProgramYear)
                    .HasForeignKey(d => d.SchoolYearId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProgramYear_SchoolYear");

                entity.HasOne(d => d.SelectionCriteria)
                    .WithMany(p => p.ProgramYear)
                    .HasForeignKey(d => d.SelectionCriteriaId)
                    .HasConstraintName("FK_ProgramYear_SelectionCriteria");
            });

            modelBuilder.Entity<Rating>(entity =>
            {
                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.RatingName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.AssessmentLevel)
                    .WithMany(p => p.Rating)
                    .HasForeignKey(d => d.AssessmentLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Rating_AssessmentLevel");
            });

            modelBuilder.Entity<RecentUpdate>(entity =>
            {
                entity.Property(e => e.RecentUpdateId).HasColumnName("RecentUpdateID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");
            });

            modelBuilder.Entity<RoleGrantee>(entity =>
            {
                entity.Property(e => e.RoleGranteeId).HasColumnName("RoleGranteeID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.RoleGrantee)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleGrantee_Grantee");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleGrantee)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_RoleGrantee_webpages_Roles");
            });

            modelBuilder.Entity<SchoolYear>(entity =>
            {
                entity.Property(e => e.SchoolYearId)
                    .HasColumnName("SchoolYearID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DefaultPirsubmissionDate)
                    .HasColumnName("DefaultPIRSubmissionDate")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<SelectionCriteria>(entity =>
            {
                entity.Property(e => e.SelectionCriteriaId).HasColumnName("SelectionCriteriaID");

                entity.Property(e => e.ApprovedDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomAdmissionForm1Name).HasMaxLength(100);

                entity.Property(e => e.CustomAdmissionForm2Name).HasMaxLength(100);

                entity.Property(e => e.CustomAdmissionForm3Name).HasMaxLength(100);

                entity.Property(e => e.CustomAdmissionForm4Name).HasMaxLength(100);

                entity.Property(e => e.CustomAdmissionForm5Name).HasMaxLength(100);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.SelectionCriteria)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SelectionCriteria_Grantee");
            });

            modelBuilder.Entity<SelectionCriteriaCategory>(entity =>
            {
                entity.Property(e => e.SelectionCriteriaCategoryId).HasColumnName("SelectionCriteriaCategoryID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.SelectionCriteriaId).HasColumnName("SelectionCriteriaID");

                entity.HasOne(d => d.SelectionCriteria)
                    .WithMany(p => p.SelectionCriteriaCategory)
                    .HasForeignKey(d => d.SelectionCriteriaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SelectionCriteriaCategory_SelectionCriteria");
            });

            modelBuilder.Entity<SelectionCriteriaCategoryItem>(entity =>
            {
                entity.Property(e => e.SelectionCriteriaCategoryItemId).HasColumnName("SelectionCriteriaCategoryItemID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ItemTitle)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.SelectionCriteriaCategoryId).HasColumnName("SelectionCriteriaCategoryID");

                entity.HasOne(d => d.SelectionCriteriaCategory)
                    .WithMany(p => p.SelectionCriteriaCategoryItem)
                    .HasForeignKey(d => d.SelectionCriteriaCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SelectionCriteriaCategoryItem_SelectionCriteriaCategory");
            });

            modelBuilder.Entity<ShineInsightMigrationMapping>(entity =>
            {
                entity.Property(e => e.DestinationName).HasMaxLength(100);

                entity.Property(e => e.FieldName).HasMaxLength(100);

                entity.Property(e => e.GranteeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SourceName).HasMaxLength(100);
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.StateAbbreviations)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StateName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StatusCodeDescriptionMapping>(entity =>
            {
                entity.Property(e => e.GranteeName).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(100);

                entity.Property(e => e.RefusalDate).HasMaxLength(100);

                entity.Property(e => e.ScreeningResultsStatus).HasMaxLength(100);

                entity.Property(e => e.StatusCodeDescription).HasMaxLength(100);
            });

            modelBuilder.Entity<SummaryViewParticipantRecord>(entity =>
            {
                entity.HasKey(e => e.ParticipantRecordId)
                    .ForSqlServerIsClustered(false);

                entity.HasIndex(e => e.CenterId);

                entity.HasIndex(e => e.ClassroomId);

                entity.HasIndex(e => e.FamilyAdvocateUserId)
                    .HasName("IX_SummaryViewParticipantRecord_FamilyAdvocateUserId");

                entity.HasIndex(e => e.GranteeId);

                entity.HasIndex(e => e.ProgramYearId);

                entity.HasIndex(e => new { e.ParticipantId, e.ParticipantRecordStatus })
                    .HasName("IX_SummaryViewParticipantRecord_ParticipantID")
                    .ForSqlServerIsClustered();

                entity.HasIndex(e => new { e.CenterId, e.ClassroomId, e.ProgramYearId })
                    .HasName("IX_SummaryViewParticipantRecord_CenterID_ClassroomID");

                entity.HasIndex(e => new { e.ParticipantRecordStatus, e.ParticipantId, e.StatusLastUpdatedDate })
                    .HasName("IX_SummaryViewParticipantRecord_StatusLastUpdatedDate");

                entity.HasIndex(e => new { e.ProgramYearId, e.CenterId, e.ClassroomId, e.FamilyAdvocateUserId })
                    .HasName("IX_SummaryViewParticipantRecord");

                entity.HasIndex(e => new { e.ParticipantRecordId, e.ProgramTypeId, e.ProgramYearId, e.ParticipantId, e.ParticipantRecordStatus })
                    .HasName("IX_SummaryViewParticipantRecord_ParticipantRecordStatus");

                entity.HasIndex(e => new { e.ProgramTypeId, e.ParticipantRecordStatus, e.ParticipantId, e.ProgramYearId, e.ParticipantRecordId })
                    .HasName("IX_SummaryViewParticipantRecord_ParticipantRecordID");

                entity.Property(e => e.ParticipantRecordId)
                    .HasColumnName("ParticipantRecordID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AlertGroupAdmin).HasColumnName("AlertGroup_Admin");

                entity.Property(e => e.AlertGroupClerkAttendance).HasColumnName("AlertGroup_ClerkAttendance");

                entity.Property(e => e.AlertGroupDisabilities).HasColumnName("AlertGroup_Disabilities");

                entity.Property(e => e.AlertGroupEducation).HasColumnName("AlertGroup_Education");

                entity.Property(e => e.AlertGroupEnrollment).HasColumnName("AlertGroup_Enrollment");

                entity.Property(e => e.AlertGroupErsea).HasColumnName("AlertGroup_ERSEA");

                entity.Property(e => e.AlertGroupExecutiveDirector).HasColumnName("AlertGroup_ExecutiveDirector");

                entity.Property(e => e.AlertGroupExtendedDay).HasColumnName("AlertGroup_ExtendedDay");

                entity.Property(e => e.AlertGroupFamilyServices).HasColumnName("AlertGroup_FamilyServices");

                entity.Property(e => e.AlertGroupHealth).HasColumnName("AlertGroup_Health");

                entity.Property(e => e.AlertGroupHomeBased).HasColumnName("AlertGroup_HomeBased");

                entity.Property(e => e.AlertGroupInKind).HasColumnName("AlertGroup_InKind");

                entity.Property(e => e.AlertGroupMentalHealth).HasColumnName("AlertGroup_MentalHealth");

                entity.Property(e => e.ApplicationDate).HasMaxLength(100);

                entity.Property(e => e.CenterId)
                    .HasColumnName("CenterID")
                    .HasMaxLength(50);

                entity.Property(e => e.CenterName).HasMaxLength(100);

                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.ClassroomId)
                    .HasColumnName("ClassroomID")
                    .HasMaxLength(50);

                entity.Property(e => e.ClassroomName).HasMaxLength(100);

                entity.Property(e => e.ClassroomProgramYearId).HasColumnName("ClassroomProgramYearID");

                entity.Property(e => e.EnrollmentDate).HasColumnType("date");

                entity.Property(e => e.EnrollmentTerminationDate).HasColumnType("datetime");

                entity.Property(e => e.EntryDate).HasColumnType("date");

                entity.Property(e => e.FamilyAdvocateUserId).HasColumnName("FamilyAdvocateUserID");

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.ParticipantDateofBirth).HasColumnType("date");

                entity.Property(e => e.ParticipantId).HasColumnName("ParticipantID");

                entity.Property(e => e.ParticipantParentGuardianId).HasColumnName("ParticipantParentGuardianID");

                entity.Property(e => e.ParticipantTypeId).HasColumnName("ParticipantTypeID");

                entity.Property(e => e.PhotoUrl)
                    .HasColumnName("photoURL")
                    .HasMaxLength(1000);

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.ProgramName).HasMaxLength(100);

                entity.Property(e => e.ProgramType)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ProgramTypeId).HasColumnName("ProgramTypeID");

                entity.Property(e => e.ProgramYear).HasMaxLength(100);

                entity.Property(e => e.ProgramYearId)
                    .HasColumnName("ProgramYearID")
                    .HasMaxLength(50);

                entity.Property(e => e.SchoolDistrictCutoffDate).HasColumnType("date");

                entity.Property(e => e.SchoolYear).HasMaxLength(100);

                entity.Property(e => e.SchoolYearId).HasColumnName("SchoolYearID");

                entity.Property(e => e.StatusLastUpdatedDate).HasColumnType("date");
            });

            modelBuilder.Entity<TableColumnValue>(entity =>
            {
                entity.HasIndex(e => e.Text);

                entity.HasIndex(e => new { e.TableName, e.ColumnName, e.ValueId })
                    .HasName("IX_TableColumnValue")
                    .IsUnique();

                entity.HasIndex(e => new { e.ValueId, e.TableName, e.Text, e.ColumnName })
                    .HasName("_dta_index_TableColumnValue_5_2062630391__K2_K5_K3_4");

                entity.HasIndex(e => new { e.Text, e.TableName, e.ColumnName, e.ValueId, e.TableColumnValueId })
                    .HasName("IX_TableColumnValue_ColumnValues");

                entity.HasIndex(e => new { e.Text, e.ValueId, e.ColumnName, e.TableName, e.TableColumnValueId })
                    .HasName("IX_TableColumnValue_ColumnName");

                entity.Property(e => e.TableColumnValueId).HasColumnName("TableColumnValueID");

                entity.Property(e => e.ColumnName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ValueId).HasColumnName("ValueID");
            });

            modelBuilder.Entity<TbChildPluslErrorRecords>(entity =>
            {
                entity.ToTable("tbChildPluslErrorRecords");

                entity.HasIndex(e => new { e.ChildPlusId, e.FileId, e.ErrorType })
                    .HasName("tbChildPluslErrorRecords_ErrorType_Covering");

                entity.HasIndex(e => new { e.ErrorDescription, e.FileId, e.ChildPlusId, e.DataFileName })
                    .HasName("tbChildPluslErrorRecords_ChildPlusID_DataFileName_Covering");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AgencyNameGranteeName)
                    .HasColumnName("AgencyName/GranteeName")
                    .IsUnicode(false);

                entity.Property(e => e.Center).IsUnicode(false);

                entity.Property(e => e.ChildPlusId).HasColumnName("ChildPlusID");

                entity.Property(e => e.ClassName).IsUnicode(false);

                entity.Property(e => e.DataFileName).HasMaxLength(100);

                entity.Property(e => e.ErrorDescription).HasColumnType("varchar(max)");

                entity.Property(e => e.ErrorType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FileId).HasColumnName("FileID");

                entity.Property(e => e.Program).IsUnicode(false);

                entity.Property(e => e.RefId).HasColumnName("RefID");
            });

            modelBuilder.Entity<TblParticipantStatusesMapping>(entity =>
            {
                entity.HasKey(e => e.ValueId);

                entity.ToTable("tbl_ParticipantStatusesMapping");

                entity.Property(e => e.ValueId)
                    .HasColumnName("ValueID")
                    .ValueGeneratedNever();

                entity.Property(e => e.StatusName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Themes>(entity =>
            {
                entity.HasKey(e => e.ThemeId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ThemeName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TimeStamp>(entity =>
            {
                entity.Property(e => e.CenterProgramYearId).HasColumnName("CenterProgramYearID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<TraceLog>(entity =>
            {
                entity.HasKey(e => e.RowNumber);

                entity.ToTable("Trace_Log");

                entity.Property(e => e.ApplicationName).HasMaxLength(128);

                entity.Property(e => e.BinaryData).HasColumnType("image");

                entity.Property(e => e.ClientProcessId).HasColumnName("ClientProcessID");

                entity.Property(e => e.Cpu).HasColumnName("CPU");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.LoginName).HasMaxLength(128);

                entity.Property(e => e.NtuserName)
                    .HasColumnName("NTUserName")
                    .HasMaxLength(128);

                entity.Property(e => e.Spid).HasColumnName("SPID");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.TextData).HasColumnType("ntext");
            });

            modelBuilder.Entity<UserAdditionalTrainings>(entity =>
            {
                entity.HasKey(e => e.UserAdditionalTrainingId);

                entity.Property(e => e.UserAdditionalTrainingId).HasColumnName("UserAdditionalTrainingID");

                entity.Property(e => e.CompletionDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.PointValue).HasDefaultValueSql("((0))");

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.TrainingType).HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAdditionalTrainings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAdditionalTrainings_UserProfile");
            });

            modelBuilder.Entity<UserAlert>(entity =>
            {
                entity.Property(e => e.UserAlertId).HasColumnName("UserAlertID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAlert)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAlert_UserProfile");
            });

            modelBuilder.Entity<UserAnnualTrainings>(entity =>
            {
                entity.HasKey(e => e.UserAnnualTrainingId);

                entity.Property(e => e.UserAnnualTrainingId).HasColumnName("UserAnnualTrainingID");

                entity.Property(e => e.CompletionDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.FormatofTraining).HasMaxLength(100);

                entity.Property(e => e.NameofPresenter).HasMaxLength(100);

                entity.Property(e => e.NumberofHoursCompleted).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.TypeofTraining).HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAnnualTrainings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAnnualTrainings_UserProfile");
            });

            modelBuilder.Entity<UserAppraisalStaffEvaluation>(entity =>
            {
                entity.Property(e => e.UserAppraisalStaffEvaluationId).HasColumnName("UserAppraisalStaffEvaluationID");

                entity.Property(e => e.AppraisalEvaluationType).HasMaxLength(100);

                entity.Property(e => e.EvaluationDate).HasColumnType("date");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserAppraisalStaffEvaluation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAppraisalStaffEvaluation_UserProfile");
            });

            modelBuilder.Entity<UserCenter>(entity =>
            {
                entity.HasIndex(e => new { e.CenterId, e.UserId })
                    .HasName("IX_UserCenter")
                    .IsUnique();

                entity.Property(e => e.UserCenterId).HasColumnName("UserCenterID");

                entity.Property(e => e.CenterId).HasColumnName("CenterID");

                entity.Property(e => e.DateAdded).HasColumnType("date");

                entity.Property(e => e.DateRemoved).HasColumnType("date");

                entity.HasOne(d => d.Center)
                    .WithMany(p => p.UserCenter)
                    .HasForeignKey(d => d.CenterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserCenter_Center");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCenter)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserCenter_UserProfile");
            });

            modelBuilder.Entity<UserChecksandClearances>(entity =>
            {
                entity.Property(e => e.UserChecksandClearancesId).HasColumnName("UserChecksandClearancesID");

                entity.Property(e => e.CheckClearanceType).HasMaxLength(100);

                entity.Property(e => e.CompletionDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserChecksandClearances)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserChecksandClearances_UserProfile");
            });

            modelBuilder.Entity<UserClassroom>(entity =>
            {
                entity.Property(e => e.UserClassroomId).HasColumnName("UserClassroomID");

                entity.Property(e => e.ClassroomId).HasColumnName("ClassroomID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateAdded).HasColumnType("date");

                entity.Property(e => e.DateRemoved).HasColumnType("date");

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.HasOne(d => d.Classroom)
                    .WithMany(p => p.UserClassroom)
                    .HasForeignKey(d => d.ClassroomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserClassroom_Classroom");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserClassroom)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserClassroom_UserProfile");
            });

            modelBuilder.Entity<UserClassScore>(entity =>
            {
                entity.Property(e => e.UserClassScoreId).HasColumnName("UserClassScoreID");

                entity.Property(e => e.EsevaluationDate)
                    .HasColumnName("ESEvaluationDate")
                    .HasColumnType("date");

                entity.Property(e => e.EsobservationCompletedBy)
                    .HasColumnName("ESObservationCompletedBy")
                    .HasMaxLength(100);

                entity.Property(e => e.Esscore)
                    .HasColumnName("ESScore")
                    .HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<UserCoaching>(entity =>
            {
                entity.Property(e => e.UserCoachingId).HasColumnName("UserCoachingID");

                entity.Property(e => e.CpaagreementDate)
                    .HasColumnName("CPAAgreementDate")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<UserDashboard>(entity =>
            {
                entity.Property(e => e.UserDashboardId).HasColumnName("UserDashboardID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDashboard)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserDashboard_UserProfile");
            });

            modelBuilder.Entity<UserEmergencyContacts>(entity =>
            {
                entity.HasKey(e => e.UserEmergencyContactId);

                entity.Property(e => e.UserEmergencyContactId).HasColumnName("userEmergencyContactID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.EmergencyContactName).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(15);

                entity.Property(e => e.RelationToEmployee).HasMaxLength(100);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserEmergencyContacts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserEmergencyContacts_UserProfile");
            });

            modelBuilder.Entity<UserEmployeePhoneNumbers>(entity =>
            {
                entity.HasKey(e => e.UserEmployeePhoneNumberId);

                entity.Property(e => e.UserEmployeePhoneNumberId).HasColumnName("UserEmployeePhoneNumberID");

                entity.Property(e => e.PhoneNumber).HasMaxLength(15);

                entity.Property(e => e.PhoneTypeId).HasColumnName("PhoneTypeID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserEmployeePhoneNumbers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserEmployeePhoneNumbers_UserProfile");
            });

            modelBuilder.Entity<UserHireInformation>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EmploymentHireDate).HasColumnType("date");

                entity.Property(e => e.EndTime).HasColumnType("time(0)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartTime).HasColumnType("time(0)");

                entity.Property(e => e.TerminationDate).HasColumnType("date");

                entity.Property(e => e.TerminationReasonOtherExplanation).HasMaxLength(1000);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserHireInformation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserHireInformation_UserProfile");
            });

            modelBuilder.Entity<UserImmunizations>(entity =>
            {
                entity.Property(e => e.UserImmunizationsId).HasColumnName("UserImmunizationsID");

                entity.Property(e => e.EvaluationDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.ImmunizationsId).HasColumnName("ImmunizationsID");

                entity.Property(e => e.ScreenDate).HasColumnType("date");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserImmunizations)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserImmunizations_UserProfile");
            });

            modelBuilder.Entity<UserJobDescriptions>(entity =>
            {
                entity.HasKey(e => e.UserJobDescriptionId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserJobDescriptions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserJobDescriptions_UserProfile");
            });

            modelBuilder.Entity<UserLanguageProficiency>(entity =>
            {
                entity.Property(e => e.UserLanguageProficiencyId).HasColumnName("UserLanguageProficiencyID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLanguageProficiency)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserLanguageProficiency_UserProfile");
            });

            modelBuilder.Entity<UserLoginLog>(entity =>
            {
                entity.Property(e => e.UserLoginLogId).HasColumnName("UserLoginLogID");

                entity.Property(e => e.LoginDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLoginLog)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserLoginLog_UserID");
            });

            modelBuilder.Entity<UserPhysical>(entity =>
            {
                entity.Property(e => e.UserPhysicalId).HasColumnName("UserPhysicalID");

                entity.Property(e => e.CompletionDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");
            });

            modelBuilder.Entity<UserPirposition>(entity =>
            {
                entity.ToTable("UserPIRPosition");

                entity.Property(e => e.UserPirpositionId).HasColumnName("UserPIRPositionID");

                entity.Property(e => e.Pirposition).HasColumnName("PIRPosition");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPirposition)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserPIRPosition_UserProfile");
            });

            modelBuilder.Entity<UserProfessional>(entity =>
            {
                entity.Property(e => e.UserProfessionalId).HasColumnName("UserProfessionalID");

                entity.Property(e => e.PdpplanDate)
                    .HasColumnName("PDPPlanDate")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.HasIndex(e => e.UserName)
                    .HasName("UQ__UserProf__C9F284566662DD51")
                    .IsUnique();

                entity.HasIndex(e => new { e.GranteeId, e.UserId })
                    .HasName("IX_GranteeID");

                entity.HasIndex(e => new { e.FirstName, e.IsUserActive, e.UserId, e.LastName })
                    .HasName("IX_UserProfile_IsUserActive");

                entity.HasIndex(e => new { e.UserName, e.Email, e.FirstName, e.Gender, e.LastName, e.MiddleName, e.UserId })
                    .HasName("IX_UserProfile_UserId");

                entity.Property(e => e.AdvocateType).HasMaxLength(50);

                entity.Property(e => e.AnnualSalary).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Apartment).HasMaxLength(100);

                entity.Property(e => e.BiracialRace).HasMaxLength(500);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.BpcompletionDate)
                    .HasColumnName("BPCompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.BpexpiratioDate)
                    .HasColumnName("BPExpiratioDate")
                    .HasColumnType("date");

                entity.Property(e => e.CancompletionDate)
                    .HasColumnName("CANCompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.CanexpirationDate)
                    .HasColumnName("CANExpirationDate")
                    .HasColumnType("date");

                entity.Property(e => e.CfacompletionDate)
                    .HasColumnName("CFACompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.CfaexpiratioDate)
                    .HasColumnName("CFAExpiratioDate")
                    .HasColumnType("date");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.ClassroomOrganization).HasMaxLength(100);

                entity.Property(e => e.CoevaluationDate)
                    .HasColumnName("COEvaluationDate")
                    .HasColumnType("date");

                entity.Property(e => e.CoobservationCompletedBy)
                    .HasColumnName("COObservationCompletedBy")
                    .HasMaxLength(100);

                entity.Property(e => e.CpaagreementDate)
                    .HasColumnName("CPAAgreementDate")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DefaultCenters)
                    .HasColumnName("defaultCenters")
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'ALL')");

                entity.Property(e => e.DefaultClassrooms)
                    .HasColumnName("defaultClassrooms")
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'ALL')");

                entity.Property(e => e.DefaultDmhstatuses)
                    .HasColumnName("DefaultDMHStatuses")
                    .IsUnicode(false);

                entity.Property(e => e.DefaultFamilyAdvocates)
                    .HasColumnName("defaultFamilyAdvocates")
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'ALL')");

                entity.Property(e => e.DefaultProgramYears)
                    .HasColumnName("defaultProgramYears")
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'ALL')");

                entity.Property(e => e.DefaultTab)
                    .HasColumnName("defaultTab")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(N'Enrollment')");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.EmotionalSupport).HasMaxLength(100);

                entity.Property(e => e.EmploymentHireDate).HasColumnType("date");

                entity.Property(e => e.EndTime).HasColumnType("time(0)");

                entity.Property(e => e.EsevaluationDate)
                    .HasColumnName("ESEvaluationDate")
                    .HasColumnType("date");

                entity.Property(e => e.EsobservationCompletedBy)
                    .HasColumnName("ESObservationCompletedBy")
                    .HasMaxLength(100);

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDateCda5)
                    .HasColumnName("ExpirationDateCDA5")
                    .HasColumnType("date");

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.GranteeId).HasColumnName("GranteeID");

                entity.Property(e => e.ImmunizationsTbexpirationDate)
                    .HasColumnName("ImmunizationsTBExpirationDate")
                    .HasColumnType("date");

                entity.Property(e => e.ImmunizationsTbscreenDate)
                    .HasColumnName("ImmunizationsTBScreenDate")
                    .HasColumnType("date");

                entity.Property(e => e.ImmunizationsTdapmmrexpirationDate)
                    .HasColumnName("ImmunizationsTDAPMMRExpirationDate")
                    .HasColumnType("date");

                entity.Property(e => e.ImmunizationsTdapmmrscreenDate)
                    .HasColumnName("ImmunizationsTDAPMMRScreenDate")
                    .HasColumnType("date");

                entity.Property(e => e.ImmunizationsTetanusExpirationDate).HasColumnType("date");

                entity.Property(e => e.ImmunizationsTetanusScreenDate).HasColumnType("date");

                entity.Property(e => e.InstructionalSupport).HasMaxLength(100);

                entity.Property(e => e.IsevaluationDate)
                    .HasColumnName("ISEvaluationDate")
                    .HasColumnType("date");

                entity.Property(e => e.IsobservationCompletedBy)
                    .HasColumnName("ISObservationCompletedBy")
                    .HasMaxLength(100);

                entity.Property(e => e.LanguageProficiencyOther).HasMaxLength(50);

                entity.Property(e => e.LastModified).HasColumnType("datetime2(0)");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.LicenseNumber).HasMaxLength(100);

                entity.Property(e => e.Major).HasMaxLength(200);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.NhocompletionDate)
                    .HasColumnName("NHOCompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.NumberofEcecredits).HasColumnName("NumberofECECredits");

                entity.Property(e => e.PdgcompletionDate)
                    .HasColumnName("PDGCompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.PdgexpiratioDate)
                    .HasColumnName("PDGExpiratioDate")
                    .HasColumnType("date");

                entity.Property(e => e.PdpplanDate)
                    .HasColumnName("PDPPlanDate")
                    .HasColumnType("date");

                entity.Property(e => e.PercentageAnnualSalaryHsorEhs).HasColumnName("PercentageAnnualSalaryHSorEHS");

                entity.Property(e => e.PhysicalCompletionDate).HasColumnType("date");

                entity.Property(e => e.PositionDescription).HasMaxLength(100);

                entity.Property(e => e.QualificationTypeOther).HasMaxLength(100);

                entity.Property(e => e.RaceOther).HasMaxLength(50);

                entity.Property(e => e.RaceUnspecified).HasMaxLength(100);

                entity.Property(e => e.RegistryCertificateLevel).HasMaxLength(200);

                entity.Property(e => e.SbscompletionDate)
                    .HasColumnName("SBSCompletionDate")
                    .HasColumnType("date");

                entity.Property(e => e.SbsexpiratioDate)
                    .HasColumnName("SBSExpiratioDate")
                    .HasColumnType("date");

                entity.Property(e => e.SchoolName).HasMaxLength(200);

                entity.Property(e => e.StartTime).HasColumnType("time(0)");

                entity.Property(e => e.State).HasMaxLength(50);

                entity.Property(e => e.StateOfIssue).HasMaxLength(100);

                entity.Property(e => e.StreetAddress).HasMaxLength(100);

                entity.Property(e => e.TerminationDate).HasColumnType("date");

                entity.Property(e => e.TerminationReasonOtherExplanation).HasMaxLength(1000);

                entity.Property(e => e.Token).HasMaxLength(255);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(56);

                entity.Property(e => e.ZipCode).HasMaxLength(20);

                entity.HasOne(d => d.Grantee)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.GranteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserProfile_Grantee");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_UserProfile_webpages_Roles");
            });

            modelBuilder.Entity<UserProfileHistory>(entity =>
            {
                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDateCda5)
                    .HasColumnName("ExpirationDateCDA5")
                    .HasColumnType("date");

                entity.Property(e => e.Major).HasMaxLength(200);

                entity.Property(e => e.NumberofEcecredits).HasColumnName("NumberofECECredits");

                entity.Property(e => e.RegistryCertificateLevel).HasMaxLength(200);

                entity.Property(e => e.SchoolName).HasMaxLength(2000);
            });

            modelBuilder.Entity<UserRace>(entity =>
            {
                entity.Property(e => e.UserRaceId).HasColumnName("UserRaceID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRaceNavigation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRace_UserProfile");
            });

            modelBuilder.Entity<UserSuccessRubrics>(entity =>
            {
                entity.Property(e => e.UserSuccessRubricsId).HasColumnName("UserSuccessRubricsID");

                entity.Property(e => e.EvaluationDate).HasColumnType("date");

                entity.Property(e => e.RubricType).HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserSuccessRubrics)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserSuccessRubrics_UserProfile");
            });

            modelBuilder.Entity<UserTab>(entity =>
            {
                entity.HasIndex(e => new { e.UserId, e.Tab })
                    .HasName("IX_UserTab")
                    .IsUnique();

                entity.Property(e => e.UserTabId).HasColumnName("UserTabID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserTab)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserTab_UserProfile");
            });

            modelBuilder.Entity<WebpagesMembership>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("webpages_Membership");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.ConfirmationToken).HasMaxLength(128);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IsConfirmed).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastPasswordFailureDate).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.PasswordChangedDate).HasColumnType("datetime");

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.PasswordVerificationToken).HasMaxLength(128);

                entity.Property(e => e.PasswordVerificationTokenExpirationDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.WebpagesMembership)
                    .HasForeignKey<WebpagesMembership>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_webpages_Membership_UserProfile");
            });

            modelBuilder.Entity<WebpagesOauthMembership>(entity =>
            {
                entity.HasKey(e => new { e.Provider, e.ProviderUserId });

                entity.ToTable("webpages_OAuthMembership");

                entity.Property(e => e.Provider).HasMaxLength(30);

                entity.Property(e => e.ProviderUserId).HasMaxLength(100);
            });

            modelBuilder.Entity<WebpagesRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("webpages_Roles");

                entity.HasIndex(e => e.RoleName)
                    .HasName("UQ__webpages__8A2B616000A40667")
                    .IsUnique();

                entity.Property(e => e.RoleId).ValueGeneratedNever();

                entity.Property(e => e.RoleDisplay)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<WebpagesUsersInRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.ToTable("webpages_UsersInRoles");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.WebpagesUsersInRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_RoleId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WebpagesUsersInRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_UserId");
            });

            modelBuilder.HasSequence("CountBy1");
        }
    }
}

