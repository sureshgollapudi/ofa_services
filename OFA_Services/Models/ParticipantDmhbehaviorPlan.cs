﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhbehaviorPlan
    {
        public ParticipantDmhbehaviorPlan()
        {
            ParticipantDmhbehaviorPlanStrategyForChild = new HashSet<ParticipantDmhbehaviorPlanStrategyForChild>();
            ParticipantDmhbehaviorPlanStrategyForClassroom = new HashSet<ParticipantDmhbehaviorPlanStrategyForClassroom>();
            ParticipantDmhbehaviorPlanStrategyForHome = new HashSet<ParticipantDmhbehaviorPlanStrategyForHome>();
            ParticipantDmhbehaviorPlanType = new HashSet<ParticipantDmhbehaviorPlanType>();
        }

        public int ParticipantDmhbehaviorPlanId { get; set; }
        public int ParticipantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? InputDate { get; set; }
        public string StrategiesForChildDescription { get; set; }
        public string StrategiesForClassroomDescription { get; set; }
        public string StrategiesForHomeDescription { get; set; }
        public string PlanTypeOther { get; set; }
        public string StrategyForChildOther { get; set; }
        public string StrategyForClassroomOther { get; set; }
        public string StrategyForHomeOther { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? BehaviorPlanQuestion { get; set; }
        public DateTime? FbacompletionDate { get; set; }
        public int? BehaviorPlanUpdateFrequency { get; set; }
        public int? DmhprocessDocumentationId { get; set; }

        public ICollection<ParticipantDmhbehaviorPlanStrategyForChild> ParticipantDmhbehaviorPlanStrategyForChild { get; set; }
        public ICollection<ParticipantDmhbehaviorPlanStrategyForClassroom> ParticipantDmhbehaviorPlanStrategyForClassroom { get; set; }
        public ICollection<ParticipantDmhbehaviorPlanStrategyForHome> ParticipantDmhbehaviorPlanStrategyForHome { get; set; }
        public ICollection<ParticipantDmhbehaviorPlanType> ParticipantDmhbehaviorPlanType { get; set; }
    }
}
