﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class CaseLoadUser
    {
        public int UserId { get; set; }
        public string GranteeId { get; set; }
        public string RoleId { get; set; }
        public string UserName { get; set; }
        public bool? IsUserActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public string Type { get; set; }
        public string PositionDescription { get; set; }
        public int Pirposition { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }
}
