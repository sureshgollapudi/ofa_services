﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantEducationScreeningReport
    {
        public int ParticipantEducationScreeningReportId { get; set; }
        public int ParticipantId { get; set; }
        public int EducationScreeningId { get; set; }
        public int? ParticipantEducationScreeningDocumentationId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? RescreenStatusId { get; set; }
        public DateTime? RescreenDueDate { get; set; }
    }
}
