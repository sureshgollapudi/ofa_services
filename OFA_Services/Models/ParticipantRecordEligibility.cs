﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEligibility
    {
        public int ParticipantRecordEligibilityId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? AgeAtApplication { get; set; }
        public bool? LivesInServiceArea { get; set; }
        public int? CategoricallyEligibleType { get; set; }
        public int? HomelessStatus { get; set; }
        public bool? PrimaryParentGuardianIncomeNoneConfirmation { get; set; }
        public string PrimaryParentGuardianIncomeNoneExplanation { get; set; }
        public int? PrimaryParentGuardianIncomeSource { get; set; }
        public DateTime? PrimaryParentGuardianIncomeSourceDate { get; set; }
        public double? PrimaryParentGuardianIncome { get; set; }
        public int? PrimaryParentGuardianIncomeFrequency { get; set; }
        public double? PrimaryParentGuardianIncomeAnnualized { get; set; }
        public string PrimaryParentGuardianIncomeNotes { get; set; }
        public bool? SecondaryParentGuardianIncomeNoneConfirmation { get; set; }
        public string SecondaryParentGuardianIncomeNoneExplanation { get; set; }
        public int? SecondaryParentGuardianIncomeSource { get; set; }
        public DateTime? SecondaryParentGuardianIncomeSourceDate { get; set; }
        public double? SecondaryParentGuardianIncome { get; set; }
        public int? SecondaryParentGuardianIncomeFrequency { get; set; }
        public double? SecondaryParentGuardianIncomeAnnualized { get; set; }
        public string SecondaryParentGuardianIncomeNotes { get; set; }
        public int? OtherParentGuardianIncomeSource1 { get; set; }
        public DateTime? OtherParentGuardianIncomeSourceDate1 { get; set; }
        public double? OtherParentGuardianIncome1 { get; set; }
        public int? OtherParentGuardianIncomeFrequency1 { get; set; }
        public double? OtherParentGuardianIncomeAnnualized1 { get; set; }
        public string OtherParentGuardianIncomeNotes1 { get; set; }
        public int? OtherParentGuardianIncomeSource2 { get; set; }
        public DateTime? OtherParentGuardianIncomeSourceDate2 { get; set; }
        public double? OtherParentGuardianIncome2 { get; set; }
        public int? OtherParentGuardianIncomeFrequency2 { get; set; }
        public double? OtherParentGuardianIncomeAnnualized2 { get; set; }
        public string OtherParentGuardianIncomeNotes2 { get; set; }
        public double? IncomeTotal { get; set; }
        public DateTime? ParentGuardianSignatureDate { get; set; }
        public int? SignerUserId { get; set; }
        public DateTime? UserSignOffDate { get; set; }
        public DateTime? FamilyAdvocateSupervisorSignOffDate { get; set; }
        public DateTime? ThirdYearFamilyAdvocateSupervisorSignOffDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? OtherParentGuardianIncomeNoneConfirmation1 { get; set; }
        public string OtherParentGuardianIncomeNoneExplanation1 { get; set; }
        public bool? OtherParentGuardianIncomeNoneConfirmation2 { get; set; }
        public string OtherParentGuardianIncomeNoneExplanation2 { get; set; }
        public int? IncomeEligibleType { get; set; }
        public int? CategoricallyEligible { get; set; }
        public double? IncomePercentageFederalPoverty { get; set; }
        public string PrimaryParentGuardianIncomeSourceOther { get; set; }
        public string SecondaryParentGuardianIncomeSourceOther { get; set; }
        public string OtherParentGuardianIncomeSourceOther1 { get; set; }
        public string OtherParentGuardianIncomeSourceOther2 { get; set; }
        public bool? IsCopiedFromPreviousYear { get; set; }
        public bool? IncomeNoneConfirmation { get; set; }
        public string IncomeNoneExplanation { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
        public UserProfile SignerUser { get; set; }
    }
}
