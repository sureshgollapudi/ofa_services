﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class EducationStatusCodeDescriptionMapping
    {
        public int Id { get; set; }
        public string StatusCodeDescription { get; set; }
        public string ScreeningResultsStatus { get; set; }
        public string Notes { get; set; }
        public bool? Refusal { get; set; }
        public string RefusalDate { get; set; }
        public bool? Exemption { get; set; }
        public string ExemptionDate { get; set; }
    }
}
