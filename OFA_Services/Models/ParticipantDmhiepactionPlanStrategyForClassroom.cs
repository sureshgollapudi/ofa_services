﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepactionPlanStrategyForClassroom
    {
        public int ParticipantDmhiepactionPlanStrategyForClassroomId { get; set; }
        public int ParticipantDmhiepactionPlanId { get; set; }
        public int StrategyForClassroom { get; set; }

        public ParticipantDmhiepactionPlan ParticipantDmhiepactionPlan { get; set; }
    }
}
