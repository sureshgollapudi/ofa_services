﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class SelectionCriteriaCategoryItem
    {
        public SelectionCriteriaCategoryItem()
        {
            ParticipantRecordSelectionCriteriaCategoryItem = new HashSet<ParticipantRecordSelectionCriteriaCategoryItem>();
        }

        public int SelectionCriteriaCategoryItemId { get; set; }
        public int SelectionCriteriaCategoryId { get; set; }
        public string ItemTitle { get; set; }
        public int ItemPoints { get; set; }
        public int? ListingOrder { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public SelectionCriteriaCategory SelectionCriteriaCategory { get; set; }
        public ICollection<ParticipantRecordSelectionCriteriaCategoryItem> ParticipantRecordSelectionCriteriaCategoryItem { get; set; }
    }
}
