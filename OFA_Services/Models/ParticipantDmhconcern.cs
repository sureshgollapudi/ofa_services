﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhconcern
    {
        public ParticipantDmhconcern()
        {
            ParticipantDmhconcernArea = new HashSet<ParticipantDmhconcernArea>();
        }

        public int ParticipantDmhconcernId { get; set; }
        public int ParticipantId { get; set; }
        public int ConcernArea { get; set; }
        public string ConcernAreaOtherDescription { get; set; }
        public int? InitiatedFrom { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? UserModifiedDate { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public DateTime? ConcernInitiatedDate { get; set; }
        public string ConcernNote { get; set; }

        public UserProfile CreatedByUser { get; set; }
        public Participant Participant { get; set; }
        public ICollection<ParticipantDmhconcernArea> ParticipantDmhconcernArea { get; set; }
    }
}
