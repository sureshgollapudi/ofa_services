﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserLanguageProficiency
    {
        public int UserLanguageProficiencyId { get; set; }
        public int UserId { get; set; }
        public int Language { get; set; }

        public UserProfile User { get; set; }
    }
}
