﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class TimeStamp
    {
        public TimeStamp()
        {
            ParticipantEvaluationCenterprogramyear = new HashSet<ParticipantEvaluationCenterprogramyear>();
        }

        public int TimeStampId { get; set; }
        public string Name { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? CenterProgramYearId { get; set; }

        public ICollection<ParticipantEvaluationCenterprogramyear> ParticipantEvaluationCenterprogramyear { get; set; }
    }
}
