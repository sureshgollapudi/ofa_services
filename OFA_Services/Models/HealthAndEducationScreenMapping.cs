﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthAndEducationScreenMapping
    {
        public int Id { get; set; }
        public string SourceEventType { get; set; }
        public string DestinationEventType { get; set; }
        public string DestinationTool { get; set; }
        public string EventName { get; set; }
        public string SourceGrantee { get; set; }
        public string DestinationGrantee { get; set; }
    }
}
