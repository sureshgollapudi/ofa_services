﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PresentAttendance
    {
        public int PresentAttendanceId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? ParticipantId { get; set; }
        public string ParticipantName { get; set; }
        public string CenterProgramYear { get; set; }
        public string ClassroomProgramYear { get; set; }
        public int? ClassroomProgramYearId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? LastDate { get; set; }
        public DateTime? AttendanceDate { get; set; }
        public string AttendanceStatus { get; set; }
    }
}
