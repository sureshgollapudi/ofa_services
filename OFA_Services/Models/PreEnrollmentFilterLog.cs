﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PreEnrollmentFilterLog
    {
        public int PreEnrollmentFilterLogId { get; set; }
        public string SearchText { get; set; }
        public string Centers { get; set; }
        public string Statuses { get; set; }
        public string Advocates { get; set; }
        public int? ItemsQuantity { get; set; }
        public int? PresentItemsCount { get; set; }
        public string SortOrderKey { get; set; }
        public string SortOrderValue { get; set; }
        public int? GranteeId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
