﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantHealthConcern
    {
        public ParticipantHealthConcern()
        {
            ParticipantFollowUp = new HashSet<ParticipantFollowUp>();
            ParticipantHealthConcernParticipantHealthScreeningDocumentation = new HashSet<ParticipantHealthConcernParticipantHealthScreeningDocumentation>();
            PrimaryGuardianGoalFollowUp = new HashSet<PrimaryGuardianGoalFollowUp>();
        }

        public int ParticipantHealthConcernId { get; set; }
        public int? ParticipantId { get; set; }
        public int? NameId { get; set; }
        public int? InitiatedFromId { get; set; }
        public DateTime? Date { get; set; }
        public int? UserInputId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public int? ChronicOrAcuteConditionDeterminationId { get; set; }
        public int? ChronicConditionDiagnosedByHealthCareProfessionalId { get; set; }
        public int? ChronicConditionDiagnosisId { get; set; }
        public string ChronicConditionDiagnosisOther { get; set; }
        public DateTime? ChronicConditionDiagnosisDate { get; set; }
        public string ForeignConcernId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string OtherDescription { get; set; }
        public int? GoalRequiredId { get; set; }
        public int? AssociatedPrimaryGuardianGoalId { get; set; }

        public PrimaryGuardianGoal AssociatedPrimaryGuardianGoal { get; set; }
        public ICollection<ParticipantFollowUp> ParticipantFollowUp { get; set; }
        public ICollection<ParticipantHealthConcernParticipantHealthScreeningDocumentation> ParticipantHealthConcernParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<PrimaryGuardianGoalFollowUp> PrimaryGuardianGoalFollowUp { get; set; }
    }
}
