﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ImmunizationSet
    {
        public int ImmunizationSetId { get; set; }
        public int GranteeId { get; set; }
        public string Name { get; set; }
        public bool? IsMaster { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
    }
}
