﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordFamilyAdvocate
    {
        public int ParticipantRecordFamilyAdvocateId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int FamilyAdvocateUserId { get; set; }
        public int AdvocateAssignmentEnteredByUserId { get; set; }
        public DateTime AdvocateAssignmentDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile AdvocateAssignmentEnteredByUser { get; set; }
        public UserProfile FamilyAdvocateUser { get; set; }
    }
}
