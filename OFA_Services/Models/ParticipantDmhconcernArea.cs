﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhconcernArea
    {
        public int ParticipantDmhconcernAreaId { get; set; }
        public int ParticipantDmhconcernId { get; set; }
        public int Area { get; set; }

        public ParticipantDmhconcern ParticipantDmhconcern { get; set; }
    }
}
