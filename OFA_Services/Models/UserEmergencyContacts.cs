﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserEmergencyContacts
    {
        public int UserEmergencyContactId { get; set; }
        public int UserId { get; set; }
        public string EmergencyContactName { get; set; }
        public string RelationToEmployee { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }

        public UserProfile User { get; set; }
    }
}
