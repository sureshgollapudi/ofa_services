﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Calendar
    {
        public int CalendarId { get; set; }
        public int ParticipantId { get; set; }
        public string ItemName { get; set; }
        public DateTime Date { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? AlertId { get; set; }
        public int? GoalId { get; set; }

        public Alert Alert { get; set; }
        public Participant Participant { get; set; }
    }
}
