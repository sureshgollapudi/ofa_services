﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserRace
    {
        public int UserRaceId { get; set; }
        public int UserId { get; set; }
        public int Race { get; set; }

        public UserProfile User { get; set; }
    }
}
