﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthScreeningSet
    {
        public HealthScreeningSet()
        {
            HealthScreeningSetHealthScreening = new HashSet<HealthScreeningSetHealthScreening>();
        }

        public int HealthScreeningSetId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ICollection<HealthScreeningSetHealthScreening> HealthScreeningSetHealthScreening { get; set; }
    }
}
