﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepactionPlan
    {
        public ParticipantDmhiepactionPlan()
        {
            ParticipantDmhiepactionPlanStrategyForChild = new HashSet<ParticipantDmhiepactionPlanStrategyForChild>();
            ParticipantDmhiepactionPlanStrategyForClassroom = new HashSet<ParticipantDmhiepactionPlanStrategyForClassroom>();
            ParticipantDmhiepactionPlanStrategyForHome = new HashSet<ParticipantDmhiepactionPlanStrategyForHome>();
            ParticipantDmhiepactionPlanType = new HashSet<ParticipantDmhiepactionPlanType>();
        }

        public int ParticipantDmhiepactionPlanId { get; set; }
        public int ParticipantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? InputDate { get; set; }
        public string StrategiesForChildDescription { get; set; }
        public string StrategiesForClassroomDescription { get; set; }
        public string StrategiesForHomeDescription { get; set; }
        public string PlanTypeOther { get; set; }
        public string StrategyForChildOther { get; set; }
        public string StrategyForClassroomOther { get; set; }
        public string StrategyForHomeOther { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? IepactionPlanUpdateFrequency { get; set; }
        public int? DmhprocessDocumentationId { get; set; }

        public ICollection<ParticipantDmhiepactionPlanStrategyForChild> ParticipantDmhiepactionPlanStrategyForChild { get; set; }
        public ICollection<ParticipantDmhiepactionPlanStrategyForClassroom> ParticipantDmhiepactionPlanStrategyForClassroom { get; set; }
        public ICollection<ParticipantDmhiepactionPlanStrategyForHome> ParticipantDmhiepactionPlanStrategyForHome { get; set; }
        public ICollection<ParticipantDmhiepactionPlanType> ParticipantDmhiepactionPlanType { get; set; }
    }
}
