﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Alert
    {
        public Alert()
        {
            AlertGroup = new HashSet<AlertGroup>();
            AlertProgramType = new HashSet<AlertProgramType>();
            Calendar = new HashSet<Calendar>();
            GranteeAlert = new HashSet<GranteeAlert>();
            ParticipantAlert = new HashSet<ParticipantAlert>();
        }

        public int AlertId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public string Link { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? AdminAlertGroupId { get; set; }

        public ICollection<AlertGroup> AlertGroup { get; set; }
        public ICollection<AlertProgramType> AlertProgramType { get; set; }
        public ICollection<Calendar> Calendar { get; set; }
        public ICollection<GranteeAlert> GranteeAlert { get; set; }
        public ICollection<ParticipantAlert> ParticipantAlert { get; set; }
    }
}
