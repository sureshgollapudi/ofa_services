﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationHomeBasedVisitDocumentation
    {
        public int ParticipantRecordEducationHomeBasedVisitDocumentationId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime HomeBasedVisitDate { get; set; }
        public int? HomeBasedVisitDocumentationAttachment { get; set; }
        public string HomeBasedVisitSchoolReadinessDomain { get; set; }
        public string HomeBasedVisitPregnantWomenServices { get; set; }
        public string HomeBasedVisitObjectivesObserved { get; set; }
        public string HomeBasedVisitParentInvolvement { get; set; }
        public string HomeBasedVisitDocumentationNotes { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string HomeBasedVisitPirservices { get; set; }
        public int? Twoweekpostpartumvisit { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsSocialEmotional { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsApproachesToLearningAndPlay { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsLanguageAndLiteracy { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsMathAndScience { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsSocialStudies { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsTheArts { get; set; }
        public bool HomeBasedVisitSchoolReadinessDomainsPhysical { get; set; }
        public bool HomeBasedVisitPregnantWomenPirservicesPrenantalHealthCare { get; set; }
        public bool HomeBasedVisitPregnantWomenPirservicesPostpartumHealthCare { get; set; }
        public bool HomeBasedVisitPregnantWomenPirservicesPrenataleducationOnFetalDevelopment { get; set; }
        public bool HomeBasedVisitPregnantWomenPirservicesInformationOnBenefitsOfBreastfeeding { get; set; }
        public bool HomeBasedVisitPirservicesEmergencyOrCrisisIntervention { get; set; }
        public bool HomeBasedVisitPirservicesMentalHealthServices { get; set; }
        public bool HomeBasedVisitPirservicesSubstanceAbusePrevention { get; set; }
        public bool HomeBasedVisitPirservicesSubstanceAbuseTreatment { get; set; }
        public bool HomeBasedVisitPirservicesChildAbuseAndNeglectServices { get; set; }
        public bool HomeBasedVisitPirservicesDomesticViolenceServices { get; set; }
        public bool HomeBasedVisitPirservicesParentingEducation { get; set; }
        public bool HomeBasedVisitPirservicesRelationshipOrMarriageEducation { get; set; }
        public bool HomeBasedVisitPirservicesAssistanceToFamiliesOfIncarceratedIndividuals { get; set; }
        public bool HomeBasedVisitPirservicesHousingAssistance { get; set; }
        public bool HomeBasedVisitPirservicesEsltraining { get; set; }
        public bool HomeBasedVisitPirservicesAdultEducation { get; set; }
        public bool HomeBasedVisitPirservicesJobTraining { get; set; }
        public bool HomeBasedVisitPirservicesChildSupportAssistance { get; set; }
        public bool HomeBasedVisitPirservicesHealthEducation { get; set; }
        public bool HomeBasedVisitPirservicesAssetBuilding { get; set; }
        public int FatherFigureInvolved { get; set; }
        public int? LastModifiedBy { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
