﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthScreening
    {
        public HealthScreening()
        {
            GranteeHealthScreening = new HashSet<GranteeHealthScreening>();
            HealthScreeningPirquestion = new HashSet<HealthScreeningPirquestion>();
            HealthScreeningSetHealthScreening = new HashSet<HealthScreeningSetHealthScreening>();
        }

        public int HealthScreeningId { get; set; }
        public string Name { get; set; }
        public bool IsRequired { get; set; }
        public bool IsLicensingRequired { get; set; }
        public bool IsActive { get; set; }
        public int ParticipantType { get; set; }
        public int? ChildStartAge { get; set; }
        public int? ChildStartAgeUnits { get; set; }
        public int? ChildEndAge { get; set; }
        public int? ChildEndAgeUnits { get; set; }
        public bool? IsAdultDeliveryDateDependent { get; set; }
        public int? ValidityStartType { get; set; }
        public int? ValidityDateStartNumber { get; set; }
        public int? ValidityDateStartUnits { get; set; }
        public int? ValidityStartNumber { get; set; }
        public int? ValidityStartUnits { get; set; }
        public int? ValidityEndNumber { get; set; }
        public int? ValidityEndUnits { get; set; }
        public int? NextDueDateNoneValidEntryDateRule { get; set; }
        public int? NextDueDateNoneValidChildAgeRule { get; set; }
        public int? NextDueDateNoneValidChildAgeUnits { get; set; }
        public int? NextDueDateNoneValidDeliveryDateRule { get; set; }
        public int? NextDueDateNoneValidAdultDeliveryDateUnits { get; set; }
        public int? NextDueDateExpirationDateRule { get; set; }
        public int? NextDueDateExpirationDateRuleUnits { get; set; }
        public bool HasNoneValidDueDateOverride { get; set; }
        public bool HasExpirationDateOverride { get; set; }
        public bool HasGrowthAssessmentFields { get; set; }
        public bool IsOnPir { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? ScreeningOrder { get; set; }
        public int? HealthScreeningGroupId { get; set; }

        public ICollection<GranteeHealthScreening> GranteeHealthScreening { get; set; }
        public ICollection<HealthScreeningPirquestion> HealthScreeningPirquestion { get; set; }
        public ICollection<HealthScreeningSetHealthScreening> HealthScreeningSetHealthScreening { get; set; }
    }
}
