﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantFollowUp
    {
        public ParticipantFollowUp()
        {
            ParticipantAction = new HashSet<ParticipantAction>();
        }

        public int ParticipantFollowUpId { get; set; }
        public int? ParticipantHealthConcernId { get; set; }
        public int? NameId { get; set; }
        public int? StatusId { get; set; }
        public int? OwnerId { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string ForeignConcernFustepId { get; set; }
        public int? UserInputId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime? StatusDate { get; set; }
        public bool? DoesStatusExpire { get; set; }
        public bool? OngoingActionRequired { get; set; }
        public int? DaysWithoutAction { get; set; }
        public DateTime? DentalTreatmentDiagnosisDate { get; set; }

        public ParticipantHealthConcern ParticipantHealthConcern { get; set; }
        public ICollection<ParticipantAction> ParticipantAction { get; set; }
    }
}
