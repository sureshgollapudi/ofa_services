﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserAnnualTrainings
    {
        public int UserAnnualTrainingId { get; set; }
        public int UserId { get; set; }
        public string TypeofTraining { get; set; }
        public string FormatofTraining { get; set; }
        public string NameofPresenter { get; set; }
        public decimal? NumberofHoursCompleted { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public UserProfile User { get; set; }
    }
}
