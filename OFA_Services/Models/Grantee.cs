﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Grantee
    {
        public Grantee()
        {
            EducationScreeningSet = new HashSet<EducationScreeningSet>();
            Goal = new HashSet<Goal>();
            GranteeAlert = new HashSet<GranteeAlert>();
            GranteeHealthScreening = new HashSet<GranteeHealthScreening>();
            GranteeHealthScreeningSet = new HashSet<GranteeHealthScreeningSet>();
            GranteeImmunization = new HashSet<GranteeImmunization>();
            GranteeSchoolYearDefault = new HashSet<GranteeSchoolYearDefault>();
            GranteeTheme = new HashSet<GranteeTheme>();
            HealthRequirementsActionStatusOptionsOnCreateNewScreening = new HashSet<HealthRequirementsActionStatusOptionsOnCreateNewScreening>();
            ImmunizationSet = new HashSet<ImmunizationSet>();
            MasterGoalGrantee = new HashSet<MasterGoalGrantee>();
            OfaParentRegistration = new HashSet<OfaParentRegistration>();
            OfaParticipant = new HashSet<OfaParticipant>();
            ParentGuardian = new HashSet<ParentGuardian>();
            Participant = new HashSet<Participant>();
            Program = new HashSet<Program>();
            RoleGrantee = new HashSet<RoleGrantee>();
            SelectionCriteria = new HashSet<SelectionCriteria>();
            UserProfile = new HashSet<UserProfile>();
        }

        public int GranteeId { get; set; }
        public string Name { get; set; }
        public bool? IsGranteeActive { get; set; }
        public string GrantNumber { get; set; }
        public string Duns { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string AgencyType { get; set; }
        public string AgencyAffiliation { get; set; }
        public string AgencyDescription { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? IsSof { get; set; }

        public ICollection<EducationScreeningSet> EducationScreeningSet { get; set; }
        public ICollection<Goal> Goal { get; set; }
        public ICollection<GranteeAlert> GranteeAlert { get; set; }
        public ICollection<GranteeHealthScreening> GranteeHealthScreening { get; set; }
        public ICollection<GranteeHealthScreeningSet> GranteeHealthScreeningSet { get; set; }
        public ICollection<GranteeImmunization> GranteeImmunization { get; set; }
        public ICollection<GranteeSchoolYearDefault> GranteeSchoolYearDefault { get; set; }
        public ICollection<GranteeTheme> GranteeTheme { get; set; }
        public ICollection<HealthRequirementsActionStatusOptionsOnCreateNewScreening> HealthRequirementsActionStatusOptionsOnCreateNewScreening { get; set; }
        public ICollection<ImmunizationSet> ImmunizationSet { get; set; }
        public ICollection<MasterGoalGrantee> MasterGoalGrantee { get; set; }
        public ICollection<OfaParentRegistration> OfaParentRegistration { get; set; }
        public ICollection<OfaParticipant> OfaParticipant { get; set; }
        public ICollection<ParentGuardian> ParentGuardian { get; set; }
        public ICollection<Participant> Participant { get; set; }
        public ICollection<Program> Program { get; set; }
        public ICollection<RoleGrantee> RoleGrantee { get; set; }
        public ICollection<SelectionCriteria> SelectionCriteria { get; set; }
        public ICollection<UserProfile> UserProfile { get; set; }
    }
}
