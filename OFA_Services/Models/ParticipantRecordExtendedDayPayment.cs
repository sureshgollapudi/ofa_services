﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordExtendedDayPayment
    {
        public int ParticipantRecordExtendedDayPaymentId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int Type { get; set; }
        public DateTime TimePeriodtoApplyPaymentTo { get; set; }
        public decimal AmountToApply { get; set; }
        public string Notes { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
