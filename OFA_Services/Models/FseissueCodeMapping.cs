﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class FseissueCodeMapping
    {
        public int Id { get; set; }
        public string SourceEventType { get; set; }
        public string SourceissueCode { get; set; }
        public string DestinationGoal { get; set; }
        public string GranteeName { get; set; }
    }
}
