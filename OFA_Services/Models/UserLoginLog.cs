﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserLoginLog
    {
        public int UserLoginLogId { get; set; }
        public int UserId { get; set; }
        public DateTime? LoginDate { get; set; }

        public UserProfile User { get; set; }
    }
}
