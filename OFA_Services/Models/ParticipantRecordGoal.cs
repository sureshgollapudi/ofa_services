﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordGoal
    {
        public ParticipantRecordGoal()
        {
            ParticipantRecordGoalFollowUp = new HashSet<ParticipantRecordGoalFollowUp>();
        }

        public int ParticipantRecordGoalId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int GoalId { get; set; }
        public DateTime? InitiatedDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime? IsActiveDate { get; set; }
        public string Objective1 { get; set; }
        public string Objective2 { get; set; }
        public string Objective3 { get; set; }
        public string Objective4 { get; set; }
        public string Objective5 { get; set; }
        public string ObjectiveOther1 { get; set; }
        public string ObjectiveOther2 { get; set; }
        public string ObjectiveOther3 { get; set; }
        public DateTime? Objective1TargetDate { get; set; }
        public DateTime? Objective2TargetDate { get; set; }
        public DateTime? Objective3TargetDate { get; set; }
        public DateTime? Objective4TargetDate { get; set; }
        public DateTime? Objective5TargetDate { get; set; }
        public DateTime? ObjectiveOther1TargetDate { get; set; }
        public DateTime? ObjectiveOther2TargetDate { get; set; }
        public DateTime? ObjectiveOther3TargetDate { get; set; }
        public DateTime? Benchmark1CompletedDate { get; set; }
        public DateTime? Benchmark2CompletedDate { get; set; }
        public DateTime? Benchmark3CompletedDate { get; set; }
        public DateTime? Benchmark4CompletedDate { get; set; }
        public string Resource1Organization { get; set; }
        public string Resource1ContactName { get; set; }
        public string Resource1Address { get; set; }
        public string Resource1Phone { get; set; }
        public DateTime? Resource1GivenDate { get; set; }
        public DateTime? Resource1TakenDate { get; set; }
        public string Resource2Organization { get; set; }
        public string Resource2ContactName { get; set; }
        public string Resource2Address { get; set; }
        public string Resource2Phone { get; set; }
        public DateTime? Resource2GivenDate { get; set; }
        public DateTime? Resource2TakenDate { get; set; }
        public string Resource3Organization { get; set; }
        public string Resource3ContactName { get; set; }
        public string Resource3Address { get; set; }
        public string Resource3Phone { get; set; }
        public DateTime? Resource3GivenDate { get; set; }
        public DateTime? Resource3TakenDate { get; set; }
        public string Resource4Organization { get; set; }
        public string Resource4ContactName { get; set; }
        public string Resource4Address { get; set; }
        public string Resource4Phone { get; set; }
        public DateTime? Resource4GivenDate { get; set; }
        public DateTime? Resource4TakenDate { get; set; }
        public string FamilyEngagementOutcome1 { get; set; }
        public string FamilyEngagementOutcome2 { get; set; }
        public string FamilyEngagementOutcome3 { get; set; }
        public string PirservicesReceived1 { get; set; }
        public string PirservicesReceived2 { get; set; }
        public string PirservicesReceived3 { get; set; }
        public DateTime? PirservicesReceived1Date { get; set; }
        public DateTime? PirservicesReceived2Date { get; set; }
        public DateTime? PirservicesReceived3Date { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile CreatorUser { get; set; }
        public Goal Goal { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
        public ICollection<ParticipantRecordGoalFollowUp> ParticipantRecordGoalFollowUp { get; set; }
    }
}
