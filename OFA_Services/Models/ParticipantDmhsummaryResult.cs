﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhsummaryResult
    {
        public int ParticipantDmhsummaryResultId { get; set; }
        public int ParticipantId { get; set; }
        public int? ProcessDocumentationId { get; set; }
        public int? AssociatedConcernId { get; set; }
        public int? MostRecentTakenId { get; set; }
        public DateTime? MostRecentActionDate { get; set; }
        public int? NextRequieredActionId { get; set; }
        public DateTime? NextRequireDate { get; set; }
        public int? StatusId { get; set; }

        public Participant Participant { get; set; }
    }
}
