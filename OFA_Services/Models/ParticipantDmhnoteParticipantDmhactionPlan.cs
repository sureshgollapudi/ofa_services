﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteParticipantDmhactionPlan
    {
        public int ParticipantDmhnoteParticipantDmhactionPlanId { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ParticipantDmhactionPlanId { get; set; }
    }
}
