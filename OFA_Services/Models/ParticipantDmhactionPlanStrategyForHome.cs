﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhactionPlanStrategyForHome
    {
        public int ParticipantDmhactionPlanStrategyForHomeId { get; set; }
        public int ParticipantDmhactionPlanId { get; set; }
        public int StrategyForHome { get; set; }

        public ParticipantDmhactionPlan ParticipantDmhactionPlan { get; set; }
    }
}
