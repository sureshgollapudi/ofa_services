﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserPirposition
    {
        public int UserPirpositionId { get; set; }
        public int UserId { get; set; }
        public int Pirposition { get; set; }

        public UserProfile User { get; set; }
    }
}
