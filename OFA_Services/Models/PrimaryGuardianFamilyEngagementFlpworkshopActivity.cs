﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyEngagementFlpworkshopActivity
    {
        public int PrimaryGuardianFamilyEngagementFlpworkshopActivityId { get; set; }
        public int PrimaryGuardianFamilyEngagementId { get; set; }
        public int FlpworkshopActivity { get; set; }

        public PrimaryGuardianFamilyEngagement PrimaryGuardianFamilyEngagement { get; set; }
    }
}
