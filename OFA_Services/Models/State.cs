﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace OFA_Services.Models
{
    public partial class State
    {
        public State()
        {
            OfaParticipant = new HashSet<OfaParticipant>();
            OfaParticipantEmergencyContact = new HashSet<OfaParticipantEmergencyContact>();
        }
        public int StateId { get; set; }
        public string StateAbbreviations { get; set; }
        public string StateName { get; set; }
        [NotMapped]
        public string value { get; set; }
        [NotMapped]
        public string label { get; set; }

        public ICollection<OfaParticipant> OfaParticipant { get; set; }
        public ICollection<OfaParticipantEmergencyContact> OfaParticipantEmergencyContact { get; set; }
    }
}
