﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeHealthScreeningPirquestion
    {
        public int GranteeHealthScreeningPirquestionId { get; set; }
        public int GranteeHealthScreeningId { get; set; }
        public int PirquestionId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public GranteeHealthScreening GranteeHealthScreening { get; set; }
        public Pirquestion Pirquestion { get; set; }
    }
}
