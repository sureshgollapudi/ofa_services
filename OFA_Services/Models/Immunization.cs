﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Immunization
    {
        public Immunization()
        {
            GranteeImmunization = new HashSet<GranteeImmunization>();
            ParticipantRecordImmunization = new HashSet<ParticipantRecordImmunization>();
            ParticipantRecordImmunizationPlanning = new HashSet<ParticipantRecordImmunizationPlanning>();
        }

        public int ImmunizationId { get; set; }
        public string Name { get; set; }
        public string AlternateName { get; set; }
        public int ListingOrder { get; set; }
        public int DateFieldCount { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ICollection<GranteeImmunization> GranteeImmunization { get; set; }
        public ICollection<ParticipantRecordImmunization> ParticipantRecordImmunization { get; set; }
        public ICollection<ParticipantRecordImmunizationPlanning> ParticipantRecordImmunizationPlanning { get; set; }
    }
}
