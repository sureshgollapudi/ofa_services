﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationChallengingBehaviour
    {
        public int ParticipantRecordEducationChallengingBehaviourId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime EventDateTime { get; set; }
        public int CenterDirectorReview { get; set; }
        public int Location { get; set; }
        public string LocationOther { get; set; }
        public string PrecedingBehaviour { get; set; }
        public string PrecedingBehaviourOther { get; set; }
        public string ChallengingBehaviour { get; set; }
        public string ChallengingBehaviourOther { get; set; }
        public string Description { get; set; }
        public string Strategies { get; set; }
        public int ReferralRequired { get; set; }
        public int ParentNotificationVia { get; set; }
        public string ParentNotificationViaOther { get; set; }
        public DateTime? ParentNotificationDateTime { get; set; }
        public string ParentInput { get; set; }
        public int DirectorStrategies { get; set; }
        public string DirectorStrategiesOther { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
