﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserCenter
    {
        public int UserCenterId { get; set; }
        public int UserId { get; set; }
        public int CenterId { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateRemoved { get; set; }

        public Center Center { get; set; }
        public UserProfile User { get; set; }
    }
}
