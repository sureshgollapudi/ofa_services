﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepactionPlanType
    {
        public int ParticipantDmhiepactionPlanTypeId { get; set; }
        public int ParticipantDmhiepactionPlanId { get; set; }
        public int Type { get; set; }

        public ParticipantDmhiepactionPlan ParticipantDmhiepactionPlan { get; set; }
    }
}
