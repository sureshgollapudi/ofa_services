﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class NightlyJobLog
    {
        public int StepId { get; set; }
        public string Stepname { get; set; }
        public string Jobname { get; set; }
        public string Status { get; set; }
        public DateTime? StepDate { get; set; }
        public bool? InitialLoadFlag { get; set; }
    }
}
