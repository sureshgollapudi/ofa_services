﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordImmunizationPlanning
    {
        public int ParticipantRecordImmunizationPlanningId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? ImmunizationId { get; set; }
        public string Note { get; set; }
        public DateTime? Date { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Immunization Immunization { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
