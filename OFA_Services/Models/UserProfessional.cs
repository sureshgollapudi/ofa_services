﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserProfessional
    {
        public int UserProfessionalId { get; set; }
        public int UserId { get; set; }
        public DateTime? PdpplanDate { get; set; }
    }
}
