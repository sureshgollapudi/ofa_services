﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeAlert
    {
        public int GranteeAlertId { get; set; }
        public int GranteeId { get; set; }
        public int AlertId { get; set; }
        public int Points { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Alert Alert { get; set; }
        public Grantee Grantee { get; set; }
    }
}
