﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class WebpagesRoles
    {
        public WebpagesRoles()
        {
            RoleGrantee = new HashSet<RoleGrantee>();
            UserProfile = new HashSet<UserProfile>();
            WebpagesUsersInRoles = new HashSet<WebpagesUsersInRoles>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDisplay { get; set; }

        public ICollection<RoleGrantee> RoleGrantee { get; set; }
        public ICollection<UserProfile> UserProfile { get; set; }
        public ICollection<WebpagesUsersInRoles> WebpagesUsersInRoles { get; set; }
    }
}
