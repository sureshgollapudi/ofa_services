﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class CalendarItem
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public int? AlertId { get; set; }
    }
}
