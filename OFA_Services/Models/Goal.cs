﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Goal
    {
        public Goal()
        {
            MasterGoalGrantee = new HashSet<MasterGoalGrantee>();
            ParticipantRecordGoal = new HashSet<ParticipantRecordGoal>();
            PrimaryGuardianGoal = new HashSet<PrimaryGuardianGoal>();
        }

        public int GoalId { get; set; }
        public int GranteeId { get; set; }
        public bool? IsGoalActive { get; set; }
        public bool? IsMasterGoal { get; set; }
        public bool? IsMasterGoalActive { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? GoalCategoryId { get; set; }
        public int? GoalSubCategoryId { get; set; }
        public string Objective1 { get; set; }
        public string Objective2 { get; set; }
        public string Objective3 { get; set; }
        public string Objective4 { get; set; }
        public string Objective5 { get; set; }
        public string Benchmark1Description { get; set; }
        public string Benchmark2Description { get; set; }
        public string Benchmark3Description { get; set; }
        public string Benchmark4Description { get; set; }
        public string Resource1Organization { get; set; }
        public string Resource1ContactName { get; set; }
        public string Resource1Address { get; set; }
        public string Resource1Phone { get; set; }
        public string Resource2Organization { get; set; }
        public string Resource2ContactName { get; set; }
        public string Resource2Address { get; set; }
        public string Resource2Phone { get; set; }
        public string Resource3Organization { get; set; }
        public string Resource3ContactName { get; set; }
        public string Resource3Address { get; set; }
        public string Resource3Phone { get; set; }
        public string Resource4Organization { get; set; }
        public string Resource4ContactName { get; set; }
        public string Resource4Address { get; set; }
        public string Resource4Phone { get; set; }
        public string FamilyEngagementOutcome1 { get; set; }
        public string FamilyEngagementOutcome2 { get; set; }
        public string FamilyEngagementOutcome3 { get; set; }
        public string PirservicesReceived1 { get; set; }
        public string PirservicesReceived2 { get; set; }
        public string PirservicesReceived3 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public GoalCategory GoalCategory { get; set; }
        public GoalSubCategory GoalSubCategory { get; set; }
        public Grantee Grantee { get; set; }
        public ICollection<MasterGoalGrantee> MasterGoalGrantee { get; set; }
        public ICollection<ParticipantRecordGoal> ParticipantRecordGoal { get; set; }
        public ICollection<PrimaryGuardianGoal> PrimaryGuardianGoal { get; set; }
    }
}
