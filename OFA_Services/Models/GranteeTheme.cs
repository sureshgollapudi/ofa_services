﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeTheme
    {
        public int GranteeThemeId { get; set; }
        public int ThemeId { get; set; }
        public int GranteeId { get; set; }

        public Grantee Grantee { get; set; }
        public Themes Theme { get; set; }
    }
}
