﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeImmunization
    {
        public GranteeImmunization()
        {
            ParticipantImmunization = new HashSet<ParticipantImmunization>();
        }

        public int GranteeImmunizationId { get; set; }
        public int GranteeId { get; set; }
        public int ImmunizationId { get; set; }
        public bool? IsActive { get; set; }
        public int? FirstDueAge { get; set; }
        public int? FirstDueAgeUnits { get; set; }
        public int? BeginTimeframe2 { get; set; }
        public int? BeginTimeframeUnits2 { get; set; }
        public int? EndTimeframe2 { get; set; }
        public int? EndTimeframeUnits2 { get; set; }
        public int? BeginTimeframe3 { get; set; }
        public int? BeginTimeframeUnits3 { get; set; }
        public int? EndTimeframe3 { get; set; }
        public int? EndTimeframeUnits3 { get; set; }
        public int? BeginTimeframe4 { get; set; }
        public int? BeginTimeframeUnits4 { get; set; }
        public int? EndTimeframe4 { get; set; }
        public int? EndTimeframeUnits4 { get; set; }
        public int? BeginTimeframe5 { get; set; }
        public int? BeginTimeframeUnits5 { get; set; }
        public int? EndTimeframe5 { get; set; }
        public int? EndTimeframeUnits5 { get; set; }
        public string ImmunizationCdclink { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? DateFieldCount { get; set; }

        public Grantee Grantee { get; set; }
        public Immunization Immunization { get; set; }
        public ICollection<ParticipantImmunization> ParticipantImmunization { get; set; }
    }
}
