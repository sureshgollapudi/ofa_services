﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilyEngagement
    {
        public PrimaryGuardianFamilyEngagement()
        {
            PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes = new HashSet<PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes>();
            PrimaryGuardianFamilyEngagementAssociatedPirservices = new HashSet<PrimaryGuardianFamilyEngagementAssociatedPirservices>();
            PrimaryGuardianFamilyEngagementFlpworkshopActivity = new HashSet<PrimaryGuardianFamilyEngagementFlpworkshopActivity>();
            PrimaryGuardianFamilyEngagementGoals = new HashSet<PrimaryGuardianFamilyEngagementGoals>();
        }

        public int PrimaryGuardianFamilyEngagementId { get; set; }
        public int PrimaryGuardianId { get; set; }
        public int Type { get; set; }
        public DateTime? Date { get; set; }
        public int? CreatorUserId { get; set; }
        public int? Hours { get; set; }
        public int? Minutes { get; set; }
        public string Note { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int FatherFigureInvolved { get; set; }
        public int? LastModifiedUserId { get; set; }
        public int? AdministeredBy { get; set; }
        public string PartnerName { get; set; }

        public UserProfile CreatorUser { get; set; }
        public ParentGuardian PrimaryGuardian { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes> PrimaryGuardianFamilyEngagementAssociatedFamilyEngagementOutcomes { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagementAssociatedPirservices> PrimaryGuardianFamilyEngagementAssociatedPirservices { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagementFlpworkshopActivity> PrimaryGuardianFamilyEngagementFlpworkshopActivity { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagementGoals> PrimaryGuardianFamilyEngagementGoals { get; set; }
    }
}
