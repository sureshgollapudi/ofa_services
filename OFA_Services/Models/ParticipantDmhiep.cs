﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiep
    {
        public ParticipantDmhiep()
        {
            ParticipantDmhiepadditionalDiagnosis = new HashSet<ParticipantDmhiepadditionalDiagnosis>();
            ParticipantDmhiepassociatedConcern = new HashSet<ParticipantDmhiepassociatedConcern>();
        }

        public int ParticipantDmhiepid { get; set; }
        public int ParticipantId { get; set; }
        public int? PlanType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? InputDate { get; set; }
        public DateTime? DiagnosisDate { get; set; }
        public int? PrimaryDiagnosis { get; set; }
        public string AdditionalDiagnosisOther { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string AssociatedConcernOther { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public DateTime? TransitionDate { get; set; }

        public UserProfile CreatedByUser { get; set; }
        public Participant Participant { get; set; }
        public ICollection<ParticipantDmhiepadditionalDiagnosis> ParticipantDmhiepadditionalDiagnosis { get; set; }
        public ICollection<ParticipantDmhiepassociatedConcern> ParticipantDmhiepassociatedConcern { get; set; }
    }
}
