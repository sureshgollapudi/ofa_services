﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaPhoneType
    {
        public OfaPhoneType()
        {
            OfaParentPhoneNumber = new HashSet<OfaParentPhoneNumber>();
        }

        public int PhoneTypeId { get; set; }
        public string PhoneType { get; set; }

        public ICollection<OfaParentPhoneNumber> OfaParentPhoneNumber { get; set; }
    }
}
