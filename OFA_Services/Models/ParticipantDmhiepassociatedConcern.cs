﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepassociatedConcern
    {
        public int ParticipantDmhiepassociatedConcernId { get; set; }
        public int ParticipantDmhiepid { get; set; }
        public int AssociatedConcern { get; set; }

        public ParticipantDmhiep ParticipantDmhiep { get; set; }
    }
}
