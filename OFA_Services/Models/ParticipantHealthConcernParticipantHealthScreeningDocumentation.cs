﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantHealthConcernParticipantHealthScreeningDocumentation
    {
        public int ParticipantHealthConcernParticipantHealthScreeningDocumentationId { get; set; }
        public int? ParticipantHealthScreeningDocumentationId { get; set; }
        public int? ParticipantHealthConcernId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantHealthConcern ParticipantHealthConcern { get; set; }
        public ParticipantHealthScreeningDocumentation ParticipantHealthScreeningDocumentation { get; set; }
    }
}
