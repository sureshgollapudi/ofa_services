﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Classroom
    {
        public Classroom()
        {
            ClassroomProgramYear = new HashSet<ClassroomProgramYear>();
            UserClassroom = new HashSet<UserClassroom>();
        }

        public int ClassroomId { get; set; }
        public int CenterId { get; set; }
        public string Name { get; set; }
        public bool IsClassroomActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Center Center { get; set; }
        public ICollection<ClassroomProgramYear> ClassroomProgramYear { get; set; }
        public ICollection<UserClassroom> UserClassroom { get; set; }
    }
}
