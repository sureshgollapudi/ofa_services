﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordPirscreeningGroupStatus
    {
        public int ParticipantRecordPirscreeningGroupStatusId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int TypeId { get; set; }
        public int? C8typeAtEnrollmentStatus { get; set; }
        public int? C8typeAtEndOfEnrollmentStatus { get; set; }
        public int? C8typeAsOfTodayStatus { get; set; }
        public int? C29typeStatus { get; set; }
        public int ScreeningType { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? C20atEndofEnrollmentStatus { get; set; }
        public bool? IsFrozen { get; set; }

        public UserProfile CreatedByNavigation { get; set; }
        public UserProfile ModifiedByNavigation { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
