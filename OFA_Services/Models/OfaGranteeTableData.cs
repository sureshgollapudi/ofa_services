﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaGranteeTableData
    {
        public int GranteeTableValueId { get; set; }
        public int GranteeId { get; set; }
        public string Urlpath { get; set; }
        public string LogoPath { get; set; }
        public string LandpageHeader { get; set; }
        public string LandpageContent { get; set; }
        public string Landpagesubpoint1 { get; set; }
        public string Landpagesubpoint2 { get; set; }
        public string Landpagesubpoint3 { get; set; }
        public string LandingBackgroundPath { get; set; }
        public string LoginHeader { get; set; }
        public string LoginContent { get; set; }
        public string SignInHeader { get; set; }
        public string SignInContent { get; set; }
        public string RegisterHeader { get; set; }
        public string RegisterContent { get; set; }
        public string ParentRegisterHeader { get; set; }
        public string ParentRegisterContent { get; set; }
        public string DashboardHeader { get; set; }
        public string DashboardContent { get; set; }
        public string LeftMenuHeader { get; set; }
        public string LeftMenuContent { get; set; }
    }
}
