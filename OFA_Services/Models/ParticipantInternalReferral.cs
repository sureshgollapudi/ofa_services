﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantInternalReferral
    {
        public ParticipantInternalReferral()
        {
            ParticipantInternalReferralResolutionConcern = new HashSet<ParticipantInternalReferralResolutionConcern>();
        }

        public int ParticipantInternalReferralId { get; set; }
        public int ParticipantId { get; set; }
        public DateTime ReferralDate { get; set; }
        public int ReferralInitiatedFrom { get; set; }
        public int? ReferralInitiatedBy { get; set; }
        public string Notes { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsResolved { get; set; }
        public DateTime? ResolvedDate { get; set; }
        public int? ResolvedReason { get; set; }
        public int? ParticipantHealthScreeningDocumentationId { get; set; }
        public int? ParticipantEducationScreeningDocumentationId { get; set; }
        public int? ParticipantProfileLinked { get; set; }

        public Participant Participant { get; set; }
        public ParticipantEducationScreeningDocumentation ParticipantEducationScreeningDocumentation { get; set; }
        public ParticipantHealthScreeningDocumentation ParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<ParticipantInternalReferralResolutionConcern> ParticipantInternalReferralResolutionConcern { get; set; }
    }
}
