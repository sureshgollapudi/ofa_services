﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserAppraisalStaffEvaluation
    {
        public int UserAppraisalStaffEvaluationId { get; set; }
        public int UserId { get; set; }
        public string AppraisalEvaluationType { get; set; }
        public DateTime? EvaluationDate { get; set; }

        public UserProfile User { get; set; }
    }
}
