﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantAdditionalFamilyMember
    {
        public int ParticipantAdditionalFamilyMemberId { get; set; }
        public int ParticipantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ChildRelationshipToParentGuardian { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int? Gender { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? DoNotCount { get; set; }
        public int? NumberofFamilyMembers { get; set; }
        public int? AdditionalFamilyMemberId { get; set; }

        public Participant Participant { get; set; }
    }
}
