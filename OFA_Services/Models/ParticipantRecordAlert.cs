﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordAlert
    {
        public int ParticipantRecordAlertId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int AlertId { get; set; }
        public DateTime? AlertSetDate { get; set; }
        public int Points { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
