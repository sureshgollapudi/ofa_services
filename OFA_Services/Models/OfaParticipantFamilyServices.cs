﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipantFamilyServices
    {
        public int ParticipantFamilyServicesId { get; set; }
        public int OfaParticipantId { get; set; }
        public int FamilyServiceId { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParticipant OfaParticipant { get; set; }
    }
}
