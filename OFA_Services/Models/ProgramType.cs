﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ProgramType
    {
        public ProgramType()
        {
            AlertProgramType = new HashSet<AlertProgramType>();
            MetricProgramType = new HashSet<MetricProgramType>();
            ParticipantEducationScreeningDocumentation = new HashSet<ParticipantEducationScreeningDocumentation>();
            ParticipantHealthScreeningDocumentation = new HashSet<ParticipantHealthScreeningDocumentation>();
            Program = new HashSet<Program>();
            ProgramPriority = new HashSet<ProgramPriority>();
        }

        public int ProgramTypeId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }

        public ICollection<AlertProgramType> AlertProgramType { get; set; }
        public ICollection<MetricProgramType> MetricProgramType { get; set; }
        public ICollection<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentation { get; set; }
        public ICollection<ParticipantHealthScreeningDocumentation> ParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<Program> Program { get; set; }
        public ICollection<ProgramPriority> ProgramPriority { get; set; }
    }
}
