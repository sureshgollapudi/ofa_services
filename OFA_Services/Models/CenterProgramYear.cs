﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class CenterProgramYear
    {
        public CenterProgramYear()
        {
            ClassroomProgramYear = new HashSet<ClassroomProgramYear>();
            ParticipantRecord = new HashSet<ParticipantRecord>();
        }

        public int CenterProgramYearId { get; set; }
        public int CenterId { get; set; }
        public int ProgramYearId { get; set; }
        public int? GranteeHealthScreeningSetId { get; set; }
        public int? EducationScreeningSetId { get; set; }
        public DateTime? SchoolDistrictCutoffDate { get; set; }
        public bool? IsCenterProgramYearActive { get; set; }
        public bool? IsCenterProgramYearFederalInterestEstablished { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? IsPpvtfieldsActive { get; set; }
        public bool? IsWoodcockJohnsonFieldsActive { get; set; }
        public int? DoesCenterServesPregnantWoman { get; set; }
        public int? Iscopied { get; set; }
        public int? AssessmentTypeId { get; set; }
        public int? DmhsetId { get; set; }

        public Center Center { get; set; }
        public EducationScreeningSet EducationScreeningSet { get; set; }
        public GranteeHealthScreeningSet GranteeHealthScreeningSet { get; set; }
        public ProgramYear ProgramYear { get; set; }
        public ICollection<ClassroomProgramYear> ClassroomProgramYear { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecord { get; set; }
    }
}
