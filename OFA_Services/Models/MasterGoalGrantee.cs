﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class MasterGoalGrantee
    {
        public int MasterGoalGranteeId { get; set; }
        public int GoalId { get; set; }
        public int GranteeId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Goal Goal { get; set; }
        public Grantee Grantee { get; set; }
    }
}
