﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OFA_Services.Models
{
    public partial class FileUpload
    {        
        public string Base64String { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        // public IFormFile File { get; set; }
    }
}
