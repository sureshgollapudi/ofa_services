﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DmhsummaryPriority
    {
        public int DmhsummaryPriorityId { get; set; }
        public int? ProcessDocumentId { get; set; }
        public int? NoteTypeId { get; set; }
        public int? Priority { get; set; }
        public int? NextRequieredActionId { get; set; }
        public string NextRequireDueDateFeild { get; set; }
        public string NextRequireDueDateDays { get; set; }
        public int? StatusId { get; set; }
        public string DuedateInitial { get; set; }
    }
}
