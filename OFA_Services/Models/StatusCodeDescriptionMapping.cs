﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class StatusCodeDescriptionMapping
    {
        public int Id { get; set; }
        public string StatusCodeDescription { get; set; }
        public string ScreeningResultsStatus { get; set; }
        public string Notes { get; set; }
        public bool? Refusal { get; set; }
        public string RefusalDate { get; set; }
        public string GranteeName { get; set; }
    }
}
