﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantInternalReferralResolutionConcern
    {
        public int ParticipantInternalReferralResolutionConcernId { get; set; }
        public int? ParticipantInternalReferralId { get; set; }
        public int ConcernId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? AssociatedConcernId { get; set; }
        public int? StatusId { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ParticipantInternalReferral ParticipantInternalReferral { get; set; }
    }
}
