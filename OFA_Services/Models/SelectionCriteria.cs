﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class SelectionCriteria
    {
        public SelectionCriteria()
        {
            Program = new HashSet<Program>();
            ProgramYear = new HashSet<ProgramYear>();
            SelectionCriteriaCategory = new HashSet<SelectionCriteriaCategory>();
        }

        public int SelectionCriteriaId { get; set; }
        public int GranteeId { get; set; }
        public bool? IsSelectionCriteriaActive { get; set; }
        public string Name { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public bool? IsEmergencyContactsFormActive { get; set; }
        public bool? IsEmergencyContactsFormRequired { get; set; }
        public bool? IsProofOfResidencyActive { get; set; }
        public bool? IsProofOfResidencyRequired { get; set; }
        public bool? IsEmergencyConsentActive { get; set; }
        public bool? IsEmergencyConsentRequired { get; set; }
        public bool? IsPermissionToReleaseAndRequestInformationActive { get; set; }
        public bool? IsPermissionToReleaseAndRequestInformationRequired { get; set; }
        public bool? IsPermissionForProgramActivitiesActive { get; set; }
        public bool? IsPermissionForProgramActivitiesRequired { get; set; }
        public bool? IsFamilyInvolvementContractActive { get; set; }
        public bool? IsFamilyInvolvementContractRequired { get; set; }
        public bool? IsFamilyVolunteeringContractActive { get; set; }
        public bool? IsFamilyVolunteeringContractRequired { get; set; }
        public bool? IsExtendedDayFormActiveActive { get; set; }
        public bool? IsExtendedDayFormRequired { get; set; }
        public string CustomAdmissionForm1Name { get; set; }
        public string CustomAdmissionForm2Name { get; set; }
        public string CustomAdmissionForm3Name { get; set; }
        public string CustomAdmissionForm4Name { get; set; }
        public string CustomAdmissionForm5Name { get; set; }
        public bool? IsCustomAdmissionForm1Active { get; set; }
        public bool? IsCustomAdmissionForm1Required { get; set; }
        public bool? IsCustomAdmissionForm2Active { get; set; }
        public bool? IsCustomAdmissionForm2Required { get; set; }
        public bool? IsCustomAdmissionForm3Active { get; set; }
        public bool? IsCustomAdmissionForm3Required { get; set; }
        public bool? IsCustomAdmissionForm4Active { get; set; }
        public bool? IsCustomAdmissionForm4Required { get; set; }
        public bool? IsCustomAdmissionForm5Active { get; set; }
        public bool? IsCustomAdmissionForm5Required { get; set; }
        public bool? IsImmunizationsFormActive { get; set; }
        public bool? IsImmunizationsFormRequired { get; set; }
        public bool? IsAdditionalHealthRequirementsFormActive { get; set; }
        public bool? IsAdditionalHealthRequirementsFormRequired { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<Program> Program { get; set; }
        public ICollection<ProgramYear> ProgramYear { get; set; }
        public ICollection<SelectionCriteriaCategory> SelectionCriteriaCategory { get; set; }
    }
}
