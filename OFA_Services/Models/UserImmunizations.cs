﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserImmunizations
    {
        public int UserImmunizationsId { get; set; }
        public int UserId { get; set; }
        public int ImmunizationsId { get; set; }
        public DateTime? ScreenDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? EvaluationDate { get; set; }

        public UserProfile User { get; set; }
    }
}
