﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserClassroom
    {
        public int UserClassroomId { get; set; }
        public int UserId { get; set; }
        public int ClassroomId { get; set; }
        public string Type { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateRemoved { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Classroom Classroom { get; set; }
        public UserProfile User { get; set; }
    }
}
