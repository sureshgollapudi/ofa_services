﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Themes
    {
        public Themes()
        {
            GranteeTheme = new HashSet<GranteeTheme>();
        }

        public int ThemeId { get; set; }
        public string ThemeName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int Duration { get; set; }
        public int? Unit { get; set; }

        public ICollection<GranteeTheme> GranteeTheme { get; set; }
    }
}
