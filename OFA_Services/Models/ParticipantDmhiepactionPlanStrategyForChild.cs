﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepactionPlanStrategyForChild
    {
        public int ParticipantDmhiepactionPlanStrategyForChildId { get; set; }
        public int ParticipantDmhiepactionPlanId { get; set; }
        public int StrategyForChild { get; set; }

        public ParticipantDmhiepactionPlan ParticipantDmhiepactionPlan { get; set; }
    }
}
