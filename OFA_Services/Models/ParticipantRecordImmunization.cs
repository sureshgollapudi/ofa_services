﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordImmunization
    {
        public int ParticipantRecordImmunizationId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int ImmunizationId { get; set; }
        public DateTime? FirstDate { get; set; }
        public DateTime? SecondDate { get; set; }
        public DateTime? ThirdDate { get; set; }
        public DateTime? FourthDate { get; set; }
        public DateTime? FifthDate { get; set; }
        public DateTime? OverrideDate { get; set; }
        public bool? Exempt { get; set; }
        public string ExemptReason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Immunization Immunization { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
