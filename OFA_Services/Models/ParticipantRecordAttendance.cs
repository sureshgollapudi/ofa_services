﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordAttendance
    {
        public int ParticipantRecordAttendanceId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int ClassroomProgramYearId { get; set; }
        public DateTime Date { get; set; }
        public int? ClassroomStatus { get; set; }
        public int? AttendanceCode { get; set; }
        public TimeSpan? ArrivalTime { get; set; }
        public TimeSpan? DepartureTime { get; set; }
        public int? AbsenceReason { get; set; }
        public string AbsenceReasonNotes { get; set; }
        public bool? Amsnack { get; set; }
        public bool? Pmsnack { get; set; }
        public bool? Breakfast { get; set; }
        public bool? Lunch { get; set; }
        public bool? Dinner { get; set; }
        public int? AttendanceInputUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedUserid { get; set; }

        public ClassroomProgramYear ClassroomProgramYear { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
