﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserAdditionalTrainings
    {
        public int UserAdditionalTrainingId { get; set; }
        public int UserId { get; set; }
        public string TrainingType { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? PointValue { get; set; }
        public int? StateId { get; set; }

        public UserProfile User { get; set; }
    }
}
