﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationHomeBasedSocialization
    {
        public int ParticipantRecordEducationHomeBasedSocializationId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime HomeBasedSocializationDate { get; set; }
        public string HomeBasedSocializationTheme { get; set; }
        public string HomeBasedSocializationNotes { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string HomeBasedSocializationPregnantWomenServices { get; set; }
        public string HomeBasedSocializationPirservices { get; set; }
        public bool HomeBasedSocializationPirservicesEmergencyOrCrisisIntervention { get; set; }
        public bool HomeBasedSocializationPirservicesMentalHealthServices { get; set; }
        public bool HomeBasedSocializationPirservicesSubstanceAbusePrevention { get; set; }
        public bool HomeBasedSocializationPirservicesSubstanceAbuseTreatment { get; set; }
        public bool HomeBasedSocializationPirservicesChildAbuseAndNeglectServices { get; set; }
        public bool HomeBasedSocializationPirservicesDomesticViolenceServices { get; set; }
        public bool HomeBasedSocializationPirservicesParentingEducation { get; set; }
        public bool HomeBasedSocializationPirservicesRelationshipOrMarriageEducation { get; set; }
        public bool HomeBasedSocializationPirservicesAssistanceToFamiliesOfIncarceratedIndividuals { get; set; }
        public bool HomeBasedSocializationPirservicesHousingAssistance { get; set; }
        public bool HomeBasedSocializationPirservicesEsltraining { get; set; }
        public bool HomeBasedSocializationPirservicesAdultEducation { get; set; }
        public bool HomeBasedSocializationPirservicesJobTraining { get; set; }
        public bool HomeBasedSocializationPirservicesChildSupportAssistance { get; set; }
        public bool HomeBasedSocializationPirservicesHealthEducation { get; set; }
        public bool HomeBasedSocializationPirservicesAssetBuilding { get; set; }
        public bool HomeBasedSocializationPregnantWomenPirservicesPrenantalHealthCare { get; set; }
        public bool HomeBasedSocializationPregnantWomenPirservicesPostpartumHealthCare { get; set; }
        public bool HomeBasedSocializationPregnantWomenPirservicesPrenatalEducationOnFetalDevelopment { get; set; }
        public bool HomeBasedSocializationPregnantWomenPirservicesInformationOnBenefitsOfBreastfeeding { get; set; }
        public int FatherFigureInvolved { get; set; }
        public int? LastModifiedBy { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
