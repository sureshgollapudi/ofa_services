﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationHomeVisits
    {
        public int ParticipantRecordEducationHomeVisitsId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int HomeVisit { get; set; }
        public int PlanningEducationHvptcstatus { get; set; }
        public DateTime PlanningEducationHvptcattemptDate { get; set; }
        public int PlanningEducationHvptcattemptReason { get; set; }
        public string PlanningEducationHvptcnotes { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedBy { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
