﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantEvaluationCenterprogramyear
    {
        public ParticipantEvaluationCenterprogramyear()
        {
            Evaluation = new HashSet<Evaluation>();
        }

        public int ParticipantEvaluationCenterprogramyearId { get; set; }
        public int? ParticipantId { get; set; }
        public int? TimeStampId { get; set; }
        public int? CenterProgramYearId { get; set; }

        public Participant Participant { get; set; }
        public TimeStamp TimeStamp { get; set; }
        public ICollection<Evaluation> Evaluation { get; set; }
    }
}
