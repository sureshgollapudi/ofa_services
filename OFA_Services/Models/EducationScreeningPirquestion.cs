﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class EducationScreeningPirquestion
    {
        public int EducationScreeningPirquestionId { get; set; }
        public int EducationScreeningId { get; set; }
        public int PirquestionId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModified { get; set; }

        public EducationScreening EducationScreening { get; set; }
    }
}
