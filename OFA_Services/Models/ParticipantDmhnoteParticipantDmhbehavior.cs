﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteParticipantDmhbehavior
    {
        public int ParticipantDmhnoteParticipantDmhbehaviorId { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ParticipantDmhbehaviorPlanId { get; set; }
    }
}
