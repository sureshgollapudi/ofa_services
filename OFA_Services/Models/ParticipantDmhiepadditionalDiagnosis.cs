﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepadditionalDiagnosis
    {
        public int ParticipantDmhiepadditionalDiagnosisId { get; set; }
        public int ParticipantDmhiepid { get; set; }
        public int AdditionalDiagnosis { get; set; }

        public ParticipantDmhiep ParticipantDmhiep { get; set; }
    }
}
