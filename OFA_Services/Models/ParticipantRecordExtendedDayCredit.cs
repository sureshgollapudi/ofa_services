﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordExtendedDayCredit
    {
        public int ParticipantRecordExtendedDayCreditId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int Type { get; set; }
        public DateTime TimePeriodtoApplyCreditTo { get; set; }
        public decimal? AmountToApply { get; set; }
        public string Notes { get; set; }
        public int? Reason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
