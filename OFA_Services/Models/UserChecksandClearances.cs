﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserChecksandClearances
    {
        public int UserChecksandClearancesId { get; set; }
        public int UserId { get; set; }
        public string CheckClearanceType { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? StateId { get; set; }

        public UserProfile User { get; set; }
    }
}
