﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AssessmentDocument
    {
        public AssessmentDocument()
        {
            AssessmentDocumentTag = new HashSet<AssessmentDocumentTag>();
            EvaluationDocument = new HashSet<EvaluationDocument>();
        }

        public int AssessmentDocId { get; set; }
        public string AnecDote { get; set; }
        public int AlfaType { get; set; }
        public DateTime Date { get; set; }
        public string Path { get; set; }
        public int DocType { get; set; }
        public int TeacherId { get; set; }
        public int IsCompleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? FileAttachmentId { get; set; }

        public ICollection<AssessmentDocumentTag> AssessmentDocumentTag { get; set; }
        public ICollection<EvaluationDocument> EvaluationDocument { get; set; }
    }
}
