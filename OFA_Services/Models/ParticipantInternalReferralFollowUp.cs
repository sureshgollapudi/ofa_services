﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantInternalReferralFollowUp
    {
        public int ParticipantInternalReferralFollowUpId { get; set; }
        public int ParticipantInternalReferralId { get; set; }
        public DateTime FollowUpDate { get; set; }
        public string Notes { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
