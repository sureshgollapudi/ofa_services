﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantAlert
    {
        public int ParticipantAlertId { get; set; }
        public int ParticipantId { get; set; }
        public int AlertId { get; set; }
        public int Points { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime? AlertSetDate { get; set; }
        public int? GoalId { get; set; }

        public Alert Alert { get; set; }
        public Participant Participant { get; set; }
    }
}
