﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserProfile
    {
        public UserProfile()
        {
            FileAttachment = new HashSet<FileAttachment>();
            FileAttachmentTemp = new HashSet<FileAttachmentTemp>();
            ParticipantDmhactionPlan = new HashSet<ParticipantDmhactionPlan>();
            ParticipantDmhconcern = new HashSet<ParticipantDmhconcern>();
            ParticipantDmhiep = new HashSet<ParticipantDmhiep>();
            ParticipantDmhnote = new HashSet<ParticipantDmhnote>();
            ParticipantEducationScreeningDocumentationScreener = new HashSet<ParticipantEducationScreeningDocumentation>();
            ParticipantEducationScreeningDocumentationScreeningDocumentationUser = new HashSet<ParticipantEducationScreeningDocumentation>();
            ParticipantEducationScreeningPlanning = new HashSet<ParticipantEducationScreeningPlanning>();
            ParticipantHealthScreeningDocumentation = new HashSet<ParticipantHealthScreeningDocumentation>();
            ParticipantHealthScreeningPlanning = new HashSet<ParticipantHealthScreeningPlanning>();
            ParticipantRecordCaseNote = new HashSet<ParticipantRecordCaseNote>();
            ParticipantRecordClassroomProgramYearCreatedByNavigation = new HashSet<ParticipantRecordClassroomProgramYear>();
            ParticipantRecordClassroomProgramYearModifiedByNavigation = new HashSet<ParticipantRecordClassroomProgramYear>();
            ParticipantRecordCreatedByNavigation = new HashSet<ParticipantRecord>();
            ParticipantRecordEligibility = new HashSet<ParticipantRecordEligibility>();
            ParticipantRecordEnrollmentTransactionHistory = new HashSet<ParticipantRecordEnrollmentTransactionHistory>();
            ParticipantRecordExtendedDayFollowUp = new HashSet<ParticipantRecordExtendedDayFollowUp>();
            ParticipantRecordFamilyAdvocateAdvocateAssignmentEnteredByUser = new HashSet<ParticipantRecordFamilyAdvocate>();
            ParticipantRecordFamilyAdvocateFamilyAdvocateUser = new HashSet<ParticipantRecordFamilyAdvocate>();
            ParticipantRecordFamilyAdvocateUser = new HashSet<ParticipantRecord>();
            ParticipantRecordFile = new HashSet<ParticipantRecordFile>();
            ParticipantRecordGoal = new HashSet<ParticipantRecordGoal>();
            ParticipantRecordGoalFollowUp = new HashSet<ParticipantRecordGoalFollowUp>();
            ParticipantRecordImmunizationSetStatusEnrollmentStatusAtEnrollmentOwnerUser = new HashSet<ParticipantRecordImmunizationSetStatus>();
            ParticipantRecordImmunizationSetStatusEnrollmentStatusEndOfEnrollmentOwnerUser = new HashSet<ParticipantRecordImmunizationSetStatus>();
            ParticipantRecordModifiedByNavigation = new HashSet<ParticipantRecord>();
            ParticipantRecordPirscreeningGroupStatusCreatedByNavigation = new HashSet<ParticipantRecordPirscreeningGroupStatus>();
            ParticipantRecordPirscreeningGroupStatusModifiedByNavigation = new HashSet<ParticipantRecordPirscreeningGroupStatus>();
            ParticipantRecordPirscreeningStatusCreatedByNavigation = new HashSet<ParticipantRecordPirscreeningStatus>();
            ParticipantRecordPirscreeningStatusModifiedByNavigation = new HashSet<ParticipantRecordPirscreeningStatus>();
            ParticipantRecordPreEnrollmentTransactionHistory = new HashSet<ParticipantRecordPreEnrollmentTransactionHistory>();
            ParticipantRecordPrimaryOwnerUser = new HashSet<ParticipantRecord>();
            ParticipantRecordSecondaryOwnerUser = new HashSet<ParticipantRecord>();
            PrimaryGuardianCaseNote = new HashSet<PrimaryGuardianCaseNote>();
            PrimaryGuardianFamilyEngagement = new HashSet<PrimaryGuardianFamilyEngagement>();
            PrimaryGuardianGoal = new HashSet<PrimaryGuardianGoal>();
            PrimaryGuardianGoalFollowUp = new HashSet<PrimaryGuardianGoalFollowUp>();
            PrimaryGuardianPireducationOrTrainingCreatedByNavigation = new HashSet<PrimaryGuardianPireducationOrTraining>();
            PrimaryGuardianPireducationOrTrainingModifiedByNavigation = new HashSet<PrimaryGuardianPireducationOrTraining>();
            PrimaryGuardianPirfamilyInformation = new HashSet<PrimaryGuardianPirfamilyInformation>();
            UserAdditionalTrainings = new HashSet<UserAdditionalTrainings>();
            UserAlert = new HashSet<UserAlert>();
            UserAnnualTrainings = new HashSet<UserAnnualTrainings>();
            UserAppraisalStaffEvaluation = new HashSet<UserAppraisalStaffEvaluation>();
            UserCenter = new HashSet<UserCenter>();
            UserChecksandClearances = new HashSet<UserChecksandClearances>();
            UserClassroom = new HashSet<UserClassroom>();
            UserDashboard = new HashSet<UserDashboard>();
            UserEmergencyContacts = new HashSet<UserEmergencyContacts>();
            UserEmployeePhoneNumbers = new HashSet<UserEmployeePhoneNumbers>();
            UserHireInformation = new HashSet<UserHireInformation>();
            UserImmunizations = new HashSet<UserImmunizations>();
            UserJobDescriptions = new HashSet<UserJobDescriptions>();
            UserLanguageProficiency = new HashSet<UserLanguageProficiency>();
            UserLoginLog = new HashSet<UserLoginLog>();
            UserPirposition = new HashSet<UserPirposition>();
            UserRaceNavigation = new HashSet<UserRace>();
            UserSuccessRubrics = new HashSet<UserSuccessRubrics>();
            UserTab = new HashSet<UserTab>();
            WebpagesUsersInRoles = new HashSet<WebpagesUsersInRoles>();
        }

        public int UserId { get; set; }
        public int GranteeId { get; set; }
        public int? RoleId { get; set; }
        public string UserName { get; set; }
        public bool? IsUserActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public string Gender { get; set; }
        public int Type { get; set; }
        public string PositionDescription { get; set; }
        public string RaceOther { get; set; }
        public int? Ethnicity { get; set; }
        public string LanguageProficiencyOther { get; set; }
        public DateTime? EmploymentHireDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public int? TerminationReason { get; set; }
        public string TerminationReasonOtherExplanation { get; set; }
        public int? CurrentFormerHeadStartParent { get; set; }
        public int? QualificationLevel { get; set; }
        public int? QualificationType { get; set; }
        public string QualificationTypeOther { get; set; }
        public int? QualificationLevelEnrollment { get; set; }
        public decimal? AnnualSalary { get; set; }
        public int? PercentageAnnualSalaryHsorEhs { get; set; }
        public int? HourlyRate { get; set; }
        public bool? EnrollmentRights { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string Token { get; set; }
        public string DefaultTab { get; set; }
        public string DefaultProgramYears { get; set; }
        public string DefaultCenters { get; set; }
        public string DefaultClassrooms { get; set; }
        public string DefaultFamilyAdvocates { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Apartment { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string LicenseNumber { get; set; }
        public string StateOfIssue { get; set; }
        public bool? ProofofResidence { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string SchoolName { get; set; }
        public string Major { get; set; }
        public int? NumberofEcecredits { get; set; }
        public int? YearsofHeadStartExperience { get; set; }
        public int? YearsofChildcareDaycareExperience { get; set; }
        public string RegistryCertificateLevel { get; set; }
        public int? HighestDegreeEarned { get; set; }
        public DateTime? PhysicalCompletionDate { get; set; }
        public DateTime? CancompletionDate { get; set; }
        public DateTime? CanexpirationDate { get; set; }
        public DateTime? SbscompletionDate { get; set; }
        public DateTime? SbsexpiratioDate { get; set; }
        public DateTime? PdgcompletionDate { get; set; }
        public DateTime? PdgexpiratioDate { get; set; }
        public DateTime? BpcompletionDate { get; set; }
        public DateTime? BpexpiratioDate { get; set; }
        public DateTime? CfacompletionDate { get; set; }
        public DateTime? CfaexpiratioDate { get; set; }
        public DateTime? NhocompletionDate { get; set; }
        public DateTime? PdpplanDate { get; set; }
        public DateTime? CpaagreementDate { get; set; }
        public string EmotionalSupport { get; set; }
        public string InstructionalSupport { get; set; }
        public string ClassroomOrganization { get; set; }
        public string EsobservationCompletedBy { get; set; }
        public string IsobservationCompletedBy { get; set; }
        public string CoobservationCompletedBy { get; set; }
        public DateTime? EsevaluationDate { get; set; }
        public DateTime? IsevaluationDate { get; set; }
        public DateTime? CoevaluationDate { get; set; }
        public DateTime? ImmunizationsTdapmmrscreenDate { get; set; }
        public DateTime? ImmunizationsTdapmmrexpirationDate { get; set; }
        public DateTime? ImmunizationsTbscreenDate { get; set; }
        public DateTime? ImmunizationsTbexpirationDate { get; set; }
        public DateTime? ImmunizationsTetanusScreenDate { get; set; }
        public DateTime? ImmunizationsTetanusExpirationDate { get; set; }
        public bool? IsAssignPrograms { get; set; }
        public bool? AllowUserAdminPermissions { get; set; }
        public bool? IsDirectorsCredential { get; set; }
        public bool IsCaseLoad { get; set; }
        public bool? IsAssessmentOverride { get; set; }
        public string AdvocateType { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? ExpirationDateCda5 { get; set; }
        public string BiracialRace { get; set; }
        public string RaceUnspecified { get; set; }
        public int UserRace { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public bool? IsInternalReferralResolve { get; set; }
        public string DefaultDmhstatuses { get; set; }

        public Grantee Grantee { get; set; }
        public WebpagesRoles Role { get; set; }
        public WebpagesMembership WebpagesMembership { get; set; }
        public ICollection<FileAttachment> FileAttachment { get; set; }
        public ICollection<FileAttachmentTemp> FileAttachmentTemp { get; set; }
        public ICollection<ParticipantDmhactionPlan> ParticipantDmhactionPlan { get; set; }
        public ICollection<ParticipantDmhconcern> ParticipantDmhconcern { get; set; }
        public ICollection<ParticipantDmhiep> ParticipantDmhiep { get; set; }
        public ICollection<ParticipantDmhnote> ParticipantDmhnote { get; set; }
        public ICollection<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentationScreener { get; set; }
        public ICollection<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentationScreeningDocumentationUser { get; set; }
        public ICollection<ParticipantEducationScreeningPlanning> ParticipantEducationScreeningPlanning { get; set; }
        public ICollection<ParticipantHealthScreeningDocumentation> ParticipantHealthScreeningDocumentation { get; set; }
        public ICollection<ParticipantHealthScreeningPlanning> ParticipantHealthScreeningPlanning { get; set; }
        public ICollection<ParticipantRecordCaseNote> ParticipantRecordCaseNote { get; set; }
        public ICollection<ParticipantRecordClassroomProgramYear> ParticipantRecordClassroomProgramYearCreatedByNavigation { get; set; }
        public ICollection<ParticipantRecordClassroomProgramYear> ParticipantRecordClassroomProgramYearModifiedByNavigation { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecordCreatedByNavigation { get; set; }
        public ICollection<ParticipantRecordEligibility> ParticipantRecordEligibility { get; set; }
        public ICollection<ParticipantRecordEnrollmentTransactionHistory> ParticipantRecordEnrollmentTransactionHistory { get; set; }
        public ICollection<ParticipantRecordExtendedDayFollowUp> ParticipantRecordExtendedDayFollowUp { get; set; }
        public ICollection<ParticipantRecordFamilyAdvocate> ParticipantRecordFamilyAdvocateAdvocateAssignmentEnteredByUser { get; set; }
        public ICollection<ParticipantRecordFamilyAdvocate> ParticipantRecordFamilyAdvocateFamilyAdvocateUser { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecordFamilyAdvocateUser { get; set; }
        public ICollection<ParticipantRecordFile> ParticipantRecordFile { get; set; }
        public ICollection<ParticipantRecordGoal> ParticipantRecordGoal { get; set; }
        public ICollection<ParticipantRecordGoalFollowUp> ParticipantRecordGoalFollowUp { get; set; }
        public ICollection<ParticipantRecordImmunizationSetStatus> ParticipantRecordImmunizationSetStatusEnrollmentStatusAtEnrollmentOwnerUser { get; set; }
        public ICollection<ParticipantRecordImmunizationSetStatus> ParticipantRecordImmunizationSetStatusEnrollmentStatusEndOfEnrollmentOwnerUser { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecordModifiedByNavigation { get; set; }
        public ICollection<ParticipantRecordPirscreeningGroupStatus> ParticipantRecordPirscreeningGroupStatusCreatedByNavigation { get; set; }
        public ICollection<ParticipantRecordPirscreeningGroupStatus> ParticipantRecordPirscreeningGroupStatusModifiedByNavigation { get; set; }
        public ICollection<ParticipantRecordPirscreeningStatus> ParticipantRecordPirscreeningStatusCreatedByNavigation { get; set; }
        public ICollection<ParticipantRecordPirscreeningStatus> ParticipantRecordPirscreeningStatusModifiedByNavigation { get; set; }
        public ICollection<ParticipantRecordPreEnrollmentTransactionHistory> ParticipantRecordPreEnrollmentTransactionHistory { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecordPrimaryOwnerUser { get; set; }
        public ICollection<ParticipantRecord> ParticipantRecordSecondaryOwnerUser { get; set; }
        public ICollection<PrimaryGuardianCaseNote> PrimaryGuardianCaseNote { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagement> PrimaryGuardianFamilyEngagement { get; set; }
        public ICollection<PrimaryGuardianGoal> PrimaryGuardianGoal { get; set; }
        public ICollection<PrimaryGuardianGoalFollowUp> PrimaryGuardianGoalFollowUp { get; set; }
        public ICollection<PrimaryGuardianPireducationOrTraining> PrimaryGuardianPireducationOrTrainingCreatedByNavigation { get; set; }
        public ICollection<PrimaryGuardianPireducationOrTraining> PrimaryGuardianPireducationOrTrainingModifiedByNavigation { get; set; }
        public ICollection<PrimaryGuardianPirfamilyInformation> PrimaryGuardianPirfamilyInformation { get; set; }
        public ICollection<UserAdditionalTrainings> UserAdditionalTrainings { get; set; }
        public ICollection<UserAlert> UserAlert { get; set; }
        public ICollection<UserAnnualTrainings> UserAnnualTrainings { get; set; }
        public ICollection<UserAppraisalStaffEvaluation> UserAppraisalStaffEvaluation { get; set; }
        public ICollection<UserCenter> UserCenter { get; set; }
        public ICollection<UserChecksandClearances> UserChecksandClearances { get; set; }
        public ICollection<UserClassroom> UserClassroom { get; set; }
        public ICollection<UserDashboard> UserDashboard { get; set; }
        public ICollection<UserEmergencyContacts> UserEmergencyContacts { get; set; }
        public ICollection<UserEmployeePhoneNumbers> UserEmployeePhoneNumbers { get; set; }
        public ICollection<UserHireInformation> UserHireInformation { get; set; }
        public ICollection<UserImmunizations> UserImmunizations { get; set; }
        public ICollection<UserJobDescriptions> UserJobDescriptions { get; set; }
        public ICollection<UserLanguageProficiency> UserLanguageProficiency { get; set; }
        public ICollection<UserLoginLog> UserLoginLog { get; set; }
        public ICollection<UserPirposition> UserPirposition { get; set; }
        public ICollection<UserRace> UserRaceNavigation { get; set; }
        public ICollection<UserSuccessRubrics> UserSuccessRubrics { get; set; }
        public ICollection<UserTab> UserTab { get; set; }
        public ICollection<WebpagesUsersInRoles> WebpagesUsersInRoles { get; set; }
    }
}
