﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class HealthScreeningPirquestion
    {
        public int HealthScreeningPirquestionId { get; set; }
        public int HealthScreeningId { get; set; }
        public int PirquestionId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public HealthScreening HealthScreening { get; set; }
        public Pirquestion Pirquestion { get; set; }
    }
}
