﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class MetricProgramType
    {
        public int MetricProgramTypeId { get; set; }
        public int MetricId { get; set; }
        public int ProgramTypeId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Metric Metric { get; set; }
        public ProgramType ProgramType { get; set; }
    }
}
