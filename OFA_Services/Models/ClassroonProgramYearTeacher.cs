﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ClassroonProgramYearTeacher
    {
        public int ClassroonProgramYearTeacherId { get; set; }
        public int? ClassroomId { get; set; }
        public int? ProgramYearId { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateRemoved { get; set; }
        public string TeacherType { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
    }
}
