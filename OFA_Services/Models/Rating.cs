﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Rating
    {
        public int RatingId { get; set; }
        public int LevelId { get; set; }
        public string RatingName { get; set; }
        public string Description { get; set; }
        public int AssessmentLevelId { get; set; }
        public int? DisplayOrder { get; set; }

        public AssessmentLevel AssessmentLevel { get; set; }
    }
}
