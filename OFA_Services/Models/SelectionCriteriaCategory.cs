﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class SelectionCriteriaCategory
    {
        public SelectionCriteriaCategory()
        {
            SelectionCriteriaCategoryItem = new HashSet<SelectionCriteriaCategoryItem>();
        }

        public int SelectionCriteriaCategoryId { get; set; }
        public int SelectionCriteriaId { get; set; }
        public string Name { get; set; }
        public int ListingOrder { get; set; }
        public bool IsMultiSelect { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public SelectionCriteria SelectionCriteria { get; set; }
        public ICollection<SelectionCriteriaCategoryItem> SelectionCriteriaCategoryItem { get; set; }
    }
}
