﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEnrollmentHistory
    {
        public int ParticipantRecordEnrollmentHistoryId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int? StatusId { get; set; }
        public bool? StatusIdchangeIndicator { get; set; }
        public int? CenterProgramYearId { get; set; }
        public bool? CenterProgramYearChangeIndicator { get; set; }
        public int? ClassroomProgramYearId { get; set; }
        public bool? ClassroomProgramYearChangeIndicator { get; set; }
        public DateTime? FirstDayInNewClassroom { get; set; }
        public bool? FirstDayInNewClassroomChangeIndicator { get; set; }
        public int? FamilyAdvocateUserId { get; set; }
        public bool? FamilyAdvocateUserIdChangeIndicator { get; set; }
        public DateTime? EnrollmentDate { get; set; }
        public bool? EnrollmentDateChangeIndicator { get; set; }
        public DateTime? EntryDate { get; set; }
        public bool? EntryDateChangeIndicator { get; set; }
        public int? ParticipationYear { get; set; }
        public bool? ParticipationYearChangeIndicator { get; set; }
        public DateTime? EnrollmentTerminationDate { get; set; }
        public bool? TerminationDateChangeIndicator { get; set; }
        public int? EnrollmentTerminationReasonId { get; set; }
        public bool? EnrollmentTerminationReasonIdchangeIndicator { get; set; }
        public DateTime? TransactionDateAndTime { get; set; }
        public int? CreatedBy { get; set; }
        public string EnrollmentTerminationReasonOther { get; set; }
    }
}
