﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordClassroomProgramYear
    {
        public int ParticipantRecordClassroomProgramYearId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int ClassroomProgramYearId { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? PreviousClassroomProgramYearId { get; set; }
        public DateTime? FirstDayInNewClassroom { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }

        public ClassroomProgramYear ClassroomProgramYear { get; set; }
        public UserProfile CreatedByNavigation { get; set; }
        public UserProfile ModifiedByNavigation { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
