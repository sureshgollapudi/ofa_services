﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class EducationScreeningSet
    {
        public EducationScreeningSet()
        {
            CenterProgramYear = new HashSet<CenterProgramYear>();
            EducationScreening = new HashSet<EducationScreening>();
        }

        public int EducationScreeningSetId { get; set; }
        public int GranteeId { get; set; }
        public string Name { get; set; }
        public bool? IsMaster { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<CenterProgramYear> CenterProgramYear { get; set; }
        public ICollection<EducationScreening> EducationScreening { get; set; }
    }
}
