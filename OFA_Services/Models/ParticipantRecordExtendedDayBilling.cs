﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordExtendedDayBilling
    {
        public int ParticipantRecordExtendedDayBillingId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Interval { get; set; }
        public decimal? AmountPerInterval { get; set; }
        public int? BillingType { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
