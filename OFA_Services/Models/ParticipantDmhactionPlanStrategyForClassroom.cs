﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhactionPlanStrategyForClassroom
    {
        public int ParticipantDmhactionPlanStrategyForClassroomId { get; set; }
        public int ParticipantDmhactionPlanId { get; set; }
        public int StrategyForClassroom { get; set; }

        public ParticipantDmhactionPlan ParticipantDmhactionPlan { get; set; }
    }
}
