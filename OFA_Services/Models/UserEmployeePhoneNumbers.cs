﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserEmployeePhoneNumbers
    {
        public int UserEmployeePhoneNumberId { get; set; }
        public int UserId { get; set; }
        public int? PhoneTypeId { get; set; }
        public string PhoneNumber { get; set; }

        public UserProfile User { get; set; }
    }
}
