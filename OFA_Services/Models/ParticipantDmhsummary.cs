﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhsummary
    {
        public int ParticipantDmhsummaryId { get; set; }
        public int? ParticipantId { get; set; }
        public int? DisabilitiesServicesStatus { get; set; }
        public DateTime? DisabilitiesServicesStatusLastModified { get; set; }
        public int? DisabilitiesServicesStepInProcess { get; set; }
        public DateTime? DisabilitiesServicesStepInProcessLastModified { get; set; }
        public string DisabilitiesServicesMostRecentActionTaken { get; set; }
        public DateTime? DisabilitiesServicesMostRecentActionDate { get; set; }
        public int? MentalHealthServicesStatus { get; set; }
        public DateTime? MentalHealthServicesStatusDate { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime? CreatedOn { get; set; }

        public Participant Participant { get; set; }
    }
}
