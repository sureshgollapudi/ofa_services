﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class RecentUpdate
    {
        public int RecentUpdateId { get; set; }
        public int ParticipantId { get; set; }
        public string Message { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
    }
}
