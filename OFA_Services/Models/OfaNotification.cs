﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaNotification
    {
        public OfaNotification()
        {
            OfaParticipantNotification = new HashSet<OfaParticipantNotification>();
        }

        public int NotificationId { get; set; }
        public string Name { get; set; }

        public ICollection<OfaParticipantNotification> OfaParticipantNotification { get; set; }
    }
}
