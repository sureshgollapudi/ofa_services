﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianFamilySof
    {
        public int PrimaryGuardianFamilySofid { get; set; }
        public int? PrimaryGuardianFamilyEngagementId { get; set; }
        public int? ParticipantId { get; set; }
        public int? ActivityCardId { get; set; }
        public bool? Applicable { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
