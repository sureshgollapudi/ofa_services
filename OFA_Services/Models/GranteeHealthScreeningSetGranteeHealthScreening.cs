﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeHealthScreeningSetGranteeHealthScreening
    {
        public int GranteeHealthScreeningSetGranteeHealthScreeningId { get; set; }
        public int GranteeHealthScreeningSetId { get; set; }
        public int GranteeHealthScreeningId { get; set; }

        public GranteeHealthScreening GranteeHealthScreening { get; set; }
        public GranteeHealthScreeningSet GranteeHealthScreeningSet { get; set; }
    }
}
