﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordHealthInfo
    {
        public int ParticipantRecordHealthInfoId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime? PregnancyExpectedDueDate { get; set; }
        public DateTime? PregnancyActualDeliveryDate { get; set; }
        public int? PregnancyMedicallyHighRisk { get; set; }
        public string MedicalHome { get; set; }
        public int? MedicalHomeEnrollment { get; set; }
        public int? MedicalHomeEndOfEnrollment { get; set; }
        public string DentalHome { get; set; }
        public int? DentalHomeEnrollment { get; set; }
        public int? DentalHomeEndOfEnrollment { get; set; }
        public int? HealthInsuranceEnrollment { get; set; }
        public string HealthInsuranceEnrollmentOther { get; set; }
        public int? HealthInsuranceEndOfEnrollment { get; set; }
        public string HealthInsuranceEndOfEnrollmentOther { get; set; }
        public int? MedicalServicesThroughIndianHealthServiceEnrollment { get; set; }
        public int? MedicalServicesThroughIndianHealthServiceEndOfEnrollment { get; set; }
        public int? MedicalServicesThroughMigrantCommunityHealthCenterEnrollment { get; set; }
        public int? MedicalServicesThroughMigrantCommunityHealthCenterEndOfEnrollment { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string InsuranceNo { get; set; }
        public string InsuranceProvider { get; set; }
        public int? PreventiveDental { get; set; }
        public DateTime? MedicalHomeDateofDetermination { get; set; }
        public DateTime? DentalHomeDateofDetermination { get; set; }
        public DateTime? HealthInsuranceDateofDetermination { get; set; }
        public bool? IsMedicalHomeChecked { get; set; }
        public bool? IsDentalHomeChecked { get; set; }
        public bool? IsHealthInsuranceChecked { get; set; }
        public DateTime? MedicalHomeGoalRecentDate { get; set; }
        public DateTime? DentalHomeGoalRecentDate { get; set; }
        public DateTime? HealthInsuranceGoalRecentDate { get; set; }
        public int? MedicalHomeGoalHighestBenchmark { get; set; }
        public int? DentalHomeGoalHighestBenchmark { get; set; }
        public int? HealthInsuranceGoalHighestBenchmark { get; set; }
        public bool? IsFamilyHealthInsuranceChecked { get; set; }
        public DateTime? FamilyInsuranceDateofDetermination { get; set; }
        public int? FamilyEligibleforInsurance { get; set; }
        public int? FamilyHeathInsuranceStatusatEnrollment { get; set; }
        public int? FamilyEndofEnrollment { get; set; }
        public DateTime? FamilyInsuranceGoalRecentDate { get; set; }
        public int? FamilyInsuranceGoalHighestBenchmark { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
