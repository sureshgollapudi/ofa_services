﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipant
    {
        public OfaParticipant()
        {
            OfaParticipantAdditionalMember = new HashSet<OfaParticipantAdditionalMember>();
            OfaParticipantConcern = new HashSet<OfaParticipantConcern>();
            OfaParticipantEmergencyContact = new HashSet<OfaParticipantEmergencyContact>();
            OfaParticipantFamilyServices = new HashSet<OfaParticipantFamilyServices>();
            OfaParticipantHowDidyouHearabtProgram = new HashSet<OfaParticipantHowDidyouHearabtProgram>();
            OfaParticipantNotification = new HashSet<OfaParticipantNotification>();
            OfaParticipantOtherChildCare = new HashSet<OfaParticipantOtherChildCare>();
            OfaParticipantRace = new HashSet<OfaParticipantRace>();
            OfaParticipantFileAttachment = new HashSet<OfaParticipantFileAttachment>();
    }

        public int OfaParticipantId { get; set; }
        public int ParentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Preferred { get; set; }
        public DateTime? DateofBirth { get; set; }
        public int? Gender { get; set; }
        public int? ParentalStatus { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public int GranteeId { get; set; }
        public string ZipCode { get; set; }
        public string RaceOther { get; set; }
        public int? Ethnicity { get; set; }
        public string Nationality { get; set; }
        public int? ChildPrimaryLanguage { get; set; }
        public bool? IsChildDisable { get; set; }
        public string ChildDisabilityList { get; set; }
        public string ChildDisabilityDesc { get; set; }
        public bool? IsChildIepifsp { get; set; }
        public bool? IsChildRecieveSpecialEduServices { get; set; }
        public string OtherMedicalConcern { get; set; }
        public string OtherDevelopmentConcern { get; set; }
        public string OtherBehaviorConcern { get; set; }
        public bool? IsChildPreviouslyEnrolled { get; set; }
        public string PreviousEnrolledProgram { get; set; }
        public bool? IsFamilyMemberOnStaff { get; set; }
        public bool? IsChildMotherFatherAttendEhehsprogram { get; set; }
        public bool? IsChildSiblingEnrolledinHsprogram { get; set; }
        public bool? IsSiblingCurrentlyEnrolled { get; set; }
        public DateTime? SiblingEnrolledStartDate { get; set; }
        public DateTime? SiblingEnrolledEndDate { get; set; }
        public string HowDidyouHearabtProgOtherSource { get; set; }
        public bool? IsFullDayChildCare { get; set; }
        public string FullDayChildCareReason { get; set; }
        public bool? IsChildCareSubsidy { get; set; }
        public bool? IsAppliedinthePast { get; set; }
        public bool? IsInterestedinPrivateExtendedPay { get; set; }
        public string FirstCenterChoice { get; set; }
        public int? FirstCenterChoiceProgramOption { get; set; }
        public string SecondCenterChoice { get; set; }
        public int? SecondCenterChoiceProgramOption { get; set; }
        public string ThirdCenterChoice { get; set; }
        public int? ThirdCenterChoiceProgramOption { get; set; }
        public int? TotalnoofFamilyMembers { get; set; }
        public bool? IsDyfsdcfsdhs { get; set; }
        public bool? IsTnaf { get; set; }
        public bool? IsSsi { get; set; }
        public bool? IsChildCareSubsidyVoucher { get; set; }
        public bool? IsHomeless { get; set; }
        public bool? IsLegalIssues { get; set; }
        public string LegalIssuesClarify { get; set; }
        public bool? IsdisplacedHomedue { get; set; }
        public bool? IsFosterKinshipCare { get; set; }
        public bool? IsAnyoneCourtorderRestricts { get; set; }
        public string RestrictExplaination { get; set; }
        public string RestrictPersonName { get; set; }
        public string RestrictPersonRelationship { get; set; }
        public bool? IsAnyOtherPersonPickupChild { get; set; }
        public string PickupExplaination { get; set; }
        public string PickupPersonName { get; set; }
        public string PickupPersonRelationship { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public Grantee Grantee { get; set; }
        public OfaParentRegistration Parent { get; set; }
        public State State { get; set; }
        public ICollection<OfaParticipantAdditionalMember> OfaParticipantAdditionalMember { get; set; }
        public ICollection<OfaParticipantConcern> OfaParticipantConcern { get; set; }
        public ICollection<OfaParticipantEmergencyContact> OfaParticipantEmergencyContact { get; set; }
        public ICollection<OfaParticipantFamilyServices> OfaParticipantFamilyServices { get; set; }
        public ICollection<OfaParticipantHowDidyouHearabtProgram> OfaParticipantHowDidyouHearabtProgram { get; set; }
        public ICollection<OfaParticipantNotification> OfaParticipantNotification { get; set; }
        public ICollection<OfaParticipantOtherChildCare> OfaParticipantOtherChildCare { get; set; }
        public ICollection<OfaParticipantRace> OfaParticipantRace { get; set; }
        public ICollection<OfaParticipantFileAttachment> OfaParticipantFileAttachment { get; set; }
    }
}
