﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Dmhset
    {
        public int DmhsetId { get; set; }
        public int? GranteeId { get; set; }
        public string Name { get; set; }
        public int? DisabilitiesServices { get; set; }
        public int? EvaluationDue { get; set; }
        public int? Iepifspdue { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
    }
}
