﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeHealthScreeningSet
    {
        public GranteeHealthScreeningSet()
        {
            CenterProgramYear = new HashSet<CenterProgramYear>();
            GranteeHealthScreeningSetGranteeHealthScreening = new HashSet<GranteeHealthScreeningSetGranteeHealthScreening>();
        }

        public int GranteeHealthScreeningSetId { get; set; }
        public int GranteeId { get; set; }
        public string Name { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<CenterProgramYear> CenterProgramYear { get; set; }
        public ICollection<GranteeHealthScreeningSetGranteeHealthScreening> GranteeHealthScreeningSetGranteeHealthScreening { get; set; }
    }
}
