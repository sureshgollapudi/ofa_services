﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class JobErrors
    {
        public int Id { get; set; }
        public int? ParticipantRecordId { get; set; }
        public string Description { get; set; }
        public DateTime? LoggedOn { get; set; }
        public string JobName { get; set; }
    }
}
