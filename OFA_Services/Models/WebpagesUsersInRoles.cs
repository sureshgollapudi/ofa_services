﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class WebpagesUsersInRoles
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public WebpagesRoles Role { get; set; }
        public UserProfile User { get; set; }
    }
}
