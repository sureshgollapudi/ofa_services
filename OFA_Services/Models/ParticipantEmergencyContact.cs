﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantEmergencyContact
    {
        public int ParticipantEmergencyContactId { get; set; }
        public int ParticipantFamilyProfileId { get; set; }
        public string EmergencyContactFirstName { get; set; }
        public string EmergencyContactLastName { get; set; }
        public string EmergencyContactStreetAddress { get; set; }
        public string EmergencyContactCity { get; set; }
        public string EmergencyContactState { get; set; }
        public string EmergencyContactZipcode { get; set; }
        public string EmergencyContactPhone1 { get; set; }
        public string EmergencyContactPhone2 { get; set; }
        public bool? EmergencyContactPickUpChild { get; set; }
        public string EmergencyContactPickUpNotes { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string EmergencyContactEmail { get; set; }

        public ParticipantFamilyProfile ParticipantFamilyProfile { get; set; }
    }
}
