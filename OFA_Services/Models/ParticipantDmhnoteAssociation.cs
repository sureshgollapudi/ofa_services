﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteAssociation
    {
        public int ParticipantDmhnoteAssociationId { get; set; }
        public int ParticipantDmhnoteId { get; set; }
        public int AssociationType { get; set; }
        public int ForeignKeyId { get; set; }

        public ParticipantDmhnote ParticipantDmhnote { get; set; }
    }
}
