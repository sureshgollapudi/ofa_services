﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AlertProgramType
    {
        public int AlertProgramTypeId { get; set; }
        public int AlertId { get; set; }
        public int ProgramTypeId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Alert Alert { get; set; }
        public ProgramType ProgramType { get; set; }
    }
}
