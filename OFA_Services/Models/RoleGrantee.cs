﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class RoleGrantee
    {
        public int RoleGranteeId { get; set; }
        public int GranteeId { get; set; }
        public int? RoleId { get; set; }
        public string Name { get; set; }
        public bool Immunization { get; set; }
        public bool MentalHealthAndDisabilities { get; set; }
        public bool Education { get; set; }
        public bool Health { get; set; }
        public bool FamilyInfo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Grantee Grantee { get; set; }
        public WebpagesRoles Role { get; set; }
    }
}
