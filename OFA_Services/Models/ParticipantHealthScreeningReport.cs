﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantHealthScreeningReport
    {
        public int ParticipantHealthScreeningReportId { get; set; }
        public int ParticipantId { get; set; }
        public int GranteeHealthScreeningId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? ParticipantHealthScreeningDocumentationId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public GranteeHealthScreening GranteeHealthScreening { get; set; }
        public Participant Participant { get; set; }
    }
}
