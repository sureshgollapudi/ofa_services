﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class AssessmentType
    {
        public AssessmentType()
        {
            AssessmentLevel = new HashSet<AssessmentLevel>();
            LevelMaster = new HashSet<LevelMaster>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public string DisplayName { get; set; }

        public ICollection<AssessmentLevel> AssessmentLevel { get; set; }
        public ICollection<LevelMaster> LevelMaster { get; set; }
    }
}
