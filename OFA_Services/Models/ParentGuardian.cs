﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParentGuardian
    {
        public ParentGuardian()
        {
            ParticipantParentGuardian = new HashSet<ParticipantParentGuardian>();
            PrimaryGuardianCaseNote = new HashSet<PrimaryGuardianCaseNote>();
            PrimaryGuardianFamilyEngagement = new HashSet<PrimaryGuardianFamilyEngagement>();
            PrimaryGuardianFamilyStrengthsAssessment = new HashSet<PrimaryGuardianFamilyStrengthsAssessment>();
            PrimaryGuardianGoal = new HashSet<PrimaryGuardianGoal>();
            PrimaryGuardianPirfamilyInformation = new HashSet<PrimaryGuardianPirfamilyInformation>();
        }

        public int ParentGuardianId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Gender { get; set; }
        public bool? IsMailingAddressSameAsParticipantLivingAddress { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int? PhoneNumberType { get; set; }
        public string PhoneNumberNotes { get; set; }
        public string OtherPhoneNumber { get; set; }
        public int? OtherPhoneNumberType { get; set; }
        public string OtherPhoneNumberNotes { get; set; }
        public string OtherPhoneNumber2 { get; set; }
        public int? OtherPhoneNumber2Type { get; set; }
        public string OtherPhoneNumber2Notes { get; set; }
        public string OtherPhoneNumber3 { get; set; }
        public int? OtherPhoneNumber3Type { get; set; }
        public string OtherPhoneNumber3Notes { get; set; }
        public bool? ReceivesTextMessages { get; set; }
        public int? PreferredContact { get; set; }
        public bool? HasCustody { get; set; }
        public int? EnglishLevel { get; set; }
        public int? EducationLevel { get; set; }
        public int? EmploymentStatus { get; set; }
        public int? Training { get; set; }
        public string Suffix { get; set; }
        public string LegacyFamId1Gid { get; set; }
        public string LegacyFamId1 { get; set; }
        public string LegacyFamIdname1 { get; set; }
        public string LegacyFamId2 { get; set; }
        public string LegacyFamIdname2 { get; set; }
        public int? FamPrimaryStaging { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? GranteeId { get; set; }
        public bool? PhoneNumberAvailableToReceiveTexts { get; set; }
        public bool? OtherPhoneNumberAvailableToReceiveTexts { get; set; }
        public bool? OtherPhoneNumber2AvailableToReceiveTexts { get; set; }
        public bool? OtherPhoneNumber3AvailableToReceiveTexts { get; set; }
        public int? ChildPlusId { get; set; }
        public int? ChildPlusFamilyId { get; set; }
        public DateTime? FoundationCardDate { get; set; }
        public int? PrimaryLanguageSpoken { get; set; }
        public int? PrimaryLanguageRead { get; set; }
        public int? LanguageUsedToCommunicateWithTheirChild { get; set; }
        public string PrimaryLanguageSpokenOther { get; set; }
        public string PrimaryLanguageReadOther { get; set; }
        public string LanguageUsedToCommunicateWithTheirChildOther { get; set; }
        public string OtherLanguagesSpokenOrReadOther { get; set; }

        public Grantee Grantee { get; set; }
        public ICollection<ParticipantParentGuardian> ParticipantParentGuardian { get; set; }
        public ICollection<PrimaryGuardianCaseNote> PrimaryGuardianCaseNote { get; set; }
        public ICollection<PrimaryGuardianFamilyEngagement> PrimaryGuardianFamilyEngagement { get; set; }
        public ICollection<PrimaryGuardianFamilyStrengthsAssessment> PrimaryGuardianFamilyStrengthsAssessment { get; set; }
        public ICollection<PrimaryGuardianGoal> PrimaryGuardianGoal { get; set; }
        public ICollection<PrimaryGuardianPirfamilyInformation> PrimaryGuardianPirfamilyInformation { get; set; }
    }
}
