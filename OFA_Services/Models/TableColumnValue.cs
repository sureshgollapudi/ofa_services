﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class TableColumnValue
    {
        public TableColumnValue()
        {
            DataListAccess = new HashSet<DataListAccess>();
        }

        public int TableColumnValueId { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public int ValueId { get; set; }
        public string Text { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ICollection<DataListAccess> DataListAccess { get; set; }
    }
}
