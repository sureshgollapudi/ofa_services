﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnote
    {
        public ParticipantDmhnote()
        {
            ParticipantDmhnoteAssociation = new HashSet<ParticipantDmhnoteAssociation>();
        }

        public int ParticipantDmhnoteId { get; set; }
        public int ParticipantId { get; set; }
        public int? NoteType { get; set; }
        public DateTime? NoteDate { get; set; }
        public string Description { get; set; }
        public int? CreatedByUserId { get; set; }
        public int? PirservicesReceived1 { get; set; }
        public int? PirservicesReceived2 { get; set; }
        public int? PirservicesReceived3 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? IsMentalHealth { get; set; }
        public DateTime? UserInputDate { get; set; }
        public int? DisabilitiesServicesId { get; set; }

        public UserProfile CreatedByUser { get; set; }
        public Participant Participant { get; set; }
        public ICollection<ParticipantDmhnoteAssociation> ParticipantDmhnoteAssociation { get; set; }
    }
}
