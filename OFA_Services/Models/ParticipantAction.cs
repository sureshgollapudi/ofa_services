﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantAction
    {
        public ParticipantAction()
        {
            PrimaryGuardianGoalFollowUp = new HashSet<PrimaryGuardianGoalFollowUp>();
        }

        public int ParticipantActionId { get; set; }
        public int? ParticipantFollowUpId { get; set; }
        public int? StatusId { get; set; }
        public DateTime? StatusDate { get; set; }
        public string Note { get; set; }
        public int? UserInputId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public ParticipantFollowUp ParticipantFollowUp { get; set; }
        public ICollection<PrimaryGuardianGoalFollowUp> PrimaryGuardianGoalFollowUp { get; set; }
    }
}
