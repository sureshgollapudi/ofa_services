﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class EducationScreening
    {
        public EducationScreening()
        {
            EducationScreeningPirquestion = new HashSet<EducationScreeningPirquestion>();
            ParticipantEducationScreeningDocumentation = new HashSet<ParticipantEducationScreeningDocumentation>();
        }

        public int EducationScreeningId { get; set; }
        public int EducationScreeningSetId { get; set; }
        public int? Type { get; set; }
        public int? Tool { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsRequired { get; set; }
        public bool? IsLicensingRequired { get; set; }
        public int? ChildStartAge { get; set; }
        public int? ChildStartAgeUnits { get; set; }
        public int? ChildEndAge { get; set; }
        public int? ChildEndAgeUnits { get; set; }
        public bool? IsValidityAcrossEducationSets { get; set; }
        public int? ValidityStartType { get; set; }
        public int? ValidityStartNumber { get; set; }
        public int? ValidityStartUnits { get; set; }
        public int? ValidityEndNumber { get; set; }
        public int? ValidityEndUnits { get; set; }
        public int? ValidityDateStartNumber { get; set; }
        public int? ValidityDateStartUnits { get; set; }
        public int? NextDueDateNoneValidEntryDateRule { get; set; }
        public int? NextDueDateNoneValidChildAgeRule { get; set; }
        public int? NextDueDateNoneValidChildAgeUnits { get; set; }
        public int? ExpirationBasedOnScreeningDateOrChildAge { get; set; }
        public int? NextDueDateExpirationDateRule { get; set; }
        public int? NextDueDateExpirationDateRuleUnits { get; set; }
        public bool? HasNoneValidDueDateOverride { get; set; }
        public bool? IsOnPir { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? RescreenDueDateWithinRule { get; set; }
        public int? RescreenDueDateWithinUnits { get; set; }
        public bool? NotCalculateInitialDueDate { get; set; }
        public bool? NotCalculateRescreenDate { get; set; }
        public bool? AllowRescreenDueDateOverride { get; set; }
        public bool? NotCalculateExpirationDate { get; set; }

        public EducationScreeningSet EducationScreeningSet { get; set; }
        public ICollection<EducationScreeningPirquestion> EducationScreeningPirquestion { get; set; }
        public ICollection<ParticipantEducationScreeningDocumentation> ParticipantEducationScreeningDocumentation { get; set; }
    }
}
