﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteParticipantDmhiep
    {
        public int ParticipantDmhnoteParticipantDmhiepid { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ParticipantDmhiepid { get; set; }
    }
}
