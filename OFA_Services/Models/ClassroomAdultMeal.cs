﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ClassroomAdultMeal
    {
        public int ClassroomAdultMealId { get; set; }
        public int ClassroomProgramYearId { get; set; }
        public DateTime Date { get; set; }
        public int Amsnack { get; set; }
        public int Pmsnack { get; set; }
        public int BreakFast { get; set; }
        public int Lunch { get; set; }
        public int Dinner { get; set; }
        public int UserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
    }
}
