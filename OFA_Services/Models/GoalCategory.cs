﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GoalCategory
    {
        public GoalCategory()
        {
            Goal = new HashSet<Goal>();
            GoalSubCategory = new HashSet<GoalSubCategory>();
        }

        public int GoalCategoryId { get; set; }
        public string Name { get; set; }

        public ICollection<Goal> Goal { get; set; }
        public ICollection<GoalSubCategory> GoalSubCategory { get; set; }
    }
}
