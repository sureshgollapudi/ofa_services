﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserHireInformation
    {
        public int UserHireInformationId { get; set; }
        public int UserId { get; set; }
        public DateTime? EmploymentHireDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public int? TerminationReason { get; set; }
        public string TerminationReasonOtherExplanation { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }

        public UserProfile User { get; set; }
    }
}
