﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordCaseNote
    {
        public ParticipantRecordCaseNote()
        {
            ParticipantRecordCaseNoteFollowUp = new HashSet<ParticipantRecordCaseNoteFollowUp>();
        }

        public int ParticipantRecordCaseNoteId { get; set; }
        public int ParticipantRecordId { get; set; }
        public DateTime Date { get; set; }
        public int CreatorUserId { get; set; }
        public int Type { get; set; }
        public string Note { get; set; }
        public int? PirservicesReceived1 { get; set; }
        public int? PirservicesReceived2 { get; set; }
        public int? PirservicesReceived3 { get; set; }
        public bool ClosureIndicator { get; set; }
        public int? ClosureReason { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public string ForeignNoteId { get; set; }

        public UserProfile CreatorUser { get; set; }
        public ParticipantRecord ParticipantRecord { get; set; }
        public ICollection<ParticipantRecordCaseNoteFollowUp> ParticipantRecordCaseNoteFollowUp { get; set; }
    }
}
