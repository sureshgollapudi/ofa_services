﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserProfileHistory
    {
        public int UserProfileHistoryId { get; set; }
        public int UserId { get; set; }
        public int? HighestDegreeEarned { get; set; }
        public bool? IsDirectorsCredential { get; set; }
        public string SchoolName { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Major { get; set; }
        public int? NumberofEcecredits { get; set; }
        public int? YearsofHeadStartExperience { get; set; }
        public int? YearsofChildcareDaycareExperience { get; set; }
        public string RegistryCertificateLevel { get; set; }
        public DateTime? ExpirationDateCda5 { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
