﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryGuardianPirfamilyInformation
    {
        public PrimaryGuardianPirfamilyInformation()
        {
            PrimaryGuardianPireducationOrTraining = new HashSet<PrimaryGuardianPireducationOrTraining>();
        }

        public int PrimaryGuardianPirfamilyInformationId { get; set; }
        public int PrimaryGuardianId { get; set; }
        public int SchoolYearId { get; set; }
        public int TanfatEnrollment { get; set; }
        public int WicatEnrollment { get; set; }
        public int SsiatEnrollment { get; set; }
        public int SnapatEnrollment { get; set; }
        public int TanfatEndOfEnrollment { get; set; }
        public int WicatEndOfEnrollment { get; set; }
        public int SsiatEndOfEnrollment { get; set; }
        public int SnapatEndOfEnrollment { get; set; }
        public int FamilyHomeless { get; set; }
        public int? FamilyAcquiredHome { get; set; }
        public int ReceivedChildcareAtEndOfEnrollment { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public UserProfile CreatedByNavigation { get; set; }
        public ParentGuardian PrimaryGuardian { get; set; }
        public SchoolYear SchoolYear { get; set; }
        public ICollection<PrimaryGuardianPireducationOrTraining> PrimaryGuardianPireducationOrTraining { get; set; }
    }
}
