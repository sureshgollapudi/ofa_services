﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class Pirmapping
    {
        public int PirmappingId { get; set; }
        public int SchoolYearId { get; set; }
        public string SystemColumnName { get; set; }
        public string OverrideColumnName { get; set; }
        public string TableName { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
    }
}
