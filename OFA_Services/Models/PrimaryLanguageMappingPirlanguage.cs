﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class PrimaryLanguageMappingPirlanguage
    {
        public int PrimaryLanguageMappingPirlanguageId { get; set; }
        public int PrimaryLanguageId { get; set; }
        public int PirlanguageId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
