﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhiepactionPlanStrategyForHome
    {
        public int ParticipantDmhiepactionPlanStrategyForHomeId { get; set; }
        public int ParticipantDmhiepactionPlanId { get; set; }
        public int StrategyForHome { get; set; }

        public ParticipantDmhiepactionPlan ParticipantDmhiepactionPlan { get; set; }
    }
}
