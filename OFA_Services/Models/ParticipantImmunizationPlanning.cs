﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantImmunizationPlanning
    {
        public ParticipantImmunizationPlanning()
        {
            ParticipantImmunizationPlanningHistory = new HashSet<ParticipantImmunizationPlanningHistory>();
        }

        public int ParticipantImmunizationPlanningId { get; set; }
        public int ParticipantId { get; set; }
        public string Note { get; set; }
        public DateTime? Date { get; set; }
        public int? CreatedByUserId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedBy { get; set; }

        public Participant Participant { get; set; }
        public ICollection<ParticipantImmunizationPlanningHistory> ParticipantImmunizationPlanningHistory { get; set; }
    }
}
