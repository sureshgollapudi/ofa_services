﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRace
    {
        public int ParticipantRaceId { get; set; }
        public int ParticipantId { get; set; }
        public int Race { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public Participant Participant { get; set; }
    }
}
