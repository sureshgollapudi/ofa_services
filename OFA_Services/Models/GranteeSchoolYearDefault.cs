﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class GranteeSchoolYearDefault
    {
        public int GranteeSchoolYearDefaultId { get; set; }
        public int GranteeId { get; set; }
        public int SchoolYearId { get; set; }

        public Grantee Grantee { get; set; }
        public SchoolYear SchoolYear { get; set; }
    }
}
