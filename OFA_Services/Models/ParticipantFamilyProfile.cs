﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantFamilyProfile
    {
        public ParticipantFamilyProfile()
        {
            ParticipantEmergencyContact = new HashSet<ParticipantEmergencyContact>();
        }

        public int ParticipantFamilyProfileId { get; set; }
        public int? ParticipantId { get; set; }
        public bool? EmergencyConsentEmergencyMeasures { get; set; }
        public bool? EmergencyConsentXrays { get; set; }
        public bool? EmergencyConsentTransported { get; set; }
        public bool? EmergencyConsentEvacuated { get; set; }
        public bool? EmergencyConsentContactEmergencyResourcesBeforeParents { get; set; }
        public bool? PermissionToReleaseAndRequestInformationAttachFile { get; set; }
        public bool? PermissionToReleaseAndRequestInformationFamilyContactInfo { get; set; }
        public bool? PermissionToReleaseAndRequestInformationChildsBirthCertificate { get; set; }
        public bool? PermissionToReleaseAndRequestInformationChildHealthInformation { get; set; }
        public bool? PermissionToReleaseAndRequestInformationChildDevelopmentandEducationInformation { get; set; }
        public bool? PermissionToReleaseAndRequestInformationRegardingSpecialNeeds { get; set; }
        public bool? PermissionToReleaseAndRequestInformationFamilyServices { get; set; }
        public bool? PermissionforProgramActivitiesWalkingTrips { get; set; }
        public bool? PermissionforProgramChildActivitiesPhotographed { get; set; }
        public bool? PermissionforProgramActivitiesHealthScreenings { get; set; }
        public bool? PermissionforProgramActivitiesDevelopmentalScreenings { get; set; }
        public bool? PermissionforProgramActivitiesSocialEmotionalScreenings { get; set; }
        public bool? PermissionforProgramActivitiesSpeechScreening { get; set; }
        public bool? PermissionforProgramActivitiesClassroomScreenings { get; set; }
        public bool? PermissionforProgramActivitiesUseofInterperter { get; set; }
        public bool? PermissionforProgramActivitiesCustom1 { get; set; }
        public bool? PermissionforProgramActivitiesCustom2 { get; set; }
        public bool? PermissionforProgramActivitiesCustom3 { get; set; }
        public bool? PermissionforProgramActivitiesCustom4 { get; set; }
        public bool? PermissionforProgramActivitiesCustom5 { get; set; }
        public string PermissionforProgramActivitiesCustom1Title { get; set; }
        public string PermissionforProgramActivitiesCustom2Title { get; set; }
        public string PermissionforProgramActivitiesCustom3Title { get; set; }
        public string PermissionforProgramActivitiesCustom4Title { get; set; }
        public string PermissionforProgramActivitiesCustom5Title { get; set; }
        public bool? FamilyInvolvementContractBringChildOnTime { get; set; }
        public bool? FamilyInvolvementContractReadEveryNight { get; set; }
        public bool? FamilyInvolvementContractParticipateInParentMeetingsCenterMeetingsEvents { get; set; }
        public bool? FamilyInvolvementContractVolunteer24HoursPerProgramYear { get; set; }
        public bool? FamilyInvolvementContractVolunteerCustom1 { get; set; }
        public bool? FamilyInvolvementContractVolunteerCustom2 { get; set; }
        public bool? FamilyInvolvementContractVolunteerCustom3 { get; set; }
        public string FamilyInvolvementContractVolunteerCustom1Title { get; set; }
        public string FamilyInvolvementContractVolunteerCustom2Title { get; set; }
        public string FamilyInvolvementContractVolunteerCustom3Title { get; set; }
        public bool? FamilyVolunteeringContractParentCommittee { get; set; }
        public bool? FamilyVolunteeringContractPolicyCouncil { get; set; }
        public bool? FamilyVolunteeringContractAdvisoryCommittee { get; set; }
        public bool? FamilyVolunteeringContractRegularClassroomVolunteer { get; set; }
        public bool? FamilyVolunteeringContractSpecialClassroomVolunteer { get; set; }
        public bool? FamilyVolunteeringContractVolunteerOnFieldTrips { get; set; }
        public bool? FamilyVolunteeringContractContributeSkills { get; set; }
        public string FamilyVolunteeringContractContributeSkillsNotes { get; set; }
        public string ExtendedDayInterest { get; set; }
        public string ExtendedDayHasVoucher { get; set; }
        public string ExtendedDayVoucherApplicationSubmitted { get; set; }
        public string ApplicationCustomFormNotes1 { get; set; }
        public string ApplicationCustomFormNotes2 { get; set; }
        public string ApplicationCustomFormNotes3 { get; set; }
        public string ApplicationCustomFormNotes4 { get; set; }
        public string ApplicationCustomFormNotes5 { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public bool? PermissionforProgramParentActivitiesPhotographed { get; set; }
        public bool? PermissionforActivitiesPermissionToText { get; set; }
        public DateTime? GuardianSignatureDate { get; set; }

        public Participant Participant { get; set; }
        public ICollection<ParticipantEmergencyContact> ParticipantEmergencyContact { get; set; }
    }
}
