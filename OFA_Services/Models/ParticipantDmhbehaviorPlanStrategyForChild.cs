﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhbehaviorPlanStrategyForChild
    {
        public int ParticipantDmhbehaviorPlanStrategyForChildId { get; set; }
        public int ParticipantDmhbehaviorPlanId { get; set; }
        public int StrategyForChild { get; set; }

        public ParticipantDmhbehaviorPlan ParticipantDmhbehaviorPlan { get; set; }
    }
}
