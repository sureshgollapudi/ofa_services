﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class FileHistory
    {
        public int FileHistoryId { get; set; }
        public int FileAttachmentId { get; set; }
        public int? Identifier { get; set; }
        public string RecordType { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string DisplayName { get; set; }
        public int? IdentifierType { get; set; }
        public int? Sequence { get; set; }
        public string FileAction { get; set; }
        public int ModifiedUserId { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
