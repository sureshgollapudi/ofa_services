﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationIncident
    {
        public int ParticipantRecordEducationIncidentId { get; set; }
        public int ParticipantRecordId { get; set; }
        public bool IllnessAccident { get; set; }
        public DateTime DateOfEvent { get; set; }
        public int ReferralRequired { get; set; }
        public int Location { get; set; }
        public string LocationOther { get; set; }
        public string Symptoms { get; set; }
        public string SymptomsOther { get; set; }
        public int? WitnessTeacher1 { get; set; }
        public int? WitnessTeacher2 { get; set; }
        public string WitnessOther1 { get; set; }
        public string WitnessOther2 { get; set; }
        public string Description { get; set; }
        public string ActionTaken { get; set; }
        public int User { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? ParentNotificationVia { get; set; }
        public string ParentNotificationViaOther { get; set; }
        public DateTime? ParentNotificationDateTime { get; set; }
        public int? AdministrationNotified { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
