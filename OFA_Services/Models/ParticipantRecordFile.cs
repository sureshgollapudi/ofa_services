﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordFile
    {
        public int ParticipantRecordFileId { get; set; }
        public int Idtype { get; set; }
        public int Type { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }

        public UserProfile CreatorUser { get; set; }
    }
}
