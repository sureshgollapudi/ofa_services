﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantRecordEducationHomeBasedVisits
    {
        public int ParticipantRecordEducationHomeBasedVisitsId { get; set; }
        public int ParticipantRecordId { get; set; }
        public int PlanningHomeBasedVisitStatus { get; set; }
        public DateTime PlanningHomeBasedVisitAttemptDate { get; set; }
        public int PlanningHomeBasedVisitAttemptReason { get; set; }
        public string PlanningHomeBasedVisitNotes { get; set; }
        public int User { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedBy { get; set; }

        public ParticipantRecord ParticipantRecord { get; set; }
    }
}
