﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class DmhsummaryAssociatedConcern
    {
        public int DmhsummaryAssociatedConcernId { get; set; }
        public int DmhsummaryPrimaryResultId { get; set; }
        public int? AssociateConcernId { get; set; }
        public int? ConcernId { get; set; }

        public DmhsummaryPrimaryResult DmhsummaryPrimaryResult { get; set; }
    }
}
