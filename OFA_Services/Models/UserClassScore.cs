﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserClassScore
    {
        public int UserClassScoreId { get; set; }
        public int UserId { get; set; }
        public int ClassScoreType { get; set; }
        public decimal? Esscore { get; set; }
        public DateTime? EsevaluationDate { get; set; }
        public string EsobservationCompletedBy { get; set; }
    }
}
