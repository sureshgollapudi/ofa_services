﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhnoteParticipantDmhiepactionPlan
    {
        public int ParticipantDmhnoteParticipantDmhiepactionPlanId { get; set; }
        public int? ParticipantDmhnoteId { get; set; }
        public int? DmhprocessDocumentationId { get; set; }
        public int? ParticipantDmhiepactionPlanId { get; set; }
    }
}
