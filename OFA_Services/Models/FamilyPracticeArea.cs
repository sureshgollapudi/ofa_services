﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class FamilyPracticeArea
    {
        public FamilyPracticeArea()
        {
            ActivityCard = new HashSet<ActivityCard>();
        }

        public int FamilyPracticeAreaId { get; set; }
        public string FamilyPracticeAreaText { get; set; }

        public ICollection<ActivityCard> ActivityCard { get; set; }
    }
}
