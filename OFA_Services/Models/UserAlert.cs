﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserAlert
    {
        public int UserAlertId { get; set; }
        public int UserId { get; set; }
        public int Alert { get; set; }

        public UserProfile User { get; set; }
    }
}
