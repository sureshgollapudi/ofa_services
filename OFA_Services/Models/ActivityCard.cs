﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ActivityCard
    {
        public int ActivityCardId { get; set; }
        public int FamilyPracticeAreaId { get; set; }
        public string CardDescription { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public int? ThemeId { get; set; }
        public int? CardType { get; set; }

        public FamilyPracticeArea FamilyPracticeArea { get; set; }
    }
}
