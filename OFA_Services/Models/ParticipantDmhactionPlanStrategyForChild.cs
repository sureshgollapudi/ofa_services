﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantDmhactionPlanStrategyForChild
    {
        public int ParticipantDmhactionPlanStrategyForChildId { get; set; }
        public int ParticipantDmhactionPlanId { get; set; }
        public int StrategyForChild { get; set; }

        public ParticipantDmhactionPlan ParticipantDmhactionPlan { get; set; }
    }
}
