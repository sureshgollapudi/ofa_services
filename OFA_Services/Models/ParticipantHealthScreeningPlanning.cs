﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantHealthScreeningPlanning
    {
        public int ParticipantHealthScreeningPlanningId { get; set; }
        public int ParticipantId { get; set; }
        public int GranteeHealthScreeningId { get; set; }
        public DateTime? Date { get; set; }
        public int? Status { get; set; }
        public DateTime? AttemptDate { get; set; }
        public int? AttemptReason { get; set; }
        public int? PlanningUserId { get; set; }
        public string Note { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? LastModifiedBy { get; set; }

        public UserProfile PlanningUser { get; set; }
    }
}
