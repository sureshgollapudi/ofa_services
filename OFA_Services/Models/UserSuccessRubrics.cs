﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserSuccessRubrics
    {
        public int UserSuccessRubricsId { get; set; }
        public int UserId { get; set; }
        public string RubricType { get; set; }
        public DateTime? EvaluationDate { get; set; }

        public UserProfile User { get; set; }
    }
}
