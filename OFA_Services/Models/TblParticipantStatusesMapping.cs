﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class TblParticipantStatusesMapping
    {
        public int ValueId { get; set; }
        public string StatusName { get; set; }
        public bool StatusRollOverSection4 { get; set; }
        public bool StatusRollOverSection8 { get; set; }
    }
}
