﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class ParticipantEducationScreeningDocumentation
    {
        public ParticipantEducationScreeningDocumentation()
        {
            ParticipantInternalReferral = new HashSet<ParticipantInternalReferral>();
        }

        public int ParticipantEducationScreeningDocumentationId { get; set; }
        public int ParticipantId { get; set; }
        public int EducationScreeningId { get; set; }
        public int? ProgramTypeId { get; set; }
        public int? ScreenerId { get; set; }
        public bool? IsRescreen { get; set; }
        public bool? IsRefused { get; set; }
        public DateTime? RefusedDate { get; set; }
        public bool? IsExempt { get; set; }
        public DateTime? ExemptDate { get; set; }
        public DateTime? ScreeningDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? CompletionStatus { get; set; }
        public string Results { get; set; }
        public int? ResultsStatus { get; set; }
        public DateTime? RescreenDueDate { get; set; }
        public string Notes { get; set; }
        public int? NotificationMethod { get; set; }
        public string NotificationMethodOther { get; set; }
        public DateTime? NotificationDate { get; set; }
        public int? ScreeningDocumentationUserId { get; set; }
        public DateTime? UserInputDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModified { get; set; }
        public int? CreatedBy { get; set; }

        public EducationScreening EducationScreening { get; set; }
        public Participant Participant { get; set; }
        public ProgramType ProgramType { get; set; }
        public UserProfile Screener { get; set; }
        public UserProfile ScreeningDocumentationUser { get; set; }
        public ICollection<ParticipantInternalReferral> ParticipantInternalReferral { get; set; }
    }
}
