﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class FundedEnrollmentOption
    {
        public int FundedEnrollmentOptionId { get; set; }
        public string Name { get; set; }
    }
}
