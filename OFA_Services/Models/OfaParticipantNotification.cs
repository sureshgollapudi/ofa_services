﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipantNotification
    {
        public int ParticipantNotificationId { get; set; }
        public int OfaParticipantId { get; set; }
        public int NotificationId { get; set; }
        public int? Step { get; set; }
        public bool? Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaNotification Notification { get; set; }
        public OfaParticipant OfaParticipant { get; set; }
    }
}
