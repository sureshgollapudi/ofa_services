﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserTab
    {
        public int UserTabId { get; set; }
        public int UserId { get; set; }
        public int Tab { get; set; }

        public UserProfile User { get; set; }
    }
}
