﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class OfaParticipantAdditionalMember
    {
        public int ParticipantAdditionalMemberId { get; set; }
        public int OfaParticipantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateofBirth { get; set; }
        public string RealtionshiptoApplicant { get; set; }
        public string PhoneNumber { get; set; }
        public int? PhoneTypeId { get; set; }
        public int CreatedBy { get; set; }
        public int? LastModifiedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }

        public OfaParticipant OfaParticipant { get; set; }
    }
}
