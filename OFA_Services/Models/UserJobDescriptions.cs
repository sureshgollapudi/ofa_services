﻿using System;
using System.Collections.Generic;

namespace OFA_Services.Models
{
    public partial class UserJobDescriptions
    {
        public int UserJobDescriptionId { get; set; }
        public int UserId { get; set; }
        public DateTime? Date { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }

        public UserProfile User { get; set; }
    }
}
