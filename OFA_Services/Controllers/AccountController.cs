﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OFA_Services.Models;
using OFA_Services.UnitOfWork;
using OFA_Services.Helpers;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Text;
using OFA_Services.Services;
using OFA_Services.Entities;
using OFA_Services.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace OFA_Services.Controllers
{

    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private readonly AceleroContext _context;
        private readonly IUserServices _IUserServices;
        private readonly IUnitOfWork _IUnitOfWork;
        public AccountController(AceleroContext context, IUserServices IUserServices, IOptions<AppSettings> appSettings)
        {
            _IUserServices = IUserServices;
            _context = context;
            _appSettings = appSettings.Value;
        }

        //GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "From Test to Test2" };
        }

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody]OfaParentRegistration user)
        {
            OfaParentRegistration ObjUser = new OfaParentRegistration();
            if (ModelState.IsValid)
            {
                ObjUser = _IUserServices.Authenticate(user.GranteeId, user.UserName,user.ParentId, user.Password, _appSettings.Secret);
            }
            return Ok(new
            {
                status = ObjUser != null ? 1 : 0,
                username = ObjUser != null ? ObjUser.FirstName + ' ' + ObjUser.LastName : "",
                token = ObjUser != null ? ObjUser.Token : null,
                parentid = ObjUser != null ? ObjUser.ParentId : 0
            });

        }

        [HttpPost]
        [Route("Register")]
        public IActionResult Register(OfaParentRegistration user)
        {
            string grrr = Convert.ToString(user.GranteeId);
            OfaParentRegistration ObjUserValidate = new OfaParentRegistration();
            List<OfaParentPhoneNumber> objnn = new List<OfaParentPhoneNumber>();
            try
            {
                string userEmail = user.Email != null ? user.Email : "";
                string parentPhoneNumber = "";
                foreach (var iItem in user.OfaParentPhoneNumber)
                {
                    iItem.CreatedOn = DateTime.Now;
                    iItem.PhoneTypeId = 1;
                    iItem.PhoneNumber = Regex.Replace(iItem.PhoneNumber, "[^a-zA-Z0-9_]+", "");
                    parentPhoneNumber = iItem.PhoneNumber;
                }

                if ((_IUserServices.GetAll().Where(spt => spt.UserName.ToLower().Trim() == user.UserName.ToLower().Trim() 
                                    && spt.GranteeId == user.GranteeId).Count() > 0) || 
                         (_IUserServices.GetAll().Where(spt => spt.Email == userEmail && userEmail != "" && spt.GranteeId == user.GranteeId ).Count() > 0 ))                                
                {

                    bool username = false;
                    bool email = false;
                    string ValidationMsg = "";

                    if (_IUserServices.GetAll().Where(spt => spt.UserName == user.UserName.ToLower().Trim() && spt.GranteeId == user.GranteeId).Count() > 0)
                    {
                        username = true;                      
                    }
                    if (_IUserServices.GetAll().Where(spt => spt.Email == userEmail && userEmail != "" && spt.GranteeId == user.GranteeId).Count() > 0)
                    {
                        email = true;                        
                    }                       
                    
                        if (username == true && email == false)
                        {
                        ValidationMsg = "User exist with same Username";
                           
                        }
                        if (username == false && email == true)
                        {
                        ValidationMsg = "User exist with same Email";
                         
                        }
                       if (username == true && email == true)
                         {

                        ValidationMsg = "User exist with same Username and Email";
                            
                        }

                          return Ok(new{status = ValidationMsg});


                }
                else
                {
                    OfaParentRegistration ObjUser = new OfaParentRegistration();
                    if (user.ParentId != null)
                    {
                        ObjUser = _IUserServices.Create(user);
                        if (ObjUser != null && ObjUser.Email != null)
                        {
                            SendMail(ObjUser, "shine-password@acelero.net", user.Email, "User Created", "Welcome user creation");
                        }
                    }
                    return Ok(new
                    {
                        status = ObjUser != null ? 1 : 0,
                        username = ObjUser != null ? ObjUser.FirstName + ' ' + ObjUser.LastName : "",
                        parentid = ObjUser.ParentId,
                        token = ObjUser != null ? ObjUser.Token : null
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost]
        [Route("Update")]
        public IActionResult Update(OfaParentRegistration user)
        {
            OfaParentRegistration ObjUserValidate = new OfaParentRegistration();
            List<OfaParentPhoneNumber> objnn = new List<OfaParentPhoneNumber>();
            try
            {
                bool IsExistPhone = false;
                string ValidationMsg = "";
                foreach (var iItem in user.OfaParentPhoneNumber.ToList())
                {
                    if (iItem.PhoneNumber != "" && iItem.PhoneNumber != " ")
                    {
                        iItem.PhoneNumber = Regex.Replace(iItem.PhoneNumber, "[^a-zA-Z0-9_]+", "");

                        if (_IUserServices.GetAllPhones().Where(obj => obj.ParentId != user.ParentId && obj.PhoneNumber == iItem.PhoneNumber).Count() >= 1)
                        {
                            IsExistPhone = true;
                            
                        }
                        if (iItem.PhoneNumber == "")
                        {
                            objnn.Remove(iItem);
                        }

                        if (iItem.ParentPhoneNumberId != 0)
                        {
                            iItem.LastModifiedOn = DateTime.Now;
                            iItem.LastModifiedBy = user.ParentId;
                        }

                        else
                        {
                            iItem.CreatedOn = DateTime.Now;
                            iItem.CreatedBy = user.ParentId;
                        }
                        iItem.PhoneTypeId = iItem.PhoneTypeId;
                        iItem.PhoneNumber = Regex.Replace(iItem.PhoneNumber, "[^a-zA-Z0-9_]+", "");
                    }
                    else
                    {
                        user.OfaParentPhoneNumber.Remove(iItem);
                    }
                 }
                OfaParentRegistration parentuser = _IUserServices.GetAll().Where(spt => spt.ParentId == user.ParentId && spt.GranteeId == user.GranteeId).FirstOrDefault();
                   bool username = false;
                    bool email = false;
                if (parentuser.UserName.ToLower().Trim() == user.UserName.ToLower().Trim() || parentuser.UserName.ToLower().Trim() != user.UserName.ToLower().Trim() || parentuser.Email != user.Email || IsExistPhone == true)
                {                 

                    username = (_IUserServices.GetAll().Where(spt => spt.ParentId != user.ParentId && spt.UserName.ToLower().Trim() == user.UserName.ToLower().Trim() && spt.GranteeId == user.GranteeId).Count() >= 1) ? true : false;
                    email = (_IUserServices.GetAll().Where(spt => spt.ParentId != user.ParentId && spt.Email == user.Email && spt.Email != "" && spt.GranteeId == user.GranteeId).Count() >= 1) ? true : false;
                    ValidationMsg = (username == true && email == false) ? "User exist with same Username" : (username == false && email == true) ? "User exist with same Email" : (username == true && email == true)
                                                                         ? "User exist with same Username and Email" : "";
                }

                if (ValidationMsg != "")
                {
                    return Ok(new
                    {
                        status = ValidationMsg
                    });

                }               
                else
                {
                    if (user.newPassword != "")
                    {
                       if(CurrentPasswordValidation(user) != null)
                        {
                            if(user.Password != user.newPassword)
                            {
                                byte[] passwordHash, passwordSalt;
                                CreatePasswordHash(user.newPassword, out passwordHash, out passwordSalt);
                                parentuser.PasswordHash = passwordHash;
                                parentuser.PasswordSalt = passwordSalt;
                            }
                            else
                            {
                                ValidationMsg = "Both Password can't be same";
                                return Ok(new
                                {
                                    status = ValidationMsg
                                });
                            }
                            
                        }
                        else
                        {
                            ValidationMsg = "Incorrect password";
                            return Ok(new
                            {
                                status = ValidationMsg
                            });
                        }
                       
                    }
                    parentuser.FirstName = user.FirstName.Trim();
                    parentuser.LastName = user.LastName.Trim();
                    parentuser.DateofBirth = user.DateofBirth;
                    parentuser.Email = user.Email.Trim();
                    parentuser.ZipCode = user.ZipCode.Trim();
                    parentuser.OfaParentPhoneNumber = user.OfaParentPhoneNumber;//.Where(a => a.PhoneNumber != "");
                    parentuser.LastModifiedBy = user.ParentId;
                    parentuser.LastModifiedOn = DateTime.Now;
                    parentuser.UserName = user.UserName.TrimEnd().TrimStart();
                    DeletePhone(user.ParentId, user.OfaParentPhoneNumber);
                    _context.Update(parentuser);
                    _context.SaveChanges();
                    return Ok(new
                    {
                        status = parentuser != null ? 1 : 0,
                        username = parentuser != null ? parentuser.FirstName + ' ' + parentuser.LastName : "",
                        parentid = parentuser.ParentId,
                        token = parentuser != null ? parentuser.Token : null
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        public bool DeletePhone(int ParentId, ICollection<OfaParentPhoneNumber> phones)
        {
            bool success = false;
            try
            {
                var result = _IUserServices.GetByIdForPhone(ParentId).ToList().Where(p => phones.All(p2 => p2.ParentPhoneNumberId != p.ParentPhoneNumberId));
                foreach (var iItem in result)
                {
                    _context.Remove(iItem);
                    _context.SaveChanges();
                    success = true;
                }                
            }
            catch (Exception ex)
            {
                success = false;
            }
            return success;
        }

        public OfaParentRegistration CurrentPasswordValidation([FromBody]OfaParentRegistration user)
        {
            OfaParentRegistration ObjUser = new OfaParentRegistration();
            ObjUser = _IUserServices.Authenticate(user.GranteeId, user.UserName,user.ParentId , user.Password, _appSettings.Secret);
            return ObjUser;

        }
        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        public static void SendMail(OfaParentRegistration user, string from, string to, string subject, string body)
        {
            string smtpUsername = "shine-password@acelero.net";
            string smtpPassword = "p4ssw0rd!";
            string emailBody;
            try
            {
                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(from);
                // Recipient e-mail address.
                Msg.To.Add(to);
                Msg.Subject = subject;
                StringBuilder MyStringBuilder = new StringBuilder(body);
                emailBody = string.Format("<div><h1>New user has been created with </h1><br /></div>" + " First Name : " + user.FirstName + ' ' + " Last Name : " + user.LastName + "\n\t<br />" + "Email : " + user.Email + "\n\t<br />");
                Msg.Body = emailBody;
                Msg.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                smtp.EnableSsl = true;
                smtp.Send(Msg);
            }
            catch (Exception ex)
            {

            }

        }
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _IUserServices.GetById(id);
            return Ok(user);
        }

    }
}