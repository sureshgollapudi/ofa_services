﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OFA_Services.Helpers;
using OFA_Services.Models;
using OFA_Services.Repositories;
using OFA_Services.UnitOfWork;

namespace OFA_Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly AppSettings _appSettings;
        private AceleroContext _context;
        private readonly IUnitOfWork _IUnitOfWork;
        public ValuesController(AceleroContext context, IUnitOfWork IUnitOfWork, IOptions<AppSettings> appSettings)
        {
            _IUnitOfWork = IUnitOfWork;
            _context = context;
            _appSettings = appSettings.Value;
        }

       


        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2 Test" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        //[HttpPost]
        //public async Task<string> DeptSave(Department department)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        var userTb = await _context.OfaParticipant.SingleOrDefaultAsync(m => m.UserId == id);
        //        await _context.OfaParticipant.AddAsync(department);
        //        await _context.SaveChangesAsync();
        //    }

        //    return "Saved";
        //}

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
