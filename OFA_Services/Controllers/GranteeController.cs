﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OFA_Services.Models;
using OFA_Services.UnitOfWork;
using OFA_Services.Helpers;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Text;
using OFA_Services.Services;
using OFA_Services.Entities;
using OFA_Services.Models;


namespace OFA_Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GranteeController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private AceleroContext _context;
        private readonly IGranteeServices _IGranteeServices;
        private readonly IUnitOfWork _IUnitOfWork;
        private readonly IStatesServices _IStatesServices;
        public GranteeController(AceleroContext context, IGranteeServices IGranteeServices, IOptions<AppSettings> appSettings)
        {
            _IGranteeServices = IGranteeServices;
            _context = context;
            _appSettings = appSettings.Value;
        }


        [AllowAnonymous]
        [HttpPost("GetGranteeData")]
        public IActionResult GetGranteeData([FromBody]OfaGranteeTableData user)
        {
            Int32 GranteeId = 0;
            if (user.Urlpath.ToString().Contains("/"))
            {
                var idx = user.Urlpath.ToString().IndexOf("/");
                GranteeId = Convert.ToInt32(user.Urlpath.ToString().Remove(idx, 1));
            }
            OfaGranteeTableData GranteeObj = new OfaGranteeTableData();
            try
            {
                GranteeObj = _IGranteeServices.GetGranteeData(GranteeId);
            }
            catch (Exception ex)
            {
            }
            return Ok(new
            {
                status = GranteeObj != null ? 1 : 0,
                granteeid = GranteeObj != null ? GranteeObj.GranteeId : 0,
                logopath = GranteeObj != null ? String.Format("{0}/{1}", GranteeObj.GranteeId, GranteeObj.LogoPath) : "",
                landpageheader = GranteeObj != null ? GranteeObj.LandpageHeader : "",
                landpagecontent = GranteeObj != null ? GranteeObj.LandpageContent : "",
                landpagesubpoint1 = GranteeObj != null ? GranteeObj.Landpagesubpoint1 : "",
                landpagesubpoint2 = GranteeObj != null ? GranteeObj.Landpagesubpoint2 : "",
                landpagesubpoint3 = GranteeObj != null ? GranteeObj.Landpagesubpoint3 : "",
                landingbackgroundpath = GranteeObj != null ? String.Format("{0}/{1}", GranteeId, GranteeObj.LandingBackgroundPath) : "",
            });
        }

        [HttpGet]
        [Route("GetStates")]
        public IActionResult GetStates()
        {
            IEnumerable<State> stateslist = _IStatesServices.GetAll();
            return Ok(stateslist);
        }
    }
}