﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OFA_Services.Models;
using OFA_Services.UnitOfWork;
using OFA_Services.Helpers;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Text;
using OFA_Services.Services;
using OFA_Services.Entities;
using OFA_Services.Models;
using System.Security.Cryptography;
using System.IO;

namespace OFA_Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParentProfileController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private AceleroContext _context;
        private readonly IParentServices _IParentServices;
        private readonly IUserServices _IUserServices;
        private readonly IUnitOfWork _IUnitOfWork;
        public ParentProfileController(AceleroContext context, IUserServices IUserServices, IParentServices IParentServices, IOptions<AppSettings> appSettings)
        {
            _IParentServices = IParentServices;
            _IUserServices = IUserServices;
            _context = context;
            _appSettings = appSettings.Value;
        }
        [HttpPost]
        [Route("SaveParentProfile")]
        public IActionResult SaveParentProfile(OfaParentRegistration ofaparentregistration)
        {
            OfaParentRegistration ObjUserprofile = new OfaParentRegistration();
            try
            {
                if (_IParentServices.GetAll().Where(spt => spt.ParentId == ofaparentregistration.ParentId).Count() == 0)
                {
                    ObjUserprofile = _IParentServices.Create(ofaparentregistration);
                }
                else
                {
                    ObjUserprofile = _IParentServices.Update(ofaparentregistration);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            return Ok(new
            {
                status = ObjUserprofile != null ? 1 : 0,
            });
        }

        [AllowAnonymous]
        [HttpPost("GetParentProfile")]
        public IActionResult GetParentProfile([FromBody]OfaParentRegistration obj)
        {
            OfaParentRegistration ParentObj = new OfaParentRegistration();
            try
            {
                ParentObj = _IParentServices.GetAll().Where(spt => spt.ParentId == obj.ParentId).FirstOrDefault();
                ParentObj.OfaParentPhoneNumber = _IUserServices.GetByIdForPhone(obj.ParentId).ToList();
                // ParentObj.OfaParentPhoneNumber = _IUserServices.GetByIdForPhone(obj.ParentId).OrderByDescending(s => s.IsPrimaryPhone).ToList();              
            }
            catch (Exception ex)
            {

            }
            return Ok(new
            {
                status = ParentObj != null ? 1 : 0,
                parentId = ParentObj != null ? ParentObj.ParentId : 0,
                granteeId = ParentObj != null ? ParentObj.GranteeId : 0,
                firstName = ParentObj != null ? ParentObj.FirstName : "",
                lastName = ParentObj != null ? ParentObj.LastName : "",
                dateofBirth = ParentObj.DateofBirth,
                type = ParentObj != null ? ParentObj.Type : 0,
                zipCode = ParentObj != null ? ParentObj.ZipCode : "",
                email = ParentObj != null ? ParentObj.Email : "",
                username = ParentObj != null ? ParentObj.UserName : "",
                firstNamelastName = ParentObj != null ? ParentObj.FirstName+' '+ParentObj.LastName: "",
                Password =  "",
                ofaparentphonenumber = ParentObj.OfaParentPhoneNumber,
            });
        }
        public static string DecryptString(string cipherText, string keyString)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }
        [AllowAnonymous]
        [HttpGet("GetStates")]
        public IEnumerable<State> GetStates()
        {
            List<State> lststates = new List<State>();
            lststates = _IParentServices.GetAllStates().AsEnumerable().Select(dr => new State { value = Convert.ToString(dr.StateId), label = dr.StateName, StateAbbreviations = dr.StateAbbreviations, StateName = dr.StateName }).ToList();
            return lststates.OrderBy(s => s.StateName); 
        }
       
    }
}