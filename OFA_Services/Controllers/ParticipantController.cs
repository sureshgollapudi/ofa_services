﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OFA_Services.Models;
using OFA_Services.UnitOfWork;
using OFA_Services.Helpers;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Text;
using OFA_Services.Services;
using OFA_Services.Entities;
using OFA_Services.Models;
using Microsoft.AspNetCore.Hosting;

namespace OFA_Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParticipantController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private AceleroContext _context;
        private readonly IStatesServices _IStatesServices;
        private readonly IUnitOfWork _IUnitOfWork;
        private readonly IParticipantServices _IParticipantServices;
        private readonly ITableColumnValueServices _ITableColumnValueServices;
        private readonly IFileUploadServices _IFileUploadServices;
        private readonly IHostingEnvironment _hostingEnvironment;
        public ParticipantController(AceleroContext context, IParticipantServices IParticipantServices,
            IFileUploadServices IFileUploadServices, 
            ITableColumnValueServices ITableColumnValueServices,
            IHostingEnvironment IHostingEnvironment,
            IOptions<AppSettings> appSettings)
       {
            _IParticipantServices = IParticipantServices;
            _IFileUploadServices = IFileUploadServices;
            _ITableColumnValueServices = ITableColumnValueServices;
            _context = context;
            _appSettings = appSettings.Value;
            _hostingEnvironment = IHostingEnvironment;
        }
        
        [HttpPost]
        [Route("SaveParticipantProfile")]
        public IActionResult SaveParticipantProfile(OfaParticipant ofaParticipant)
        {
            OfaParticipant ofaParticipantdata = new OfaParticipant();
            OfaParticipantFileAttachment ofaParticipantFileAttachment = new OfaParticipantFileAttachment();
            OfaParticipantFileAttachment ofaParticipantFileAttachmentdata = new OfaParticipantFileAttachment();
            try
            {
                if (_IParticipantServices.GetAll().Where(p => p.ParentId == ofaParticipant.ParentId && p.DateofBirth == ofaParticipant.DateofBirth && p.LastName == ofaParticipant.LastName.ToLower().Trim() && p.FirstName == ofaParticipant.FirstName.ToLower().Trim() && p.GranteeId == ofaParticipant.GranteeId).Count() == 0)
                {
                    //Create Participant
                    ofaParticipantdata = _IParticipantServices.Create(ofaParticipant);
                    bool hasfileAttachment = _IParticipantServices.GetParticipantFileAttachmentbyParticipantId(ofaParticipantdata.OfaParticipantId) == null;

                    //Create Participant file attachment
                    if (hasfileAttachment)
                    {
                        //Upload file
                        FileUpload file = new FileUpload();
                        file.Base64String = ofaParticipant.OfaParticipantFileAttachment.Select(x => x.Base64String).FirstOrDefault().Split(';')[1].Split(',')[1]; ;
                        file.FileExtension = "." + ofaParticipant.OfaParticipantFileAttachment.Select(x => x.Base64String).FirstOrDefault().Split(';')[0].Split('/')[1];
                        file.FileName = "participant_profileImage_" + ofaParticipant.GranteeId + "_" + ofaParticipantdata.OfaParticipantId+file.FileExtension;
                        file.FilePath = "C:/inetpub/wwwroot/OFA_Server_Code/OFA_Services/uploadFiles/" + ofaParticipantdata.OfaParticipantId + "/";

                        int status = _IFileUploadServices.Create(file);
                            
                        if (status == 1)
                        {
                            //Create File attachment
                            ofaParticipantFileAttachment.OfaParticipantId = ofaParticipantdata.OfaParticipantId;
                            ofaParticipantFileAttachment.DocumentTypeId = 
                            _ITableColumnValueServices.GetByTablenameColumnname("OFA_Participant", "OFA_DocumentType")
                                                                            .Where(x => x.Text == "ParticipantPhoto")
                                                                            .Select(x => x.ValueId).FirstOrDefault();
                            ofaParticipantFileAttachment.FileName = file.FileName;
                            ofaParticipantFileAttachment.FilePath = file.FilePath;
                            ofaParticipantFileAttachment.CreatedBy = ofaParticipant.CreatedBy;
                            ofaParticipantFileAttachment.CreatedOn = DateTime.Now;
                            ofaParticipantFileAttachmentdata = _IParticipantServices.CreateParticipantFileAttachment(ofaParticipantFileAttachment);
                        }
                        else
                        {
                            return Ok(new
                            {
                                status = "Could't upload file for the particiapnt",
                            });
                        }
                        return Ok(new
                        {
                            status = ofaParticipantdata != null ? 1 : 0,
                            participantid = ofaParticipantdata.OfaParticipantId != 0 ? ofaParticipantdata.OfaParticipantId : 0,
                            participantname = ofaParticipant.FirstName +' '+ ofaParticipant.LastName,
                            FileAttachmentId = ofaParticipantFileAttachmentdata.FileAttachmentId
                        });
                    }

                    else
                    {
                        return Ok(new
                        {
                            status = "Could't update file attacment for the participant",
                        });
                    }
                }
                else
                {
                    return Ok(new
                    {
                        status = "Participant already exist",
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }

        }
        [HttpPost]
        [Route("GetParticipant")]
        public List<OfaParticipant> GetParticipant(OfaParticipant ofaParticipan)
        {
            List <OfaParticipant> ofaParticipant = new List<OfaParticipant>();           
            ofaParticipant = _IParticipantServices.GetByParentId(ofaParticipan.ParentId).ToList();
            List<OfaParticipantFileAttachment> files = _context.OfaParticipantFileAttachment.ToList();
            foreach (OfaParticipant p in ofaParticipant)
            {
                OfaParticipantFileAttachment f = files.Where(x => x.OfaParticipantId == p.OfaParticipantId).FirstOrDefault();
                if (f != null)
                {
                    f.FilePath = f.FilePath.Split("uploadFiles")[1];

                    p.OfaParticipantFileAttachment.Add(f);
                }
            }
            return ofaParticipant;
        }

        [HttpGet]
        [Route("GetParticipantTest")]
        public List<OfaParticipant> GetParticipantTest()
        {
            int ParentId = 289;
            List<OfaParticipant> ofaParticipant = new List<OfaParticipant>();
            ofaParticipant = _IParticipantServices.GetByParentId(ParentId).ToList();
            List<OfaParticipantFileAttachment> files = _context.OfaParticipantFileAttachment.ToList();
            foreach (OfaParticipant p in ofaParticipant)
            {
                OfaParticipantFileAttachment f = files.Where(x => x.OfaParticipantId == p.OfaParticipantId).FirstOrDefault();               
                p.OfaParticipantFileAttachment.Add(f);
            }
            return ofaParticipant;
        }
    }
}