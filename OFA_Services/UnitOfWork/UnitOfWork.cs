﻿
#region Using Namespaces...

using System;
using System.Collections.Generic;
using System.Diagnostics;
using OFA_Services.Repositories;
using OFA_Services.Models;

#endregion
namespace OFA_Services.UnitOfWork
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        #region Private member variables
        private AceleroContext _context = null;

        private GenericRepository<OfaParentRegistration> _userRepository;
        //private GenericRepository<OfaParentGuardianProfile> _userparentguardianRepository;
        private GenericRepository<OfaParentRegistration> _userparentguardianRepository;
        private GenericRepository<OfaPhoneType> _phonetypeRepository;
        private GenericRepository<OfaParentPhoneNumber> _phonenumbertypeRepository;
        private GenericRepository<OfaGranteeTableData> _granteeRepository;
        private GenericRepository<TableColumnValue> _tablecolumnRepository;
        private GenericRepository<State> _stateRepository;
        private GenericRepository<OfaParticipant> _participantRepository;
        private GenericRepository<OfaParticipantFileAttachment> _ParticipantFileAttachmentRepository;
        // private GenericRepository<OfaParentRegistration> _userparentguardianRepository;
        #endregion
        public UnitOfWork()
        {
            _context = new AceleroContext();
        }

        #region Public Repository Creation properties...
        /// <summary>
        /// Get/Set Property for product repository.
        /// </summary>
        public GenericRepository<OfaParentRegistration> userRepository
        {
            get
            {
                if (this._userRepository == null)
                    this._userRepository = new GenericRepository<OfaParentRegistration>(_context);
                return _userRepository;
            }
        }
        public GenericRepository<OfaParticipant> participantRepository
        {
            get
            {
                if (this._participantRepository == null)
                    this._participantRepository = new GenericRepository<OfaParticipant>(_context);
                return _participantRepository;
            }
        }
        public GenericRepository<OfaParticipantFileAttachment> ParticipantFileAttachmentRepository
        {
            get
            {
                if (this._ParticipantFileAttachmentRepository == null)
                    this._ParticipantFileAttachmentRepository = new GenericRepository<OfaParticipantFileAttachment>(_context);
                return _ParticipantFileAttachmentRepository;
            }
        }
        //public GenericRepository<OfaParentGuardianProfile> userparentguardianRepository
        //{
        //    get
        //    {
        //        if (this._userparentguardianRepository == null)
        //            this._userparentguardianRepository = new GenericRepository<OfaParentGuardianProfile>(_context);
        //        return _userparentguardianRepository;
        //    }
        //}
        public GenericRepository<OfaParentRegistration> userparentguardianRepository
        {
            get
            {
                if (this._userparentguardianRepository == null)
                    this._userparentguardianRepository = new GenericRepository<OfaParentRegistration>(_context);
                return _userparentguardianRepository;
            }
        }
        public GenericRepository<OfaPhoneType> phonetypeRepository
        {
            get
            {
                if (this._phonetypeRepository == null)
                    this._phonetypeRepository = new GenericRepository<OfaPhoneType>(_context);
                return _phonetypeRepository;
            }
        }
        public GenericRepository<OfaParentPhoneNumber> phonenumbertypeRepository
        {
            get
            {
                if (this._phonenumbertypeRepository == null)
                    this._phonenumbertypeRepository = new GenericRepository<OfaParentPhoneNumber>(_context);
                return _phonenumbertypeRepository;
            }
        }
        public GenericRepository<OfaGranteeTableData> granteeRepository
        {
            get
            {
                if (this._granteeRepository == null)
                    this._granteeRepository = new GenericRepository<OfaGranteeTableData>(_context);
                return _granteeRepository;
            }
        }

        public GenericRepository<TableColumnValue> tablecolumnRepository
        {
            get
            {
                if (this._tablecolumnRepository == null)
                    this._tablecolumnRepository = new GenericRepository<TableColumnValue>(_context);
                return _tablecolumnRepository;
            }
        }

        public GenericRepository<State> stateRepository
        {
            get
            {
                if (this._stateRepository == null)
                    this._stateRepository = new GenericRepository<State>(_context);
                return _stateRepository;
            }
        }
        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var outputLines = new List<string>();
                outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", DateTime.Now, ex.Message));
            }

        }
      
        #endregion



    }
}
