﻿using System;
using System.Collections.Generic;

namespace OFA_Services
{
    public partial class Auditing
    {
        public int Auditid { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string Ipaddress { get; set; }
        public string Domain { get; set; }
        public DateTime? Date { get; set; }
        public string TableName { get; set; }
        public string Operation { get; set; }
    }
}
